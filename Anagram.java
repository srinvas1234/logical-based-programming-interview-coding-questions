Craete Immutable class in java:-
===============================
//Make class as a final
//Make variable as Private
//Make variables as final
//No Setter method
//Make Deep copy for object

package com.app;
class Engine{
  int speed;
  Engine(int speed)
  {
      this.speed=speed;
  }
}

package com.app;
public final class ImmutableClass
{
  private final int id;
  private final String name;
  private Engine engine;
  public int getId()
  {
      return id;
  }

  public  String getName()
  {
      return name;
  }

public ImmutableClass(int id,String name,Engine e)
{
  this.id=id;
  this.name=name;
  Engine newEngine=new Engine(e.speed);
  this.engine=newEngine;
 // this.engine=e; shallow copy if we use we change value beacause don't use shallow copy we use deep copy only
}

public static void main(String args[])
{
    Engine e=new Engine(50);
  ImmutableClass a=new ImmutableClass(1,"Srinivas",e);
  System.out.println(a.name=="Srinivas");
   System.out.println(a.engine.speed);
   e.speed=70;
  System.out.println(a.engine.speed);

}
}

output:-
==========
true
50
50


Anagram Logic:-
===========
package com.app.practicenew;

import java.util.Arrays;

public class AnagramString {  
    static void isAnagram(String str1, String str2) {  
        String s1 = str1.replaceAll("\\s", "");  
        String s2 = str2.replaceAll("\\s", "");
    
        boolean status = true;  
        if (s1.length() != s2.length()) {  
            status = false;  
        } else {  
            char[] ArrayS1 = s1.toLowerCase().toCharArray();  
            char[] ArrayS2 = s2.toLowerCase().toCharArray();  
            Arrays.sort(ArrayS1);  
            Arrays.sort(ArrayS2);  
            status = Arrays.equals(ArrayS1, ArrayS2);  
        }  
        if (status) {  
            System.out.println(s1 + " and " + s2 + " are anagrams");  
        } else {  
            System.out.println(s1 + " and " + s2 + " are not anagrams");  
        
        }
    }  

    public static void main(String[] args) {  
        isAnagram("Keep", "Peek");  
        isAnagram("Mother In Law", "Hitler Woman");
    }  
}  

output:-
========
Keep and Peek are anagrams
MotherInLaw and HitlerWoman are anagrams

Anagram Using Java 8:-
=====================
package com.app.practicenew;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnagramStringJava8 {

    public static void main(String[] args) {
        
        String str1="keep";
        String str2="peek";
        
        str1=Stream.of(str1.split("")).map(s->s.toLowerCase()).sorted().collect(Collectors.joining());
        str2=Stream.of(str2.split("")).map(s->s.toLowerCase()).sorted().collect(Collectors.joining());
        
        if(str1.equals(str2))
        {
            System.out.println("Given Strings are anagram");
        }else {
            System.out.println("Given Strings are not anagram");
        }
        
    }
}
output:-
=========
Given Strings are anagram

ArrayLargeNumber Logic:-
==========================
package com.app.practicenew;
public class ArrayLargeNumber {

    public static void main(String[] args) {

        int[] a= {4,55,11,1222};
            System.out.println(a.length);
        
        int small=a[0];
        int large=a[0];
        
        for(int i=0;i<a.length;i++) {//0<4
            if(a[i]>large) {
                large=a[i];
            }else if (a[i]<small) {
                small=a[i];
            }
        }
        System.out.println("this is small number: "+small);
    System.out.println("This is large number: "+large);
    
    }
}

output:-
4
This is large number: 1222
this is small number: 4


ArrayLargeNumber Logic using Java 8:-
================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ArrayLargeSmallJava8 {

    public static void main(String[] args) {
        
        List<Integer> list=Arrays.asList(4,55,11,1222);
        
        Optional<Integer> minValue = list.stream().min(Comparator.comparing(Integer::intValue));
        
        if(minValue.isPresent())
        {
            System.out.println(minValue.get());
        }
        
        list.stream().min(Comparator.comparing(Integer::intValue)).ifPresent(no->System.out.println("The min value is: "+no));
        list.stream().max(Comparator.comparing(Integer::intValue)).ifPresent(no->System.out.println("The max value is: "+no));
    }
}

output:-
=======
4
The min value is: 4
The max value is: 1222

Using Java 8 Stream find minValue and MaxValue :-
=============================================
package com.app.practicenew;

import java.util.Comparator;
import java.util.stream.Stream;

public class StreamsApiExample {
    public static void main(String[] args) {
        // Get Min or Max Number
        Integer maxNumber = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
                  .max(Comparator.comparing(Integer::valueOf))
                  .get();
         
        Integer minNumber = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9)
                  .min(Comparator.comparing(Integer::valueOf))
                  .get();
        
        System.out.println(minNumber);
        System.out.println(maxNumber);

        System.out.println();

        String min1=Stream.of("a","b","r","d","s","v","q","x").min(Comparator.comparing(String::valueOf)).get();
        String max1=Stream.of("a","b","r","d","s","v","q","x").max(Comparator.comparing(String::valueOf)).get();
        System.out.println("Min String Character: "+min1);
        System.out.println("Max String Character: "+max1);
    }
}

output:-
===========
Minimium Number: 1
MaxNumber Number: 9
Min String Character: a
Max String Character: x


Duplicate String Word java 8:-
===============================
package com.app.practicenew;

import java.util.LinkedHashMap;
import java.util.Map;

public class DuplicateString {

    public static void main(String[] args) {
        
        String str="srinivas,to,java,and,java,tutorial,java";
        
        String[] splitValues=str.split(",");
        
        Map<String,Integer> hmap=new LinkedHashMap<String, Integer>();
        
        for (String tempString : splitValues) {
            if(hmap.get(tempString)==null)
            {
                hmap.put(tempString, 1);
            }else {
                hmap.put(tempString, hmap.get(tempString)+1);
            }
        }
        hmap.entrySet().stream().filter(m->m.getValue()>1).forEach(System.out::println);    
    }
}

output:-
======
java=3


package com.app.practicenew;

public class ArrayStringExample {

    public static void main(String[] args) {

        String[] my_array= {"abc","bcd","abc","ccc","ddd","ccc"};
        for(int i=0;i<my_array.length-1;i++) {
            for(int j=0;j<my_array.length;j++) {
                if(my_array[i].equals(my_array[j])&& i!=j){
                System.out.println("Duplicate Array element: "+my_array[j]);
                    
                }
            }
        }
        
    }
}

output:-
Duplicate Array element: abc
Duplicate Array element: abc
Duplicate Array element: ccc


Duplicate String Word Count:-
================================
package com.app.practicenew;

import java.util.Iterator;
import java.util.LinkedHashMap;

public class DuplicateStringWordCount {
    public static void DuplicateString(String str) {

        LinkedHashMap<String, Integer> lhm=new LinkedHashMap<String, Integer>();
        String[] str1=str.split(" ");
        for (String tempstring : str1) {

            if(lhm.get(tempstring)!=null)
            {
                lhm.put(tempstring, lhm.get(tempstring)+1);
            }else {
                lhm.put(tempstring, 1);
            }
        }
        System.out.println(lhm);

        Iterator<String> tempstring=lhm.keySet().iterator();
        while (tempstring.hasNext()) {
            String temp=tempstring.next();
            if(lhm.get(temp)>1) {
                System.out.println("The word "+temp+" "+lhm.get(temp));
            }
        }

    }

    public static void main(String[] args) {

        DuplicateString("srinivas to java and java tutorial java");

    }

}

output:-
============
{srinivas=1, to=1, java=3, and=1, tutorial=1}
The word java 3


Duplicate String Word Count Using Java 8:-
========================================

package com.app.practicenew;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateWordCountJava8 {

    public static void main(String[] args) {
        
        List<String> list=Arrays.asList("srinivas","to","java","and","java","tutorial","java");
        
        Map<String, Long> duplicateWordCount = list.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        System.out.println(duplicateWordCount);
    }
}

output:-
===========
{java=3, and=1, srinivas=1, tutorial=1, to=1}


Duplicate Array Elements:-
=============================
package com.app.practicenew;

public class DuplicateArrayElements {
    public static void main(String[] args) {
        int[] a= {1,1,2,3,4,5,2,7,9,8,7,4,3};
        
        System.out.println("Duplicate elements: ");
        
        for(int i=0;i<a.length;i++)
        {
            for(int j=i+1;j<a.length;j++)
            {
                if(a[i]==a[j])
                {
                    System.out.print(a[j]+" ");
                }
            }
        }
        
    }
}
output:-
Duplicate elements: 
1 2 3 4 7

Using Java8 find DuplicateArrayElements:-
=========================================
Method 1:-
----------
package com.app.practicenew;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DuplicateNumbersUsingJava8 {

    public static void main(String[] args) {
        List<Integer> duplicateelements = Arrays.asList(1,1,2,3,4,5,2,7,9,8,7,4,3);
        Set<Integer> duplicate = duplicateelements.stream().filter(e->Collections.frequency(duplicateelements, e)>1).collect(Collectors.toSet());
         System.out.println(duplicate);
    }
}

output:-
[1, 2, 3, 4, 7]

Method 2:-
----------
package com.app.practicenew;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DuplicateNumbersUsingJava8 {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1,1,2,3,4,5,2,7,9,8,7,4,3);
         Set<Integer> dupl=new HashSet<Integer>();
         Set<Integer> duplicate1=list.stream().filter(ee->!dupl.add(ee)).collect(Collectors.toSet());
         System.out.println(duplicate1);
    }
}
output:-
[1, 2, 3, 4, 7]


Duplicate Character Given String:-
==================================

Method 1:-
==============
package com.app.practicenew;

import java.util.LinkedHashMap;

public class DuplicateCharacters {
    public static void DuplicateCharater(String str) {
        LinkedHashMap<Character, Integer> lmm=new LinkedHashMap<Character, Integer>();
                
                for(int i=0;i<str.length();i++) {
                    char ch=str.charAt(i);
                    
                    
                    if(lmm.get(ch)!=null){
                        lmm.put(ch, lmm.get(ch)+1);
                        }else {
                            lmm.put(ch, 1);
                        }
                    }
        System.out.println(lmm);
            }

    public static void main(String[] args) {
        DuplicateCharater("srinivas");
    }
}
output:-
=========
{s=2, r=1, i=2, n=1, v=1, a=1}


Duplicate Characters Given String:-
=====================================
package com.app.practicenew;

public class ArrayDuplicateCharacters {

    public static void main(String[] args) {
        String str="Srinivas Vadla";
        int count=0;
        char[] ch=str.toCharArray();
        System.out.println("Duplicate Characters are: ");
        
        for(int i=0;i<str.length();i++)
        {
            for(int j=i+1;j<str.length();j++)
            {
                if(ch[i]==ch[j])
                {
                    System.out.print(ch[j]+" ");
                    count++;
                    break;
                }
            }
        }   
    }
}
output:-
=========
Duplicate Characters are: 
i a a 


DuplicateCharater Using Java 8:-
==============================

package com.app.practicenew;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateCharactersUsingJava8 {
    public static void main(String[] args) {
        String input="srinivas";
        List<String> duplicateCharacter = Arrays.stream(input.split("")).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
        .entrySet().stream().filter(e->e.getValue()>1).map(Map.Entry::getKey).collect(Collectors.toList());
        System.out.println(duplicateCharacter);
    }
}

output:-
========
[s, i]

DuplicateCharater Using Java 8:-
=================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateCharacterUsingJava8 {
    public static void main(String[] args) {

        String duplicateCharacter="srinivas";
        Map<String, Long>ssss=Arrays.stream(duplicateCharacter.split("")).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        System.out.println(ssss);
    }
}

output:-
=========
{a=1, r=1, s=2, v=1, i=2, n=1}


Remove Duplicate Elements Given Array:-
======================================
package com.app.practicenew;

public class RemoveDuplicateElementArray {
    
public static void main(String[] args) {
    int[] a= {4,5,6,2,3,4,5,23,4,5};
    for(int i=0;i<a.length;i++) {//0<9
        for(int j=i+1;j<a.length;j++) {//1<9
            if(a[i]==a[j]) {
                a[i]=-1;
            }
        }
    }
    System.out.println("Remove array");
    for(int i=0;i<a.length;i++) {
        
        if(a[i]!=-1) {
            System.out.print(a[i]+" ");
        }
    }   
}
}
output:-
==========
Remove array
6 2 3 23 4 5 

Remove Duplicate Elements Given Array Using Java 8:-
====================================================

package com.app.practicenew;

import java.util.Arrays;

public class RemoveDuplicateElementsJava8 {

    public static void main(String[] args) {
        
        //Using int[]
        int[] arr= {4,5,6,2,3,4,5,23,4,5};
        Arrays.stream(arr).distinct().forEach(element->System.out.print(element+" "));
        System.out.println();
        
        //Using String[]
        String[] str= {"srinivas","to","java","and","java","tutorial","java"};
        Arrays.stream(str).distinct().forEach(s->System.out.print(s+" "));
        System.out.println();
        
        //Using Character[]
        Character[] ch= {'s','r','i','n','i','v','a','s'};
        Arrays.stream(ch).distinct().forEach(c->System.out.print(c+" "));
    }   
}

output:-
===========
4 5 6 2 3 23 
srinivas to java and tutorial 
s r i n v a


Remove Duplicate Characters Using StringBuilder:-
===============================================
package com.app.practicenew;

public class RemoveDuplicateCharacters {

    public static void main(String[] args) {
        String str="srinivas";

StringBuilder sb1=new StringBuilder();
    for(int i=0;i<str.length();i++) {
        char ch=str.charAt(i);
        int index=str.indexOf(ch, i+1);
        if(index==-1) {
            sb1.append(ch);
        }
    }
    System.out.println(sb1);
}
}
output:-
========
rnivas

Remove Duplicate Characters Using Arrays:-
=========================================
package com.app.practicenew;

public class RemoveDuplicateCharacters {

    public static void main(String[] args) {
        String str="srinivas";

        char[] ch=str.toCharArray();
        StringBuilder sb2=new StringBuilder();
        for(int i=0;i<ch.length;i++) {
            boolean repeated=false;
            for(int j=i+1;j<ch.length;j++) {
                if(ch[i]==ch[j]) {
                    repeated=true;
                break;
                }
            }
            if(!repeated) {
                sb2.append(ch[i]);
            }
        }
        System.out.println(sb2);
}
}
output:-
=========
rnivas


Remove Duplicate Characters Using Set:-
=========================================
package com.app.practicenew;

import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicateCharacters {

    public static void main(String[] args) {
        String str="srinivas";
        StringBuilder sb3=new StringBuilder();
         Set<Character> set=new LinkedHashSet<Character>();
         for(int i=0;i<str.length();i++) {
             set.add(str.charAt(i));
         }
    for (Character character : set) {
        sb3.append(character);
    }   
        System.out.println(sb3);
}
}
output:-
=============
srinva

Remove Duplicate Characters Using Java 8:-
=========================================
package com.app.practicenew;

public class RemoveDuplicateCharacters {

    public static void main(String[] args) {
        String str="srinivas";
        StringBuilder sb=new StringBuilder();
        str.chars().distinct().forEach(c->sb.append((char)c));
        System.out.println(sb);
}
}
output:-
=======
srinva

Ascending Order Given Array Elements:-
=====================================

package com.app.practicenew;

import java.util.Arrays;

public class SortingAscendingOrder {
public static void main(String[] args) {
    int[] arr= {5,1,3,9,11,13,16,4,99};
    int temp;
    System.out.println("Array Before Sort: "+Arrays.toString(arr));
    for(int i=0;i<arr.length;i++) {
        for(int j=i+1;j<arr.length;j++)
        {
            if(arr[i]>arr[j])
            {
                temp=arr[i];
                arr[i]=arr[j];
                arr[j]=temp;
            }
        }
    }
    System.out.println("Array Before Sort: "+Arrays.toString(arr));
}
}

output:-
=========
Array Before Sort: [5, 1, 3, 9, 11, 13, 16, 4, 99]
Array Before Sort: [1, 3, 4, 5, 9, 11, 13, 16, 99]

Descending Order Given Array Elements:-
======================================

package com.app.practicenew;

import java.util.Arrays;

public class SortingDescendingOrder {
    public static void main(String[] args) {
        int[] arr= {5,1,3,9,11,13,16,4,99};
        int temp;
        System.out.println("Array Before Sort: "+Arrays.toString(arr));
        for(int i=0;i<arr.length;i++) {
            for(int j=i+1;j<arr.length;j++)
            {
                if(arr[i]<arr[j])
                {
                    temp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=temp;
                }
            }
        }
        System.out.println("Array Before Sort: "+Arrays.toString(arr));
    }
    }
output:-
===========
Array Before Sort: [5, 1, 3, 9, 11, 13, 16, 4, 99]
Array Before Sort: [99, 16, 13, 11, 9, 5, 4, 3, 1]


Array Sorting Predefined Method:-
=====================================

package com.app.practicenew;

import java.util.Arrays;

public class ArraySortPredefined {
    public static void main(String[] args) {
        int[] arr= {5, 1, 3, 9, 11, 13, 16, 4, 99};
        System.out.println("Array Before Sorting: "+Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println("Array After Sorting: "+Arrays.toString(arr));
    }
}

output:-
========
package com.app.practicenew;

import java.util.Arrays;

public class ArraySortPredefined {
    public static void main(String[] args) {
        int[] arr= {5, 1, 3, 9, 11, 13, 16, 4, 99};
        System.out.println("Array Before Sorting: "+Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println("Array After Sorting: "+Arrays.toString(arr));
    }
}

output:-
===========
Array Before Sorting: [5, 1, 3, 9, 11, 13, 16, 4, 99]
Array After Sorting: [1, 3, 4, 5, 9, 11, 13, 16, 99]


Sorted Array Using Comparable:-
==============================

package com.app.interviews;

import java.util.Comparator;

public class ComparableExample implements Comparable<ComparableExample> {

    private int id;
    private String name;
    public ComparableExample() {
        super();
    }
    public ComparableExample(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "ComparableExample [id=" + id + ", name=" + name + "]";
    }
    @Override
    public int compareTo(ComparableExample o) {

        if(id==o.id) 
        {
            return 0;
        }
        else if (id>o.id) 
        {
            return 1;
        }
        else 
        {
            return -1;
        }
        
    }
    public static Comparator<ComparableExample> NameComparator=new Comparator<ComparableExample>() {
        
        @Override
        public int compare(ComparableExample o1, ComparableExample o2) {
            //return o1.getId()-o2.getId();
    //Sorted : [ComparableExample [id=33, name=eeee], ComparableExample [id=333, name=yee], ComparableExample [id=733, name=keee]]
            return o1.getName().compareTo(o2.getName());
        }
    };
}

package com.app.interviews;

import java.util.Arrays;

public class ComparableExampleArray {

    public static void main(String[] args) {

        ComparableExample[] cc=new ComparableExample[3];
        cc[0]=new ComparableExample(333, "yee");
        cc[1]=new ComparableExample(33, "eeee");
        cc[2]=new ComparableExample(733, "keee");
        
          Arrays.sort(cc,ComparableExample.NameComparator);
          System.out.println("Sorted : "+Arrays.toString(cc));
    }
}

output:-
==========
Sorted : [ComparableExample [id=33, name=eeee], ComparableExample [id=733, name=keee], ComparableExample [id=333, name=yee]]


Array Sort Using Java 8:-
============================


package com.app.practicenew;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class ArraySortUsingJava8 {

    public static void main(String[] args) {
        int[] arr= {5, 1, 3, 9, 11, 13, 16, 4, 99};
        Arrays.stream(arr).sorted().forEach(e->System.out.print(e+" "));
        
        System.out.println();
        
        List<Integer> asList = Arrays.asList(5, 1, 3, 9, 11, 13, 16, 4, 99);
        List<Integer> sorted = asList.stream().sorted().collect(Collectors.toList());
        System.out.print(sorted+" ");
    }
}

output:-
=========
1 3 4 5 9 11 13 16 99 
[1, 3, 4, 5, 9, 11, 13, 16, 99]


Array Descending order Sort Using Java 8:-
========================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ArraySortDescUsingJava8 {
    public static void main(String[] args) {    
        List<Integer> asList = Arrays.asList(5, 1, 3, 9, 11, 13, 16, 4, 99);
        List<Integer> sorted = asList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.print(sorted+" ");
        
    }
}
output:-
[99, 16, 13, 11, 9, 5, 4, 3, 1] 


Array Sort Given String Ascending Order:-
=========================================
package com.app.practicenew;

public class SortedArray {
    public static void main(String[] args) {
        String str="srinivas";
        char[] ch=str.toCharArray();
        
        char temp;
        
        for(int i=0;i<ch.length;i++) { //0<8
            for(int j=i+1;j<ch.length;j++) {//1<8
                if(ch[i]>ch[j]) {
                    temp=ch[i];
                    ch[i]=ch[j];
                    ch[j]=temp;
                }
            }
        }
        System.out.println(new String(ch));
    }
}
output:-
========
aiinrssv

Array Sort Given String Descending Order:-
=========================================

package com.app.practicenew;

public class SortedArray {
    public static void main(String[] args) {
        String str="srinivas";
        char[] ch=str.toCharArray();
        char temp;
        for(int i=0;i<ch.length;i++) { //0<8
            for(int j=i+1;j<ch.length;j++) {//1<8
                if(ch[i]<ch[j]) {
                    temp=ch[i];
                    ch[i]=ch[j];
                    ch[j]=temp;
                }
            }
        }
        System.out.println(new String(ch));
    }
}
output:-
===========
vssrniia

Array Sort Given String Using Predefined Method:-
=================================================

package com.app.practicenew;

import java.util.Arrays;

public class SortedArrayPredefined {

    public static void main(String[] args) {
        
        String str="srinivas";
        char[] ch1=str.toCharArray();
         Arrays.sort(ch1);
        System.out.println(new String(ch1)); 
    }
}

output:-
============
aiinrssv


Array Sort Given String By Using Java 8:-
=============================================
Ascending Order:-
=================

package com.app.practicenew;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArraySortGivenStringJava8 {

    public static void main(String[] args) {
        List<Character> asList = Arrays.asList('s', 'r', 'i','n','i','v','a','s');
        List<Character> sorted = asList.stream().sorted().collect(Collectors.toList());
        System.out.print(sorted+" ");
    }
}

output:-
=========
[a, i, i, n, r, s, s, v]

Array Sort Given String By Using Java 8:-
=============================================
Descending Order:-
=================

package com.app.practicenew;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ArraySortGivenStringJava8 {

    public static void main(String[] args) {
        List<Character> asList = Arrays.asList('s', 'r', 'i','n','i','v','a','s');
        List<Character> sorted = asList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.print(sorted+" ");
    }
} 

output:-
===========
[v, s, s, r, n, i, i, a] 

Array Ascending Sort By using String:-
======================================

package com.app.practicenew;

import java.util.Arrays;
import java.util.Collections;

public class StringArraySort {
    public static void main(String[] args) {
        String[] str= {"srinu","ravi","mani","sneha","nihas","vihas"};
        for(int i=0;i<str.length-1;i++) {
            for(int j=i+1;j<str.length;j++) {
                if(str[i].compareTo(str[j])>0) {
                    String temp=str[i];
                    str[i]=str[j];
                    str[j]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(str));
    }
}

output:-
[mani, nihas, ravi, sneha, srinu, vihas]

Array Descending Sort By using String:-
======================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Collections;

public class StringArraySort {
    public static void main(String[] args) {

        String[] str= {"srinu","ravi","mani","sneha","nihas","vihas"};

        //without predefined

        for(int i=0;i<str.length-1;i++) {
            for(int j=i+1;j<str.length;j++) {
                if(str[i].compareTo(str[j])<0) {
                    String temp=str[i];
                    str[i]=str[j];
                    str[j]=temp;
                }
            }
        }
        System.out.println(Arrays.toString(str));
    }
}
output:-
[vihas, srinu, sneha, ravi, nihas, mani]


Arrays Sort Using Predefined Method:-
======================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Collections;

public class StringArraySort {
    public static void main(String[] args) {

        String[] str= {"srinu","ravi","mani","sneha","nihas","vihas"};

        System.out.println("Using predefind method ");
        Arrays.sort(str);
        System.out.println(Arrays.toString(str));

        System.out.println();

        System.out.println("Descending Order");
        Arrays.sort(str,Collections.reverseOrder());
        System.out.println(Arrays.toString(str));
    }
}
output:-
=========
Using predefind method: 
[mani, nihas, ravi, sneha, srinu, vihas]
Descending Order: 
[vihas, srinu, sneha, ravi, nihas, mani]


Singleton Class:-
===================
package com.app.practicenew;

//public class
public class Singleton {
    //private static variable
    private static Singleton single_instance=null;
    //public variable
    public String s;
    
    //private constructor
    private Singleton() {
        s="I am good";
    }
    //static instance
    public static synchronized  Singleton getInstance() {
        if(single_instance==null) {
            single_instance=new Singleton();
        }
        return single_instance;
    }
    public static void main(String[] args) {

        Singleton x=Singleton.getInstance();
        System.out.println(x.hashCode());
        Singleton y=Singleton.getInstance();
        System.out.println(y.hashCode());
        Singleton z=Singleton.getInstance();
        System.out.println(z.hashCode());
        
        if(x==y && y==z) {
            System.out.println("same memory pointing");
        }else {
            System.out.println("different memory pointing");
        }
    }
}

output:-
===========
1365202186
1365202186
1365202186
same memory pointing

Singleton class Db Connection class:-
=======================================

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {

    static Connection getConnection() throws Exception {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:SPEC", "SYSTEM", "welcome@1234");
        return c;

    }
}

===============================

Singleton class Hibernate Connection class:-
=======================================

package com.app.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static SessionFactory sf=null;
    static {
        sf=new Configuration().configure().buildSessionFactory();
        
    }
    public static SessionFactory getSf() {
        return sf;
    }
    
}
=============================================

Second Largest Number:-
==========================
package com.app.practicenew;

public class SecondLargestNumber {
    public static int secondLargestNumber(int[] a,int total) {
        int temp=0;
    for(int i=0;i<total;i++) {
    for(int j=i+1;j<total;j++) {
            if(a[i]<a[j]) {
                temp=a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
    }
    return a[total-3];  
    }
    public static void main(String[] args) {
        int[] a= {5,3346,1223,444};
        System.out.println("Second Largest Number: "+secondLargestNumber(a,4));
    }
}
output:-
===========
Second Largest Number: 1223


SecondLargest String Using Java 8:-
====================================

package com.app.practicenew;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FindSecondLargestString {

    public static void main(String[] args) {
        
        List<String> ss=Arrays.asList("I", "am","good","programmer","sss");
        String sss=ss.stream().distinct().sorted(Comparator.reverseOrder()).limit(2).skip(1).findFirst().get();
        System.out.println(sss);
    }
}
output:-
==========
programmer

SecondLargest Integer Using Java 8:-
====================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FindSecondLargestString {

    public static void main(String[] args) {
        List<Integer> ss1=Arrays.asList(1,4,6,66,10);
        Integer abc=ss1.stream().distinct().sorted(Comparator.reverseOrder()).limit(2).skip(1).findFirst().get();
        System.out.println(abc);
    }
}
output:-
===========
10

CharWithOccurance Count :-
=======================

package com.app.practicenew;

public class CharWithOccurance {
    public static void main(String[] args) {
        String str="srinivas";
    char charToReplace='s';
        if(str.indexOf(charToReplace)==-1) {
            System.out.println("In existing String character not present");
            System.exit(0);
        }
        int count=1;
        System.out.println(str.length());
        for(int i=0;i<str.length();i++) {
            char ch=str.charAt(i);
            if(ch==charToReplace) {
                str=str.replaceFirst(String.valueOf(charToReplace), String.valueOf(count));
                        count++;
            }
        }
        System.out.println(str);
    }
}

output:-
=========
8
1riniva2

CharWithOccurance Count using Java 8:-
=======================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CharacterCountUsingJava8 {

    public static void main(String[] args) {
        
        String input="gainjavaknowledge";
        String[] array=input.split("");
        
        Map<String, Long> output=Arrays.stream(array).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        System.out.println(output);
    }
}
output:-
==========
{a=3, d=1, e=2, v=1, w=1, g=2, i=1, j=1, k=1, l=1, n=2, o=1}

CharWithOccurance Count using Java 8:-
=======================================
package com.app.practicenew;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CharacterCountUsingNJava8 {
    public static void main(String[] args) {
        String input="gainjavaknowledge";
        Map<String,Long> output=Arrays.stream(input.split("")).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
System.out.println(output);
    }
}

output:-
===========
{a=3, d=1, e=2, v=1, w=1, g=2, i=1, j=1, k=1, l=1, n=2, o=1}


Truncate String:-
=================
package com.app.practicenew;

public class TruncateString {
    public static void main(String[] args) {
        String str="welcome to india to worldcup";
        System.out.println(str.length());
        String ss=str.substring(17, 28);
        System.out.println(ss);
    }
}
output:-
=========
28
to worldcup


Balanced Using Stack:-
====================

package com.app.practicenew;

import java.util.ArrayDeque;
import java.util.Deque;

public class BalancedTreeExample {

    static boolean areBalanced(String exp) {
        Deque<Character> stack=new ArrayDeque<Character>(); 
        for (int i = 0; i < exp.length(); i++) {
            char x=exp.charAt(i);
            if(x=='(' || x=='[' || x=='{') {
                stack.push(x);
                continue;
            }
            if(stack.isEmpty())
        return false;
            char check;
            switch (x) {
            case ')':
                check=stack.pop();
                if(check=='{' || check=='[')
                    return false;
                break;
            case '}':
                check=stack.pop();
                if(check=='(' || check=='[')
                    return false;
                break;
            case ']':
                check=stack.pop();
                if(check=='(' || check=='{')
                    return false;
                break;
        
            }
        }
        return (stack.isEmpty());
    }

    public static void main(String[] args) {
        String exp="([{}])";
        if(areBalanced(exp)) {
            System.out.println("Balanced");
        }else {
            System.out.println("Not Balanced");
        }
    }
}

output:-
============
Balanced


Balanced Using Stack:-
====================

package com.app.practicenew;

import java.util.Stack;

public class BalancedUsingStack {
public static void main(String[] args) {
    
    String str="{()}[]";
    Stack<Character> st=new Stack<Character>();
    
    for(int i=0;i<str.length();i++)
{
    if(str.charAt(i)=='{'|| str.charAt(i)=='[' || str.charAt(i)=='(')
    {
        st.push(str.charAt(i));
    }
    else if (!st.empty() && 
            ((str.charAt(i)==']' && st.peek()=='[') ||
            (str.charAt(i)=='}' && st.peek()=='{') ||
            (str.charAt(i)==')' && st.peek()=='('))){
    
        st.pop();
    }else {
        st.push(str.charAt(i));
    }
}
    if(st.empty())
    {
        System.out.println("Balanced");
    }else {
        System.out.println("Not Balanced");
    }
}
}

output:-
=========
Balanced


Perfect Number Given Number:-
===============================
package com.app.practicenew;
import java.util.Scanner;

public class PerfectNumber {
    public static boolean perfect(int n)
    {
        int sum=0;
        for(int i=1;i<n;i++) {
            if(n%i==0)
            {
                sum=sum+i;
            }
        }
        return sum==n;
    }
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter number: ");
int n=sc.nextInt();
System.out.println(PerfectNumber.perfect(n));
    }
}

output:-
==========
Enter number: 
6
true
Enter number: 
4
false
Enter number: 
28
true


Remove White Spaces Given String:-
===============================

package com.app.practicenew;

public class RemoveWhiteSpaces {
public static void main(String[] args) {
    
    String str=" I am java new c language";
     String dd=str.replaceAll("\\s", "");
    System.out.println(dd);
}
}
output:-
==========
Iamjavanewclanguage

RemoveSpecialCharacters:-
==========================
package com.app.practicenew;

public class RemoveSpecialCharacters {
    
public static void main(String[] args) {
    String str="%ja%%&*())java&&*";
    String removeSpeacialcha=str.replaceAll("[^a-zA-Z0-9]", "");
    System.out.println(removeSpeacialcha);
}
}
output:-
=======
jajava


ReverseString:-
==============
package com.app.practicenew;

public class ReverseString {

    public static void main(String[] args) {
        String str="Hello";     
        StringBuffer sb=new StringBuffer(str);
        System.out.println(sb.reverse());
        StringBuilder sb1=new StringBuilder(str);
        System.out.println(sb1.reverse());
        char[] ch=str.toCharArray();
        for(int i=ch.length-1;i>=0;i--) {
            System.out.print(ch[i]);
        }
        System.out.println();
        for(int i=str.length()-1;i>=0;i--) {
            System.out.print(str.charAt(i));
        }
    }
}
output:-
==========
olleH
olleH
olleH
olleH


Programs:-
===========
package com.app.practicenew;

public class Employee {
//POJO 
    private long empId;
    private String empName;
    private long salary;
    private Gender gender;
    public Employee() {
        super();
    }
    public Employee(long empId, String empName, long salary, Gender gender) {
        super();
        this.empId = empId;
        this.empName = empName;
        this.salary = salary;
        this.gender = gender;
    }
    public long getEmpId() {
        return empId;
    }
    public void setEmpId(long empId) {
        this.empId = empId;
    }
    public String getEmpName() {
        return empName;
    }
    public void setEmpName(String empName) {
        this.empName = empName;
    }
    public long getSalary() {
        return salary;
    }
    public void setSalary(long salary) {
        this.salary = salary;
    }
    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    @Override
    public String toString() {
        return "Employee [empId=" + empId + ", empName=" + empName + ", salary=" + salary + ", gender=" + gender + "]";
    }
}

package com.app.practicenew;

public enum Gender {

    Male,Female
}

package com.app.practicenew;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {

        Employee e1=new Employee(122, "srinivas", 78787, Gender.Male);
        Employee e2=new Employee(123, "Nihas", 6787, Gender.Male);
        Employee e3=new Employee(124, "Hihas", 787, Gender.Female);
        Employee e4=new Employee(125, "jihas", 48787, Gender.Female);
        
        List<Employee> empList=new ArrayList<Employee>();
        empList.add(e1);
        empList.add(e2);
        empList.add(e3);
        empList.add(e4);
        
        //Get Salary More Than 10000
        List<Employee> ss=empList.stream().filter(e->e.getSalary()>10000).collect(Collectors.toList());
        System.out.println(ss);
        //get Employee Name 
        List<String> sss=empList.stream().map(i->i.getEmpName()).collect(Collectors.toList());
         System.out.println(sss);
         //get Gender
         List<Gender> sss1=empList.stream().map(i->i.getGender()).collect(Collectors.toList());
         System.out.println(sss1);
    }
}

output:-
=======
[Employee [empId=122, empName=srinivas, salary=78787, gender=Male], Employee [empId=125, empName=jihas, salary=48787, gender=Female]]
[srinivas, Nihas, Hihas, jihas]
[Male, Male, Female, Female]


Comparable and Comparator Example:-
===================================
package com.app;

import java.util.TreeMap;

public class TreeMapExample {

    public static void main(String[] args) {
        
        TreeMap<String, String> t=new TreeMap<String, String>(new SrinivasComparator());
        t.put("c", "cat");
        t.put("d", "dog");
        t.put("m", "mirror");
        t.put("b", "bellon");
        t.put("f", "fox");
        System.out.println(t);
    }
}
package com.app;

import java.util.Comparator;

public class SrinivasComparator implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {

        String s1=o1.toString();
        String s2=o2.toString();
        //return -s1.compareTo(s2);
        return s2.compareTo(s1);
        //return -s1.compareTo(s2);
        //return -s1.compareTo(s2);
    }
}

output:-
==========
{m=mirror, f=fox, d=dog, c=cat, b=bellon}


Comparable and Comparator Example:-
===================================

package com.app;

public class Employee implements Comparable<Employee>{
    private String empName;
    private Integer empId;
    public Employee() {
        super();
    }
    public Employee(String empName, Integer empId) {
        super();
        this.empName = empName;
        this.empId = empId;
    }
    public String getEmpName() {
        return empName;
    }
    public void setEmpName(String empName) {
        this.empName = empName;
    }
    public Integer getEmpId() {
        return empId;
    }
    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
    @Override
    public String toString() {
        return "Employee [empName=" + empName + ", empId=" + empId + "]";
    }
    @Override
    public int compareTo(Employee o) {
        
        int empId1=this.empId;
        
        Employee e=(Employee)o;
        int empId2=e.empId;
        
        if(empId1<empId2)
        {
            return -1;
        }else if (empId1>empId2) {
            return +1;
        }else {
            return 0;
        }
        
    }
}
package com.app;

import java.util.Comparator;

public class MyNewComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getEmpName().compareTo(o2.getEmpName());
    }

}

package com.app;

import java.util.TreeSet;

public class Test1234 {
    public static void main(String[] args) {
Employee e1=new Employee("nag", 100);
Employee e2=new Employee("balaiah", 200);
Employee e3=new Employee("chiru", 50);
Employee e4=new Employee("venki", 50);
Employee e5=new Employee("nag", 100);
Employee e6=new Employee("nag", 20);
TreeSet<Employee> t=new TreeSet<Employee>();//default nature sorting order
t.add(e1);
t.add(e2);
t.add(e3);
t.add(e4);
t.add(e5);
t.add(e6);
System.out.println(t);

@SuppressWarnings("unchecked")
TreeSet<Employee> t1=new TreeSet<Employee>(new MyNewComparator());
t1.add(e1);
t1.add(e2);
t1.add(e3);
t1.add(e4);
t1.add(e5);
System.out.println(t1);
    }
}

output:-
==========
[Employee [empName=nag, empId=20], Employee [empName=chiru, empId=50], Employee [empName=nag, empId=100], Employee [empName=balaiah, empId=200]]
[Employee [empName=balaiah, empId=200], Employee [empName=chiru, empId=50], Employee [empName=nag, empId=100], Employee [empName=venki, empId=50]]


Comparable and Comparator Example:-
==================================
package com.app;

import java.util.TreeSet;

public class TreesetExampleNew {
    
public static void main(String[] args) {
    TreeSet<Integer> t=new TreeSet<Integer>(new MyComparator());
    t.add(10);
    t.add(0);
    t.add(15);
    t.add(20);
    t.add(20);
    System.out.println(t);
}
}

package com.app;

import java.util.Comparator;

public class MyComparator implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {
        
        Integer I1=(Integer)o1;
        Integer I2=(Integer)o2;
        
        //method-1
        
        if(I1<I2)
        {
        return -1;
        }
        else if (I1>I2) {
            return +1;
        }else {
            return 0;
        }
        
    }
        //return I1.compareTo(I2);//[0, 10, 15, 20]asc
        //return I2.compareTo(I1);//[20, 15, 10, 0]desc
        //return -I1.compareTo(I2);//[20, 15, 10, 0] desc
        //return -I2.compareTo(I1);//[0, 10, 15, 20] asc
        //return +1;//[10, 0, 15, 20, 20] insertion
        //return -1;//[20, 20, 15, 0, 10] insertion
        //return 0;//[10] first element will be inserted all other elements are consider as duplicates

}

output:-
=========
[0, 10, 15, 20]


Collections Programs:-
----------------------

List:-
=======

ArrayList Example:-
===================
package com.app;

import java.util.ArrayList;

public class ArrayListExample {

    public static void main(String[] args) {
        ArrayList<String> l=new ArrayList<String>();
        l.add("A");
        l.add("111");
        l.add("A");
        l.add(null);
        System.out.println(l);
        l.remove(2);
        System.out.println(l);
        l.add(2, "M");
        l.add("N");
        System.out.println(l);
    }
}
output:-
========
[A, 111, A, null]
[A, 111, null]
[A, 111, M, null, N]


LinkedList Example:-
====================
package com.app;

import java.util.LinkedList;

public class LinkedListExample {

    public static void main(String[] args) {
        LinkedList<String> l=new LinkedList<String>();
        l.add("durga");
        l.add("Nihas");
        l.add(null);
        l.add("durga");
        l.set(0, "software");
        l.add(0,"venky");
        l.addFirst("ccc");
        l.addLast("srinivas");
        System.out.println(l);
    }
}

output:-
=========
[ccc, venky, software, Nihas, null, durga, srinivas]


Vector Example:-
=================
package com.app;

import java.util.Vector;

public class VectorExample {

    public static void main(String[] args) {
        
        Vector v=new Vector();
        //Vector v=new Vector(10,5);
        System.out.println(v.capacity());
        
        
        for(int i=0;i<10;i++) {
            v.addElement(i);
        }
        
        System.out.println(v.capacity());
        v.addElement("A");
        System.out.println(v.capacity());
        System.out.println(v);
    }
}

output:-
=========
10
10
20
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A]

Stack Example:-
=================
package com.app;

import java.util.Stack;

public class StackExample {

    public static void main(String[] args) {
        
        Stack<String> s=new Stack<String>();
        s.push("A");
        s.push("B");
        s.push("C");
        System.out.println(s);//[A, B, C]
        System.out.println(s.search("A"));//3
        System.out.println(s.search("Z"));//-1
    }
}

output:-
=========
[A, B, C]
3
-1

Set:-
=======

HashSet Example:-
================
package com.app;

import java.util.HashSet;

public class HashsetExample {

    public static void main(String[] args) {
        
        HashSet<String> set=new HashSet<String>();
        set.add("B");
        set.add("A");
        set.add("C");
        set.add("B");
        set.add("E");
        set.add(null);
        System.out.println(set.add("E"));
        System.out.println(set);
    }
}
output:-
========
false
[null, A, B, C, E]


LinkedHashSet:-
================
package com.app;

import java.util.LinkedHashSet;

public class LinkedHashsetExample {

public static void main(String[] args) {    
LinkedHashSet<String> set=new LinkedHashSet<String>();
        set.add("B");
        set.add("A");
        set.add("C");
        set.add("B");
        set.add("E");
        set.add(null);
        System.out.println(set.add("E"));
        System.out.println(set);
    }
}

output:-
=========
false
[B, A, C, E, null]

TreeSet Example:-
==================
package com.app;

import java.util.TreeSet;

public class TreeSetExample {

    public static void main(String[] args) {
        
        TreeSet<String> t=new TreeSet<String>();
        t.add("A");
        t.add("a");
        t.add("B");
        t.add("Z");
        t.add("L");
        t.add("L");
        //t.add(new Integer(10));//Exception in thread "main" java.lang.ClassCastException: java.lang.String cannot be cast to java.lang.Integer
        //t.add(null);//Exception in thread "main" java.lang.NullPointerException
        System.out.println(t);
        }
}
output:-
=========
[A, B, L, Z, a]


Queue:-
=======

package com.app;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {

    public static void main(String[] args) {
        
        Queue<String> queue=new LinkedList<String>();
        
        //add elements to queue
        queue.add("apple");
        queue.add("banana");
        queue.add("cherry");
        
        //print the queue
        System.out.println("Queue: "+queue);
        //remove the element at front of the queue
        
        String front=queue.remove();
        System.out.println("Remove element: "+front);
        
        //print the updated queue
        
        System.out.println("Queue after removal: "+queue);
        
        //add another element to the queue
        
        queue.add("date");
        
         // peek at the element at the front of the queue
        String peeked = queue.peek();
        System.out.println("Peeked element: " + peeked);
 
        // print the updated queue
        System.out.println("Queue after peek: " + queue);
    }
}

output:-
=========
Queue: [apple, banana, cherry]
Remove element: apple
Queue after removal: [banana, cherry]
Peeked element: banana
Queue after peek: [banana, cherry, date]

PriorityQueue:-
===============
package com.app;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueExample {
        public static void main(String args[])
        {
            Queue<String> pq = new PriorityQueue<String>();
            pq.add("Geeks");
            pq.add("For");
            pq.add("Geeks");
            System.out.println("Initial Queue " + pq);
            
            pq.remove("Geeks");
     
            System.out.println("After Remove " + pq);
     
            System.out.println("Poll Method " + pq.poll());
     
            System.out.println("Final Queue " + pq);
        }
    }

output:-
===========
Initial Queue [For, Geeks, Geeks]
After Remove [For, Geeks]
Poll Method For
Final Queue [Geeks]

PriorityBlockingQueue:-
=========================

package com.app;

import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

public class PriorityBlockingQueueExample {
    public static void main(String args[])
    {
        // Creating empty priority
        // blocking queue
        Queue<Integer> pbq
            = new PriorityBlockingQueue<Integer>();
 
        // Adding items to the pbq
        // using add()
        pbq.add(10);
        pbq.add(20);
        pbq.add(15);
 
        // Printing the top element of
        // the PriorityBlockingQueue
        System.out.println(pbq.peek());
 
        // Printing the top element and
        // removing it from the
        // PriorityBlockingQueue
        System.out.println(pbq.poll());
 
        // Printing the top element again
        System.out.println(pbq.peek());
    }
}

output:-
=========
10
10
15

Map:-
=======

HashMap:-
==========
package com.app;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapExample {

    public static void main(String[] args) {
        Map<String,String> map=new HashMap<String, String>();
        map.put("karan", "reddy");
        map.put("vivek", "reddy");
        map.put("venkatesh", "reddy");
        map.put("ramesh", "reddy");
        map.put("rajesh", "reddy");
        map.put("rajesh", "srinu");
        System.out.println(map);
        Set<String> ss=map.keySet();
        System.out.println(ss);
        Collection<String> sss=map.values();
        System.out.println(sss);
        System.out.println(map.containsKey("rajesh"));
        System.out.println(map.containsValue("reddys"));
    }
}
output:-
===========
{karan=reddy, venkatesh=reddy, vivek=reddy, ramesh=reddy, rajesh=srinu}
[karan, venkatesh, vivek, ramesh, rajesh]
[reddy, reddy, reddy, reddy, srinu]
true
false

LinkedHashMap:-
=============
package com.app;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MapExample {

    public static void main(String[] args) {
        Map<String,String> map=new LinkedHashMap<String, String>();
        map.put("karan", "reddy");
        map.put("vivek", "reddy");
        map.put("venkatesh", "reddy");
        map.put("ramesh", "reddy");
        map.put("rajesh", "reddy");
        map.put("rajesh", "srinu");
        map.put("anil", "srinu");
        System.out.println(map);
        Set<String> ss=map.keySet();
        System.out.println(ss);
        Collection<String> sss=map.values();
        System.out.println(sss);
        System.out.println(map.containsKey("rajesh"));
        System.out.println(map.containsValue("reddys"));
    }
}

output:-
===========
{karan=reddy, vivek=reddy, venkatesh=reddy, ramesh=reddy, rajesh=srinu, anil=srinu}
[karan, vivek, venkatesh, ramesh, rajesh, anil]
[reddy, reddy, reddy, reddy, srinu, srinu]
true
false

WeakHashMap:-
===========
package com.app;

import java.util.WeakHashMap;

public class WeakHashmapExample {

    public static void main(String[] args) throws InterruptedException {
        WeakHashMap<Integer, String> map=new WeakHashMap<Integer, String>();
        Integer i1=new Integer(10);
        map.put(i1, "karan");
        System.out.println(map);
        i1=null;
        System.gc();
        Thread.sleep(5000);
        System.out.println(map);
    }
}

output:-
=========
{10=karan}
{}

IdentityHashMap :-
===============
package com.app;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class IdentityHashMapExample {

    public static void main(String[] args) {
        Map<Integer, String> map=new HashMap<Integer,String>();
        
        Integer i1=new Integer(10);
        Integer i2=new Integer(10);
        
        map.put(i1,"karan");
        map.put(i2, "rohit");
        System.out.println(map);
        
    Map<Integer, String> map1=new IdentityHashMap<Integer,String>();
        
        Integer i11=new Integer(10);
        Integer i21=new Integer(10);
        
        map1.put(i11,"karan");
        map1.put(i21, "rohit");
        System.out.println(map1);
    }
}

output:-
============
{10=rohit}
{10=karan, 10=rohit}


 NavigableMap :-
===============
package com.app;

import java.util.TreeMap;
import java.util.TreeSet;

public class NavigableMapExample {

    public static void main(String[] args) {
        TreeMap<String,String> tm=new TreeMap<String, String>();
        tm.put("b", "bat");
        tm.put("a", "apple");
        tm.put("c", "cat");
        tm.put("e", "elephant");
        tm.put("d", "dog");
        System.out.println(tm);
        System.out.println("celling key: "+tm.ceilingKey("c"));
        System.out.println("higher key: "+tm.higherKey("d"));
        System.out.println("floor key: "+tm.floorKey("e"));
        System.out.println("lower key: "+tm.lowerKey("e"));
        System.out.println("pollFirst: "+tm.pollFirstEntry());
        System.out.println("pollLast: "+tm.pollLastEntry());
        System.out.println("descending map:"+tm.descendingMap());
        System.out.println(tm);
    }
}
output:-
==========
{a=apple, b=bat, c=cat, d=dog, e=elephant}
celling key: c
higher key: e
floor key: e
lower key: d
pollFirst: a=apple
pollLast: e=elephant
descending map:{d=dog, c=cat, b=bat}
{b=bat, c=cat, d=dog}

TreeMap:-
===========
package com.app;

import java.util.TreeMap;

public class TreeMapExample {

    public static void main(String[] args) {
        TreeMap<String, String> t=new TreeMap<String, String>();
        t.put("c", "cat");
        t.put("d", "dog");
        t.put("m", "mirror");
        t.put("b", "bellon");
        t.put("f", "fox");
        System.out.println(t);
    }
}
output:-
===========
{b=bellon, c=cat, d=dog, f=fox, m=mirror}


package com.app;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class HashMapExample {


    public static void main(String[] args) {

        /*
         * Hashmap:-
         *  1)Introduced 1.2 version
         *  2)we can't use Multi_threading environments
         *  3)It is not synchronized 
         *  4)It is not Thread safe
         *  5)Multiple threads are operate
         *  6)only one null key and multiple null values
         *  7)HashMap will generally be fastest performance
         *  8)default capacity by default 16
         *  9)Present in java.util
         * 10)throw  ConcurrentModificationException
         * 11)Single Threaded Applications
         * 12)It is very fast compare to ConcurrentHashMap

         *   *  ConcurrentHashMap:-
         *  =======================
         *  1)Introduced 1.5 version
         *  2)we can use Multi_threading environments
         *  3)it is not legacy
         *  4)it is not synchronized
         *  5)It is Thread safe
         *  6)Because one thread is operate another thread is waiting on the same segments(0-15)
         *  segment level lock will applied not object level
         *  7)Perform read and write operation >>read operation without lock and write operation bucket level
         *  8)null key and values are not allowed here
         *  9)ConcurrentHashMap will generally be fastest performance
         *  10)It will print latest/update value
         *  11)Nature of iterator >> Fail Safe  
         *  12)ConcurrentHashMap It is does throw any ConcurrentModificationException
         *  13)Present in java.util.concurrent
         *  14)Multi Threaded Applications
         *  15)It is very slow compare to HashMap
         *  
         *  
         *  Hashtable:-
         * 1)Introduced 1.0 version
         * 2)It is legacy
         * 3)it is synchronized
         * 4)It is Thread safe 
         * 5)Because one thread is operate another thread is waiting on the same segments(0-15)
         * 6)null key and values are not allowed here
         * 7)Hashtable will generally be slow performance
         *8)read and write operation performed
         * 
         *  
         *  SynchronizedMap:-
         *  ===================
         *  1)Introduced 1.2 version
         *  2)it is not legacy
         *  3)it is not synchronized
         *  4)It is Thread safe
         *  5)Because one thread is operate another thread is waiting stage(Locking mechanism)
         *  6)null key and values are  allowed here
         *  7)SynchronizedMap will generally be slow performance
         *  8)read and write operation performed
         *  
         *  
         */
        //Q)Can multiple thread write in same seqment?
        //Ans:-No ,Thread acquires a lock on segment in put() operation and at a time one thread can write in that segment

        //Q)Can two threads write in the different segement?
        //Yes,Two Threads are allowed to write concurrently in different segments.
        //Q)can Multiple thread read from the same segment?
        //Yes, thread doesn't acquire a lock on segement in get() operation and any number of thread can read from same segement
        //Q)If one thread is writing in segment,can another thread read from the segment?
        //Ans:-Yes.but in this case last updated value will be seen by the reading thread.

        //Null keys and values
        //ConcurrentHashMap doesn't allow null keys and null values

        //SynchronizedMap method in collections class
        Map<String,String> map1=new HashMap<String,String>();
        map1.put("1", "Naveen");
        map1.put("2", "To");
        map1.put("3", "Lisa");

        //create SynchronizedMap
        Map<String, String> syncMap = Collections.synchronizedMap(map1);
        System.out.println(syncMap);
        ////ConcurrentHashMap method in collections class
        ConcurrentHashMap<String, String> con=new ConcurrentHashMap<String, String>();
        con.put("A", "Java");
        con.put("B", "Python");
        con.put("C", "Ruby");
        System.out.println(con.get("A"));
        //ConcurrentHashMap It is does throw any ConcurrentModificationException
    }
}

output:-
==========
{1=Naveen, 2=To, 3=Lisa}
Java

=======================================

What is fail fast and fail safe:-
================================

*** While iterating object if we trying to add something same object system JVM will throw then ConcurrentModificationException
so program will be terminated. this is called fail fast.

***** While iterating object if we trying to add something same object system JVM will not throw then ConcurrentModificationException
so program will be continusly processing this is called fail safe.


Fail Fast example:-
==================
package com.app;

import java.util.ArrayList;
import java.util.List;

public class FailFastAndFailSafeExample {

    public static void main(String[] args) {
        
        
        List<Integer> list=new ArrayList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        
        for(int n:list)
        {
            System.out.println(n);
            list.add(50);
        }
    }
}

output:-
==========
10
Exception in thread "main" java.util.ConcurrentModificationException
    at java.util.ArrayList$Itr.checkForComodification(ArrayList.java:911)
    at java.util.ArrayList$Itr.next(ArrayList.java:861)
    at com.app.FailFastAndFailSafeExample.main(FailFastAndFailSafeExample.java:18)

========================
Fail Safe example:-
=====================

package com.app;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class FailFastAndFailSafeExample {

    public static void main(String[] args) {
        
        
        List<Integer> list=new CopyOnWriteArrayList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        
        for(int n:list)
        {
            System.out.println(n);
            list.add(50);
        }
    }
}


output:-
=========
10
20
30
40
50

=======================================

Iterators:-
==========
1)Enumeration
2)Iterator
3)ListIterator


Enumeration:-
=============
package com.app;

import java.util.Enumeration;
import java.util.Vector;

public class EnumerationExample {

    public static void main(String[] args) {
        Vector<Integer> v=new Vector<Integer>();
        
        for(int i=0;i<=10;i++)
        {
            v.addElement(i);
        }
        System.out.println(v);
        
        Enumeration<Integer> e=v.elements();
        System.out.println(e.getClass().getName());
        while(e.hasMoreElements())
        {
            Integer i=(Integer)e.nextElement();
            if(i%2==0)
            {
                System.out.println(i);
            }
        }
        System.out.println(v);
    }
}
output:-
============
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
java.util.Vector$1
0
2
4
6
8
10
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


Iterator :-
=========
package com.app;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorExample {

    public static void main(String[] args) {
        ArrayList<Integer> al=new ArrayList<Integer>();
    
        for(int i=0;i<=10;i++)
        {
            al.add(i);
        }
        System.out.println(al);
        
        
        Iterator<Integer> itr=al.iterator();
        System.out.println(itr.getClass().getName());
        
        while(itr.hasNext())
        {
            Object ob=itr.next();
            Integer i=(Integer)ob;
            
            if(i%2==0)
            {
                System.out.println(i);
            }else {
                itr.remove();
            }
            
        }
        System.out.println(al);
    }
}
output:-
=========
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
java.util.ArrayList$Itr
0
2
4
6
8
10
[0, 2, 4, 6, 8, 10]

ListIterator:-
=============
package com.app;

import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorExample {

    public static void main(String[] args) {
        LinkedList<String> ll=new LinkedList<String>();
        ll.add("Balakrishna");
        ll.add("venky");
        ll.add("chiru");
        ll.add("nag");
        
        System.out.println(ll);
        
        ListIterator<String> ltr=ll.listIterator();
        
    while(ltr.hasNext())
    {
        String s=(String)ltr.next();
        if(s.equals("venky"))
        {
            ltr.remove();
        }else if (s.equals("nag")) {
            ltr.add("chaitu");
        }else if (s.equals("chiru")) {
            ltr.set("charan");
        }
    }
        System.out.println(ll);
    }
}

output:-
=========
[Balakrishna, venky, chiru, nag]
[Balakrishna, charan, nag, chaitu]



Properties:-
==============

user.properties:-
================
1=srinivas
2=nihas
3=sneha
4=ram
===============
package com.app;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;

public class PropertiesExample {

    public static void main(String[] args) throws IOException {
        Properties p=new Properties();
    
        FileInputStream fis=new FileInputStream("C:\\Eclipse\\Core_Java_Programs\\Collections\\src\\com\\app\\user.properties");
    
        p.load(fis);
        
        System.out.println(p);
        
        String aa=p.getProperty("1");
        System.out.println(aa);
        String bb=p.getProperty("2");
        System.out.println(bb);
        String cc=p.getProperty("3");
        System.out.println(cc);
        
        System.out.println("*******");
    
        Set<String> ss=p.stringPropertyNames();
        
        for (String string : ss) {
            System.out.println(string);
        }
        System.out.println("************");
        Enumeration<?> e=p.propertyNames();

        while(e.hasMoreElements())
        {
            System.out.println(e.nextElement());
        }
    }
}

output:-
========
{4=ram, 3=sneha, 2=nihas, 1=srinivas}
srinivas
nihas
sneha
*******
4
3
2
1
************
4
3
2
1

Hashtable:-
============
package com.app;

import java.util.Hashtable;

public class HashtableExample {

    public static void main(String[] args) {
        Hashtable<String, String> ht=new Hashtable<String, String>();
        ht.put("c", "cat");
        ht.put("d", "dog");
        ht.put("r", "rear");
        ht.put("s", "sand");
        ht.put("g", "girl");
        ht.put("i", "ink");
        ht.put("m", "mirror");
        //ht.put(null, "dd");Exception in thread "main" java.lang.NullPointerException
        //ht.put(null, null);//Exception in thread "main" java.lang.NullPointerException
        //ht.put("dd", null);//Exception in thread "main" java.lang.NullPointerException
        System.out.println(ht);
    }
}
output:-
========
{m=mirror, i=ink, s=sand, g=girl, r=rear, d=dog, c=cat}


ArmStrongNumber:-
==================
package com.app.interviews;

import java.util.Scanner;

public class ArmStrongNumberExample {

    static int r,sum=0,temp;
    public static void main(String[] args) {
        //153 >>1^3+5^3+3^3=153
        Scanner scanner=new Scanner(System.in);
        System.out.println("enter your number");
        int num=scanner.nextInt();
        temp=num;
        while(num>0) {
            r=num%10;
            num=num/10;
            sum=sum+(r*r*r);
        }
        if(temp==sum) {
            System.out.println("It is an armstrong");
        }else {
            System.out.println("It is not an armstrong");
        }
        scanner.close();
    }
}
output:-
========
enter your number
153
It is an armstrong

Method Hiding:-
================
package com.app.interviews;

public class MethodHidingExample {
    public static void method1() {
        System.out.println("Method-1 of the MethodHidingExample class is executed.");  
        }
    public void method2() {
        System.out.println("Method-2 of the MethodHidingExample class is executed.");  
    }
}

package com.app.interviews;

public class MethodHidingExampleSubclasss extends MethodHidingExample{
    public static void method1() {
        System.out.println("Method-1 of the MethodHidingExampleSubclasss class is executed.");  
        }
    public void method2()  
    {  
    System.out.println("Method-2 of the MethodHidingExampleSubclasss class is executed.");  
    }  
    
    public static void main(String[] args) {
        MethodHidingExample mm=new MethodHidingExample();
        MethodHidingExampleSubclasss mm1=new MethodHidingExampleSubclasss();
        mm.method1(); //method hidding
    mm1.method1();//method hidding
        mm.method2();//method overrding
        mm1.method2();//method overrding
    }
}

output:-
=========
Method-1 of the MethodHidingExample class is executed.
Method-1 of the MethodHidingExampleSubclasss class is executed.
Method-2 of the MethodHidingExample class is executed.
Method-2 of the MethodHidingExampleSubclasss class is executed.

Swapping:-
=========
package com.app.interviews;

import java.util.Scanner;

public class SwapTwoNumbers {
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
System.out.println("Enter First Number");
int a=sc.nextInt();
System.out.println("Enter Second Number");
int b=sc.nextInt();

System.out.println("Before swapping"+a+" "+b);

a=a+b;
b=a-b;
a=a-b;

System.out.println("After swapping:"+a+" "+b);
}
}

output:-
==========
Enter First Number
10
Enter Second Number
20
Before swapping10 20
After swapping:20 10


EvenOrOdd Number:-
===================
package com.app.interviews;

import java.util.Scanner;

public class EvenOrOddNumberPrintingExample {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter a number");
         int num=sc.nextInt();
         
         if(num%2==0) {
             System.out.println(num+":->It is an Even number");
         }else {
             System.out.println(num+":-> It is an Odd number");
         }
    }
}
output:-
==========
Enter a number
4
4:->It is an Even number

NullPointerException:-
===================
package com.app.interviews;

public class NullPointerExceptionExample {
    public String name="srinivas";
}

package com.app.interviews;

public class NullPointerExceptionExampleMain {

    public static void main(String[] args) {
        NullPointerExceptionExample ss=null;
        try {
            System.out.println(ss.name);
        } catch (Exception e) {
             ss=new NullPointerExceptionExample();
             System.out.println(ss.name);
        }
    }
}

output:-
==========
srinivas

Factorial Example:-
==================
package com.app.interviews;

import java.util.Scanner;

public class FactorialExample {
    static int factorialExample(int num) {
        if(num==0) {
            return 1;
        }
        else {
            return(num*factorialExample(num-1));
        }
    }
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter First Number");
        int num=scanner.nextInt();
        int fact=1;
        fact=factorialExample(num);
        System.out.println("Factorial of num "+num+" is :"+fact);
        scanner.close();
    }
}

output:-
==========
Enter First Number
3
Factorial of num 3 is :6


Fibonacci Series:-
====================
package com.app.interviews;

import java.util.Scanner;

public class FibonacciSeriesExample {
    static int a=0;
    static int b=1;
    static int c=0;
    
     static void fibonacci(int num) {
        
        if(num>0) {
            c=a+b;
            a=b;
            b=c;
            System.out.print(" "+c);
            fibonacci(num-1);
        }
    }
    
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter your number");
    int aa=sc.nextInt();
    System.out.print(a+" "+b);
    fibonacci(aa-2);
}
}
output:-
===========
Enter your number
4
0 1 1 2

TwoSumTarget Example:-
====================
package com.app.interviews;

import java.util.Arrays;

public class TwoSumExamplePractice {
    
    public int[] twoSumData(int[] nums,int target) {
        
        for(int i=0;i<nums.length;i++) {
            for(int j=i+1;j<nums.length;j++) {
                if(nums[i]+nums[j]==target) {
                    return new int[] {i,j};
                }
            }
        }
        return null;    
    }
public static void main(String[] args) {
    
    int[] nums= {3,4,1,6};
    int target=4;
    TwoSumExamplePractice ss=new TwoSumExamplePractice();
    System.out.println(Arrays.toString(ss.twoSumData(nums,target)));
}
}

output:-
===========
[0, 2]


PrimeNumber:-
===============
package com.app.interviews;

import java.util.Scanner;

public class PrimeNumberExample {

    static int i,m=0,flag=0;
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("enter your number");
int num=sc.nextInt();

m=num/2;//m=10/2=5


if(num==0 || num==1) {
System.out.println(num+" it is not Prime Number");
    }else {
         for(int i=2;i<=m;i++) {
             if(num%i==0) {
                 System.out.println(num+" it is not Prime Number ");
                 flag=1;
                 break;
             }
         }
         if(flag==0) {
             System.out.println(num+" it is Prime Number ");
         }
         
    }

}
}

output:-
===========
enter your number
9
9 it is not Prime Number 

StringImmutable Sample TestCases:-
==================================
package com.app.interviews;

import java.util.stream.IntStream;

public class StringImmutableExample {
    
    //String -Immuable
    //StringBuffer-Mutable
    //StringBuilder-Mutable
    //String hold only address
    //StringBuffer and StringBuilder hold only data
    
@SuppressWarnings("static-access")
public static void main(String[] args) {
    
    System.out.println("***********StringBuffer**********");
    StringBuffer sb1=new StringBuffer("srinivas");//address-26
    StringBuffer sb2=new StringBuffer("srinivas");//address-36
    System.out.println(sb1==sb2);//26==36
    sb1.append("Vadla");//address-26 srinivasVadla
    System.out.println(sb1==sb2);
    
    StringBuffer sb3=new StringBuffer("Vadlasrinivas");
    System.out.println(sb1==sb3);
    System.out.println(sb1.capacity());
    System.out.println("***********String**********");
    
    String s1="srinivas";
    String s2="srinivas";
    
    System.out.println(s1==s2);
    
    String s3=s1+"Vadla";
    System.out.println(s1==s3);
    
    String s4="srinivasVadla";
    System.out.println(s1==s4);
    System.out.println(s1);
    System.out.println("***********String Builder**********");
    
    StringBuilder sbb1=new StringBuilder("srinivas");
    StringBuilder sbb2=new StringBuilder("srinivas");
    
    char ch=sbb1.charAt(0);
    System.out.println((int)ch);
    System.out.println(ch);
    System.out.println(sbb1.capacity());
    System.out.println(sbb1==sbb2);
    sb1.append("Vadla");//address-26 srinivasVadla
    System.out.println(sbb1==sbb2);
    
    StringBuilder sbb3=new StringBuilder("Vadlasrinivas");
    System.out.println(sbb1==sbb3);
    System.out.println(sb1.capacity());
    
    System.out.println("*********Insert operation in StringBuilder");
    sbb1.insert(0, "Vadla ");
    System.out.println(sbb1);
    
    sbb1.deleteCharAt(4);
    System.out.println(sbb1);
    
    sbb1.delete(0, 5);
    System.out.println(sbb1);
    
    System.out.println(sbb1.reverse());
    System.out.println("*********");
sb1.ensureCapacity(100);
sb1.capacity();
System.out.println(sb1.capacity());

System.out.println("*****************Compare");
System.out.println(s1.equals(s2));
System.out.println(sb1.equals(sb2));
System.out.println(sb1.compareTo(sb2));
System.out.println(sbb1.compareTo(sbb2));

IntStream ii=sbb1.chars();
ii.forEachOrdered(System.out::println);

for (String st : args) {
System.out.println(st.charAt((int)ch));
}
}
}

output:-
========
***********StringBuffer**********
false
false
false
24
***********String**********
true
false
false
srinivas
***********String Builder**********
115
s
24
false
false
false
24
*********Insert operation in StringBuilder
Vadla srinivas
Vadl srinivas
srinivas
savinirs
*********
100
*****************Compare
true
false
10
-17
115
97
118
105
110
105
114
115


EqualAndHashcodeMethod:-
========================

package com.app.interviews;

public class EqualAndHashcodeMethodExamples {

    private int id;
    private String name;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        
        if(obj==null|| getClass()!=obj.getClass()) {
            return false;
        }
        if(obj==this) {
            return true;
        }
        EqualAndHashcodeMethodExamples sss=(EqualAndHashcodeMethodExamples)obj;
        return (this.getId()==sss.getId());
        
    }
    
    @Override
    public int hashCode() {
        
        return getId();
    }
}

package com.app.interviews;

public class EqualAndHashcodeMethodMain {
    
public static void main(String[] args) {
    
    EqualAndHashcodeMethodExamples eqq=new EqualAndHashcodeMethodExamples();
    eqq.setId(333);
    eqq.setName("srinivas");
    System.out.println(eqq.hashCode());
    EqualAndHashcodeMethodExamples eqq1=new EqualAndHashcodeMethodExamples();
    eqq1.setId(333);
    eqq1.setName("srinivas");
    System.out.println(eqq1.hashCode());
    
    
    System.out.println("Shallow Comparision+ "+(eqq==eqq1));
    
    System.out.println("Deep Comparision+ "+(eqq.equals(eqq1)));
    System.out.println(eqq.hashCode());
}
}

output:-
=========
333
333
Shallow Comparision+ false
Deep Comparision+ true
333


MYSQL Database:-
=================
 Check MYSQL version==> mysql -V
v should be captital

login:- mysql -u root -p
Enter password: ***********

//show databases
mysql> show databases;
//Create New Database
mysql> create database testdb;
//Select Database for operations
mysql> use testdb;
//View All Tables
mysql> show tables;


// table fields name, email, password, created_on, is_email_verified, is_mobile_verified, is_active, is_blocked, dob

//create table
create table users(name varchar(255),email varchar(255),password varchar(255),created_on varchar(255),is_email_verified varchar(10),is_mobile_verified varchar(10),is_active varchar(10),is_blocked varchar(10),dob date);

//'srinivas', 'srinivas.vadla4421@gmail.com', 'welcome1234', '2024-01-29 23:59:59', NULL, NULL, NULL, NULL, NULL
//'srinivas', 'srinivas.vadla4421@gmail.com', 'welcome1234', '2024-01-29 23:59:59', NULL, NULL, NULL, NULL, NULL
//'srinivas', 'srinivas.vadla4421@gmail.com', 'welcome1234', '2024-01-29 23:59:59', NULL, NULL, NULL, NULL, NULL
//'srinu', 'srinu@gmail.com', 'welcome707', '2024-01-29 20:33:23', NULL, NULL, NULL, NULL, NULL
//'nihas', 'nihas@gmail.com', 'welcome709', '2024-01-29 10:33:23', NULL, NULL, NULL, NULL, NULL
//'nihas', 'nihas@gmail.com', 'welcome709', '2024-01-29 10:33:23', NULL, NULL, NULL, NULL, NULL
///'raju', 'raju@gmail.com', 'welcome799', '2024-01-29 10:38:23', NULL, NULL, NULL, NULL, NULL
//'raju', 'raju@gmail.com', 'welcome799', '2024-01-29 10:38:23', NULL, NULL, NULL, NULL, NULL

//insert into table
insert into users(name,email,password,created_on) values('raju','raju@gmail.com','welcome799','2024-01-29 10:38:23');

//update operation
mysql> update users set password='welcomesrinvas' where name='srinu';
//delete operation
mysql> delete from users where email='srinivas.vadla4421@gmail.com';

//Add column usr_Id :-
ALTER TABLE users
ADD usr_Id int;

//select name,email from users;
select name,email from users;
//matching email 
select * from users where email='raju@gmail.com';

//single line comment --
//login<<email,password

//And Operation
select * from users where email='raju@gmail.com' and password='welcome799';
//Or operation
select * from users where email='raju@gmail.com' or password='welcome79';



//create table student(student_Id int primary key auto_increment,stu_Name varchar(255),stud_marks varchar(255));
//insert into student values(1,'srinivas',333);
///insert into student values(1,'srinu',444); >>>>> Duplicate entry '1' for key 'student.PRIMARY' 0.000 sec
//19:37:04 insert into student values(1,'srinu',444)   Error Code: 1062. Duplicate entry '1' for key 'student.PRIMARY' 0.000 sec


Joins:-
=======

create table users(
user_id int primary key auto_increment,
name varchar(255),
email varchar(255),
password varchar(255),
is_email_verified varchar(10),
created_on datetime,
is_actice varchar(10),
is_blocked varchar(10)
);

create table products(
product_id int primary key auto_increment,
title varchar(255),
description text,
price decimal(7,2),
discount int,
stock int,
created_on date,
is_active varchar(10)
);

create table orders(
order_id int primary key auto_increment,
product_id int,
user_id int,
price decimal(7,2),
payment_mode varchar(100),
status varchar(255),
order_placed_on datetime,
order_delivered_on datetime,
order_cancelled_on datetime
);

//insert into users table
insert into users(name,email,password) 
values('Nihas Vadla','Nihas@gmail.com','welcome1323');

insert into users(name,email,password) 
values('sneha Vadla','Nihas@gmail.com','welcome1323');

//insert into products table
insert into products(title,description,price)
values('iphone 13','nice mobile',99.9);

insert into products(title,description,price)
values('iphone 14','nice mobile',100);

insert into products(title,description,price)
values('iphone 15','nice mobile',200);

//insert into orders table

insert into orders(product_id,user_id,price,payment_mode,status)
values(2,1,100,'COD','In Transient');

insert into orders(product_id,user_id,price,payment_mode,status)
values(3,3,259,'Credit card','Out of delivery');

insert into orders(product_id,user_id,price,payment_mode,status)
values(1,2,300,'Netbanking','Read to ship');

insert into orders(product_id,user_id,price,payment_mode,status)
values(2,3,500,'Netbanking','Delivered');

//select all query

select * from users;
select * from products;
select * from orders;

//Inner Join
================
select * from
orders inner join products on
orders.product_id=products.product_id;
//order_id, product_id, user_id, price, payment_mode, status, order_placed_on, order_delivered_on, order_cancelled_on, product_id, title, description, price, discount, stock, created_on, is_active

'1', '2', '1', '100.00', 'COD', 'In Transient', NULL, NULL, NULL, '2', 'iphone 14', 'nice mobile', '100.00', NULL, NULL, NULL, NULL
'2', '3', '3', '259.00', 'Credit card', 'Out of delivery', NULL, NULL, NULL, '3', 'iphone 15', 'nice mobile', '200.00', NULL, NULL, NULL, NULL
'3', '1', '2', '300.00', 'Netbanking', 'Read to ship', NULL, NULL, NULL, '1', 'iphone 13', 'nice mobile', '99.90', NULL, NULL, NULL, NULL
'4', '2', '3', '500.00', 'Netbanking', 'Delivered', NULL, NULL, NULL, '2', 'iphone 14', 'nice mobile', '100.00', NULL, NULL, NULL, NULL
=================================================================
//specfic column

select orders.order_id,orders.product_id,orders.user_id,products.title from  
orders inner join products on
orders.product_id=products.product_id;
//
order_id, product_id, user_id, title
'1', '2', '1', 'iphone 14'
'2', '3', '3', 'iphone 15'
'3', '1', '2', 'iphone 13'
'4', '2', '3', 'iphone 14'
=================================================
With using alies/short way to write query:-
===========================================
select o.order_id,o.product_id,o.user_id,p.title from 
orders as o inner join products as p on
o.product_id=p.product_id;
=========================================================

//where condition
select o.order_id,o.product_id,o.user_id,p.title from 
orders as o inner join products as p on
o.product_id=p.product_id 
where o.order_id=1;
//order_id, product_id, user_id, title
'1', '2', '1', 'iphone 14'
======================================================
select orders.order_id,orders.price,orders.payment_mode,orders.status,products.title,products.description
from orders
inner join
products
on orders.product_id=products.product_id;
//select this fields to get the data
order_id, price, payment_mode, status, title, description
'1', '100.00', 'COD', 'In Transient', 'iphone 14', 'nice mobile'
'2', '259.00', 'Credit card', 'Out of delivery', 'iphone 15', 'nice mobile'
'3', '300.00', 'Netbanking', 'Read to ship', 'iphone 13', 'nice mobile'
'4', '500.00', 'Netbanking', 'Delivered', 'iphone 14', 'nice mobile'
===========================================================================

With using alies/short way to write query:-
===========================================
select o.order_id,o.price,o.payment_mode,o.status,p.title,p.description
from orders as o
inner join
products as p
on o.product_id=p.product_id;
=========================================================================
left join:-
===========
select * from 
orders
left join
products
on orders.product_id=products.product_id;
//order_id, product_id, user_id, price, payment_mode, status, order_placed_on, order_delivered_on, order_cancelled_on, product_id, title, description, price, discount, stock, created_on, is_active
'1', '2', '1', '100.00', 'COD', 'In Transient', NULL, NULL, NULL, '2', 'iphone 14', 'nice mobile', '100.00', NULL, NULL, NULL, NULL
'2', '3', '3', '259.00', 'Credit card', 'Out of delivery', NULL, NULL, NULL, '3', 'iphone 15', 'nice mobile', '200.00', NULL, NULL, NULL, NULL
'3', '1', '2', '300.00', 'Netbanking', 'Read to ship', NULL, NULL, NULL, '1', 'iphone 13', 'nice mobile', '99.90', NULL, NULL, NULL, NULL
'4', '2', '3', '500.00', 'Netbanking', 'Delivered', NULL, NULL, NULL, '2', 'iphone 14', 'nice mobile', '100.00', NULL, NULL, NULL, NULL
============================================================================================================================================
right join:-
============
select * from 
orders
right join
products
on orders.product_id=products.product_id;
//order_id, product_id, user_id, price, payment_mode, status, order_placed_on, order_delivered_on, order_cancelled_on, product_id, title, description, price, discount, stock, created_on, is_active
'3', '1', '2', '300.00', 'Netbanking', 'Read to ship', NULL, NULL, NULL, '1', 'iphone 13', 'nice mobile', '99.90', NULL, NULL, NULL, NULL
'4', '2', '3', '500.00', 'Netbanking', 'Delivered', NULL, NULL, NULL, '2', 'iphone 14', 'nice mobile', '100.00', NULL, NULL, NULL, NULL
'1', '2', '1', '100.00', 'COD', 'In Transient', NULL, NULL, NULL, '2', 'iphone 14', 'nice mobile', '100.00', NULL, NULL, NULL, NULL
'2', '3', '3', '259.00', 'Credit card', 'Out of delivery', NULL, NULL, NULL, '3', 'iphone 15', 'nice mobile', '200.00', NULL, NULL, NULL, NULL
====================================================================================================================
select orders.price,orders.payment_mode,orders.payment_mode,products.description,products.title from
orders
LEFT JOIN
products
on orders.product_id=products.product_id;
=============================================
//price, payment_mode, payment_mode, description, title
'100.00', 'COD', 'COD', 'nice mobile', 'iphone 14'
'259.00', 'Credit card', 'Credit card', 'nice mobile', 'iphone 15'
'300.00', 'Netbanking', 'Netbanking', 'nice mobile', 'iphone 13'
'500.00', 'Netbanking', 'Netbanking', 'nice mobile', 'iphone 14'

==================================================================================
select orders.price,orders.payment_mode,orders.payment_mode,products.description,products.title from
orders
RIGHT JOIN
products
on orders.product_id=products.product_id;
=====================================================
//price, payment_mode, payment_mode, description, title
'300.00', 'Netbanking', 'Netbanking', 'nice mobile', 'iphone 13'
'500.00', 'Netbanking', 'Netbanking', 'nice mobile', 'iphone 14'
'100.00', 'COD', 'COD', 'nice mobile', 'iphone 14'
'259.00', 'Credit card', 'Credit card', 'nice mobile', 'iphone 15'

==============================================================================
select orders.price,orders.payment_mode,orders.payment_mode,products.description,products.title from
orders
INNER JOIN
products
on orders.product_id=products.product_id;
==============================================
//price, payment_mode, payment_mode, description, title
'100.00', 'COD', 'COD', 'nice mobile', 'iphone 14'
'259.00', 'Credit card', 'Credit card', 'nice mobile', 'iphone 15'
'300.00', 'Netbanking', 'Netbanking', 'nice mobile', 'iphone 13'
'500.00', 'Netbanking', 'Netbanking', 'nice mobile', 'iphone 14'
======================================================================

And operation:-
==================
select orders.payment_mode,products.title 
from 
orders
inner join
products
where orders.payment_mode='Credit card' AND products.title='iphone 15';
//payment_mode, title
'Credit card', 'iphone 15'
==================================================
OR operation:-
=====================
select orders.payment_mode,products.title 
from 
orders
inner join
products
where orders.payment_mode='Credit card' OR products.title='iphone 15';
//payment_mode, title
'COD', 'iphone 15'
'Credit card', 'iphone 15'
'Credit card', 'iphone 14'
'Credit card', 'iphone 13'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 15'
==============================================
select orders.payment_mode,products.title 
from 
orders
left join
products on
orders.payment_mode='Credit card' OR products.title='iphone 15';
=================================================================
select orders.payment_mode,products.title 
from 
orders
right join
products on
orders.payment_mode='Credit card' OR products.title='iphone 15';
==================================================================
select orders.payment_mode,products.title 
from 
orders
inner join
products on
orders.payment_mode='Credit card' OR products.title='iphone 15';
===================================================================
select orders.payment_mode,products.title 
from 
orders
inner join
products on
orders.payment_mode='Credit card' AND products.title='iphone 15';
-------------------------------------------------------------------
select orders.payment_mode,products.title 
from 
orders
left join
products on
orders.payment_mode='Credit card' AND products.title='iphone 15';
==================================================================
select orders.payment_mode,products.title 
from 
orders
right join
products on
orders.payment_mode='Credit card' AND products.title='iphone 15';
===================================================================

select orders.payment_mode,products.title 
from 
orders
inner join
products
where 
NOT
orders.payment_mode='Credit card';
//payment_mode, title
'COD', 'iphone 15'
'COD', 'iphone 14'
'COD', 'iphone 13'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 13'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 13'

===================================================
select orders.payment_mode,products.title 
from 
orders
left join
products on
NOT
orders.payment_mode='Credit card';
//payment_mode, title
'COD', 'iphone 15'
'COD', 'iphone 14'
'COD', 'iphone 13'
'Credit card', NULL
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 13'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 13'

=============================
select orders.payment_mode,products.title 
from 
orders
right join
products on
NOT
orders.payment_mode='Credit card';
//payment_mode, title
'Netbanking', 'iphone 13'
'Netbanking', 'iphone 13'
'COD', 'iphone 13'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 14'
'COD', 'iphone 14'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 15'
'COD', 'iphone 15'

===========================================

order by:-
===============
select orders.payment_mode,products.title 
from 
orders
inner join
products
ORDER BY
products.title;

//payment_mode, title
'COD', 'iphone 13'
'Credit card', 'iphone 13'
'Netbanking', 'iphone 13'
'Netbanking', 'iphone 13'
'COD', 'iphone 14'
'Credit card', 'iphone 14'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 14'
'COD', 'iphone 15'
'Credit card', 'iphone 15'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 15'

==============================
select orders.payment_mode,products.title 
from 
orders
inner join
products
ORDER BY
orders.payment_mode;
//payment_mode, title
'COD', 'iphone 15'
'COD', 'iphone 14'
'COD', 'iphone 13'
'Credit card', 'iphone 15'
'Credit card', 'iphone 14'
'Credit card', 'iphone 13'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 13'
'Netbanking', 'iphone 15'
'Netbanking', 'iphone 14'
'Netbanking', 'iphone 13'

==============================================
group by:-
=================
select status,count(payment_mode) from orders group by status;
//status, count(payment_mode)
'In Transient', '1'
'Out of delivery', '1'
'Read to ship', '1'
'Delivered', '1'

==========================
count:-
==========
select count(payment_mode) from orders;
//count(payment_mode)
'4'
=======================================
max:-
=========
select max(payment_mode) from orders;
//max(payment_mode)
'Netbanking'

select max(payment_mode) from orders where orders.order_id;
//max(payment_mode)
'Netbanking'
==============
min:-
=======
select min(payment_mode) from orders;
//min(payment_mode)
'COD'

select min(payment_mode) from orders where orders.order_id;
//min(payment_mode)
'COD'

==============================
average:-
===========
select price,avg(price) from orders group by orders.order_id;
//price, avg(price)
'100.00', '100.000000'
'259.00', '259.000000'
'300.00', '300.000000'
'500.00', '500.000000'
===========================================
Example for second highest query:-
==============================
SELECT MAX(SALARY) FROM Employee WHERE SALARY < (SELECT MAX(SALARY) FROM Employee);

Using the LIMIT Clause Nth highest:=
-------------------------------------
Syntax:

Select Salary from table_name order by Salary DESC limit n-1,1;

To find the 4th Highest salary query will be
=============================================
Query:

Select emp_sal from Emp order by emp_sal DESC limit 3,1;

=================================================================================

//order_id, product_id, user_id, price, payment_mode, status, order_placed_on, order_delivered_on, order_cancelled_on
'1', '2', '1', '100.00', 'COD', 'In Transient', NULL, NULL, NULL
'2', '3', '3', '259.00', 'Credit card', 'Out of delivery', NULL, NULL, NULL
'3', '1', '2', '300.00', 'Netbanking', 'Read to ship', NULL, NULL, NULL
'4', '2', '3', '500.00', 'Netbanking', 'Delivered', NULL, NULL, NULL


Second Highest SQL:-
=====================
SELECT MAX(price) FROM orders WHERE price < (SELECT MAX(price) FROM orders);
//MAX(price)
'300.00'
SELECT MAX(payment_mode) FROM orders WHERE payment_mode < (SELECT MAX(payment_mode) FROM orders);
//MAX(payment_mode)
'Credit card'
=======================================================

students - stid, name
1, anvesh
2, srinivas
3, ramesh 
4, suresh
5, naresh

players - stid, sport
1, cricket
2, football

students names who are non-playes (without using subquery)

(without using subquery) with using subquery also

With subquery:-
==============
SELECT name FROM students WHERE stid NOT IN (SELECT stid FROM players);
without subquery:-
================
With Left join
SELECT s.name
FROM students s
LEFT JOIN players p ON s.stid = p.stid
WHERE p.stid IS NULL;

Java 8:-
========

package com.app;

public class Book {
    private int id;
    private String name;
    private int pages;
    public Book() {
        super();
    }
    public Book(int id, String name, int pages) {
        super();
        this.id = id;
        this.name = name;
        this.pages = pages;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPages() {
        return pages;
    }
    public void setPages(int pages) {
        this.pages = pages;
    }
    @Override
    public String toString() {
        return "Book [id=" + id + ", name=" + name + ", pages=" + pages + "]";
    }    
}

package com.app;

import java.util.ArrayList;
import java.util.List;

public class BookDao {
    public List<Book> getBook()
    {
        List<Book> books=new ArrayList<Book>();
        books.add(new Book(101, "Core java", 400));
        books.add(new Book(363, "Hibernate", 100));
        books.add(new Book(273, "Spring", 200));
        books.add(new Book(893, "WebService", 300));
        return books;
    }
}
package com.app;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BookService {

//  public List<Book> getBookinSort()
//  {
//      List<Book> books=new BookDao().getBook();
//      Collections.sort(books,new Comparator<Book>() {
//
//          @Override
//          public int compare(Book o1, Book o2) {
//              return o1.getName().compareTo(o2.getName());
//          }
//      });
//      return books;
//  }
    
    public List<Book> getBookinSort()
    {
        List<Book> books=new BookDao().getBook();
        Collections.sort(books,(o1,o2)->o2.getName().compareTo(o1.getName()));
        return books;
    }
    
    
    
    public static void main(String[] args) {
        System.out.println(new BookService().getBookinSort());
    }
}


//class MyComparator implements Comparator<Book>
//{
//
//  @Override
//  public int compare(Book o1, Book o2) {
//
//      return o2.getName().compareTo(o1.getName());
//  }
//  
//}
output:-
==========
[Book [id=893, name=WebService, pages=300], Book [id=273, name=Spring, pages=200], Book [id=363, name=Hibernate, pages=100], Book [id=101, name=Core java, pages=400]]
===================================

package com.app;

public interface Calculator {
    //void swichOn();
    
    //void sum(int a);
    int substract(int a,int b);
}
class Calculatorimpl
{
    public static void main(String[] args) {
        //Calculator calculator=new Calculatorimpl();
        //calculator.swichOn();//traditional way
        //Calculator cc=()->System.out.println("I am good");
        //cc.swichOn();//lambda expression
        
        /*
         * Calculator cc=(a)->{System.out.println("sum: "+a); }; cc.sum(898);
         */
        Calculator cc=(a,b)->{System.out.println("minus: "+(b-a));
        if(b<a)//8<10
        {
        throw new RuntimeException("message");  
        }else {
            return b-a;
        }
        };
        System.out.println(cc.substract(8, 10));    
    }
}
output:-
===========
minus: 2
2

=============================
package com.app;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

//public class ConsumerDemo implements Consumer<Integer>{
//
//  @Override
//  public void accept(Integer t) {
//      System.out.println("Printing: "+t);
//  }// traditional way
public class ConsumerDemo {
    public static void main(String[] args) {
        
        /*Consumer<Integer> sss=t->System.out.println("Printing data: "+t);
        sss.accept(100);*/
        List<Integer> list11=Arrays.asList(2,6,8,4,10,14,12,5,7,17);
        list11.forEach(t->System.out.println("Printing data: "+t));
        }
    }
output:-
===========
Printing data: 2
Printing data: 6
Printing data: 8
Printing data: 4
Printing data: 10
Printing data: 14
Printing data: 12
Printing data: 5
Printing data: 7
Printing data: 17
======================
package com.app;

import java.util.List;
import java.util.Optional;

public class Customer {
    private int id;
    private String name;
    private String email;
    private List<String> phoneNumbes;
    public Customer() {
        super();
    }
    public Customer(int id, String name, String email, List<String> phoneNumbes) {
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumbes = phoneNumbes;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Optional<String> getEmail() {
        return Optional.ofNullable(email);
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public List<String> getPhoneNumbes() {
        return phoneNumbes;
    }
    public void setPhoneNumbes(List<String> phoneNumbes) {
        this.phoneNumbes = phoneNumbes;
    }
    @Override
    public String toString() {
        return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", phoneNumbes=" + phoneNumbes + "]";
    }
}

package com.app;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DatabaseCustomer {
    public static List<Customer> getAllCustomers()
    {
        return Stream.of(
                new Customer(101, "john", "john@gmail.com", Arrays.asList("9010458895","8780866158")),
                new Customer(102, "smith", "smith@gmail.com", Arrays.asList("810458895","6780866158")),
                new Customer(103, "peter", "peter@gmail.com", Arrays.asList("5010458895","2780866158")),
                new Customer(104, "kely", "kely@gmail.com", Arrays.asList("8010458895","7780866158"))
                ).collect(Collectors.toList());
    }   
}
package com.app;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class OptionalDemo {

    
    @SuppressWarnings("unlikely-arg-type")
    public static Customer getCustomerByEmail(String email) throws Exception
    {
        List<Customer> customers=DatabaseCustomer.getAllCustomers();
    
        return customers.stream().filter(cc->cc.getEmail().equals(email)).findAny().orElseThrow(()->new Exception("no customer this mail"));
    }
    
    
    
    
    public static void main(String[] args) throws Exception {
        
        Customer c=new Customer(101, "john", "nn@gmail,com", Arrays.asList("8989898","7878778"));
        
        //empty
        //of
        //ofNullable
        
        Optional<Object> emptyOptional=Optional.empty();
        System.out.println(emptyOptional);
        
        //Optional<String> emailOptional=Optional.of(c.getEmail());
        //System.out.println(emailOptional);
        
        Optional<String> ss=Optional.ofNullable(c.getName());
        System.out.println(ss.get());
        
        Optional<String> ss1=Optional.empty();
        /*
         * if(ss1.isPresent()) { System.out.println(ss1.get()); }
         */
        //System.out.println(ss1);
        
        //System.out.println(ss1.orElse("default@gmail.com"));
        //System.out.println(ss1.orElseThrow(()->new IllegalArgumentException("email not present")));
        
        
        //System.out.println(ss1.map(String::toUpperCase).orElseGet(()->"email not presnt"));//NN@GMAIL,COM
        
        getCustomerByEmail("pgr");
        
        
        /*
         * public static <T> Optional<T> ofNullable(T value) {
         *  return value == null ? empty() : of(value); }
         */
    }
}
output:-
========
Optional.empty
john
Exception in thread "main" java.lang.Exception: no customer this mail
    at com.app.OptionalDemo.lambda$1(OptionalDemo.java:14)
    at java.util.Optional.orElseThrow(Optional.java:290)
    at com.app.OptionalDemo.getCustomerByEmail(OptionalDemo.java:14)
    at com.app.OptionalDemo.main(OptionalDemo.java:49)

=================================
package com.app;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MapVsFlatMapExample {
    public static void main(String[] args) {
        List<Customer> customers=DatabaseCustomer.getAllCustomers();
        
        //      List<Customer> convert List<String> >>>Data transformation
        
        //mapping >>> customer to customer.getEmail()
        //one to one
        List<Optional<String>> emails = customers.stream().map(c->c.getEmail()).collect(Collectors.toList());
        System.out.println(emails);
        System.out.println();
        //customer ->  customer.getPhoneNumbes()->>>one to Many
        List<List<String>> phones = customers.stream().map(c->c.getPhoneNumbes()).collect(Collectors.toList());
        System.out.println(phones);
        System.out.println();
        List<String> phone = customers.stream().flatMap(cu->cu.getPhoneNumbes().stream()).collect(Collectors.toList());
        System.out.println(phone);
    }
}
output:-
==========
[Optional[john@gmail.com], Optional[smith@gmail.com], Optional[peter@gmail.com], Optional[kely@gmail.com]]

[[9010458895, 8780866158], [810458895, 6780866158], [5010458895, 2780866158], [8010458895, 7780866158]]

[9010458895, 8780866158, 810458895, 6780866158, 5010458895, 2780866158, 8010458895, 7780866158]
=============================

package com.app;

import java.util.List;

public class Employee {

    
    private Integer empId;
    private String empName;
    private Integer age;
    private List<String> langs;
    public Employee() {
        super();
    }
    public Employee(Integer empId, String empName, Integer age, List<String> langs) {
        super();
        this.empId = empId;
        this.empName = empName;
        this.age = age;
        this.langs = langs;
    }
    
    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
    public String getEmpName() {
        return empName;
    }
    public void setEmpName(String empName) {
        this.empName = empName;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public List<String> getLangs() {
        return langs;
    }
    public void setLangs(List<String> langs) {
        this.langs = langs;
    }
    @Override
    public String toString() {
        return "Employee [empId=" + empId + ", empName=" + empName + ", age=" + age + ", langs=" + langs + "]";
    }
}
package com.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Test1 {

    public static void main(String[] args) {
        /*
         * List<Integer> numbers=Arrays.asList(1,2,3,4,1,3,6,8,10,24,20);
         * 
         * 
         * long aa=numbers.stream().filter(n->n%2==0).count(); System.out.println(aa);
         */
        List<Employee> list=new ArrayList<Employee>();
        list.add(new Employee(11, "srinivas", 23, Arrays.asList("telugu","hindi")));
        list.add(new Employee(12, "nihas", 24, Arrays.asList("gujarath","marati")));
        list.add(new Employee(13, "ram", 26, Arrays.asList("malayam","english")));
        //list.add(new Employee(14, "krishna", 27, Arrays.asList("koria","urdu")));
        //list.add(new Employee(14, "krishna", 27, Arrays.asList("koria","urdu")));
        System.out.println(list);
        List<Employee> list1=new ArrayList<Employee>();
        list.add(new Employee(21, "a", 24, Arrays.asList("ss","bb")));
        list.add(new Employee(32, "b", 25, Arrays.asList("dd","ll")));
        list.add(new Employee(33, "d", 26, Arrays.asList("ff","oo")));
        list.add(new Employee(35, "s", 28, Arrays.asList("vv","ii")));
        list.add(new Employee(45, "c", 29, Arrays.asList("bb","jj")));
        System.out.println(list1);
        /*
         * System.out.println("*********************"); List<String> list1=new
         * ArrayList<String>(); list1.add("telugu"); list1.add("hindi");
         * 
         * Employee ee=new Employee(11, "srinivas", 23, list1);
         * 
         * 
         * List<String> list2=new ArrayList<String>(); list2.add("gujarath");
         * list2.add("marati");
         * 
         * Employee ee1=new Employee(12, "ram", 24, list2);
         * 
         * List<String> list3=new ArrayList<String>(); list3.add("malayam");
         * list3.add("english");
         * 
         * Employee ee2=new Employee(13, "krishna", 26, list3);
         * 
         * 
         * List<String> list4=new ArrayList<String>(); list4.add("koria");
         * list4.add("urdu");
         * 
         * Employee ee3=new Employee(14, "srinivas", 27, list4);
         * 
         * List<String> list5=new ArrayList<String>(); list5.add("koria");
         * list5.add("urdu"); Employee ee4=new Employee(14, "srinivas", 27, list5);
         * 
         * 
         * 
         * List<Employee> empList=new ArrayList<Employee>(); empList.add(ee);
         * empList.add(ee1); empList.add(ee2); empList.add(ee3); empList.add(ee4);
         * System.out.println(ee); System.out.println(ee1); System.out.println(ee2);
         * System.out.println(ee3); System.out.println(ee4);
         */
        System.out.println("************************");
        //using forEachOrdered
        list.stream().map(emp->emp.getEmpName()).forEachOrdered(System.out::println);
        System.out.println();
        //      srinivas
        //      nihas
        //      ram
        //      krishna
        //      krishna
        //using forEach
        list.stream().map(emp->emp.getEmpName()).forEach(System.out::println);
        System.out.println();
        System.out.println("(((((((((((((((((((");
        //using collect
        List<String> ss=list.stream().map(emp->emp.getEmpName()).collect(Collectors.toList());
        ss.forEach(System.out::println);
        System.out.println("(((((((((((((((((((");
        System.out.println("*******");
        System.out.println(ss);
        System.out.println("*******");
        //startWith using filter()
        list.stream().filter(emp->emp.getEmpName().startsWith("k")).forEach(System.out::println);
        System.out.println();
        //startWith using map()
        list.stream().map(emp11->emp11.getEmpName().startsWith("k")).forEach(System.out::println);

        //      false
        //      false
        //      false
        //      true
        //      true
        //List<Integer> numbers=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        //List<Integer> numbers1=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
        //List<List<Integer>> multipleLists=Arrays.asList(numbers,numbers1);
        //flatMap
        //multipleLists.stream().flatMap(list1->list1.stream()).map(nn->nn+2).forEach(System.out::println);
        //Set<String> sss=employeeList.stream().flatMap(emp->emp.getCitiesWorkedIn().stream()).collect(Collectors.toSet());
        List<String> sss=list.stream().flatMap(empe->empe.getLangs().stream()).collect(Collectors.toList());
        System.out.println(sss);
        System.out.println("********************");
        List<List<Employee>> aaa=Arrays.asList(list,list1);
        aaa.stream().flatMap(employee->employee.stream()).map(r->r.getEmpName()).forEach(System.out::println);
        System.out.println("*****************");
        System.out.println("********************");
        List<List<Employee>> aaa1=Arrays.asList(list,list1);
        aaa1.stream().flatMap(employee->employee.stream()).map(r->r.getLangs()).forEach(System.out::println);
        System.out.println("*****************");
        /*
         * List<Integer> numbers=Arrays.asList(1,2,3,4,5,6,7,8,9,10,null);
         * Stream<Integer> stream=numbers.stream();
         */
        //String[] str=new String[10];
        //String lowecase=str[5].toLowerCase();
        //System.out.println(lowecase);//Exception in thread "main" java.lang.NullPointerException
        /*
         * Optional<String> checkNull=Optional.ofNullable(str[5]);
         * if(checkNull.isPresent())//checking value present or not { String
         * lowercase=str[5].toLowerCase(); }else {
         * System.out.println("The value not present"); }
         */
        /*
         * Optional<Integer> ss=Optional.ofNullable(33); if(ss.isPresent()) {
         * System.out.println("sss"); }else { System.out.println("dd"); }
         */
        /*
         * Stream<Integer> filter=stream.filter(i->i%2==0).map(i->i+10);
         * filter.forEach(System.out::println);
         */
        /*
         * int[] arryNumbes={4,2,3,1,3,41,334,13}; Stream<int[]>
         * of=Stream.of(arryNumbes);
         */
        System.out.println("+++++++++++++++++");
        List<Integer> num1=Arrays.asList(2,4,6,8,9,1,2,4);
        System.out.println("limit using");
        num1.stream().limit(5).forEach(System.out::println);
        System.out.println("+++++++++++++++++");
        System.out.println("********SKIP Using**********");
        num1.stream().skip(3).forEach(System.out::println);
        System.out.println("******************");
        System.out.println("&&&&&&&&&  dropWhile &&&&&&&&&&&&");

        //java 9 feature
        //num1.stream().dropWhile(n->n%3==0).forEach(System.out::println);
        //using filter
        //num1.stream().filter(n->n%3==0).forEach(System.out::println); working fine

        System.out.println("&&&&&&&&&&&&&&&&&&&&&");

        num1.stream().distinct().forEach(System.out::println);
        System.out.println("+++++++++++++++++");

        System.out.println("count of distinct: "+num1.stream().distinct().count()); 

        System.out.println("total count: "+num1.stream().count());  
        System.out.println("********Sorting ascending");
        num1.stream().sorted().forEach(System.out::println);
        System.out.println("********Sorting descending");
        num1.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);
        System.out.println("sorting with empName============");
        list.stream().sorted((e1,e2)->e1.getEmpName().compareTo(e2.getEmpName())).forEach(System.out::println); 
        System.out.println("sorting with empId ==================");    
        list.stream().sorted((ee,ee1)->ee.getEmpId().compareTo(ee1.getEmpId())).forEach(System.out::println);
        //ctrl+shift+l will get return
        List<String> collect=list.stream().map(ee->ee.getEmpName()).collect(Collectors.toList());
        System.out.println(collect);
        System.out.println("*************Using Map interface***********");
        Map<Integer, List<String>> map8 = list.stream().collect(Collectors.toMap(Employee::getEmpId, Employee::getLangs));
        System.out.println(map8);
        System.out.println("*************Using Map interface***********");
        Map<Integer, List<String>> map80 = list.stream().collect(Collectors.toMap(Employee::getEmpId, Employee::getLangs));
        System.out.println(map80);  
    } 
}
output:-
=========
[Employee [empId=11, empName=srinivas, age=23, langs=[telugu, hindi]], Employee [empId=12, empName=nihas, age=24, langs=[gujarath, marati]], Employee [empId=13, empName=ram, age=26, langs=[malayam, english]]]
[]
************************
srinivas
nihas
ram
a
b
d
s
c

srinivas
nihas
ram
a
b
d
s
c

(((((((((((((((((((
srinivas
nihas
ram
a
b
d
s
c
(((((((((((((((((((
*******
[srinivas, nihas, ram, a, b, d, s, c]
*******

false
false
false
false
false
false
false
false
[telugu, hindi, gujarath, marati, malayam, english, ss, bb, dd, ll, ff, oo, vv, ii, bb, jj]
********************
srinivas
nihas
ram
a
b
d
s
c
*****************
********************
[telugu, hindi]
[gujarath, marati]
[malayam, english]
[ss, bb]
[dd, ll]
[ff, oo]
[vv, ii]
[bb, jj]
*****************
+++++++++++++++++
limit using
2
4
6
8
9
+++++++++++++++++
********SKIP Using**********
8
9
1
2
4
******************
&&&&&&&&&  dropWhile &&&&&&&&&&&&
&&&&&&&&&&&&&&&&&&&&&
2
4
6
8
9
1
+++++++++++++++++
count of distinct: 6
total count: 8
********Sorting ascending
1
2
2
4
4
6
8
9
********Sorting descending
9
8
6
4
4
2
2
1
sorting with empName============
Employee [empId=21, empName=a, age=24, langs=[ss, bb]]
Employee [empId=32, empName=b, age=25, langs=[dd, ll]]
Employee [empId=45, empName=c, age=29, langs=[bb, jj]]
Employee [empId=33, empName=d, age=26, langs=[ff, oo]]
Employee [empId=12, empName=nihas, age=24, langs=[gujarath, marati]]
Employee [empId=13, empName=ram, age=26, langs=[malayam, english]]
Employee [empId=35, empName=s, age=28, langs=[vv, ii]]
Employee [empId=11, empName=srinivas, age=23, langs=[telugu, hindi]]
sorting with empId ==================
Employee [empId=11, empName=srinivas, age=23, langs=[telugu, hindi]]
Employee [empId=12, empName=nihas, age=24, langs=[gujarath, marati]]
Employee [empId=13, empName=ram, age=26, langs=[malayam, english]]
Employee [empId=21, empName=a, age=24, langs=[ss, bb]]
Employee [empId=32, empName=b, age=25, langs=[dd, ll]]
Employee [empId=33, empName=d, age=26, langs=[ff, oo]]
Employee [empId=35, empName=s, age=28, langs=[vv, ii]]
Employee [empId=45, empName=c, age=29, langs=[bb, jj]]
[srinivas, nihas, ram, a, b, d, s, c]
*************Using Map interface***********
{32=[dd, ll], 33=[ff, oo], 35=[vv, ii], 21=[ss, bb], 11=[telugu, hindi], 12=[gujarath, marati], 45=[bb, jj], 13=[malayam, english]}
*************Using Map interface***********
{32=[dd, ll], 33=[ff, oo], 35=[vv, ii], 21=[ss, bb], 11=[telugu, hindi], 12=[gujarath, marati], 45=[bb, jj], 13=[malayam, english]}
==============================
package com.app;

public class Employee800 {
    private int id;
    private String name;
    private String dept;
    private long salary;
    public Employee800() {
        super();
    }
    public Employee800(int id, String name, String dept, long salary) {
        super();
        this.id = id;
        this.name = name;
        this.dept = dept;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDept() {
        return dept;
    }
    public void setDept(String dept) {
        this.dept = dept;
    }
    public long getSalary() {
        return salary;
    }
    public void setSalary(long salary) {
        this.salary = salary;
    }
    @Override
    public String toString() {
        return "Employee800 [id=" + id + ", name=" + name + ", dept=" + dept + ", salary=" + salary + "]";
    }
}
package com.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class SortedMapDemo {
    public static void main(String[] args) {

        //      Map<String,Integer> map=new HashMap<String,Integer> ();
        //      map.put("eight", 8);
        //      map.put("nine", 9);
        //      map.put("ten", 10);
        //      map.put("two", 2);
        //      map.put("three", 3);
        //      map.put("four", 4);
        //      System.out.println(map);
        //      Map<Employee800,Integer> employeemap=new TreeMap<>(new Comparator<Employee800>() {
        //
        //          @Override
        //          public int compare(Employee800 o1, Employee800 o2) {
        //              return (int) ((int)o1.getSalary()-o2.getSalary());
        //          }
        //      });
        Map<Employee800,Integer> employeemap=new TreeMap<>((o1,o2)->(int)
                (o2.getSalary()-o1.getSalary()));
        employeemap.put(new Employee800(176, "Roshan","IT", 600000), 60);
        employeemap.put(new Employee800(388, "Bikash","CIVIL", 900000), 90);
        employeemap.put(new Employee800(470, "Bimal","DEFENCE", 500000), 50);
        employeemap.put(new Employee800(624, "Sourav","CORE", 400000), 40);
        employeemap.put(new Employee800(284, "Prakash","SOCIAL", 1200000), 120);
        System.out.println(employeemap);
        employeemap.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.comparing(Employee800::getSalary))).forEach(System.out::println);
        System.out.println();
        employeemap.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.comparing(Employee800::getSalary).reversed())).forEach(System.out::println);

        System.out.println();
        //sorting by dept
        employeemap.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.comparing(Employee800::getDept))).forEach(System.out::println);  
    }
}
//traditional map
//List<Entry<String,Integer>> entries=new ArrayList<>(map.entrySet());
//Collections.sort(entries,new Comparator<Entry<String, Integer>>() {
//
//  @Override
//  public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
//
//      return o1.getKey().compareTo(o2.getKey());
//  }
//});

//Collections.sort(entries,(o1,o2)->o1.getKey().compareTo(o2.getKey()));

//for(Entry<String, Integer> entry:entries)
//{
//  System.out.println(entry.getKey()+" "+entry.getValue());
//}
//
//System.out.println("*************");
//map.entrySet().stream().sorted(Map.Entry.comparingByValue()).forEach(System.out::println);
output:-
==========
{Employee800 [id=284, name=Prakash, dept=SOCIAL, salary=1200000]=120, Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]=90, Employee800 [id=176, name=Roshan, dept=IT, salary=600000]=60, Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]=50, Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]=40}
Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]=40
Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]=50
Employee800 [id=176, name=Roshan, dept=IT, salary=600000]=60
Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]=90
Employee800 [id=284, name=Prakash, dept=SOCIAL, salary=1200000]=120

Employee800 [id=284, name=Prakash, dept=SOCIAL, salary=1200000]=120
Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]=90
Employee800 [id=176, name=Roshan, dept=IT, salary=600000]=60
Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]=50
Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]=40

Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]=90
Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]=40
Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]=50
Employee800 [id=176, name=Roshan, dept=IT, salary=600000]=60
Employee800 [id=284, name=Prakash, dept=SOCIAL, salary=1200000]=120

=================================

package com.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortListDemo {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<Integer>();
        list.add(8);
        list.add(18);
        list.add(33);
        list.add(67);
        list.add(92);
        list.add(108);
        List<Employee800> employees=Database.getEmployee();  
        Collections.sort(employees,(o1,o2)->(int)(o1.getSalary()-o2.getSalary()));
        System.out.println(employees);
        System.out.println();
        employees.stream().sorted(Comparator.comparing(Employee800::getSalary)).forEach(System.out::println);//salary
        System.out.println();
        employees.stream().sorted(Comparator.comparing(Employee800::getName)).forEach(System.out::println);//name
        System.out.println();
        employees.stream().sorted(Comparator.comparing(Employee800::getId)).forEach(System.out::println);//id
    }
}
//System.out.println("****************");
//Collections.sort(list);
//System.out.println(list);//asc
//Collections.reverse(list);
//System.out.println(list);//desc
//
//list.stream().sorted().forEach(System.out::println);//ascending
//list.stream().sorted(Comparator.reverseOrder()).forEach(i->System.out.print(i+" "));//descending
//System.out.println("*********************");
/*
 * Collections.sort(employees,new Comparator<Employee800>() {
 * 
 * @Override public int compare(Employee800 o1, Employee800 o2) { // 
 * Auto-generated method stub return 0; }
 * 
 * System.out.println(employees); }
 */
//class MyComparator implements Comparator<Employee800>
//{
//  @Override
//  public int compare(Employee800 o1, Employee800 o2) {
//      return (int) (o2.getSalary()-o1.getSalary());
//  }
//System.out.println(employees);
//employees.stream().sorted((o1,o2)->(int)(o2.getSalary()-o1.getSalary())).forEach(System.out::println);
//System.out.println();
//List<Employee800> collect = employees.stream().sorted((o1,o2)->(int)(o2.getSalary()-o1.getSalary())).collect(Collectors.toList());
//System.out.println(collect);
//Comparator.comparing using

output:-
=========
[Employee800 [id=624, name=Sourav, dept=CORE, salary=400000], Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000], Employee800 [id=176, name=Roshan, dept=IT, salary=600000], Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000], Employee800 [id=176, name=Prakash, dept=SOCIAL, salary=1200000]]

Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]
Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]
Employee800 [id=176, name=Roshan, dept=IT, salary=600000]
Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]
Employee800 [id=176, name=Prakash, dept=SOCIAL, salary=1200000]

Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]
Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]
Employee800 [id=176, name=Prakash, dept=SOCIAL, salary=1200000]
Employee800 [id=176, name=Roshan, dept=IT, salary=600000]
Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]

Employee800 [id=176, name=Roshan, dept=IT, salary=600000]
Employee800 [id=176, name=Prakash, dept=SOCIAL, salary=1200000]
Employee800 [id=388, name=Bikash, dept=CIVIL, salary=900000]
Employee800 [id=470, name=Bimal, dept=DEFENCE, salary=500000]
Employee800 [id=624, name=Sourav, dept=CORE, salary=400000]

=============================================================

package com.app;

public class EmployeeGrade {
    private int id;
    private String name;
    private String grade;
    private double salary;
    public EmployeeGrade() {
        super();
    }
    public EmployeeGrade(int id, String name, String grade, double salary) {
        super();
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getGrade() {
        return grade;
    }
    public void setGrade(String grade) {
        this.grade = grade;
    }
    public double getSalary() {
        return salary;
    }
    public void setSalary(double salary) {
        this.salary = salary;
    }
    @Override
    public String toString() {
        return "EmployeeGrade [id=" + id + ", name=" + name + ", grade=" + grade + ", salary=" + salary + "]";
    }
}

package com.app;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeGradeDatabase {
    public static List<EmployeeGrade> gradeEmployees()
    {
        return Stream.of(new EmployeeGrade(101, "Jhon", "A", 60000),
                new EmployeeGrade(102, "peter", "B", 40000),
                new EmployeeGrade(103, "mark", "A", 30000),
                new EmployeeGrade(104, "kin", "A", 70000),
                new EmployeeGrade(105, "json", "C", 90000)
                ).collect(Collectors.toList()); 
        }
    }

package com.app;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MapReduceFilterExample {
    public static void main(String[] args) {
        List<Integer> numbes=Arrays.asList(3,7,8,1,5,9);
        int sum=0;
        for (Integer no : numbes) {
            sum=sum+no;
        }
        System.out.println(sum);
        System.out.println("using reduce()");
        int sum1=numbes.stream().mapToInt(i->i).sum();
        System.out.println(sum1);
        System.out.println();
        Integer reduce = numbes.stream().reduce(0,(n1,n2)->n1+n2);
        System.out.println(reduce);
        System.out.println();
        Optional<Integer> reduce2 = numbes.stream().reduce(Integer::sum);
        System.out.println(reduce2.get());
        System.out.println();
        Integer reduce3 = numbes.stream().reduce(1,(n1,n2)->n1*n2);
        System.out.println(reduce3);
        System.out.println();
        Integer max = numbes.stream().reduce(0,(n1,n2)->n1>n2?n1:n2);
        System.out.println(max);
        System.out.println();
        Integer min = numbes.stream().reduce(0,(n1,n2)->n1<n2?n1:n2);
        System.out.println(min);
        System.out.println("usiing Integer::max");
        Optional<Integer> reduce4 = numbes.stream().reduce(Integer::max);
        System.out.println(reduce4.get());
        List<String> words=Arrays.asList("corejava","spring","hibernatesss","webservices");
        String longestString = words.stream().reduce((word1,word2)->word1.length()>word2.length()?word1:word2).get();
        System.out.println(longestString);
        //get Grade A
        //Data transformation
        double avgSalary = EmployeeGradeDatabase.gradeEmployees().stream().filter(em->em.getGrade().equalsIgnoreCase("A"))
                .map(e->e.getSalary())
                .mapToDouble(i->i)
                .average().getAsDouble();
        System.out.println(avgSalary);

        double sum11 = EmployeeGradeDatabase.gradeEmployees().stream().filter(em->em.getGrade().equalsIgnoreCase("A"))
                .map(e->e.getSalary())
                .mapToDouble(i->i)
                .sum();
        System.out.println(sum11);
    }
}
output:-
========
33
using reduce()
33

33

33

7560

9

0
usiing Integer::max
9
hibernatesss
53333.333333333336
160000.0
===============
package com.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class ForEachDemo {

    //filter conditional check


    public static void main(String[] args) {
        List<String> list=new ArrayList<String>();
        list.add("abc");
        list.add("john");
        list.add("srinivas");
        list.add("nihas");
        list.add("vihas");
        list.add("sam");
        for (String a : list) {

            System.out.println(a);
        }

        System.out.println();

        for (String a : list) {
            if(a.startsWith("n"))
            {
                System.out.println(a);
            }

        }
        System.out.println();
        System.out.println("Using filter: ");
        list.stream().filter(i->i.startsWith("s")).forEach(System.out::println);
        System.out.println();
        list.stream().forEach(System.out::println);
        System.out.println();
        System.out.println("Using Map example using for each");
        Map<Integer,String> map=new HashMap<Integer,String>();
        map.put(1, "a");
        map.put(2, "t");
        map.put(3, "y");
        map.put(4, "g");
        map.put(5, "t");
        map.put(1, "g");
        map.put(6, "j");
        System.out.println(map);
        System.out.println("Using for each");
        // map.forEach((key,value)->System.out.println(key+" "+value));
        map.entrySet().stream().forEach(ob->System.out.println(ob));
        System.out.println("filer using map");
        map.entrySet().stream().filter(u->u.getKey()%2==0).forEach(s->System.out.println(s));
        System.out.println("Consumer");
        Consumer<String> consumer=(t)->System.out.println(t);
        consumer.accept("good");
        for (String ss : list) {
            consumer.accept(ss);
        } 
    }
}
output:-
===========
abc
john
srinivas
nihas
vihas
sam

nihas

Using filter: 
srinivas
sam

abc
john
srinivas
nihas
vihas
sam

Using Map example using for each
{1=g, 2=t, 3=y, 4=g, 5=t, 6=j}
Using for each
1=g
2=t
3=y
4=g
5=t
6=j
filer using map
2=t
4=g
6=j
Consumer
good
abc
john
srinivas
nihas
vihas
sam

========================
package com.app;

public interface MathOperation {
    public abstract int addition(int a,int b);
}

package com.app;

public class MathOperationImpl implements MathOperation{

    @Override
    public int addition(int a, int b) {
        
        return a+b;
    }
}
package com.app;

public class Test {

    public static void main(String[] args) {
        /*
         * MathOperation m=new MathOperationImpl(); System.out.println(m.addition(6,
         * 8));
         */
        MathOperation m=(a,b)->a+b;
        
        System.out.println(m.addition(11, 11));
    }
    }
    output:-
    22

=======================================


package com.app;

import java.util.stream.IntStream;

public class ParallelStreamExample {
    public static void main(String[] args) {
        long startTime=0;
        long endTime=0;
        startTime=System.currentTimeMillis();   
        IntStream.range(1, 10).forEach(System.out::println);
        endTime=System.currentTimeMillis();
        System.out.println("Plain stream took time:"+(endTime-startTime));
        startTime=System.currentTimeMillis();
        IntStream.range(1, 10).parallel().forEach(System.out::println);
        endTime=System.currentTimeMillis();
        System.out.println("parallel stream took time:"+(endTime-startTime));
        IntStream.range(1, 10).forEach(e->{System.out.println("Thread: "+Thread.currentThread().getName()+" "+e);
        });
        IntStream.range(1, 10).parallel().forEach(e->{System.out.println("Thread: "+Thread.currentThread().getName()+" "+e);
        });
        }
}
output:-
===========
1
2
3
4
5
6
7
8
9
Plain stream took time:31
6
5
7
9
8
3
4
2
1
parallel stream took time:3
Thread: main 1
Thread: main 2
Thread: main 3
Thread: main 4
Thread: main 5
Thread: main 6
Thread: main 7
Thread: main 8
Thread: main 9
Thread: main 6
Thread: ForkJoinPool.commonPool-worker-6 8
Thread: ForkJoinPool.commonPool-worker-1 1
Thread: ForkJoinPool.commonPool-worker-2 7
Thread: ForkJoinPool.commonPool-worker-3 5
Thread: main 2
Thread: ForkJoinPool.commonPool-worker-4 3
Thread: ForkJoinPool.commonPool-worker-6 9
Thread: ForkJoinPool.commonPool-worker-5 4
==============================
package com.app;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

//public class PredicateDemo implements Predicate<Integer>{
//
//  @Override
//  public boolean test(Integer t) {
//
//      if(t%2==0)
//      {
//          return true;
//      }else {
//          return false;
//      }
//  }
//
public class PredicateDemo {
    public static void main(String[] args) {
        //Predicate<Integer> predicate = (t) -> t%2==0;
        /*
         * { if(t%2==0) { return true; }else { return false; } };
         */
        //System.out.println(predicate.test(9));
        List<Integer> list11=Arrays.asList(2,6,8,4,10,14,12,5,7,17);
        list11.stream().filter(t->t%2==0).forEach(t->System.out.println("Printing data: "+t));
    }
}

output:-
==========
Printing data: 2
Printing data: 6
Printing data: 8
Printing data: 4
Printing data: 10
Printing data: 14
Printing data: 12

========================

Final operarations:-
====================
package com.app.finaloperations;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Test444 {
    public static void main(String[] args) {
        List<Integer> numbers=Arrays.asList(3,7,9,7,1,2,3,4,5,6,8,9);
        Optional<Integer> min=numbers.stream().min((n1,n2)->n1.compareTo(n2));
        System.out.println(min.get());
        System.out.println(min.isPresent());
        System.out.println(min.orElse(null));
        Optional<Integer> oo=min.empty();
        System.out.println(oo.isPresent());//false
        System.out.println();
        Optional<Integer> max=numbers.stream().max((n1,n2)->n1.compareTo(n2));
        System.out.println(max.get());
        System.out.println(max.isPresent());
        System.out.println(max.orElse(null));
        Optional<Integer> oo1=max.empty();
        System.out.println(oo1.isPresent());//false
        System.out.println();
        long sss=numbers.stream().count();
        System.out.println(sss);
        System.out.println("***allMatch*****");
        boolean b=numbers.stream().allMatch(n->n%1==0);
        System.out.println(b);
        System.out.println("********");
        System.out.println("*****allMatch***");
        boolean b1=numbers.stream().allMatch(n->n%3==0);
        System.out.println(b1);
        System.out.println("********");
        System.out.println("*****anyMatch***");
        boolean b2=numbers.stream().anyMatch(n->n%3==0);
        System.out.println(b2);
        System.out.println("*****anyMatch***");
        System.out.println("*****noneMatch***");
        //here 13 number not there
        boolean b3=numbers.stream().noneMatch(n->n%13==0);
        System.out.println(b3);
        System.out.println("*****noneMatch***");
        System.out.println("*****collect* convert to List**");
        List<Integer> collect=numbers.stream().filter(f->f%2==0).collect(Collectors.toList());
        System.out.println(collect);
        collect.forEach(System.out::println);
        System.out.println("*****collect* convert to Set**");
        Set<Integer> collect1=numbers.stream().filter(f->f%2==0).collect(Collectors.toSet());
        System.out.println(collect1);
        collect.forEach(System.out::println);   
    }
}
output:-
========
1
true
1
false

9
true
9
false

12
***allMatch*****
true
********
*****allMatch***
false
********
*****anyMatch***
true
*****anyMatch***
*****noneMatch***
true
*****noneMatch***
*****collect* convert to List**
[2, 4, 6, 8]
2
4
6
8
*****collect* convert to Set**
[2, 4, 6, 8]
2
4
6
8
==============================
package com.app.minmax;

import java.util.Arrays;
import java.util.List;

public class MinMaxSumAverageCount {
    public static void main(String[] args) {
        List<Integer> numbers=Arrays.asList(45,89,3,23,97,53,67);
        int count,min,max=0;
        int sum=0;
        min=numbers.get(0);
        max=numbers.get(0);
        double average=0.0;
        count=numbers.size();
        for(Integer num:numbers)
        {
            if(min>num)
            {
                min=num;
            }
            if(max<num)
            {
                max=num;
            }
            sum=sum+num;
        }
        average=sum/count;
        System.out.println("count: "+count);
        System.out.println("min: "+min);
        System.out.println("max: "+max);
        System.out.println("sum: "+sum);
        System.out.println("average: "+average);
    }
}
output:-
============
count: 7
min: 3
max: 97
sum: 377
average: 53.0
==================
package com.app.minmax;

import java.util.Arrays;
import java.util.List;

public class MinMaxSumAverageCountJava8 {

    public static void main(String[] args) {
        List<Integer> numbers=Arrays.asList(45,89,3,23,97,53,67);

        int min,max;
        int sum=0;
        long count;
        count=numbers.stream().count();
        min=numbers.stream().sorted((n1,n2)->n1.compareTo(n2)).findFirst().get();
        max=numbers.stream().sorted((n1,n2)->n2.compareTo(n1)).findFirst().get();
        System.out.println(min);
        System.out.println(max);
        //sum=0
        //one one element n1
        //total n2
        //shirt cut alt+shift+l
        Integer sum1 = numbers.stream().reduce(0,(n1,n2)->n1+n2);
        System.out.println("sum: "+sum1);
        double average=sum1/count;
        System.out.println("average"+average);
    }
}
output:-
===========
3
97
sum: 377
average53.0
==========
package com.app.minmax;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class MinMaxSumAverageCountJava8Simple {

    public static void main(String[] args) {
        List<Integer> numbers=Arrays.asList(45,89,3,23,97,53,67);
        IntSummaryStatistics stat = numbers.stream().collect(Collectors.summarizingInt(i->i));
        System.out.println(stat);
        System.out.println("count: "+stat.getCount());
        System.out.println("count: "+stat.getMax());
        System.out.println("count: "+stat.getMin());
        System.out.println("count: "+stat.getSum());
        System.out.println("count: "+stat.getAverage());
        System.out.println("count: "+stat.getClass());
    }
}

output:
===========
IntSummaryStatistics{count=7, sum=377, min=3, average=53.857143, max=97}
count: 7
count: 97
count: 3
count: 377
count: 53.857142857142854
count: class java.util.IntSummaryStatistics

===========================================================================

Hash Map internally work:-
==============================

package com.app.hashmapex;

import java.util.HashMap;
import java.util.Map;

public class HashMapInternalWork {

    public static void main(String[] args) {
        
        Map<String,Integer> marks=new HashMap<String,Integer>();
        marks.put("Naveen", 100);
        marks.put("Tom", 200);
        marks.put("Lisa", 300);
        marks.put("Peter", 400);
        marks.put("Robby", 600);
        System.out.println(marks);
        
        System.out.println(marks.get("Naveen"));
        
        //explaination
        /*
         * 1)Every Bucket behaves like a Node over here
         * 2)Maintain 1)key and value 2)int hash 3)key 4)value 5)Node<key,value> next;
         * 3)At The first by default 16 nodes created first time
         * 
         * public V put(K key,V value)
         * {
         * int hash=hashCode(key);
         * int index=hash &(n-1);
         * }
         * n=16(map size)
         * 
         * hashCode will be calculated by key and not any value
         * 
         * Example :-  Naveen hashcode >> 210678
         *             int index=hash&(n-1)>>support will get 4 index
         *             
         *             **** stored like that>>> 1)Naveen 2)210678 3)100 4)(next)->null 
         *             
         *             Tom hashcode >> 210780
         *             int index=hash&(n-1)>>support will get 9 index
         *             **** stored like that>>> 1)Tom 2)210780 3)200 4)(next)->null 
         * 
         *      
         *             ***** it is possible multiple object with the same hashcode and different hashcode
         *       
         *             Lisa hashcode >> 210678
         *             int index=hash&(n-1)>>support will get 4 index
         *             
         *             **** stored like that>>> 1)Lisa 2)210678 3)100 4)(next)->null 
         *             (make one more node created like linkedList)
         *             
         *             Peter hashcode >> 210990
         *             int index=hash&(n-1)>>support will get 6 index
         *             
         *             **** stored like that>>> 1)Peter 2)210990 3)100 4)(next)->null 
         *             (make one more node created like linkedList)
         *       
         *             Robby hashcode >> 211777
         *             int index=hash&(n-1)>>support will get 4 index
         *             
         *             **** stored like that>>> 1)Robby 2)211777 3)100 4)(next)->null 
         *             (make one more node created like linkedList)
         *      
         *      
         *      collision :- same index for naveen and lisa and robby
         *      same index and same and diffrent hashcode because one or more node creation based on previous to next node
         *      
         *      
         *      System.out.println(marks.get("Naveen"));100
         *      get() will do check equals
         *      matching hashcode 210678 to next Node
         *      
         *      Lisa already hashcode will get 4 th index check .equals nextNode (Compare)
         *      
         *      **** null key is also allowed
         *      marks.put(null,test)
         *      *****(Hashcode of null values =0) and index will be =0
         *      ***(always stored in index =0 location
         *      
         *      java 1.8 on word it is O(n) to O(log n)
         *      LinkedList >>> O(n)
         *      Balance Tree>> O(log n)
         * 
         */
    }
    
}
====================================================================================================================================
 HashSet internally work:-
 ==========================

class Test 
{     
    public static void main(String args[])  
    { 
        // creating a HashSet 
        HashSet hs = new HashSet(); 
          
        // adding elements to hashset 
        // using add() method 
        boolean b1 = hs.add("Geeks"); 
        boolean b2 = hs.add("GeeksforGeeks"); 
          
        // adding duplicate element 
        boolean b3 = hs.add("Geeks"); 
          
        // printing b1, b2, b3 
        System.out.println("b1 = "+b1); 
        System.out.println("b2 = "+b2); 
        System.out.println("b3 = "+b3); 
          
        // printing all elements of hashset 
        System.out.println(hs); 
              
    } 
} 

output:-
===============
b1 = true
b2 = true
b3 = false
[GeeksforGeeks, Geeks]

========================

Explaination:-
=================
As we know that a set is a well-defined collection of distinct objects.

How HashSet works internally in Java?

1)set will never contain duplicate elements. 


Key= Geeks
Value=GeeksforGeeks

//Creating HashSet
HashSet hs=new HashSet();

public HashSet()
{
    map=new HashMap<>();
}

//adding elements to hashset
//using add() method
public boolean add("Greek"){
    return map.put("Greek",PRESENT)==null;
}

boolean b1=hs.add("Greek");

****** Here return null >>> return true
==================================================
//adding elements to hashset
//using add() method
public boolean add("GeeksforGeeks"){
    return map.put("GeeksforGeeks",PRESENT)==null;
}

boolean b2=hs.add("GeeksforGeeks",PRESENT)==null;


****** Here return null >>> return true
================================================

//adding duplicates 

public boolean add("Greek")
{
    return map.put("Greek",PRESENT)==null;
}

boolean b3=hs.add("Greek");

****** Here return Present >> so  false because already key exit (Duplicate key not allowed)
====================================================================================================================================


DSA Programs:-
===============
package com.app;

import java.util.Scanner;
public class Addition {
    public static int add(int a,int b)
    {
        return a+b;
    }
}
class Test1{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the first Number: ");
        int a=sc.nextInt();
        System.out.println("Enter the second Number: ");
        int b=sc.nextInt();
        System.out.println(Addition.add(a,b));
    }
}
output:-
==========
Enter the first Number: 
12
Enter the second Number: 
12
24
=============
package com.app;

import java.util.Scanner;

public class EvenOrOdd {
    public static String evenOrOdd(int n)
    {
        return (n%2==0)?"even number":"odd Number";
    }
}
class Test6{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("enter the number: ");
int n=sc.nextInt();
System.out.println(EvenOrOdd.evenOrOdd(n));
    }
}
output:-
========
enter the number: 
24
even number
==============
package com.app;

import java.util.Scanner;

public class Factorial {
    public static int fact1(int n)
    {
        int fact=1;
        for(int i=1;i<=n;i++) {
            fact=fact*i;
        }
        return fact;
    }
    public static int fact2(int n)
    {
        if(n==0)
        {
            return 1;
        }
        else {
            return n*fact2(n-1);
        }
    }
}
class Test9{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the factorial: ");
        int n=sc.nextInt();
        System.out.println(Factorial.fact1(n));
        System.out.println(Factorial.fact2(n));
    }
}
output:-
=========
Enter the factorial: 
6
720
720
=============
package com.app;

import java.util.ArrayList;
import java.util.Scanner;

public class FibonnacciExample {
    public static ArrayList<Integer> fibonnacci(int n) {
        ArrayList<Integer> al=new ArrayList<Integer>();
        int a=0,b=1,c;
        al.add(a);
        al.add(b);
        for(int i=1;i<=n-2;i++)
        {
        c=a+b;
        al.add(c);
        a=b;
        b=c;
        }
        return al;
    }
}
class Test12{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the Number");
int n=sc.nextInt();
System.out.println(FibonnacciExample.fibonnacci(n));
    }
}
output:-
=========
Enter the Number
5
[0, 1, 1, 2, 3]
============
package com.app;

import java.util.ArrayList;
import java.util.Scanner;

public class FibonnacciSeriesThreeValues {
    public static ArrayList<Integer> fibonnacci(int n)
    {
        ArrayList<Integer> al=new ArrayList<Integer>();
        int a=0,b=1,c=2,d;
        al.add(a);
        al.add(b);
        al.add(c);
        for(int i=1;i<=n-3;i++)
        {
            d=a+b+c;
            al.add(d);
            a=b;
            b=c;
            c=d;
        }
        return al;
    }   
}
class Test14{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number");
int n=sc.nextInt();
System.out.println(FibonnacciSeriesThreeValues.fibonnacci(n));
    }
}
output:-
=========
Enter the number
5
[0, 1, 2, 3, 6]
===============
package com.app;

import java.util.Scanner;

public class Max_Numberab {
    public static int max1(int a,int b)
    {
        return (a>b)?a:b;
    }
    public static int max2(int a,int b)
    {
        return Math.max(a, b);
    }
}
class Test2
{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the first Number");
        int a=sc.nextInt();
        System.out.println("Enter the second Number");
        int b=sc.nextInt();
        System.out.println(Max_Numberab.max1(a,b));
        System.out.println(Max_Numberab.max2(a,b));
    }
}
output:-
==========
Enter the first Number
23
Enter the second Number
45
45
45
=================
package com.app;

import java.util.Scanner;

public class MaxNumberabc {

    public static int max1(int a,int b,int c)
    {
        return (a>b&&a>c)?a:(b>c?b:c);
    }
    
    public static int max2(int a,int b,int c)
    {
        return Math.max(Math.max(a, b),c);
    }
    
}
class Test{
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the 1st Number: ");
        int a=sc.nextInt();
        System.out.println("Enter the 2nd Number: ");
        int b=sc.nextInt();
        System.out.println("Enter the 3rd Number: ");
        int c=sc.nextInt();
        
        System.out.println(MaxNumberabc.max1(a,b,c));
        System.out.println(MaxNumberabc.max2(a,b,c));
    }
}
output:=
=========
Enter the 1st Number: 
5
Enter the 2nd Number: 
1
Enter the 3rd Number: 
3
5
5
==========================
package com.app;

import java.util.Scanner;

public class NaturalNumbers {
    public static int naturalLoop(int n)
    {
        int sum=0;
        for(int i=0;i<=n;i++)
        {
            sum=sum+i;
        }
        return sum;
    }
    public static int naturalRecursion(int n)
    {
        if(n==0)
        {
            return 0;
        }
else {
 return n+naturalRecursion(n-1);
}
    }
    public static int naturalFormula(int n)
    {
        return n*(n+1)/2;
    }
}
class Test7{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number: ");
int n=sc.nextInt();
System.out.println(NaturalNumbers.naturalLoop(n));
System.out.println(NaturalNumbers.naturalRecursion(n));
System.out.println(NaturalNumbers.naturalFormula(n));
    }
}
output:-
==========
Enter the number: 
5
15
15
15
============
package com.app;

import java.util.Scanner;

public class PrimeNumberExample {
    public static boolean isPrimeNumber1(int n)
    {
        int fact=0;
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                fact++;
            }
        }
        return fact==2;
    }
    public static boolean isPrimeNumber2(int n,int i)
    {
        if(i==1)
        {
            return true;
        }else if(n%i==0) {
            return false;
        }else {
            return isPrimeNumber2(n, --i);
        }
    }
}
class Test10{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number");
int n=sc.nextInt();
System.out.println(PrimeNumberExample.isPrimeNumber1(n)?"Yes":"No"); //100 iteration
System.out.println((PrimeNumberExample.isPrimeNumber2(n,n/2))?"Yes":"No");//1 Iteration
    }
}
output:-
=========
Enter the number
3
Yes
Yes
==============
package com.app;

import java.util.Scanner;

public class Swapping {
    public static void swap1(int a,int b) {
        System.out.println("Before swapping '"+a+"' and '"+b+"' ");
        int temp;
        temp=a;
        a=b;
        b=temp;
        System.out.println("After swapping '"+a+"' and '"+b+"' ");
    }
    public static void swap2(int a,int b) {
        System.out.println("Before swapping '"+a+"' and '"+b+"' ");
        a=a+b;
        b=a-b;
        a=a-b;
        System.out.println("After swapping '"+a+"' and '"+b+"' ");
    }
    public static void swap3(int a,int b) {
        System.out.println("Before swapping '"+a+"' and '"+b+"' ");
        a=a*b;
        b=a/b;
        a=a/b;
        System.out.println("After swapping '"+a+"' and '"+b+"' ");
    }
    public static void swap4(int a,int b) {
        System.out.println("Before swapping '"+a+"' and '"+b+"' ");
        a=a^b;
        b=a^b;
        a=a^b;
        System.out.println("After swapping '"+a+"' and '"+b+"' ");
    }
    public static void swap5(int a,int b) {
        System.out.println("Before swapping '"+a+"' and '"+b+"' ");
        a=a+b-(b=a);
        System.out.println("After swapping '"+a+"' and '"+b+"' ");
    }
}
class Test3{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter first number: ");
        int a=sc.nextInt();
        System.out.println("Enter second number: ");
        int b=sc.nextInt();
        Swapping.swap1(a,b);
        Swapping.swap2(a,b);
        Swapping.swap3(a,b);
        Swapping.swap4(a,b);
        Swapping.swap5(a,b);
    }
}
output:-
=========
Enter first number: 
2
Enter second number: 
4
Before swapping '2' and '4' 
After swapping '4' and '2' 
Before swapping '2' and '4' 
After swapping '4' and '2' 
Before swapping '2' and '4' 
After swapping '4' and '2' 
Before swapping '2' and '4' 
After swapping '4' and '2' 
Before swapping '2' and '4' 
After swapping '4' and '2' 
=============================
package com.app.adjacentXPrint;

import java.util.Scanner;

public class AdjacentXPrint {
    public static String news(String s,int index)
    {
        System.out.println(index);
        if(index<1)
        {
            return ""+s.charAt(index);
        }else {
            return news(s,index-1)+"*"+s.charAt(index);
        }
    }
}
class Test909{
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        System.out.println("Enter any String: ");
        String s=sc.nextLine();
        System.out.println(AdjacentXPrint.news(s, s.length()-1));
    }
}
output:-
===========
Enter any String: 
srinivas
7
6
5
4
3
2
1
0
s*r*i*n*i*v*a*s
========================
package com.app.adjacentXPrint;

import java.util.Scanner;

public class BeweenXPrint {
    public static String news(String s,int index) {
        if(index<1)
        {
            return ""+s.charAt(index);
        }else if (s.charAt(index)==s.charAt(index-1)) {
            System.out.println(index);
            System.out.println(index-1);
            return news(s, index-1)+"*"+s.charAt(index);
        }else {
            return news(s, index-1)+s.charAt(index);
        }
    }
}
class Test6669{
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter the String: ");
    String s=sc.nextLine();
    System.out.println(BeweenXPrint.news(s,s.length()-1));
}
}
output:-
==========
Enter the String: 
hello
3
2
hel*lo
=================
package com.app.americankeyboard;

import java.util.Scanner;

public class AmericanKeyboard {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        String r1="qwertyuiop";
        String r2="asdfghjkl";
        String r3="zxcvbnm";
        int c1=0;
        int c2=0;
        int c3=0;
        for(int i=0;i<s.length();i++)
        {
            if(r1.contains(""+s.charAt(i)))
            {
                c1++;
            }
            if(r2.contains(""+s.charAt(i)))
            {
                c2++;
            }
            if(r3.contains(""+s.charAt(i)))
            {
                c3++;
            }
        }
        System.out.println(c1==s.length()||c2==s.length()||c3==s.length());
    }
}
output:-
=========
dad
true
=============

package com.app.anagram;

import java.util.Arrays;

public class AnagramExample {
    public static void main(String[] args) {
        String s1="race";
        String s2="care";
        String s3="cary";
        char[] ch1=s1.toCharArray();
        char[] ch2=s2.toCharArray();
        char[] ch3=s3.toCharArray();
        Arrays.sort(ch1);
        Arrays.sort(ch2);
        Arrays.sort(ch3);
        System.out.println(Arrays.equals(ch1, ch2));
        System.out.println(Arrays.equals(ch1, ch3));
    }
}
output:-
=========
true
false
==========
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotation {
    public static void leftRotation(int a[],int r)
    {
        int i,j,temp,prev;
        r=r%a.length;
        for(i=0;i<r;i++)
        {
            prev=a[0];
            
            for(j=a.length-1;j>=0;j--)
            {
                temp=a[j];
                a[j]=prev;
                prev=temp;
            }
        }
    }
}
class Test5557644{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a[]= {1,2,3,4,5};
        System.out.println("Enter number of ratations: ");
        int r=sc.nextInt();
        System.out.println("Before rotating array==>"+Arrays.toString(a));
        LeftRotation.leftRotation(a,r);
        System.out.println("After '"+r+"' rotating array==>"+Arrays.toString(a));
    }
}
output:-
=========
Enter number of ratations: 
2
Before rotating array==>[1, 2, 3, 4, 5]
After '2' rotating array==>[3, 4, 5, 1, 2]
======================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotationMethod {
    static void leftRotation(int a[],int r){
        int i,j,n=a.length;
        int temp[] = new int[r];

        for(i=0;i<r;i++)
            temp[i] = a[i];

        for(i=r;i<n;i++)
            a[i-r] = a[i];

        for(i=0;i<r;i++)
            a[i+n-r] = temp[i];
    }
}
class Test2332432{ 
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);

        int a[] = {1, 2, 3, 4, 5};

        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();

        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        LeftRotationMethod.leftRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));     
    }
}
output:-
===========
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [3, 4, 5, 1, 2]
==============================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotationMethod2 {
    static void leftRotation(int a[],int r){
        int i,n=a.length;
        int temp[] = new int[n];
        for(i=0;i<n;i++)
            temp[i] = a[(i+r)%n];
        for(i=0;i<n;i++)
            a[i] = temp[i];     
    }
}
class Test3435436 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        int a[] = {1, 2, 3, 4, 5};
        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();
        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        LeftRotationMethod2.leftRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));     
    }
}
output:-
==========
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [3, 4, 5, 1, 2]
===========
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotationUsingTemp {
    static void LeftRotation(int a[],int r){
        int i,j,temp;
        r=r%a.length;
        for(i=0;i<r;i++)
        {
            temp = a[0];
            for(j=0;j<a.length-1;j++)
                a[j] = a[j+1];
            a[a.length-1]=temp;
        }
    }
}
class Test67673{
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);

        int a[] = {1, 2, 3, 4, 5};

        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();

        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        LeftRotationUsingTemp.LeftRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a)); 
    }
}
output:-
============
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [3, 4, 5, 1, 2]
===================================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class ReversalLeftRotation {
    static void reverse(int a[],int b,int e){
        int temp;
        while(b<e){
            temp = a[b];
            a[b] = a[e];
            a[e] = temp;
            b++;
            e--;
        }
    }
    static void leftRotation(int a[],int r){
        int n = a.length;
        reverse(a,0,r-1);
        reverse(a,r,n-1);
        reverse(a,0,n-1);
    }
}
class Test2144 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        int a[] = {1, 2, 3, 4, 5};
        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();
        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        ReversalLeftRotation.leftRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));     
    }
}

output:-
============
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [3, 4, 5, 1, 2]

==================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class ReversalRightRotation {
    static void reverse(int a[],int b,int e){
        int temp;
        while(b<e){
            temp = a[b];
            a[b] = a[e];
            a[e] = temp;
            b++;
            e--;
        }
    }
    static void rightRotation(int a[],int r){
        int n = a.length;
        reverse(a,0,n-1);
        reverse(a,0,r-1);
        reverse(a,r,n-1);       
    }
}

class Test2431245 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        int a[] = {1, 2, 3, 4, 5};
        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();
        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        ReversalRightRotation.rightRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));     
    }
}
output:-
=========
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [4, 5, 1, 2, 3]
========================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotation {

    public static void rightRotation(int a[],int r)
    {
        int i,j,prev,temp;
        r=r%a.length;
        for(i=0;i<r;i++)
        {
            prev=a[a.length-1];
            for(j=0;j<a.length;j++)
            {
                temp=a[j];
                a[j]=prev;
                prev=temp;
            }
        }   
    }   
}
class Test565233{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a[]= {1,2,3,4,5};
        System.out.println("Enter number of rotations: ");
        int r=sc.nextInt();
        System.out.println("Before rotating array===>"+Arrays.toString(a));
        RightRotation.rightRotation(a,r);
        System.out.println("After '"+r+"' rotating array===>"+Arrays.toString(a));
    }
}
output:-
===========
Enter number of rotations: 
2
Before rotating array===>[1, 2, 3, 4, 5]
After '2' rotating array===>[4, 5, 1, 2, 3]
===========================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationMethod {
    static void rightRotation(int a[],int r){
        int i,j,n=a.length;
        int temp[] = new int[r];

        for(i=0;i<r;i++)
            temp[i] = a[n-r+i];

        for(i=n-r-1;i>=0;i--)
            a[i+r] = a[i];

        for(i=0;i<r;i++)
            a[i] = temp[i];
    }
}
    class Test23423 
    {
        public static void main(String[] args) 
        {
            Scanner obj = new Scanner(System.in);

            int a[] = {1, 2, 3, 4, 5};

            System.out.println("Enter number of rotations:");
            int r = obj.nextInt();

            System.out.println("Before rotating array ===> "+Arrays.toString(a));
            RightRotationMethod.rightRotation(a,r);
            System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));     
        }
}
output:-
============
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [4, 5, 1, 2, 3]
============================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationMethod2 {
    static void rightRotation(int a[],int r){
        int i,n=a.length;
        int temp[] = new int[n];
        for(i=0;i<n;i++)
            temp[(i+r)%n] = a[i];

        for(i=0;i<n;i++)
            a[i] = temp[i];     
    }
}

class Test434 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        int a[] = {1, 2, 3, 4, 5};
        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();
        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        RightRotationMethod2.rightRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a));     
    }
}
output:-
=========
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [4, 5, 1, 2, 3]
============================
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationUsingTemp {
    static void rightRotation(int a[],int r){
        int i,j,temp;
        r=r%a.length;
        for(i=0;i<r;i++)
        {
            temp = a[a.length-1];
            for(j=a.length-1;j>0;j--)
                a[j] = a[j-1];
            a[0]=temp;
        }
    }
}
class Test676732{
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);

        int a[] = {1, 2, 3, 4, 5};

        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();

        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        LeftRotationUsingTemp.LeftRotation(a,r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a)); 
    }
}
output:-
=========
package com.app.arrayrotation;

import java.util.Arrays;
import java.util.Scanner;

public class RightRotationUsingTemp {
    static void rightRotation(int a[],int r){
        int i,j,temp;
        r=r%a.length;
        for(i=0;i<r;i++)
        {
            temp = a[a.length-1];
            for(j=a.length-1;j>0;j--)
                a[j] = a[j-1];
            a[0]=temp;
        }
    }
}
class Test676732{
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);

        int a[] = {1, 2, 3, 4, 5};

        System.out.println("Enter number of rotations:");
        int r = obj.nextInt();

        System.out.println("Before rotating array ===> "+Arrays.toString(a));
        RightRotationUsingTemp.rightRotation(a, r);
        System.out.println("After "+r+" rotations array ===> "+Arrays.toString(a)); 
    }
}
output:-
==========
Enter number of rotations:
2
Before rotating array ===> [1, 2, 3, 4, 5]
After 2 rotations array ===> [4, 5, 1, 2, 3]
======================
package com.app.arrays;

public class ArrayFirstNumber {
public static void main(String[] args) {
    int a[]= {1,2,3,4};
    int b[][]= {{1,2,3},{3,3,3}};
    System.out.println(a);
    System.out.println(b);
}
}
output:-
=========
[I@15db9742
[[I@6d06d69c
==================
package com.app.arrays;

public class ArraySecondExample {

    public static void main(String[] args) {
        int a[]=new int[3];
        System.out.println(a[0]);
        System.out.println(a[1]);
        System.out.println(a[2]);
        a[0]=111;
        a[1]=121;
        a[2]=131;
        System.out.println(a[0]);
        System.out.println(a[1]);
        System.out.println(a[2]);
        //System.out.println(a[3]);//java.lang.ArrayIndexOutOfBoundsException: 3
    }
}
output:-
==========
0
0
0
111
121
131
=============
package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class SortingAscendingOrder {
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter Array size: ");
    int n=sc.nextInt();
    
    int a[]=new int[n];
    System.out.println("Enter Array '"+n+"' elements");
    
    for(int i=0;i<n;i++)
    {
        a[i]=sc.nextInt();
    }
    
    System.out.println("Array Before sort: "+Arrays.toString(a));
    
    int i,j,temp;
    
    for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]>a[j])
            {
                temp=a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
    }
    System.out.println("Array After sort: "+Arrays.toString(a));
}
}

output:-
==========
Enter Array size: 
5
Enter Array '5' elements
4 5 3 2 1
Array Before sort: [4, 5, 3, 2, 1]
Array After sort: [1, 2, 3, 4, 5]
================
package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class ArraySortDescendingOrder {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Array size: ");
        int n=sc.nextInt();
        
        int a[]=new int[n];
        System.out.println("Enter Array '"+n+"' elements");
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        System.out.println("Array Before sort: "+Arrays.toString(a));
        
        int i,j,temp;
        
        for(i=0;i<n;i++)
        {
            for(j=i+1;j<n;j++)
            {
                if(a[i]<a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        System.out.println("Array After sort: "+Arrays.toString(a));
    }
    }
    output:-
    ==========
    Enter Array size: 
5
Enter Array '5' elements
1 2 3 4 5
Array Before sort: [1, 2, 3, 4, 5]
Array After sort: [5, 4, 3, 2, 1]
============================
package com.app.arrays;

import java.util.Scanner;

public class ArraysUsingForLoop {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Array Size: ");
        int n=sc.nextInt();
        int a[]=new int[n];
        System.out.println("Enter Array  '"+n+"' Elements");
        for(int i=0;i<n;i++) {
            a[i]=sc.nextInt();
        }
        System.out.println("Using while loop: ");

        int index=0;
        while(index<a.length) {
            System.out.println(a[index]);
            index++;
        }
        System.out.println("Using for loop: ");

        for(int i=0;i<a.length;i++)
        {
            System.out.println(a[i]);
        }
        System.out.println("Using foreach loop: ");

        for (int i : a) {
            System.out.println(i);
        }
        sc.close();
    }
}
output:-
============
Enter Array Size: 
5
Enter Array  '5' Elements
5
4
2
3
2
Using while loop: 
5
4
2
3
2
Using for loop: 
5
4
2
3
2
Using foreach loop: 
5
4
2
3
2
========================================
package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BeginAndEndExample {
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter the Array Size: ");
    int n=sc.nextInt();
    int[] a=new int[n];
    System.out.println("Enter Array elements: '"+n+"'");
    for(int i=0;i<n;i++)
    {
        a[i]=sc.nextInt();
    }
    System.out.println("Array before sort: "+Arrays.toString(a));
    System.out.println("Enter begin location: ");
    int begin=sc.nextInt();
    System.out.println("Enter end location: ");
    int end=sc.nextInt();
    Arrays.sort(a,begin,end);
    System.out.println("Array After sort: "+Arrays.toString(a));
}
}

output:-
===========
Enter the Array Size: 
5
Enter Array elements: '5'
3
1
6
6
2
Array before sort: [3, 1, 6, 6, 2]
Enter begin location: 
0
Enter end location: 
3
Array After sort: [1, 3, 6, 6, 2]
================================
package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BeginAndEndSwapping {
public static void main(String[] args) {
    
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter the Array size: ");
    int n=sc.nextInt();
    
    int[] a=new int[n];
    System.out.println("Enter the '"+n+"' elements");
    
    for(int i=0;i<n;i++)
    {
        a[i]=sc.nextInt();
    }
    System.out.println("Array Before elements: "+Arrays.toString(a));
    
    int begin,end;
    
    System.out.println("Enter begin location");
    begin=sc.nextInt();
    System.out.println("Enter end location");
    end=sc.nextInt();
    int i,j,t;
    for(i=begin;i<end;i++)
    {
        for(j=i+1;j<end;j++)
        {
            if(a[i]>a[j]) {
                t=a[i];
                a[i]=a[j];
                a[j]=t;
            }
        }
    }
    System.out.println("Array After elements: "+Arrays.toString(a));
}
}
output:-
===========
Enter the Array size: 
5
Enter the '5' elements
3
5
2
4
1
Array Before elements: [3, 5, 2, 4, 1]
Enter begin location
0
Enter end location
3
Array After elements: [2, 3, 5, 4, 1]
===============================
package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class FormulaArray {

    public static void main(String[] args) {
        /*
         * 1st min element = a[1-1]
         *  2nd min element = a[2-1] 
         *  3rd min element = a[3-1]
         * kth min element = a[k-1]
         *  . . 1st max element = a[n-1]
         *   2nd max element =
         * a[n-2] 3rd max element = a[n-3]
         *  kth max element = a[n-k]
         */
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Array size: ");
        int n=sc.nextInt();
        int a[]=new int[n];
        System.out.println("Enter '"+n+"' elements");
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        System.out.println("Array Before sorting: "+Arrays.toString(a));
        Arrays.sort(a);
        System.out.println("Array After sorting: "+Arrays.toString(a));
        System.out.println("1st min element="+a[1-1]);
        System.out.println("2nd min element="+a[2-1]);
        System.out.println("3rd min element="+a[3-1]);
        System.out.println("1st max element="+a[n-1]);
        System.out.println("2nd max element="+a[n-2]);
        System.out.println("3rd max element="+a[n-3]);
    }
}

output:-
===========
Enter Array size: 
5
Enter '5' elements
5
6
2
3
1
Array Before sorting: [5, 6, 2, 3, 1]
Array After sorting: [1, 2, 3, 5, 6]
1st min element=1
2nd min element=2
3rd min element=3
1st max element=6
2nd max element=5
3rd max element=3
======================
package com.app.arrays;

import java.util.Scanner;

public class MaxNumberArray {
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter Array Size: ");
    int n=sc.nextInt();
    int a[]=new int[n];
    System.out.println("Enter '"+n+"' elements");
    for(int i=0;i<n;i++)
    {
        a[i]=sc.nextInt();
    }
    int max;
    max=a[0];
    for(int i=1;i<n;i++)
    {
        if(max<a[i])
        {
            max=a[i];
        }
    }
    System.out.println("Max Number: "+max);
}
}
output:-
===========
Enter Array Size: 
5
Enter '5' elements
3
2
5
7
6
Max Number: 7
==============
package com.app.arrays;

import java.util.Scanner;

public class MinNumber {
public static void main(String[] args) {
    
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter Array Size: ");
    int n=sc.nextInt();
    int a[]=new int[n];
    System.out.println("Enter an Array elemements '"+n+"' ");
    for(int i=0;i<n;i++) {
        a[i]=sc.nextInt();
    }
    
    int min;
    min=a[0];
    for(int i=1;i<n;i++)
    {
        if(min>a[i])
        {
            min=a[i];
        }
    }
    System.out.println("Min Number: "+min);
    sc.close();
}
}
output:-
==========
Enter Array Size: 
7
Enter an Array elemements '7' 
3
4
13
34
1
3
4
Min Number: 1

========================
package com.app.arrays;

public class NegitiveArray {
    public static void main(String[] args) {
        int a[]=new int[-2];// java.lang.NegativeArraySizeException
        System.out.println(a[0]);
        System.out.println(a[1]);
    }
}

output:-
==========
Exception in thread "main" java.lang.NegativeArraySizeException
    at com.app.arrays.NegitiveArray.main(NegitiveArray.java:5)
    ====================================

package com.app.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class PredefinedMethod {
public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Enter the Array Size: ");
    int n=sc.nextInt();
    int[] a=new int[n];
    System.out.println("Enter '"+n+"' elements");
    for(int i=0;i<n;i++)
    {
        a[i]=sc.nextInt();
    }
    System.out.println("Before sorting: "+Arrays.toString(a));
    Arrays.sort(a);
    System.out.println("After sorting: "+Arrays.toString(a));
    sc.close();
}
}

output:-
=========
Enter the Array Size: 
5
Enter '5' elements
6
3
2
4
5
Before sorting: [6, 3, 2, 4, 5]
After sorting: [2, 3, 4, 5, 6]
=========================
package com.app.binarysearch;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearchIteration {

    public static int binarySearchIteration(int[] a,int key)
    {
        int low,high,mid;
        low=0;
        high=a.length-1;
        while(low<=high)
        {
            mid=(low+high)/2;
            if(key==a[mid])
                return mid;
        else if (key<a[mid])
                high=mid-1;
            else
                low=mid+1;
        }
        return -1;
    }   
}
class Test676733{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a[]= {21,12,13,111,21,12};
        System.out.println("Array elements: "+Arrays.toString(a));
        Arrays.sort(a);
        System.out.println("Arrays Elements:"+Arrays.toString(a));
        System.out.println("Enter key elements");
        int key=sc.nextInt();
        System.out.println(BinarySearchIteration.binarySearchIteration(a,key)); 
    }
}
output:-
==========
Array elements: [21, 12, 13, 111, 21, 12]
Arrays Elements:[12, 12, 13, 21, 21, 111]
Enter key elements
13
2
========================
package com.app.binarysearch;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearchRecursion {

    public static int binarySearchRecursion(int[] a,int key,int low,int high)
    {
        int mid=(low+high)/2;
        if(low>high)
        {
            return -1;
        }else if(key==a[mid])
        {
            return mid;
        }else if(key<a[mid])
        {
            return binarySearchRecursion(a,key,low,mid-1);
        }else {
            return binarySearchRecursion(a,key,mid+1,high);
        }
    }
}
class Test3543643 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        
        int a[] = {10,20,19,11,12,15,14,13,16,17,18};
        System.out.println("Array Elements:"+Arrays.toString(a));
        Arrays.sort(a);
        System.out.println("Array Elements:"+Arrays.toString(a));
        System.out.println("Enter key element");
        int key=obj.nextInt();
        System.out.println(BinarySearchRecursion.binarySearchRecursion(a,key,0,a.length-1));
        obj.close();
    }
}
output:-
=============
Array Elements:[10, 20, 19, 11, 12, 15, 14, 13, 16, 17, 18]
Array Elements:[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
Enter key element
14
4

====================================
package com.app.bubblesort;

import java.util.Arrays;
import java.util.Random;

public class BubbleSortAsc {

    public static void bubbleSortAsc(int[] a)
    {
        int i,j,t;

        for(i=0;i<a.length-1;i++)
        {
            for(j=0;j<a.length-i-1;j++)
            {
                if(a[j]>a[j+1])
                {
                    t=a[j];
                    a[j]=a[j+1];
                    a[j+1]=t;
                }
            }
        }
    }}
class Test56662{
    public static void main(String[] args) {
        Random r=new Random();
        int[] a=new int[5];

        for(int i=0;i<a.length;i++)
        {

            a[i]=r.nextInt(10);
        }

        System.out.println(Arrays.toString(a));
        BubbleSortAsc.bubbleSortAsc(a);
        System.out.println(Arrays.toString(a));
    }
}

output:-
============
[2, 4, 3, 8, 2]
[2, 2, 3, 4, 8]
=====================
package com.app.bubblesort;

import java.util.Arrays;
import java.util.Random;

public class BubbleSortDesc {

    public static void bubbleSortDesc(int[] a)
    {
        
        int i,j,t;
        
        for(i=0;i<a.length-1;i++)
        {
            for(j=0;j<a.length-i-1;j++)
            {
                if(a[j]<a[j+1])
                {
                    t=a[j];
                    a[j]=a[j+1];
                    a[j+1]=t;
                }
            }
        }
    }
}
class Test66672{
    public static void main(String[] args) {
        Random r=new Random();
        
        int[] a=new int[10];
        
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(100);
        }   
    System.out.println(Arrays.toString(a));
    BubbleSortDesc.bubbleSortDesc(a);
    System.out.println(Arrays.toString(a)); 
    }
}
output:-
========
[25, 57, 91, 0, 57, 49, 12, 56, 96, 23]
[96, 91, 57, 57, 56, 49, 25, 23, 12, 0]
====================================

package com.app.chessboard;

import java.util.Scanner;

public class ChessBoard {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int x=s.charAt(0)-96;
        int y=s.charAt(1);
        System.out.println(x%2!=y%2);
    }
}
output:-
=============
srinivas
true
============
package com.app.commaspace;

import java.util.StringTokenizer;

public class StringTokenizerEx {
    public static void main(String[] args) {
        String s="the quick brown fox jumps over the lazy dog";
        StringTokenizer s1=new StringTokenizer(s);
        while(s1.hasMoreElements()) {
            System.out.println(s1.nextToken());
        }
    }
}
output:-
========
the
quick
brown
fox
jumps
over
the
lazy
dog
=========================
package com.app.delete;

import java.util.Arrays;

public class ArrayAllOccuranceElement {
    public static int[] deleteAtLocation(int[] a,int loc)
    {
        int k,i,b[]=new int[a.length-1];
        for(i=0,k=0;i<a.length;i++)
        {
            if(i==loc)
            {
                continue;
            }
            else {
                b[k++]=a[i];
            }
        }
        return b;
    }
    public static int[] deleteElement(int[] a,int element)
    {
        int index=-1,i,k,c=0;
        
        for(i=0;i<a.length;i++)
        {
            if(a[i]==element)
            {
                a=deleteAtLocation(a, i);
            }
        }
        return a;
    }
}
class Test6663{
    public static void main(String[] args) {
    int a[]= {10, 11, 12, 13, 12, 15, 12};
    System.out.println(Arrays.toString(a));
    a=ArrayAllOccuranceElement.deleteElement(a,12);
    System.out.println(Arrays.toString(a));
    }
}
output:-
============
[10, 11, 12, 13, 12, 15, 12]
[10, 11, 13, 15]
=====================
package com.app.delete;

import java.util.Arrays;

public class DeleteAll {
    public static int[] deleteAll(int[] a)
    {
        int b[]=new int[0];
        //logic
        return b;
    }
}
class Test787{
    public static void main(String[] args) {
        int a[]= {10,11,12,13,14,15,16};
        System.out.println(Arrays.toString(a));
        a=DeleteAll.deleteAll(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
=======
[10, 11, 12, 13, 14, 15, 16]
[]
========================
package com.app.delete;

import java.util.Arrays;

public class DeleteAtLocation {

    public static int[] deleteAtLocation(int[] a,int loc)
    {
        int k,b[]=new int[a.length-1];
        int i;
        for(i=0,k=0;i<a.length;i++)
        {
            if(i==loc)
            {
                continue;
            }else {
                b[k++]=a[i];
            }
        }
        return b;
    }
}
class Test77{
    public static void main(String[] args) {
        int a[]= {10,11,12,13,14,15,16};
        System.out.println(Arrays.toString(a));
        a=DeleteAtLocation.deleteAtLocation(a,2);
        System.out.println(Arrays.toString(a));
    }
}
output:-
===========
[10, 11, 12, 13, 14, 15, 16]
[10, 11, 13, 14, 15, 16]
===========================
package com.app.delete;

import java.util.Arrays;

public class DeleteElement {

    public static int[] deleteElement(int[] a,int element)
    {
        int index=-1,i,k;
        for(i=0;i<a.length;i++)
        {
            if(a[i]==element)
            {
                index=i;
                break;
            }
        }
        if(index!=-1)
        {
            int b[]=new int[a.length-1];
            for(i=0,k=0;i<a.length;i++)
            {
                if(i==index)
                    
                {
                    continue;
                }else {
                    b[k++]=a[i];
                }
            }
            return b;
        }
        return a;
    }
}
class Test67788{
    public static void main(String[] args) {
        int a[]= {10,11,12,13,14,15,16};
        System.out.println(Arrays.toString(a));
        a=DeleteElement.deleteElement(a,13);
        System.out.println(Arrays.toString(a)); 
    }
}
output:-
==========
[10, 11, 12, 13, 14, 15, 16]
[10, 11, 12, 14, 15, 16]
=======================
package com.app.delete;

import java.util.Arrays;

public class DeleteFirstLocation {
    public static int[] deleteFirstLocation(int[] a)
    {
        int i,b[]=new int[a.length-1];
        
        for(i=0;i<a.length-1;i++)
        {
            b[i]=a[i+1];
        }
        return b;
    }
}
class Test6687{
    public static void main(String[] args) {
        
        int a[]= {10,11,12,13,14,15,16};
        System.out.println(Arrays.toString(a));
        a=DeleteFirstLocation.deleteFirstLocation(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
=========
[10, 11, 12, 13, 14, 15, 16]
[11, 12, 13, 14, 15, 16]
=============================
package com.app.delete;

import java.util.Arrays;

public class DeleteFirstOccuranceAndLastOccurance {
    
    public static int[] deleteAtLocation(int[] a,int loc)
    {
    int k,i,b[]=new int[a.length-1];
    
    for(i=0,k=0;i<a.length;i++)
    {
        if(i==loc)
        {
            continue;
        }else {
            b[k++]=a[i];
        }
    }
    return b;
    }
    public static int[] deleteElement(int[] a,int element)
    {
        int index=-1,i,k,c=0;
        for(i=0;i<a.length;i++)
        {
            if(a[i]==element)
            {
                c++;
                a=deleteAtLocation(a, i);
                if(c==2)
                {
                    break;
                }
            }
        }
        return a;
    }
}
class Test6666{
    public static void main(String[] args) {

        int a[]= {10, 11, 12, 13, 12, 15, 12};
        System.out.println(Arrays.toString(a));
        a=DeleteFirstOccuranceAndLastOccurance.deleteElement(a,12);
        System.out.println(Arrays.toString(a));
    }
}
output:-
============
[10, 11, 12, 13, 12, 15, 12]
[10, 11, 13, 15, 12]
======================
package com.app.delete;

import java.util.Arrays;

public class DeleteLastLocation {
    public static int[] lastLocation(int a[])
    {
        int b[]=new int[a.length-1];
        int i;
        
        for(i=0;i<a.length-1;i++)
        {
            b[i]=a[i];
        }
        return b;
    }   
}
class Test6676{
    public static void main(String[] args) {    
        int a[]= {10,11,12,13,14,15,16};
        System.out.println(Arrays.toString(a));
        a=DeleteLastLocation.lastLocation(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
==========
[10, 11, 12, 13, 14, 15, 16]
[10, 11, 12, 13, 14, 15]
=============================
package com.app.equalityarrays;

public class EqualElements {
    public static boolean isEqual(int a[],int b[])
    {
        for(int i=0;i<a.length;i++)
        {
            if(a[i]!=b[i])
            {
                return false;
            }
        }
        return true;
    }
}
    class Test8983{
    public static void main(String[] args) {

        int a[]= {1,2,3};
        int b[]= {4,5,6};
        int c[]= {1,2,3};
        int d[]= {1,3,2};
        System.out.println(EqualElements.isEqual(a,b));
        System.out.println(EqualElements.isEqual(a,c));
        System.out.println(EqualElements.isEqual(a,d));
    }
}

output:-
===========
false
true
false
=========
package com.app.equalityarrays;

import java.util.Arrays;

public class EqualityElementsBySorting {

    public static boolean isEqualSort(int a[],int b[])
    {
        for(int i=0;i<a.length;i++) {
        if(a[i]!=b[i])
        {
            return false;
        }
    }
    return true;
    }
}
    class Test66300{
public static void main(String[] args) {

    int a[]= {1,2,3};
    int b[]= {4,5,6};
    int c[]= {1,2,3};
    int d[]= {1,3,2};
    
    Arrays.sort(a);
    Arrays.sort(b);
    Arrays.sort(c);
    Arrays.sort(d);
    System.out.println(EqualityElementsBySorting.isEqualSort(a,b));
    System.out.println(EqualityElementsBySorting.isEqualSort(a,c));
    System.out.println(EqualityElementsBySorting.isEqualSort(a,d));
}
}
output:-
=========
false
true
true
===========
package com.app.hashtable;

public class Hashtable {
    public int size;
    int a[];
 public Hashtable(int size)
 {
     this.size=size;
     a=new int[this.size];
     
     for(int i=0;i<this.size;i++)
     {
         a[i]=-1;
     }  
 }
    public int compute(int value) {
        
        return value%size;
    }
    public boolean add(int value)
    {
        int hcode=compute(value);
        
        if(a[hcode]==-1)
        {
            a[hcode]=value;
            return true;
        }
        return false;
    }
public boolean remove(int value)
{
    int hcode=compute(value);
    
    if(a[hcode]!=-1 && a[hcode]==value)
    {
        a[hcode]=-1;
        return true;
    }
    return false;
}
    public boolean search(int value)
    {
        int hcode=compute(value);
        
        if(a[hcode]==value)
        {
            return true;
        }
        return false;
    }
    public void print() {
        System.out.println("content of hash table");
        for(int i=0;i<size;i++)
        {
            System.out.println(i+"============>"+a[i]);
            }   
    }
}
class Test565656
{
public static void main(String[] args) {
    Hashtable s=new Hashtable(7);
    System.out.println(s.size);
    s.print();
    System.out.println(s.add(3));//true
    System.out.println(s.add(12));//true
    System.out.println(s.add(50));//true
    System.out.println(s.add(91));//true
    System.out.println(s.add(104));//true
    System.out.println(s.add(177));//true
    System.out.println(s.add(39));//true
    s.print();
    System.out.println(s.search(90));//false
    System.out.println(s.search(91));//true
    System.out.println(s.search(92));//false

    System.out.println(s.remove(103));//false
    System.out.println(s.remove(104));//true
    System.out.println(s.remove(105));//false
    s.print();
}
}   


output:-
=========
7
content of hash table
0============>-1
1============>-1
2============>-1
3============>-1
4============>-1
5============>-1
6============>-1
true
true
true
true
true
true
true
content of hash table
0============>91
1============>50
2============>177
3============>3
4============>39
5============>12
6============>104
false
true
false
false
true
false
content of hash table
0============>91
1============>50
2============>177
3============>3
4============>39
5============>12
6============>-1
============================
package com.app.hashtable;

public class LinerProbingHashtable
{
    int size;
    int a[];
    LinerProbingHashtable(int size){
        this.size = size;
        a = new int[this.size];
        for(int i=0;i<this.size;i++)
            a[i] = -1;
    }
    int compute(int value){
        return value%size;
    }
    int compute1(int index){
        return index;
    }
    boolean add(int value){
        int hcode = compute(value);
        for(int i=0;i<size;i++)
        {
            if(a[hcode]==-1)
            {
                a[hcode] = value;
                return true;
            }
            hcode = hcode + compute1(i);
            hcode = hcode % size;
        }
        return false;
    }
    boolean remove(int value){
        int hcode = compute(value);
        for(int i=0;i<size;i++)
        {
            if(a[hcode]!=-1 && a[hcode]==value){
                a[hcode] = -1;
                return true;
            }
            hcode = hcode + compute1(i);
            hcode = hcode % size;
        }
        return false;
    }
    boolean search(int value){
        int hcode = compute(value);
        for(int i=0;i<size;i++)
        {
            if(a[hcode]!=-1 && a[hcode]==value)
                return true;
            hcode = hcode + compute1(i);
            hcode = hcode % size;
        }
        return false;
    }
    void print(){
        System.out.println("content of hash table");
        for(int i=0;i<size;i++){
            System.out.println(i+" ====> "+a[i]);
        }
    }

}
class Test3444 
{
    public static void main(String[] args) 
    {
        Hashtable hs = new Hashtable(10);
        System.out.println(hs.add(3));//true
        System.out.println(hs.add(13));//true
        System.out.println(hs.add(23));//true
        System.out.println(hs.add(33));//true
        System.out.println(hs.add(43));//true
        System.out.println(hs.add(53));//true
        System.out.println(hs.add(63));//false
        hs.print();
        System.out.println(hs.search(53));//true
        System.out.println(hs.search(63));//false
        System.out.println(hs.remove(53));//true
        System.out.println(hs.remove(63));//false
        hs.print();
    }
}
output:-
=========
true
false
false
false
false
false
false
content of hash table
0============>-1
1============>-1
2============>-1
3============>3
4============>-1
5============>-1
6============>-1
7============>-1
8============>-1
9============>-1
false
false
false
false
content of hash table
0============>-1
1============>-1
2============>-1
3============>3
4============>-1
5============>-1
6============>-1
7============>-1
8============>-1
9============>-1
==========================
package com.app.hashtable;

public class QuadraticProbingHashtable {
        int size;
        int a[];
        QuadraticProbingHashtable(int size){
            this.size = size;
            a = new int[this.size];
            for(int i=0;i<this.size;i++)
                a[i] = -1;
        }
        int compute(int value){
            return value%size;
        }
        int compute1(int index){
            return index*index;
        }
        boolean add(int value){
            int hcode = compute(value);
            for(int i=0;i<size;i++)
            {
                if(a[hcode]==-1)
                {
                    a[hcode] = value;
                    return true;
                }
                hcode = hcode + compute1(i);
                hcode = hcode % size;
            }
            return false;
        }
        boolean remove(int value){
            int hcode = compute(value);
            for(int i=0;i<size;i++)
            {
                if(a[hcode]!=-1 && a[hcode]==value){
                    a[hcode] = -1;
                    return true;
                }
                hcode = hcode + compute1(i);
                hcode = hcode % size;
            }
            return false;
        }
        boolean search(int value){
            int hcode = compute(value);
            for(int i=0;i<size;i++)
            {
                if(a[hcode]!=-1 && a[hcode]==value)
                    return true;
                hcode = hcode + compute1(i);
                hcode = hcode % size;
            }
            return false;
        }
        void print(){
            System.out.println("content of hash table");
            for(int i=0;i<size;i++){
                System.out.println(i+" ====> "+a[i]);
            }
        }

    }

    class Test4534673 
    {
        public static void main(String[] args) 
        {
            Hashtable hs = new Hashtable(10);
            System.out.println(hs.add(5));//true
            System.out.println(hs.add(15));//true
            System.out.println(hs.add(25));//true
            System.out.println(hs.add(35));//true
            System.out.println(hs.add(45));//false
            hs.print();     
            System.out.println(hs.search(35));//true
            System.out.println(hs.search(45));//false
            System.out.println(hs.remove(45));//false
            System.out.println(hs.remove(25));//true
            hs.print();
        }
    }
    output:-
    ===========
    true
false
false
false
false
content of hash table
0============>-1
1============>-1
2============>-1
3============>-1
4============>-1
5============>5
6============>-1
7============>-1
8============>-1
9============>-1
false
false
false
false
content of hash table
0============>-1
1============>-1
2============>-1
3============>-1
4============>-1
5============>5
6============>-1
7============>-1
8============>-1
9============>-1
================================
package com.app.hashtable;

public class SeparateChainingHashtable {
    
    int size;
    Node a[];

    class Node{
        int value;
        Node next;
        Node(int value,Node next){
            this.value = value;
            this.next = next;
        }
    }

    SeparateChainingHashtable(int size){
        this.size = size;
        a = new Node[this.size];
        for(int i=0;i<this.size;i++)
            a[i] = null;
    }
    int compute(int value){
        return value%size;
    }
    
    void add(int value){
        int hcode = compute(value);
        a[hcode] = new Node(value,a[hcode]);
    }
    boolean remove(int value){
        int hcode = compute(value);
        Node nextNode,head = a[hcode];
        if(head!=null && head.value==value){
            a[hcode] = head.next;
            return true;
        }
        while(head!=null)
        {
            nextNode = head.next;
            if(nextNode!=null && nextNode.value==value){
                head.next = nextNode.next;
                return true;
            }
            else
                head=nextNode;
        }
        return false;
    }
    boolean search(int value){
        int hcode = compute(value);
        Node head = a[hcode];
        while(head!=null){
            if(head.value == value)
                return true;
            head = head.next;
        }
        return false;
    }
    void print(){
        System.out.println("content of hash table");
        for(int i=0;i<size;i++)
        {
            Node head = a[i];
            while(head!=null){
                System.out.print(head.value+" => ");
                head = head.next;
            }
            System.out.println("null");
        }
    }

}
class Test456567547 
{
    public static void main(String[] args) 
    {
        Hashtable hs = new Hashtable(10);
        hs.add(13);
        hs.add(33);
        hs.add(53);
        hs.add(45);
        hs.add(22);
        hs.print();     
        System.out.println(hs.search(45));//true
        System.out.println(hs.search(13));//true
        System.out.println(hs.search(103));//false
        System.out.println(hs.remove(22));//true
        System.out.println(hs.remove(33));//true
        System.out.println(hs.remove(103));//false
        hs.print();
    }
}
output:-
===========
content of hash table
0============>-1
1============>-1
2============>22
3============>13
4============>-1
5============>45
6============>-1
7============>-1
8============>-1
9============>-1
true
true
false
true
false
false
content of hash table
0============>-1
1============>-1
2============>-1
3============>13
4============>-1
5============>45
6============>-1
7============>-1
8============>-1
9============>-1
===========================
package com.app.insertionsort;

import java.util.Arrays;
import java.util.Random;

public class InsertionSortAsc {

    
    public static void insertionAsc(int[] a)
    {
        int i,j,temp,n=a.length;
        for(i=0;i<n;i++)
        {
            temp=a[i];
            j=i-1;
            while(j>=0 && a[j]>temp)
            {
                a[j+1]=a[j];
                j--;            
            }
            a[j+1]=temp;
        }
    }
}
class Test78783{
    public static void main(String[] args) {
        Random r=new Random();
        
        int[] a=new int[5];
        
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        
        System.out.println(Arrays.toString(a));
    InsertionSortAsc.insertionAsc(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
=========
[9, 0, 9, 2, 2]
[0, 2, 2, 9, 9]
====================
package com.app.insertionsort;

import java.util.Arrays;
import java.util.Random;

public class InsertionDesc {
    public static void insertDesc(int[] a)
    {
        int i,j,temp,n=a.length;
        
        
        for(i=1;i<n;i++)
        {
            temp=a[i];
            j=i-1;
            while(j>=0 && a[j]<temp)
            {
                a[j++]=a[j];
                j--;
            }
            a[j++]=temp;
        }
    }
}
class Test66762{
    public static void main(String[] args) {
        Random r=new Random();
        int[] a=new int[5];
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        System.out.println(Arrays.toString(a));
        InsertionDesc.insertDesc(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
=========
[4, 8, 0, 2, 5]
[8, 0, 2, 5, 5]
===================
package com.app.insertlocation;

import java.util.Arrays;

public class InsertAtFirstLocation {
    public static int[] insertFirst(int[] a,int element)
    {
        int i,b[]=new int[a.length+1];
        
        //logic
        b[0]=element;
        for(i=0;i<a.length;i++)
        {
            b[i+1]=a[i];
        }
        return b;   
    }   
    }
class Test5465{
    public static void main(String[] args) {
        int a[]= {3,7,9};
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(InsertAtFirstLocation.insertFirst(a,999)));
    }
}
output:-
==========
[3, 7, 9]
[999, 3, 7, 9]
====================
package com.app.insertlocation;

import java.util.Arrays;

public class InsertAtLastLocation {
    public static int[] insertAtLastLocation(int[] a,int element)
    {
        int i,b[]=new int[a.length+1];
        
        for(i=0;i<a.length;i++)
        {
            b[i]=a[i];
        }
        b[i]=element;
        return b;
    }
}
class Test456{
    public static void main(String[] args) {
        int a[]= {3,7,9};
        
        System.out.println(Arrays.toString(a));
        a=InsertAtLastLocation.insertAtLastLocation(a,999);
        a=InsertAtLastLocation.insertAtLastLocation(a,888);
        System.out.println(Arrays.toString(a));
        }
    }

    output:-
    =============
    [3, 7, 9]
[3, 7, 9, 999, 888]
==========================
package com.app.insertlocation;

import java.util.Arrays;

public class InsertAtLocation {
    public static int[] insertAtLocation(int a[],int element,int index)
    {
        int i,b[]=new int[a.length+1];
        //logic
        for(i=0;i<index;i++)
        {
            b[i]=a[i];
        }
        b[index]=element;
        for(;i<a.length;i++)
            
        {
            b[i+1]=a[i];
        }
        return b;
    }
}
class Test4433{
    public static void main(String[] args) {
        int[] a= {3,7,9};
        System.out.println(Arrays.toString(a));
        a=InsertAtLocation.insertAtLocation(a,999,2);
        System.out.println(Arrays.toString(a));
        a=InsertAtLocation.insertAtLocation(a,888,1);
        System.out.println(Arrays.toString(a));
    }
}
output:-
===========
[3, 7, 9]
[3, 7, 999, 9]
[3, 888, 7, 999, 9]
=====================
package com.app.integerenglishword;

import java.util.Scanner;

public class EnglishWordNumber {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter any Number: ");
        String s=sc.nextLine();
        for(int i=0;i<s.length();i++)
        {
            char ch=s.charAt(i);
            switch(ch)
            {
            case '0' :System.out.print("zero ");break;
            case '1' :System.out.print("one ");break;
            case '2' :System.out.print("two ");break;
            case '3' :System.out.print("three ");break;
            case '4' :System.out.print("four ");break;
            case '5' :System.out.print("five ");break;
            case '6' :System.out.print("six ");break;
            case '7' :System.out.print("seven ");break;
            case '8' :System.out.print("eight ");break;
            case '9' :System.out.print("nine ");break;
            }
        }   
    }
}
output:-
============
Enter any Number: 
56
five six
===============
Java 8:-
=========
package com.app.java8;

public interface A {
    public abstract void m1();
    default void m2() {
        System.out.println("I am good to statrt");
    }
}
package com.app.java8;

public class B implements A {
    @Override
    public void m1() {
 System.out.println("B class");
    }
}
package com.app.java8;

public class C implements A {
    @Override
    public void m1() {
        System.out.println("C class");
    }
}
package com.app.java8;

public class D implements A {
    @Override
    public void m1() {
        System.out.println("D class");
    }
}
package com.app.java8;

public class DefaultMethodExampleTest {
    public static void main(String[] args) {
        A a=new B();
        a.m1();
        a.m2();
    }
}
output:-
==========
B class
I am good to statrt
======================
package com.app.java8;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DuplicateArrays {
public static void main(String[] args) {
    
    
    /*
     * numbers.stream().filter(i -> Collections.frequency(numbers, i) >1)
     * .collect(Collectors.toSet()).forEach(System.out::println);
     */
    Integer[] numbers = new Integer[] { 2,3,4,5,6,7,8,9,8,4,4 };
    Set<Integer> allItems = new HashSet<>();
    Set<Integer> duplicates = Arrays.stream(numbers)
            .filter(n -> !allItems.add(n)) //Set.add() returns false if the item was already in the set.
            .collect(Collectors.toSet());
    System.out.println(duplicates); // [1, 4]
    
List<Integer> jj=Arrays.asList(1, 1, 2, 3, 3, 3, 4, 5, 6, 6, 6, 7, 8);

List<Integer> hh=jj.stream().distinct().collect(Collectors.toList());
System.out.println(hh);
}
}
output:-
===========
[4, 8]
[1, 2, 3, 4, 5, 6, 7, 8]
============================
package com.app.java8;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateCharacterUsingJava8 {
    public static void main(String[] args) {
        String input1="srinivas";
        Map<String, Long> countMap1=Arrays.stream(input1.split("")).collect(Collectors.groupingBy(Function.identity(),Collectors.counting()));
        System.out.println(countMap1);
    }
}
output:-
=========
{a=1, r=1, s=2, v=1, i=2, n=1}
===========================
package com.app.java8;

public class Employee {

    private Integer empId;
    private String empName;
    private String dept;
    private Double salary;
    public Employee() {
        super();
    }
    public Employee(Integer empId, String empName, String dept, Double salary) {
        super();
        this.empId = empId;
        this.empName = empName;
        this.dept = dept;
        this.salary = salary;
    }
    public Integer getEmpId() {
        return empId;
    }
    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
    public String getEmpName() {
        return empName;
    }
    public void setEmpName(String empName) {
        this.empName = empName;
    }
    public String getDept() {
        return dept;
    }
    public void setDept(String dept) {
        this.dept = dept;
    }
    public Double getSalary() {
        return salary;
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }
    @Override
    public String toString() {
        return "Employee [empId=" + empId + ", empName=" + empName + ", dept=" + dept + ", salary=" + salary + "]";
    }
}
package com.app.java8;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.event.TableColumnModelListener;

public class Test {

    public static void main(String[] args) {
        List<Employee> ee=Stream.of(
                new Employee(122, "srinivas", "cse", 67677.5),
                new Employee(123, "raju", "cse", 7677.5),
                new Employee(125, "nihas", "cse", 677.5),
                new Employee(127, "sneha", "cse", 6997.5))
                .collect(Collectors.toList());
        Comparator<Employee> compareBySalary=Comparator.comparing(Employee::getSalary);
        Map<String, Optional<Employee>> eeeee=ee.stream().collect(Collectors.groupingBy(Employee::getDept,Collectors.reducing(BinaryOperator.maxBy(compareBySalary))));
        System.out.println(eeeee);
        System.out.println("*********************");
        Map<String, List<Employee>> tt=ee.stream().collect(Collectors.groupingBy(Employee::getDept));
        System.out.println(tt);
        List<Employee> ss=ee.stream().filter(e->e.getSalary()>1000).collect(Collectors.toList());
        System.out.println();
        System.out.println(ss);
        System.out.println("***************");
        ss.forEach(System.out::println);
        System.out.println("************888888");
        Set deptSet=ee.stream().collect(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(Employee::getDept))));
        deptSet.size();
        System.out.println(deptSet);
        System.out.println("((((");
        Comparator<Employee> sss=Comparator.comparing(Employee::getSalary);
        Map<Integer, Optional<Employee>> hj=ee.stream().collect(Collectors.groupingBy(Employee::getEmpId,Collectors.reducing(BinaryOperator.maxBy(sss))));
        System.out.println(hj);
    }
}
output:-
========
{cse=Optional[Employee [empId=122, empName=srinivas, dept=cse, salary=67677.5]]}
*********************
{cse=[Employee [empId=122, empName=srinivas, dept=cse, salary=67677.5], Employee [empId=123, empName=raju, dept=cse, salary=7677.5], Employee [empId=125, empName=nihas, dept=cse, salary=677.5], Employee [empId=127, empName=sneha, dept=cse, salary=6997.5]]}

[Employee [empId=122, empName=srinivas, dept=cse, salary=67677.5], Employee [empId=123, empName=raju, dept=cse, salary=7677.5], Employee [empId=127, empName=sneha, dept=cse, salary=6997.5]]
***************
Employee [empId=122, empName=srinivas, dept=cse, salary=67677.5]
Employee [empId=123, empName=raju, dept=cse, salary=7677.5]
Employee [empId=127, empName=sneha, dept=cse, salary=6997.5]
************888888
[Employee [empId=122, empName=srinivas, dept=cse, salary=67677.5]]
((((
{122=Optional[Employee [empId=122, empName=srinivas, dept=cse, salary=67677.5]], 123=Optional[Employee [empId=123, empName=raju, dept=cse, salary=7677.5]], 125=Optional[Employee [empId=125, empName=nihas, dept=cse, salary=677.5]], 127=Optional[Employee [empId=127, empName=sneha, dept=cse, salary=6997.5]]}
=======================================================
package com.app.java8;

import java.util.function.Function;


//Write a function to return String length
public class FunctionExample {
    public static void main(String[] args) {
        
        Function<String, Integer> i=s->s.length();
        System.out.println(i.apply("srinivas"));
        System.out.println(i.apply("raju"));
    }
}
output:-
============
8
4
============================
package com.app.java8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListToMapConvertions {

    public static void main(String[] args) {

        List<Student> students=new ArrayList<Student>();
        students.add(new Student(1, "Sandeep", 30));
        students.add(new Student(2, "deep", 90));
        students.add(new Student(3, "Sep", 10));
        students.add(new Student(4, "srinivas", 90));
        students.add(new Student(5, "srinivas", 70));
        //students.add(new Student(5, "srinivas", 70));Exception in thread "main" java.lang.IllegalStateException: Duplicate key srinivas
        Map<Integer,String> map=new HashMap<Integer, String>();
        /*
         * for (Student student : students) { map.put(student.getStudentId(),
         * student.getStudentName()); } System.out.println(map);
         */
Map<Integer,String> map8=students.stream().collect(Collectors.toMap(Student::getStudentId, Student::getStudentName));
    System.out.println(map8);
        Map<Integer,String> map89=students.stream().distinct().collect(Collectors.toMap(Student::getStudentId, Student::getStudentName));
        System.out.println(map89);  
    }
}
output:-
=========
{1=Sandeep, 2=deep, 3=Sep, 4=srinivas, 5=srinivas}
{1=Sandeep, 2=deep, 3=Sep, 4=srinivas, 5=srinivas}

============================================
package com.app.java8;

import java.util.function.Predicate;

public class PredicateTest {
    public static void main(String[] args) {
        Predicate<Integer> i=ii->ii>250;
        System.out.println(i.test(100));
        Predicate<String> pp=pp1->pp1.length()>5;
        System.out.println(pp.test("srinivas"));
        System.out.println();
        int a[]= {1,2,3,4,5,6};
        Predicate<Integer> ppp=pppp->pppp%2==0;
        Predicate<Integer> p55=p44->p44>2;  
        System.out.println("Print all even nunber");
        m1(ppp,a);
        System.out.println("Greater than 2 ");
        m1(p55,a);
        System.out.println("Print all odd nunber");
        m1(ppp.negate(),a);
        System.out.println("Greater than 2 or ");
        m1(p55.or(ppp),a);
        System.out.println("Greater than 2 and");
        m1(p55.and(ppp),a);
    }
    public static void m1(Predicate<Integer> p,int[] x)
    {
        for(int i:x)
        {
            if(p.test(i))
            {
                System.out.println(i);
            }
        }
    }
}
output:-
===========
false
true

Print all even nunber
2
4
6
Greater than 2 
3
4
5
6
Print all odd nunber
1
3
5
Greater than 2 or 
2
3
4
5
6
Greater than 2 and
4
6
===============================
package com.app.java8;

import java.util.stream.IntStream;

public class SequentionalAndParallamStream {

    public static void main(String[] args) {
        
        IntStream.rangeClosed(1, 10).forEach(t->System.out.println(Thread.currentThread().getName()+t));
        System.out.println("*");
        IntStream.rangeClosed(1, 10).parallel().forEach(t->System.out.println(Thread.currentThread().getName()+": "+t));
    }
}
output:-
===========
main1
main2
main3
main4
main5
main6
main7
main8
main9
main10
*
main: 7
main: 6
ForkJoinPool.commonPool-worker-1: 3
ForkJoinPool.commonPool-worker-4: 8
ForkJoinPool.commonPool-worker-1: 5
ForkJoinPool.commonPool-worker-1: 4
ForkJoinPool.commonPool-worker-2: 9
ForkJoinPool.commonPool-worker-1: 1
ForkJoinPool.commonPool-worker-3: 2
ForkJoinPool.commonPool-worker-4: 10
===================================
package com.app.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {

    public static void main(String[] args) {
        
        ArrayList<Integer> al=new ArrayList<Integer>(Arrays.asList(10,20,30,40,50));
        System.out.println(al);
        Stream s=al.stream();
        //Stream s1=s.filter(Predicate);
        /***
         * Stream -->It is an interface ,present in java.util.stream
         * 
         * we got stream by invoke stream() on top of collection object
         * 
         * we can process a object in 2 ways
         * a)configuration
         * 
         * i)filter >>>to filter object on basis of boolean condition[T/F)
         * ii)map >>>if we want to make two object for every present in collection
         * map(), filter(), distinct(), sorted(), limit(), skip()


         * 
         * b)processing
         * 
         * forEach(), toArray(), reduce(), collect(), min(),
         *  max(), count(), anyMatch(), allMatch(), noneMatch(), findFirst(), findAny()
//nonematch ??? if no element matched it return true

            */
        
        
        //using filter method >>> greater than 20
        List<Integer> collect=al.stream().filter(i->i>20).collect(Collectors.toList());
        System.out.println(collect);
        //adding 5 for each element using map 
        
        List<Integer> collect1=al.stream().map(i->i+5).collect(Collectors.toList());
        System.out.println(collect1);
        //Terminal operations
        
        //toArray()
        
        
         Stream<Integer> ss=Stream.of(1,2,3,4,5); Object[] ob=ss.toArray();
         
         for (Object object : ob) { System.out.print(object+" "); }
         System.out.println("-------------------");
         
        Stream<Integer> ss1=Stream.of(1,2,3,4,5); 
    long count=ss1.count();
        System.out.println(count);
        
        System.out.println();
        
        Stream<Integer> ss2=Stream.of(1,2,3,4,5); 
        
        ss2.forEach(ele->System.err.print(ele+" "));
        
        
        System.out.println();
    //min and max
        Stream<Integer> ss3=Stream.of(1,2,3,4,5);
        Stream<Integer> ss4=Stream.of(1,2,3,4,5);
        Optional<Integer>min=ss3.min((o1,o2)->o1.compareTo(o2));
        Optional<Integer>max=ss4.max((o1,o2)->o1.compareTo(o2));
        System.out.println("min "+min.get());
        System.out.println("max "+max.get());
        
        //anyMath
        
        List<Integer> listOfNum=Arrays.asList(22,33,411,3,3,23,334,12);
        boolean b=listOfNum.stream().anyMatch(i->i==22);
        System.out.println(b);
        
        //anyMath
        System.out.println();
        List<Integer>listOfNums= Arrays.asList(22,33,411,3,3,23,334,12);
        boolean bb=listOfNums.stream().allMatch(element->element>0);
        System.out.println(bb);
        
        //findAny
        List<Integer>findanyelement= Arrays.asList(8,22,33,411,3,3,23,334,12);
Integer ss44=findanyelement.stream().findAny().get();
System.out.println(ss44);
        
        System.out.println("***************");
        System.out.println();
        List<Integer>findanyelement1= Arrays.asList(22,33,411,3,3,23,334,12);
        Integer ss45=findanyelement1.stream().findFirst().get();
                System.out.println(ss45);
    }
}
output:-
===========
[10, 20, 30, 40, 50]
[30, 40, 50]
[15, 25, 35, 45, 55]
1 2 3 4 5 -------------------
5


1 2 3 4 5 min 1
max 5
true

true
8
***************

22
======================
package com.app.java8;

public class TestNotes {
    private Integer id;
    private String tagName;
    private Integer tagId;
    public TestNotes() {
        super();
    }
    public TestNotes(Integer id, String tagName, Integer tagId) {
        super();
        this.id = id;
        this.tagName = tagName;
        this.tagId = tagId;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTagName() {
        return tagName;
    }
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
    public Integer getTagId() {
        return tagId;
    }
    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }
    @Override
    public String toString() {
        return "TestNotes [id=" + id + ", tagName=" + tagName + ", tagId=" + tagId + "]";
    }
}

package com.app.java8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Test6767676 {
public static void main(String[] args) {
    
    List<TestNotes> noteLst=new ArrayList<TestNotes>();
    noteLst.add(new TestNotes(1, "note1", 11));
    noteLst.add(new TestNotes(2, "note2", 22));
    noteLst.add(new TestNotes(3, "note3", 33));
    noteLst.add(new TestNotes(4, "note4", 44));
    noteLst.add(new TestNotes(5, "note5", 55));
    noteLst.add(new TestNotes(6, "note4", 66));
    Map<String, Integer> tagMap=noteLst.stream().collect(Collectors.toMap(TestNotes::getTagName, TestNotes::getTagId,(existing,replacement)->replacement));
    System.out.println("output map: "+tagMap);  
}
}
output:-
============
output map: {note4=66, note5=55, note2=22, note3=33, note1=11}
============================
package com.app.matrix;

import java.util.Scanner;

public class AdditionMatrix {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        int[][] c=new int[3][3];
        int i,j;
        System.out.println("Enter Array Matrix:A ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Matrix:B ");


        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }   

        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                c[i][j]=a[i][j]+b[i][j];
            }
        }
        System.out.println("Matrix C elements are: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(c[i][j]+" ");
            }
            System.out.println();
        }   
    }
}
output:-
==========
Enter Array Matrix:A 
1 3 5
4 5 7
6 7 8
Enter Array Matrix:B 
4 5 6
6 4 5
1 2 5
Matrix C elements are: 
5 8 11 
10 9 12 
7 9 13 
========================
package com.app.matrix;

import java.util.Scanner;

public class ColumnWiseMinMaxElements {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int i,j;
        System.out.println("Enter Array Matrix: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Matrix elements: ");
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        int min;
        for(i=0;i<3;i++)
        {
            min=a[0][i];
            for(j=0;j<3;j++)
            {
                if(min>a[j][i])
                {
                    min=a[j][i];
                }
            }
            System.out.println(i+" min elements in column element "+min);
        }
int max;
        for(i=0;i<3;i++)
        {
            max=a[0][i];
            for(j=0;j<3;j++)
            {
                if(max<a[j][i])
                {
                    max=a[j][i];
                }
            }
            System.out.println(i+" max elements in column element "+max);
        }
    }
}
output:-
============
Enter Array Matrix: 
4 5 6
2 3 5
7 8 9
Enter Array Matrix elements: 
4 5 6 
2 3 5 
7 8 9 
0 min elements in column element 2
1 min elements in column element 3
2 min elements in column element 5
0 max elements in column element 7
1 max elements in column element 8
2 max elements in column element 9
=================================
package com.app.matrix;

import java.util.Scanner;

public class ColumnWiseSwap {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        
        int[][]a=new int[3][3];
        int i,j,r1,r2,t;
        System.out.println("Enter matrix elements:");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("matrix elements are:");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        
        System.out.println("Enter row value to swap:");
        r1=sc.nextInt();
        r2=sc.nextInt();
        
        for(i=0;i<3;i++)
        {
            t=a[i][r1-1];
            a[i][r1-1]=a[i][r2-1];
            a[i][r2-1]=t;
        }
        
        System.out.println("updated matrix elements are:");
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
output:-
============
Enter matrix elements:
5 6 7
8 9 2
3 4 1
matrix elements are:
5 6 7 
8 9 2 
3 4 1 
Enter row value to swap:
1
2
updated matrix elements are:
6 5 7 
9 8 2 
4 3 1 
=================
package com.app.matrix;

import java.util.Scanner;

public class DiagonalSwap {
    public static void main(String[] args) throws Exception
    {
        Scanner obj = new Scanner(System.in);
        int a[][] = new int[3][3];//3-rows and 3-cols
        int i,j,t;
        System.out.println("Enter matrix elements:");
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                a[i][j] = obj.nextInt();
            }
        }
        System.out.println("matrix elements are:");
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(i=0;i<3;i++)
        {
            t=a[i][i];
            a[i][i]=a[i][3-i-1];
            a[i][3-i-1]=t;
        }
        System.out.println("updated matrix elements are:");
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        }   
}
output:-
=========
Enter matrix elements:
4 2 1
5 6 2
8 9 7
matrix elements are:
4 2 1 
5 6 2 
8 9 7 
updated matrix elements are:
1 2 4 
5 6 2 
7 9 8 
================
package com.app.matrix;

import java.util.Scanner;

public class DigonalArrayElements {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a[][]=new int[3][3];

        int i,j,sum=0;
        System.out.println("Enter Matrix Elements: ");


        for(i=0;i<3;i++)

        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }

        System.out.println("Enter array elements: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(i==j) {
                    sum=sum+a[i][j];
                }
            }

        }
        System.out.println("Sum of digonal: "+sum); 
    }
}

output:-
==========
Enter Matrix Elements: 
5 6 2
9 8 5
3 4 1
Enter array elements: 
5 6 2 
9 8 5 
3 4 1 
Sum of digonal: 14
====================
package com.app.matrix;

import java.util.Scanner;

public class MatrixArrayExample {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        
        int a[][]=new int[3][3];
        
        System.out.println("Enter Array elements: ");
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter matrix elements are: ");
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
            sc.close();
        }
    }
}
output:-
=======

Enter Array elements: 
4 5 7
2 3 8
8 9 2
Enter matrix elements are: 
4 5 7 
2 3 8 
8 9 2 
=============
package com.app.matrix;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixSortColumn {

    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        
        int[][] a=new int[3][3];
        System.out.println("Enter Matrix Array: ");
        
    int i,j;
    
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            a[i][j]=sc.nextInt();
        }
    }
    System.out.println(" Matrix elements: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
                
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        
        int b[][]=new int[3][3];
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                b[i][j]=a[j][i];
            }
        }
        
        for(i=0;i<3;i++)
        {
            Arrays.sort(b[i]);
        }
        
        System.out.println("Updated Matrix elements: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(b[j][i]+" ");
            }
            System.out.println();
        }
        
    }
}

output:-
========
Enter Matrix Array: 
8 9 2
4 5 6
3 2 1
 Matrix elements: 
8 9 2 
4 5 6 
3 2 1 
Updated Matrix elements: 
3 2 1 
4 5 2 
8 9 6 
=============
package com.app.matrix;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixSortRows {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
    int[][] a=new int[3][3];
    System.out.println("Enter Array Matrix: ");
        int i,j;
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
                    
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(i=0;i<3;i++)
        {
            Arrays.sort(a[i]);
        }
        System.out.println("Updated Matrix elements are: ");
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }   
    }
}
output:-
========
Enter Array Matrix: 
4 5 7
8 9 3
3 2 1
4 5 7 
8 9 3 
3 2 1 
Updated Matrix elements are: 
4 5 7 
3 8 9 
1 2 3 
==============
package com.app.matrix;

import java.util.Scanner;

public class MinMaxMatrixElement {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int i,j;
        System.out.println("Enter array Matrix");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter array Matrix elements");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        int min=a[0][0];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(min>a[i][j])
                {
                    min=a[i][j];
                }
            }
        }
        int max=a[0][0];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(max<a[i][j])
                {
                    max=a[i][j];
                }
            }
        }
        System.out.println("MIN Element: "+min);
        System.out.println("MAX Element: "+max);
    }
}
output:-
========
Enter array Matrix
9 5 6
8 9 2
5 1 4
Enter array Matrix elements
9 5 6 
8 9 2 
5 1 4 
MIN Element: 1
MAX Element: 9
===================
package com.app.matrix;

import java.util.Scanner;

public class MultiMatrix {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        int[][] c=new int[3][3];
        
        int i,j,k;
        System.out.println("Enter Matrix:A");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Matrix:B");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                c[i][j]=0;
                for(k=0;k<3;k++)
                {
                    c[i][j]=c[i][j]+(a[i][k]*b[k][j]);
                }
            }
        }
        System.out.println("Matrix C elements are: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(c[i][j]+" ");
            }
            System.out.println();
        }
    }
}

output:-
==========
Enter Matrix:A
1 3 4
4 5 6
7 8 9
Enter Matrix:B
3 4 5
6 7 8
3 2 1
Matrix C elements are: 
33 33 33 
60 63 66 
96 102 108 

====================

package com.app.matrix;

import java.util.Scanner;

public class RowWiseElementsMatrix {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        System.out.println("Enter Array Elements");
        int i,j,sum=0;
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Elements are");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(i=0;i<3;i++)
        {
            sum=0;
            for(j=0;j<3;j++)
            {
                sum=sum+a[i][j];
            }
            System.out.println(i+" row sum="+sum);
        }
    }
}
output:-
==========
Enter Array Elements
4 5 7
2 3 9
1 3 2
Enter Array Elements are
4 5 7 
2 3 9 
1 3 2 
0 row sum=16
1 row sum=14
2 row sum=6
================
package com.app.matrix;

import java.util.Scanner;

public class RowWiseMinMaxElements {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int i,j;
        System.out.println("Enter Array Matrix: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)

            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Matrix Elements: ");

        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        int min;
        for(i=0;i<3;i++)
        {
            min=a[i][0];
            for(j=0;j<3;j++)
            {
                if(min>a[i][j])
                {
                    min=a[i][j];
                }
            }
            System.out.println(i+" min row wise elements "+min);
        }
        int max;

        for(i=0;i<3;i++)
        {
            max=a[i][0];
            for(j=0;j<3;j++)
            {
                if(max<a[i][j])
                {
                    max=a[i][j];
                }
            }
            System.out.println(i+" min row wise elements "+max);
        }
    }
}
output:-
==========
Enter Array Matrix: 
5 1 2
9 8 4
4 2 1
Enter Array Matrix Elements: 
5 1 2 
9 8 4 
4 2 1 
0 min row wise elements 1
1 min row wise elements 4
2 min row wise elements 1
0 min row wise elements 5
1 min row wise elements 9
2 min row wise elements 4
==============================
package com.app.matrix;

import java.util.Scanner;

public class RowWiseSwap {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][]a=new int[3][3];
        int i,j,r1,r2,t;
        System.out.println("Enter matrix elements:");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("matrix elements are:");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("Enter row value to swap:");
        r1=sc.nextInt();
        r2=sc.nextInt();
        
        for(i=0;i<3;i++)
        {
            t=a[r1-1][i];
            a[r1-1][i]=a[r2-1][i];
            a[r2-1][i]=t;
        }
        
        System.out.println("updated matrix elements are:");
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
    }
}
output:-
==========
Enter matrix elements:
4 5 6
3 2 1
7 8 9
matrix elements are:
4 5 6 
3 2 1 
7 8 9 
Enter row value to swap:
1
2
updated matrix elements are:
3 2 1 
4 5 6 
7 8 9 
==========================
package com.app.matrix;

import java.util.Scanner;

public class ScalarMatrix {

    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        
        int[][] a=new int[3][3];
        
        int i,j,t;
        System.out.println("Matrix: ");
        
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                a[i][j] = sc.nextInt();
            }
        }
        System.out.println("Enter matrix elements:");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        
        System.out.println("Enter multiplier");
        int n=sc.nextInt();

        System.out.println("updated matrix elements are:");
        for(i=0;i<3;i++){
            for(j=0;j<3;j++){
                System.out.print((n*a[i][j])+" ");
            }
            System.out.println();
        }
    }
}

output:-
===========
Matrix: 
4 5 6
1 2 4
6 7 9
Enter matrix elements:
4 5 6 
1 2 4 
6 7 9 
Enter multiplier
2
updated matrix elements are:
8 10 12 
2 4 8 
12 14 18 
==============
package com.app.matrix;

import java.util.Scanner;

public class SubMatrix {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);

        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        int[][] c=new int[3][3];
        int i,j;
        System.out.println("Enter Array Matrix:A ");

        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Matrix:B ");

        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Matrix:C ");

        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                c[i][j]=a[i][j]-b[i][j];
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(c[i][j]+" ");
            }
            System.out.println();
        }
    }
}
output:-
==========
Enter Array Matrix:A 
4 5 6 
4 5 3
7 8 9
Enter Array Matrix:B 
3 4 5
1 2 3
7 8 9
Enter Array Matrix:C 
1 1 1 
3 3 0 
0 0 0 
====================

package com.app.matrix;

import java.util.Scanner;

public class SumColumnArrayElements {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        
        int i,j,sum=0;
        System.out.println("Enter array elemens");
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter array Matrix elements: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(i=0;i<3;i++)
        {
            sum=0;
            for(j=0;j<3;j++)
            {
                sum=sum+a[j][i];
            }
            System.out.println(i+" column sum="+sum);
        }       
    }
}

output:-
=============
Enter array elemens
3 5 5
1 2 4
7 8 9
Enter array Matrix elements: 
3 5 5 
1 2 4 
7 8 9 
0 column sum=11
1 column sum=15
2 column sum=18
=====================
package com.app.matrix;

import java.util.Scanner;

public class SumOfArrayElements {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int sum=0;
        System.out.println("Enter Array elements: ");
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Matrix elements: ");
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                sum=sum+a[i][j];
            }
        }
        System.out.println("Enter sum elements: "+sum);
        sc.close();
    }
}
output:-
============
Enter Array elements: 
4 5 2
6 7 9
4 5 4
Enter Matrix elements: 
4 5 2 
6 7 9 
4 5 4 
Enter sum elements: 46
========================
package com.app.matrix;

import java.util.Scanner;

public class SumOfEvenOddExample {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);

        int a[][]=new int[3][3];
        int i,j,sum=0,sume=0,sumo=0;

        System.out.println("Enter Matrix Elements: ");

        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)

            {
                a[i][j]=sc.nextInt();
            }

        }
        System.out.println("Enter Matrix Elements are: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)

            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();

        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(a[i][j]%2==0)
                {
                    sume=sume+a[i][j];
                }else {
                    sumo=sumo+a[i][j];
                }
            }   
        }
        sum=sume+sumo;
        System.out.println("Sum of even Numbers: "+sume);
        System.out.println("Sum of odd Numbers: "+sumo);
        System.out.println("Sum of even and odd Numbers: "+sum);
    }
}

output:-
==========
Enter Matrix Elements: 
5 6 7
8 9 2
3 5 7
Enter Matrix Elements are: 
5 6 7 
8 9 2 
3 5 7 
Sum of even Numbers: 16
Sum of odd Numbers: 36
Sum of even and odd Numbers: 52
===========================
package com.app.matrix;

import java.util.Scanner;

public class SumOfOpposite {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int [][]a=new int[3][3];

        System.out.println("Enter Array Matrix: ");

        int i,j,sum=0;
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Matrix elements: ");
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        for(i=0;i<3;i++)
        {   
            sum=sum+a[i][3-i-1];
        }
        System.out.println("sum of digonal elements:"+sum);
    }
}
output:-
==========
Enter Array Matrix: 
1 2 3
7 8 3
6 4 2
Enter Array Matrix elements: 
1 2 3 
7 8 3 
6 4 2 
sum of digonal elements:17
================
package com.app.matrix;

import java.util.Scanner;

public class SymmetricMatrix {

        public static void main(String[] args) throws Exception
        {
            Scanner obj = new Scanner(System.in);

            int a[][] = new int[3][3];//3-rows and 3-cols

            int i,j,t;

            System.out.println("Enter matrix elements:");
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                    a[i][j] = obj.nextInt();
                }
            }
            System.out.println("matrix elements are:");
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                    System.out.print(a[i][j]+" ");
                }
                System.out.println();
            }
            boolean flag=true;
            
            for(i=0;i<3;i++)
            {
                for(j=0;j<3;j++)
                {
                    if(i==j && a[i][j]!=1)
                    {
                        flag=false;
                    }
                    
                    if(i!=j && a[i][j]!=0)
                    {
                        flag=false;
                        break;
                    }
                    
                }
            }
            System.out.println(flag);
    }

}
output:-
============
Enter matrix elements:
5 6 2
8 9 3
5 6 1
matrix elements are:
5 6 2 
8 9 3 
5 6 1 
false
==============
package com.app.matrix;

import java.util.Scanner;

public class SymmetricMatrix {

        public static void main(String[] args) throws Exception
        {
            Scanner obj = new Scanner(System.in);

            int a[][] = new int[3][3];//3-rows and 3-cols

            int i,j,t;

            System.out.println("Enter matrix elements:");
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                    a[i][j] = obj.nextInt();
                }
            }
            System.out.println("matrix elements are:");
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                    System.out.print(a[i][j]+" ");
                }
                System.out.println();
            }
            boolean flag=true;
            
            for(i=0;i<3;i++)
            {
                for(j=0;j<3;j++)
                {
                    if(i==j && a[i][j]!=1)
                    {
                        flag=false;
                    }
                    
                    if(i!=j && a[i][j]!=0)
                    {
                        flag=false;
                        break;
                    }
                    
                }
            }
            System.out.println(flag);
    }

}
output:-
===========
Enter matrix elements:
1 0 0
0 1 0
0 0 1
matrix elements are:
1 0 0 
0 1 0 
0 0 1 
true
Enter matrix elements:
3 4 0
3 3 1
4 5 3
matrix elements are:
3 4 0 
3 3 1 
4 5 3 
false
==============
package com.app.matrix;

import java.util.Scanner;

public class TransposeArrayElements {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);

        int a[][]=new int[3][3];

        System.out.println("Enter Array Elements");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                a[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter Array Elements: ");
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                System.out.print(a[j][i]+" ");
            }
            System.out.println();
        }
    }
}
output:-
===========
Enter Array Elements
1 2 3
4 5 6
7 8 9
Enter Array Elements: 
1 4 7 
2 5 8 
3 6 9 
=================

package com.app.mergesort;

import java.util.Arrays;
import java.util.Random;

public class MergeSortAsc {

    static void mergeSortAsc(int[] a,int n)
    {
        if(n<2) //base condition
            return;
        int mid=n/2;
        int l[] = new int[mid];
        int r[] = new int[n-mid];
        int i;
        for(i=0;i<mid;i++)
            l[i]=a[i];
        for(i=mid;i<n;i++)
            r[i-mid]=a[i];
        mergeSortAsc(l,mid);
        mergeSortAsc(r,n-mid);
        merge(a,l,r,mid,n-mid);
    }
    static void merge(int a[],int l[],int r[],int left,int right){
        int i=0,j=0,k=0;
        while(i<left && j<right){
            if(l[i]<=r[j])
                a[k++]=l[i++];
            else
                a[k++]=r[j++];
        }
        while(i<left)
            a[k++]=l[i++];
        while(j<right)
            a[k++]=r[j++];
    }
}
class Test5562{
    public static void main(String[] args) {
        Random r=new Random();
        
        int[] a=new int[6];
        
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        
        System.out.println(Arrays.toString(a));
        MergeSortAsc.mergeSortAsc(a,a.length);
        System.out.println(Arrays.toString(a));
    }
}

output:-
========
[5, 3, 7, 2, 2, 9]
[2, 2, 3, 5, 7, 9]

===================

package com.app.mergesort;

import java.util.Arrays;
import java.util.Random;

public class MergeSortDesc {
    public static void mergeSortDesc(int[] a,int n)
    {
        if(n<2)
            return;
        
        int mid=n/2;
        int[] l=new int[mid];
        int[] r=new int[n-mid];
        int i;
        
        for(i=0;i<mid;i++)
            l[i]=a[i];
            for(i=mid;i<n;i++)
            
                r[i-mid]=a[i];
            
            mergeSortDesc(l,mid);
            mergeSortDesc(r,n-mid);
            merge(a,l,r,mid,n-mid);
    }
        public static void merge(int[] a,int l[],int r[],int left,int right)
        {
            int i=0,j=0,k=0;
            
            while(i<left && j<right)
            {
                if(l[i]>=r[j])
                    a[k++]=l[i++];
                else
                    a[k++]=r[j++];
                
            }
            while(i<left)
                a[k++]=l[i++];
            while(j<right)
                a[k++]=r[j++];
            
        }
    }

class Test67293{
    public static void main(String[] args) {
        Random r=new Random();
        int a[]=new int[5];
        
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        System.out.println(Arrays.toString(a));
        MergeSortDesc.mergeSortDesc(a,a.length);
        System.out.println(Arrays.toString(a));
    }
}
output:-
========
[3, 8, 3, 1, 7]
[8, 7, 3, 3, 1]
=====================

package com.app.palindrome;

public class PalindromeEx {
    public static void main(String[] args) {
        String s="madam";
        String s1=new StringBuffer(s).reverse().toString();
        System.out.println(s.equals(s1));
        }
}
output:-
=========
true

================
package com.app.pangram;

public class PangramEx {
    public static void main(String[] args) {
        String s="the quick brown fox jumps over the lazy dog";
        boolean flag=true;
        for(int i='a';i<='z';i++)
        {
            if(s.indexOf(i)<0)
            {
                flag=false;
                break;
            }
        }
        System.out.println(flag);
    }
}
output:-
===========
true

==============
package com.app.predefindequals;

import java.util.Arrays;

public class EqualArrayMethodUsing {
public static void main(String[] args) {
    int a[]= {1,2,3};
    int b[]= {4,5,6};
    int c[]= {1,2,3};
    int d[]= {1,3,2};
    System.out.println(Arrays.equals(a, b));
    System.out.println(Arrays.equals(a, c));
    System.out.println(Arrays.equals(a, d));
}
}
output:-
=========
false
true
false
==============

package com.app.predefindequals;

import java.util.Arrays;

public class UsingPredefineEquals {

    public static void main(String[] args) {

        int a[]= {1,2,3};
        int b[]= {4,5,6};
        int c[]= {1,2,3};
        int d[]= {1,3,2};
        Arrays.sort(a);
        Arrays.sort(b);
        Arrays.sort(c);
        Arrays.sort(d);
        System.out.println(Arrays.equals(a, b));
        System.out.println(Arrays.equals(a, c));
        System.out.println(Arrays.equals(a, d));
    }
}
output:-
========
false
true
true
=============

package com.app.quicksort;

import java.util.Arrays;
import java.util.Random;

public class QuickSortAsc {
    static void quickSortAsc(int[] a,int lIndex,int rIndex)
    {
        if(lIndex>=rIndex)//base condition
            return;
        int pivot, lp, rp, temp;
        pivot = a[rIndex];
        lp = lIndex;
        rp = rIndex;
        while(lp<rp){
            while(a[lp]<=pivot && lp<rp)
                lp++;
            while(a[rp]>=pivot && lp<rp)
                rp--;
            temp = a[lp];
            a[lp]=a[rp];
            a[rp]=temp;
        }
        temp = a[lp];
        a[lp] = a[rIndex];
        a[rIndex] = temp;

        quickSortAsc(a,lIndex,lp-1);
        quickSortAsc(a,lp+1,rIndex);
    }
}
class Test6676388{
    public static void main(String[] args) 
    {
        Random r = new Random();
        int[] a = new int[10];

        for(int i=0;i<a.length;i++)
        {
            a[i] = r.nextInt(100);
        }
        System.out.println("Before Sorting====> "+Arrays.toString(a));
        QuickSortAsc.quickSortAsc(a,0,a.length-1);
        System.out.println("After Sorting====> "+Arrays.toString(a));
    }
}

output:-
==========
Before Sorting====> [63, 16, 32, 13, 32, 0, 71, 30, 40, 52]
After Sorting====> [0, 13, 16, 30, 32, 32, 40, 52, 63, 71]

=============================

package com.app.quicksort;

import java.util.Arrays;
import java.util.Random;

public class QuickSortDesc {

    static void quickSortDesc(int[] a,int lIndex,int rIndex)
    {
        if(lIndex>=rIndex)//base condition
            return;
        int pivot, lp, rp, temp;
        pivot = a[rIndex];
        lp = lIndex;
        rp = rIndex;

        while(lp<rp){
            while(a[lp]>=pivot && lp<rp)
                lp++;
            while(a[rp]<=pivot && lp<rp)
                rp--;
            temp = a[lp];
            a[lp]=a[rp];
            a[rp]=temp;
        }
        temp = a[lp];
        a[lp] = a[rIndex];
        a[rIndex] = temp;

        quickSortDesc(a,lIndex,lp-1);
        quickSortDesc(a,lp+1,rIndex);
    }
}
class Test8983 
{
    public static void main(String[] args) 
    {
        Random r = new Random();
        int[] a = new int[10];

        for(int i=0;i<a.length;i++)
        {
            a[i] = r.nextInt(100);
        }
        System.out.println("Before Sorting====> "+Arrays.toString(a));
        QuickSortDesc.quickSortDesc(a,0,a.length-1);
        System.out.println("After Sorting====> "+Arrays.toString(a));
    }
}
output:-
==========
Before Sorting====> [72, 69, 4, 57, 47, 38, 53, 87, 44, 70]
After Sorting====> [87, 72, 70, 69, 57, 53, 47, 44, 38, 4]

==============================

package com.app.recursion;

import java.util.Scanner;

public class CalculatePower {
    public static int power(int a,int b)
    {
        if(b>=1)
        {
            return a*power(a,b-1);
        }
        else {
            return 1;
        }
    }
}
class Test6623{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("enter first Number");
        int a=sc.nextInt();
        System.out.println("enter second Number");
        int b=sc.nextInt();
        System.out.println(CalculatePower.power(a,b));
        sc.close();
        
    }
}
output:-
=========
enter first Number
2
enter second Number
3
8
===============

package com.app.recursion;

import java.util.Scanner;

public class CountNumbers {
    static int count=0;
    public static int count(int n)
    {
        if(n!=0)
        {
            count++;
            count(n/10);
        }
        return (count!=0?count:1);
    }
}
class Test65767{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number: ");
        int n=sc.nextInt();
        System.out.println(CountNumbers.count(n));
    }
}
output:-
=========
Enter the number: 
12345
5
========

package com.app.recursion;

import java.util.Scanner;

public class DecimalToBinary {
    public static int convert(int n)
    {
        if(n==0)
        {
            return 0;
        }else {
            return (n%2)+(10*convert(n/2));
        }
    }
}
class Test6989{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("enter the number :");
        int n=sc.nextInt();
        System.out.println(DecimalToBinary.convert(n));
        sc.close();
    }
}
output:-
=========
enter the number :
8
1000

=================
package com.app.recursion;

import java.util.Scanner;

public class FactorialExample {
    public static int fact(int n)
    {
        if(n==0)
        {
            return 1;
        }
        else {
            return n*fact(n-1);
        }
    }
}
class Test933{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the N value: ");
        int n=sc.nextInt();
        System.out.println(FactorialExample.fact(n));
        sc.close();
    }
}

output:-
=======
Enter the N value: 
6
720
===========

package com.app.recursion;

import java.util.Scanner;

public class FibonnaciSeries {
    public static int fib(int n)
    {
        if(n==0||n==1)  
        {
            return n;
        }else {
            return fib(n-1)+fib(n-2);
        }
    }
}
class Test77{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number: ");
        int n=sc.nextInt();
        System.out.println(FibonnaciSeries.fib(n));
    }
}

output:-
========
Enter the number: 
8
21
=========

package com.app.recursion;

import java.util.Scanner;

public class GCDExample {
    public static int gcd(int a,int b)
    {
        while(a!=b)
        {
            if(a>b)
            {
                return gcd(a-b,b);
            }else {
                return gcd(a,b-a);
            }
        }
        return a;
    }
}
class Test67677{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the n1 value: ");
        int n1=sc.nextInt();
        System.out.println("Enter the n2 value: ");
        int n2=sc.nextInt();
        System.out.println(GCDExample.gcd(n1,n2));
        sc.close();
    }
}
output:-
==========
Enter the n1 value: 
4
Enter the n2 value: 
8
4
==========

package com.app.recursion;

import java.util.Scanner;

public class LCMExample {
    static int com = 1;
    static int lcm(int n1,int n2)
    {
        if(com%n1==0 && com%n2==0)
            return com;
        com++;
        return lcm(n1,n2);      
    }
}
class Test78337{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the n1 number: ");
        int n1=sc.nextInt();
        System.out.println("Enter the n2 number: ");
        int n2=sc.nextInt();
        System.out.println(LCMExample.lcm(n1,n2));
    }
}
output:-
----------
Enter the n1 number: 
4
Enter the n2 number: 
8
8
==========

package com.app.recursion;

import java.util.Scanner;

public class NaturalNumberPrint {
    public static void print(int n)
    {
        if(n>=1)
        {
            System.out.println(n+" ");
            print(n-1);
        }
    }
}
class Test666{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the natural Number: ");
int n=sc.nextInt();
NaturalNumberPrint.print(n);

    }
}

output:-
=========
Enter the natural Number: 
5
5 
4 
3 
2 
1
===========

package com.app.recursion;

import java.util.Scanner;

public class PrimeNumber {
class Test6332{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number: ");
int n=sc.nextInt();
System.out.println(PrimeNumber.isPrime(n,n/2)?"Yes":"No");
sc.close();
    }
}
output:-
=========
Enter the number: 
7
Yes
==========

package com.app.recursion;

import java.util.Scanner;

public class PrimeNumberThree {

    public static boolean isPrime(int n,int i)
    {
        if(i==1)
        {
            return true;
        }else if (n%i==0) {
            return false;
        }
        else {
            return isPrime(n, --i);
        }
    }
}
class Test565673{
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number: ");
        int n=sc.nextInt();
        
        for(int i=2;i<=n;i++) {
            if(PrimeNumberThree.isPrime(i,i/2)) {
                System.out.print(i+",");
                sc.close();
            }
        }
    }
}
output:-
==========
Enter the number: 
10
2 3 5 7 
==============
package com.app.recursion;

import java.util.Scanner;

public class PrimeNumberTwo {
    public static boolean isPrime(int n,int i)
    {
        if(i==1)
        {
            return true;
        }
        else if (n%i==0) {
            return false;
        }
        else {
            return isPrime(n, --i);
        }
    }
}
    class Test787{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the number: ");
int n=sc.nextInt();
//System.out.println(PrimeNumberTwo.isPrime(n,n/2)?"Yes":"No");

for(int i=2;i<=n;i++)
{
    System.out.println(i+"====>"+PrimeNumber.isPrime(i, i/2));
    sc.close();
}
    }
}

output:-
========
Enter the number: 
10
2====>true
3====>true
4====>false
5====>true
6====>false
7====>true
8====>false
9====>false
10====>false
==============

package com.app.recursion;

import java.util.Scanner;

public class ProductOfTwoNumbers {
    public static int product(int a,int b)
    {
        if(a<b) 
        {
            return product(b, a);
        }else if (b!=0) {
            return a+product(a, b-1);
        }else {
            return 0;
        }
    }
}
class Test6673{
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("Enter the first number: ");
int a=sc.nextInt();
System.out.println("Enter the first number: ");
int b=sc.nextInt();
System.out.println(ProductOfTwoNumbers.product(a,b));
sc.close();
    }
}

output:-
=========
Enter the first number: 
4
Enter the first number: 
5
20
=========

package com.app.recursion;

import java.util.Scanner;

public class RecursiveCallFinate {

    static int c=1;
    public static void m() {
        if(c<=10) {
            System.out.println("Good Morning: "+c);
            c++;
            m();
        }
    }
}
class Test777{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        RecursiveCallFinate.m();
        sc.close();
    }
}

output:-
==========
Good Morning: 1
Good Morning: 2
Good Morning: 3
Good Morning: 4
Good Morning: 5
Good Morning: 6
Good Morning: 7
Good Morning: 8
Good Morning: 9
Good Morning: 10
==================

package com.app.recursion;

import java.util.Scanner;

public class RecursiveCallInFinate {
    public static void m() {
        System.out.println("Good Morning");
        m();
    }
}
class Test566{
    public static void main(String[] args) {
        Scanner obj=new Scanner(System.in);
        RecursiveCallInFinate.m();
        obj.close();
    }
}

output:-
==========
Good Morning
Good Morning
Good Morning
Good Morning
Good Morning
Good Morning
Good Morning
Exception in thread "main" java.lang.StackOverflowError
=======================

package com.app.recursion;

import java.util.Scanner;

public class ReverseOfGivenNumber {
    public static int reverse(int n,int length)
    {
        if(n==0)
        {
            return 0;
        }else {
            return ((n%10)*(int)Math.pow(10, length-1))+reverse(n/10, --length);
        }
    }
}
class Test56767{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the Number: ");
        String s=sc.nextLine();
        System.out.println(ReverseOfGivenNumber.reverse(Integer.parseInt(s),s.length()));
        sc.close();
    }
}
output:-
========
Enter the Number: 
54321
12345
==============

package com.app.recursion;

import java.util.Scanner;

public class SumOfDigits {
    public static int sumOfn(int n)
    {
        if(n==0)
        {
            return 0;
        }
        else
        {
            return (n%10)+sumOfn(n/10);
        }
    }
}
class Test6676{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number: ");
        int n=sc.nextInt();
        System.out.println(SumOfDigits.sumOfn(n));
        sc.close();
    }
}
output:-
=========
Enter the number: 
54321
15
=======

package com.app.recursion;

import java.util.Scanner;

public class SumOfNaturalNumber {
    public static int sum(int n)
    {
        if(n==1)
        {
            return 1;
        }
        else {
            return n+sum(n-1);
        }
    }
}
class Test778{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the Number: ");
        int n=sc.nextInt();
        System.out.println(SumOfNaturalNumber.sum(n));
        sc.close();
    }
}
output:-
=======
Enter the Number: 
6
21

==============

package com.app.recursion.strings;

import java.util.Scanner;

public class AdjacentCharacterFind {
    public static String news(String s,int index)
    {
        if(index<1)
        {
            return ""+s.charAt(index);
        }else
        {
            return news(s, index-1)+"*"+s.charAt(index);        }
    }
}
class Test083{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(AdjacentCharacterFind.news(s,s.length()-1));
        sc.close();
    }
}
output:-
==========
Enter the String: 
srinivas
s*r*i*n*i*v*a*s
================

package com.app.recursion.strings;

import java.util.Scanner;

public class AdjacentCharacterFindBetween {
    public static String adjent(String s,int index)
    {
        if(index<1)
        {
            return ""+s.charAt(index);
        }else if (s.charAt(index)==s.charAt(index-1)) {
            return adjent(s, index-1)+"*"+s.charAt(index);
        }else {
            return adjent(s, index-1)+s.charAt(index);
        }
    }
}
class Test573{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(AdjacentCharacterFindBetween.adjent(s,s.length()-1));
        sc.close();
    }
}
output:-
========
Enter the String: 
hello
hel*lo
============

package com.app.recursion.strings;

import java.util.Scanner;

public class CountNumbers {
    public static int count(String s,int ch,int index)
    {
        if(index<0)
        {
            return 0;
        }else if (s.charAt(index)=='x') {
            return 1+count(s, ch, index-1);
        }else {
            return count(s, ch, index-1);
        }
    }
}
class Test66{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(CountNumbers.count(s,'x',s.length()-1));
        sc.close();
    }
}
output:-
========
Enter the String: 
axbxcx
3
==========
package com.app.recursion.strings;

import java.util.Scanner;

public class CountNumbersNew {

    public static int count(String s,char ch,int index)
    {
        if(index<0)
        {
            return 0;
        }else if (s.charAt(index)==ch) {
            return 1+count(s, ch, index-1);
        }else {
            return count(s, ch, index-1);
        }
    }
}
class Test{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter any String: ");
        String s=sc.nextLine();
        System.out.println(CountNumbersNew.count(s,'x',s.length()-1));
        sc.close();
    }
}
output:-
========
Enter any String: 
axbxcxdxrx
5
================

package com.app.recursion.strings;

import java.util.Scanner;

public class Pair {
    public static boolean validateOrNot(String s,int i,int j)
    {
        if(i>j)//4>3 
        {
            return true;
        }else if (s.charAt(i)=='(' && s.charAt(j)==')') {
            return validateOrNot(s, i+1, j-1);
        }
        else {
            return false;
        }
    }
}
class Test7878{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(Pair.validateOrNot(s,0,s.length()-1));
        sc.close();
        }
}

output:-
=========
Enter the String: 
(())
true
========

package com.app.recursion.strings;

import java.util.Scanner;

public class RemoveCharacter {
    public static String strremove(String s,int index)
    {
        if(index<0)
        {
            return "";
        }else if (s.charAt(index)=='x') {
            return strremove(s, index-1);
        }
        else {
            return strremove(s, index-1)+s.charAt(index);
        }
    }   
}
class Test66672{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(RemoveCharacter.strremove(s,s.length()-1));
        sc.close();
    }
}
output:-
=========
Enter the String: 
axbxcx
abc
==============

package com.app.recursion.strings;

import java.util.Scanner;

public class ReplaceFunctionExample {
    public static String replace(String s,int index)
    {
        if(index<0)
        {
            return "";
        }
        else if (s.charAt(index)=='x') {
            return replace(s, index-1)+"y";
        }else {
            return replace(s, index-1)+s.charAt(index);
        }
    }
}
class Test5563{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter any String: ");
        String s=sc.nextLine();
        System.out.println(ReplaceFunctionExample.replace(s,s.length()-1));
        sc.close();
    }
}

output:-
=========
Enter any String: 
axbxcxdx
aybycydy
============

package com.app.recursion.strings;

import java.util.Scanner;

public class ReplaceNewStringPiExample {

    public static String replacenew(String s,int index)
    {
        if(index<1)
        {
            return s.substring(0,index+1);
        }else if (s.substring(index-1, index+1).equals("pi")) {
            return replacenew(s, index-2)+"3.147";
        }else {
            return replacenew(s, index-1)+s.charAt(index);
        }
    }
}
class Test89893{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(ReplaceNewStringPiExample.replacenew(s,s.length()-1));
        sc.close();
    }
}
output:-
=========
Enter the String: 
apiy
a3.147y
===========

package com.app.recursion.strings;

import java.util.Scanner;
public class ReverseString {
    public static String revStr(String s)
    {
    if(s==null||s.length()<=1)
    {
        return s;
    }
    else {
        return revStr(s.substring(1))+s.charAt(0);
    }
}
}
class Test67633{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the String: ");
        String s=sc.nextLine();
        System.out.println(ReverseString.revStr(s));
        sc.close();
    }
}
output:-
=========
Enter the String: 
srinivas
savinirs
=============

package com.app.recursion.strings;

import java.util.Scanner;

public class TowersOfHanoi {
    static void towersOfHanoi(int n,String src,String temp,String dest)
    {
        if(n==1)
        {
            System.out.println("Move the Disk "+n+" from "+src+" to "+dest);
            return;
        }
        towersOfHanoi(n-1,src,dest,temp);
        System.out.println("Move the Disk "+n+" from "+src+" to "+dest);
        towersOfHanoi(n-1,temp,src,dest);
    }
}
class Test5565{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter no of disks: ");
        int n=sc.nextInt();
        TowersOfHanoi.towersOfHanoi(n,"S","T","D");
        sc.close();
    }
}
output:-
=========
Enter no of disks: 
2
Move the Disk 1 from S to T
Move the Disk 2 from S to D
Move the Disk 1 from T to D
=========================

package com.app.regexp;

import java.util.Scanner;

public class ATMPin {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter atm pin");
        String s=sc.nextLine();
        System.out.println(s.matches("[0-9]{4}"));
        sc.close();
    }
}
output:-
==========
Enter atm pin
4524
true
Enter atm pin
443
false
================

package com.app.regexp;

import java.util.Scanner;

public class BikeRegistration {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your bike num: ");
        String s=sc.nextLine();
        System.out.println(s.matches("TS[0-2][0-9][A-Z]{1}[0-9]{4}"));
        sc.close();
    }
}

output:-
=========
Enter your bike num: 
TS17E6046
true
Enter your bike num: 
TS17ER2
false
=============
package com.app.regexp;

public class ConsonantCount {
    public static void main(String[] args) {
        String s="welcome";
        System.out.println(s);
        int c=0;
        for(int i=0;i<s.length();i++)
        {
            char ch=s.charAt(i);
            
            if(!(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u'))
            {
                c++;
            }   
        }
        System.out.println("Consonents: "+c);
    }
}
output:-
==========
welcome
Consonents: 4
=============

package com.app.regexp;

public class Consonants {
public static void main(String[] args) {
    String s="welcome";
    System.out.println(s);
    for(int i=0;i<s.length();i++)
    {
        char ch=s.charAt(i);
        if(!(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u')) {
            System.out.println(ch);
        }
    }}
}
output:-
========
welcome
w
l
c
m
==============

package com.app.regexp;

public class EvenExample {
    public static void main(String[] args) {
        String s="welcome";
        System.out.println(s);
        for(int i=0;i<s.length();i++)
        {
            if(i%2==0)
            {
                System.out.println("index: "+i+" and char "+s.charAt(i));
            }
        }
    }
}
output:-
==========
welcome
index: 0 and char w
index: 2 and char l
index: 4 and char o
index: 6 and char e
====================

package com.app.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Regular Expressions and its applications
--------------------------------------------
a group of strings according to a particular pattern or format is called as regular exp

steps to prepare re object
--------------------------
1) import java.util.regex.*;
2) pattern object ------------> format for data
3) matcher object ------------> target string is in format

Validate Mobile Number:
-----------------------
    123 ------------> No
    1234567890 -----> No
    9123456789 -----> Yes

    format: [6-9][0-9]{9}

predefined character classes
----------------------------
\\s             space character
\\S             except space character
\\d             digit
\\D             except digit
\\w             word character (a-z,A-Z,0-9)
\\W             except word character (spaces and special characters)

Ex:
---
Pattern p = Pattern.compile("\\s");
Pattern p = Pattern.compile("\\S");
Pattern p = Pattern.compile("\\d");
Pattern p = Pattern.compile("\\D");
Pattern p = Pattern.compile("\\w");
Pattern p = Pattern.compile("\\W");
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class ExceptWordCharacterRegExpres {
    public static void main(String[] args) {
    
Pattern p=Pattern.compile("\\W");//regular expression format
Matcher m=p.matcher("ab c$123#i Jk^45 6*pQr @ wXYz");//target String

int c=0;

while(m.find())
{
    System.out.println(m.start()+"==============>"+m.end());
    c++;
}
        System.out.println(c);
    }
}

output:-
===========
2==============>3
4==============>5
8==============>9
10==============>11
13==============>14
16==============>17
18==============>19
22==============>23
23==============>24
24==============>25
10

====================

package com.app.regexp;

import java.util.Scanner;

public class GivenDateRegEx {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter date value");
        String s=sc.nextLine();
        System.out.println(s.matches("[0-3][0-9]-[0-1][012]-202[0-9]"));//30-01-2023
        sc.close();
    }
}

output:-
========
Enter date value
30-01-2023
true
==================

package com.app.regexp;

import java.util.Scanner;

public class GmailPattern {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter gmail Id: ");
        String s=sc.nextLine();
        System.out.println(s.matches("[a-z][a-zA-Z0-9][a-zA-Z0-9]+@gmail[.]com"));
        sc.close();
    }
}

output:-
===========
Enter gmail Id: 
srinivas@gmail.com
true
================

package com.app.regexp;

public class IndexValuePrint {
    public static void main(String[] args) {
        String s="welcome";
        System.out.println(s);
        for(int i=0;i<s.length();i++)
        {
            System.out.println("Index: "+i+" and char:"+s.charAt(i));
        }
    }
}
output:-
============
welcome
Index: 0 and char:w
Index: 1 and char:e
Index: 2 and char:l
Index: 3 and char:c
Index: 4 and char:o
Index: 5 and char:m
Index: 6 and char:e
===================

package com.app.regexp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MultipleAMatching {
    public static void main(String[] args) {
         Pattern p=Pattern.compile("a{3,6}");
         Matcher m=p.matcher("abaabaaabaaaabaaaaabaaaaaab");
         int c=0;
         while(m.find())
         {
             System.out.println(m.start()+" "+m.end()+" "+m.group());
             c++;
         }
                 System.out.println(c);
    }
}
output:-
============
5 8 aaa
9 13 aaaa
14 19 aaaaa
20 26 aaaaaa
4
============
package com.app.regexp;

public class OddExample {
public static void main(String[] args) {
    String s="welcome";
    System.out.println(s);
    for(int i=0;i<s.length();i++)
    {
        if(i%2!=0)
        {
            System.out.println("index: "+i+" and char: "+s.charAt(i));
        }
    }
}
}
output:-
==========
welcome
index: 1 and char: e
index: 3 and char: c
index: 5 and char: m
==============
package com.app.regexp;

import java.util.Scanner;

public class StudentUniversity {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your Id: ");
        String s=sc.nextLine();
        System.out.println(s.matches("DS[0-9]{4}"));
        sc.close();
    }
}
output:-
=======
Enter your Id: 
DS2441
true
Enter your Id: 
DS78
false
=========
package com.app.regexp;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        String s="welcome";
        char[] ch=s.toCharArray();
        System.out.println(ch);
        System.out.println(Arrays.toString(ch));
        Arrays.sort(ch);
        System.out.println(Arrays.toString(ch));    
        String ss=new String(ch);
        System.out.println(ss);
    }
}
output:-
=========
welcome
[w, e, l, c, o, m, e]
[c, e, e, l, m, o, w]
ceelmow
===============
package com.app.regexp;

import java.util.Arrays;

public class Test1 {
    public static void main(String[] args) {
        String s = "welcome";
        char[] ch = s.toCharArray();
        System.out.println(s);
        System.out.println(Arrays.toString(ch));
        Arrays.sort(ch);
        //manual logic
        char[] ch2 = new char[ch.length];
        int j=0;
        for(int i=ch.length-1;i>=0;i--)
            ch2[j++]=ch[i];
        System.out.println(Arrays.toString(ch2));
        String ss = new String(ch2);
        System.out.println(ss);
    }
}

output:-
==========
welcome
[w, e, l, c, o, m, e]
[w, o, m, l, e, e, c]
womleec
====================
package com.app.regexp;

public class VowelCount {
public static void main(String[] args) {
    String s="welcome";
    System.out.println(s);
    int count=0;
    for(int i=0;i<s.length();i++)
    {
        char ch=s.charAt(i);
        if(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u')
        {
            count++;
        }
    }
    System.out.println("No of vowels: "+count);
}
}
output:-
============
welcome
No of vowels: 3
===================
package com.app.regexp;

public class Vowels {
    public static void main(String[] args) {
        String s="welcome";
        System.out.println(s);
        for(int i=0;i<s.length();i++)
        {
            char ch=s.charAt(i);
            if(ch=='a'|| ch=='e'||ch=='i'||ch=='o'||ch=='u') {
                System.out.println(ch);
            }
        }
    }
}
output:-
============
welcome
e
o
e
==================
package com.app.removeduplicate;

import java.util.Scanner;

public class RemoveDuplicateCharacter {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter String: ");
        String s=sc.nextLine();
        String ss="";
        for(int i=0;i<s.length();i++)
        {
            if(ss.indexOf(s.charAt(i))<0)
            {
                ss=ss+s.charAt(i);
            }
        }
        System.out.println("Remove duplicates: "+ss);
        sc.close();
    }
}
output:-
=========
Enter String: 
srinivas
Remove duplicates: srinva
Enter String: 
hello
Remove duplicates: helo
========================
package com.app.removespecialcharacter;

import java.util.Scanner;

public class RemoveSpecialCharacter {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Special Character String: ");
        String s=sc.nextLine();
        String ss="";
        for(int i=0;i<s.length();i++)
        {
            char ch=s.charAt(i);
            
            if((ch>='a'&&ch<='z')||(ch>='A'&&ch<='Z')|| (ch>='0'&&ch<='9'))
            {
                ss=ss+ch;
            }
        }
        System.out.println(ss);
        sc.close();
    }
}

output:-
==========
Enter Special Character String: 
java&*programming
javaprogramming
===================
package com.app.returnmiddlechar;

import java.util.Scanner;

public class ReturnMiddleChar {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter String: ");
        String s=sc.nextLine();
        int n=s.length();
        int mid=n/2;
        if(n%2==0)
        {
            System.out.println(s.charAt(mid-1)+""+s.charAt(mid));
        }else {
            System.out.println(s.charAt(mid));
        }
        sc.close();
    }
}

output:-
========
Enter String: 
srinivas
ni
==================
package com.app.reverse;

public class ReverseStringExample {
    public static void main(String[] args) {
        String s="the quick brown fox jumps over the lazy dog";
        System.out.println(s);
        System.out.println(new StringBuffer(s).reverse());
    }
}
output:-
=========
the quick brown fox jumps over the lazy dog
god yzal eht revo spmuj xof nworb kciuq eht
==============================
package com.app.rotateshift;

import java.util.Scanner;

public class Rotateshift {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter String: ");
        String s=sc.nextLine();
        String ss=sc.nextLine();
        System.out.println((s+s).contains(ss));
        sc.close();
    }
}
output:-
========
Enter String: 
srinivas
srini
true
Enter String: 
srinivas
srinu
false
====================
package com.app.searching;

import java.util.Arrays;
import java.util.Scanner;

public class FindLocationUsingLowHighMid {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter an Array Size: ");
        int n=sc.nextInt();
        int[] a=new int[n];
        System.out.println("Enter the array elements : '"+n+"' ");
        for(int i=0;i<n;i++) {
        a[i]=sc.nextInt();
        }
        System.out.println("Array Before sorting: "+Arrays.toString(a));
        Arrays.sort(a);
        System.out.println("Array After sorting: "+Arrays.toString(a));
        System.out.println("Enter key to search: ");
        int key=sc.nextInt();
        int low,high,mid;
        low=0;
        high=n-1;
        int index=-1;
        while(low<=high)
            
        {
            mid=(low+high)/2;
            if(a[mid]==key)
            {
                index=mid;
                break;
            }else if (a[mid]>key) {
                high=mid-1;
            }else {
                low=mid+1;
            }   
        }
        System.out.println("Location: "+index);
        sc.close();
    }
}
output:-
===========
Enter an Array Size: 
5
Enter the array elements : '5' 
5 3 1 7 8
Array Before sorting: [5, 3, 1, 7, 8]
Array After sorting: [1, 3, 5, 7, 8]
Enter key to search: 
5
Location: 2
================
package com.app.searching;

import java.util.Arrays;
import java.util.Scanner;

public class KeyToIndexArrayExample {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter an Array Size: ");
        int n=sc.nextInt();
        int a[]=new int[n];
        System.out.println("Enter '"+n+"' elements");
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        System.out.println("Array Before sorting: "+Arrays.toString(a));
        System.out.println("Enter element key to search");
        int key=sc.nextInt();
        int index=-1;
        for(int i=0;i<n;i++)
        {
            if(key==a[i])
            {
                index=i;
                break;
            }
        }
        System.out.println("Location: "+index);
        sc.close();
    }
}
output:-
============
Enter an Array Size: 
5
Enter '5' elements
4 5 2 3 1
Array Before sorting: [4, 5, 2, 3, 1]
Enter element key to search
5
Location: 1
====================
package com.app.searchinglinearbinary;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class LinearSearch {
    public static int linearSearch(int a[],int key)
    {
        int i;
        for(i=0;i<a.length;i++)
        {
            if(key==a[i])
            {
                return i;
            }
        }
        return -1;
    }
}
class Test
{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Random r=new Random();
        int a[]=new int[5];

        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        System.out.println("Array Elements: "+Arrays.toString(a));
        System.out.println("Enter key element");
        int key=sc.nextInt();
        System.out.println(LinearSearch.linearSearch(a,key));
    }
}
output:-
=========
Array Elements: [6, 6, 4, 2, 8]
Enter key element
4
2
=====================
package com.app.searchinglinearbinary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LinearSearchAllMatch {

    public static ArrayList<Integer> linearSearchAllMatch(int a[],int key)
    {
        ArrayList<Integer> al=new ArrayList<Integer>();
        
        for(int i=0;i<a.length;i++)
        {
            if(key==a[i])
            {
                al.add(i);
            }
        }
        return al;
    }
}
class Test34235{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        
        int a[]={10,11,12,13,11,12,14,12,15,16,12,17};
        System.out.println("Arrays Elements: "+Arrays.toString(a));
        System.out.println("Enter key elements");
        int key=sc.nextInt();
        System.out.println(LinearSearchAllMatch.linearSearchAllMatch(a,key));
        sc.close();
    }
}
output:-
===========
Arrays Elements: [10, 11, 12, 13, 11, 12, 14, 12, 15, 16, 12, 17]
Enter key elements
11
[1, 4]
=====================
package com.app.searchinglinearbinary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LinearSearchThreeMatching {

    public static ArrayList<Integer> linearSearchThreeMatching(int []a,int key)
    {
        int c=0;
        ArrayList<Integer> al=new ArrayList<Integer>();
        
        for(int i=0;i<a.length;i++)
        {
            if(key==a[i])
            {
                c++;
                al.add(i);
                if(c==3)
                {
                break;
                }
            }
        }
        return al;
    }
}
class Test6623{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a[]= {10,11,12,13,11,12,14,12,15,16,12,17,12,12,33,12};
        System.out.println("Arrays elements: "+Arrays.toString(a));
        System.out.println("Enter key elements");
        int key=sc.nextInt();
        System.out.println(LinearSearchThreeMatching.linearSearchThreeMatching(a,key));
        sc.close();
    }
}
output:-
===========
Arrays elements: [10, 11, 12, 13, 11, 12, 14, 12, 15, 16, 12, 17, 12, 12, 33, 12]
Enter key elements
12
[2, 5, 7]
==================
package com.app.searchinglinearbinary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LinearSearchTwoMatching {

    public static ArrayList<Integer> linearSearchTwoMatching(int a[],int key)
    {
        int c=0;
        ArrayList<Integer> al=new ArrayList<Integer>();
        for(int i=0;i<a.length;i++)
        {
            if(key==a[i])
            {
                c++;
                al.add(i);
                if(c==2)
                {
                    break;
                }
            }
        }
        return al;      
    }
}
class Test555{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);      
        int a[]= {10,11,12,13,11,12,14,12,15,16,12,17};
        System.out.println("Arrays Elements:"+Arrays.toString(a));
        System.out.println("Enter key elements");
        int key=sc.nextInt();
        System.out.println(LinearSearchTwoMatching.linearSearchTwoMatching(a,key));
        sc.close();
    }
}
output:-
===========
Arrays Elements:[10, 11, 12, 13, 11, 12, 14, 12, 15, 16, 12, 17]
Enter key elements
12
[2, 5]
========================

package com.app.selectionsort;

import java.util.Arrays;
import java.util.Random;

public class SelectionSortAsc {
    public static void selectionSort(int[] a)
    {
        int i,j,min,temp,n=a.length;
        for(i=0;i<n-1;i++)
        {
            min=i;
            
            
            for(j=i+1;j<n;j++)
            {
                if(a[j]<a[min])
                {
                    min=j;
                }
                    if(min!=i) {
                        temp=a[i];
                        a[i]=a[min];
                        a[min]=temp;
                        
                        System.out.println(min);
                        System.out.println(temp+" "+a[i]+" "+a[j]);
                        
                    }
            }   
        }   
    }
}
class Test66673{
    public static void main(String[] args) {
        Random r=new Random();
        int[] a=new int[5];
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        System.out.println(Arrays.toString(a));
      SelectionSortAsc.selectionSort(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
===========
[2, 2, 1, 8, 0]
2
2 1 2
2
1 2 8
4
2 0 2
2
2 1 2
2
1 2 8
2
2 1 2
4
8 2 8
[0, 1, 2, 2, 8]
======================
package com.app.selectionsort;

import java.util.Arrays;
import java.util.Random;

public class SelectionSortDesc {
    public static void selectionDesc(int[] a)
    {
        int i,j,temp,max,n=a.length;
        for(i=0;i<n-1;i++)
        {
            max=i;
            for(j=i+1;j<n;j++)
            {
                if(a[j]>a[max])
                max=j;
                }       
            if(max!=i)
            {
                temp=a[i];
                a[i]=a[max];
                a[max]=temp;
            }   
        }   
    }
}
class Test67778{
    public static void main(String[] args) {
        Random r=new Random();
        int[] a=new int[5];
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        System.out.println(Arrays.toString(a));
        SelectionSortDesc.selectionDesc(a);
        System.out.println(Arrays.toString(a));
    }
}
output:-
=========
[2, 8, 0, 4, 4]
[8, 4, 4, 2, 0]
======================

package com.app.shellsort;

import java.util.Arrays;
import java.util.Random;

public class ShellSortAsc {
    public static void shellAsc(int a[],int n)
    {
        int gap,i,j,temp;
        for(gap=n/2;gap>=1;gap=gap/2)
        {
            for(j=gap;j<n;j++)
            {
                for(i=j-gap;i>=0;i=i-gap)
                {
                    if(a[i+gap]>a[i])
                        break;
                    else
                    {
                        temp = a[i+gap];
                        a[i+gap] = a[i];
                        a[i] = temp;
                    }
                }
            }
        }   
    }   
}
class Test55673{
    public static void main(String[] args) {
        Random r=new Random();
        int[] a=new int[5];
        for(int i=0;i<a.length;i++)
        {
            a[i]=r.nextInt(10);
        }
        System.out.println(Arrays.toString(a));
        ShellSortAsc.shellAsc(a,a.length);
        System.out.println(Arrays.toString(a));
    }
}
output:-
========
[3, 1, 9, 2, 0]
[0, 1, 2, 3, 9]
==================
package com.app.shellsort;

import java.util.Arrays;
import java.util.Random;

public class shellSortDesc {
    static void shellSortDescending(int[] a,int n)
    {
        int gap,i,j,temp;
        for(gap=n/2;gap>=1;gap=gap/2)
        {
            for(j=gap;j<n;j++)
            {
                for(i=j-gap;i>=0;i=i-gap)
                {
                    if(a[i+gap]<a[i])
                        break;
                    else
                    {
                        temp = a[i+gap];
                        a[i+gap] = a[i];
                        a[i] = temp;
                    }
                }
            }
        }   
    }   
}
class Test5663{
    public static void main(String[] args) 
    {
        Random r = new Random();
        int[] a = new int[10];

        for(int i=0;i<a.length;i++)
        {
            a[i] = r.nextInt(100);
        }
        System.out.println("Before Sorting====> "+Arrays.toString(a));
        shellSortDesc.shellSortDescending(a,a.length);
        System.out.println("After Sorting====> "+Arrays.toString(a));
    }
}
output:-
==========
Before Sorting====> [91, 23, 71, 49, 29, 18, 90, 17, 49, 83]
After Sorting====> [91, 90, 83, 71, 49, 49, 29, 23, 18, 17]
=====================================
package com.app.sorting;

import java.util.Arrays;
import java.util.Random;

public class RandomExample {
public static void main(String[] args) {
    Random r=new Random();
    int[] a=new int[10];
    for(int i=0;i<a.length;i++)
    {
        a[i]=r.nextInt(100);
    }
    System.out.println(Arrays.toString(a));
}
}
output:-
===========
[65, 48, 7, 74, 95, 21, 7, 23, 48, 95]
=========================
package com.app.stringbuffer;

public class StringBufferCapacity {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer();
     System.out.println(sb.length());
     System.out.println(sb.capacity());
    }
}

output:-
===========
0
16
===========
package com.app.stringbuffer;

public class StringBufferCapacityNew {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer(50);
        System.out.println(sb.length());
        System.out.println(sb.capacity());
    }
}

output:-
=========
0
50
================
package com.app.stringbuffer;

public class StringBufferCharAt {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer("welkomae");
        System.out.println(sb);
        System.out.println(sb.charAt(3));
        System.out.println(sb);
        sb.setCharAt(3, 'c');
        System.out.println(sb);
        sb.deleteCharAt(6);
        System.out.println(sb);
    }
}
output:-
==========
welkomae
k
welkomae
welcomae
welcome
============
package com.app.stringbuffer;

public class StringBufferExample344 {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer("welcome");
        System.out.println(sb);//welcome
        System.out.println(sb.length());
        System.out.println(sb.capacity());
        System.out.println("********");
        sb.setLength(2);
        System.out.println(sb);
        System.out.println(sb.length());
        System.out.println(sb.capacity());
        System.out.println("********");
        sb.trimToSize();
        System.out.println(sb);
        System.out.println(sb.length());
        System.out.println(sb.capacity());
        System.out.println("********");
        sb.ensureCapacity(55);
        System.out.println(sb);
        System.out.println(sb.length());
        System.out.println(sb.capacity());
    }
}
output:-
===========
welcome
7
23
********
we
2
23
********
we
2
2
********
we
2
55
=========================
package com.app.stringbuffer;

public class StringBufferFirstExample {
    public static void main(String[] args) {
        StringBuffer s=new StringBuffer("abc");
    s.append("def");
    System.out.println(s);
    }
}
output:-
========
abcdef
============
package com.app.stringbuffer;

public class StringBufferInsertData {
public static void main(String[] args) {
    StringBuffer sb = new StringBuffer();
    sb.append("python ");
    sb.append("and ");
    sb.append("java ");
    sb.append("programming");
    System.out.println(sb);//python and java programming
    sb.insert(0,"welcome ");
    System.out.println(sb);//welcome python and java programming
    sb.insert(8,"to ");
    System.out.println(sb);//welcome to python and java programming
    sb.delete(11,22);// 11th char to 21st char
    System.out.println(sb);//welcome to java programming
}
}
output:-
==========
python and java programming
welcome python and java programming
welcome to python and java programming
welcome to java programming
=================================
package com.app.stringbuffer;

public class StringBufferReverse {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer("welcome");
        System.out.println(sb);
        sb.reverse();
        System.out.println(sb);
    }
}
output:-
============
welcome
emoclew
=============
package com.app.stringbuffer;

public class StringBufferSecondExample {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer("abc");
        System.out.println(sb);//abc
        System.out.println(sb.length());//3
        System.out.println(sb.capacity());//19
    }
}
output:-
===========
abc
3
19
===========
package com.app.stringbuffer;

public class StringMaxCapacity {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer();
        System.out.println(sb.length());//0
        System.out.println(sb.capacity());//16
        System.out.println("*************");
        sb.append("abcdefghijklmnop");//abcdefghijklmnop
        System.out.println(sb);//16
        System.out.println(sb.length());//16
        System.out.println(sb.capacity());//16
        sb.append("q");
        System.out.println(sb);//abcdefghijklmnopq
        System.out.println(sb.length());//17
        System.out.println(sb.capacity());//34
    }
}
output:-
==========
0
16
*************
abcdefghijklmnop
16
16
abcdefghijklmnopq
17
34
=========================
package com.app.strings;

public class Employee {
    private int empId;
    private String empName;
    public Employee() {
        super();
    }
    public Employee(int empId, String empName) {
        super();
        this.empId = empId;
        this.empName = empName;
    }
    public int getEmpId() {
        return empId;
    }
    public void setEmpId(int empId) {
        this.empId = empId;
    }
    public String getEmpName() {
        return empName;
    }
    public void setEmpName(String empName) {
        this.empName = empName;
    }
    @Override
    public String toString() {
        return "Employee [empId=" + empId + ", empName=" + empName + "]";
    }
}

package com.app.strings;

public class Test {
    public static void main(String[] args) {
        Employee e1=new Employee(10, "Srinivas");
        Employee e2=new Employee(10, "Srinivas");
        Employee e3=e1;
        System.out.println(e1==e2);
        System.out.println(e1.equals(e2));
        
        System.out.println(e1==e3);
        System.out.println(e1.equals(e3));
    }

}
output:-
=======
false
false
true
true
================
package com.app.strings;

public class HeapScpExample {
public static void main(String[] args) {
    String s1=new String("abc");
    String s2=new String("abc");
    String s3="abc";
    String s4="abc";
    System.out.println(s1==s2);
    System.out.println(s3==s4);
    System.out.println(s1==s3);
    System.out.println(s1==s4);
    System.out.println("***********************");
    StringBuffer s11=new StringBuffer("abc");
    StringBuffer s21=new StringBuffer("abc");
    StringBuffer s31=s11;
    System.out.println(s11==s21);
    System.out.println(s31==s11);   
}
}
output:-
==========
false
true
false
false
***********************
false
true
===========
package com.app.strings;

public class InternExample {
    public static void main(String[] args) {
        String s1=new String("abc");
        String s2=s1.intern();
        String s3="abc";
        System.out.println(s1==s2);//false
        System.out.println(s2==s1);//false
        System.out.println(s2==s3);//true
    }
}
output:-
==========
false
false
true
============
package com.app.strings;

public class StartWithString {
public static void main(String[] args) {
    String s=new String("java is simple very easy");
    System.out.println(s);
    System.out.println("*******");
    System.out.println(s.startsWith("java"));
    System.out.println("*******");
    System.out.println(s.startsWith("is"));
    System.out.println("*******");
    System.out.println(s.startsWith("his"));
    System.out.println("*******");
    System.out.println(s.endsWith("abc"));
    System.out.println("*******");
    System.out.println(s.endsWith("easy"));
}
}
output:-
=========
java is simple very easy
*******
true
*******
false
*******
false
*******
false
*******
true
=============

package com.app.strings;

public class StringBufferExample
{
public static void main(String[] args) {
    StringBuffer sb1=new StringBuffer("abcd");
    StringBuffer sb2=new StringBuffer("ABCD");
    String s1=new String(sb1);
    String s2=new String(sb2);
    System.out.println(sb1);
    System.out.println(sb2);
    System.out.println(s1);
    System.out.println(s2);
    String sb11=new String("abcd");
    String sb21=new String("ABCD");
    StringBuffer s11=new StringBuffer(sb1);
    StringBuffer s21=new StringBuffer(sb2);
    System.out.println(sb11);
    System.out.println(sb21);
    System.out.println(s11);
    System.out.println(s21);
}
}
output:-
=========
abcd
ABCD
abcd
ABCD
abcd
ABCD
abcd
ABCD
============
package com.app.strings;

public class StringBufferMutable {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer("Srinivas ");
        sb.append("Vadla");
        System.out.println(sb);
    }
}

output:-
========
Srinivas Vadla
=================
package com.app.strings;

public class StringBufferRefEqualComparison {
public static void main(String[] args) {
    StringBuffer s1=new StringBuffer("srinivas");
    StringBuffer s2=new StringBuffer("srinivas");
    StringBuffer s3=s1;
    System.out.println(s1==s2);//false
    System.out.println(s1.equals(s2));//false
    System.out.println(s1==s3);//true
    System.out.println(s1.equals(s3));//true
}
}
output:-
=========
false
false
true
true
=============
package com.app.strings;

public class StringByteArray {
    public static void main(String[] args) {
        byte[] b= {65,66,67,68,97,98,99};
        String s1=new String(b);
        String s2=new String(b,0,4);
        String s3=new String(b,4,3);
        System.out.println(s1);//ABCDabc
        System.out.println(s2);//ABCD
        System.out.println(s3);//abc
    }
}

output:-
===========
ABCDabc
ABCD
abc
============
package com.app.strings;

public class StringCharArrayEx {
    public static void main(String[] args) {
        char ch[]= {'w','e','l','c','o','m','e'};
        //           0   1   2   3   4   5   6
        String s1=new String(ch);
        String s2=new String(ch,3,4);
        String s3=new String(ch,0,2);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
    }
}
output:-
===========
welcome
come
we
==============
package com.app.strings;

import java.util.Arrays;

public class StringCharArrayExample {
    public static void main(String[] args) {
        String s=new String("srinivas");
        System.out.println(s);
        byte[] b=s.getBytes();
        System.out.println(Arrays.toString(b));
    }
}
output:-
=========
srinivas
[115, 114, 105, 110, 105, 118, 97, 115]
====================================
package com.app.strings;

public class StringCharAtUsing {
    public static void main(String[] args) {
        String s=new String("abc");
        System.out.println(s);
        System.out.println(s.charAt(0));
        System.out.println(s.charAt(1));
        System.out.println(s.charAt(2));
        //System.out.println(s.charAt(3));//Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range: 3  
    }
}
output:-
=========
abc
a
b
c
=============
package com.app.strings;

public class StringConcatExample {
    public static void main(String[] args) {
        String s="java";
        System.out.println(s);
        System.out.println(s.concat("python"));
    }
}
output:-
=========
java
javapython
=============
package com.app.strings;

public class StringEqualAndIgnoreCase {
    public static void main(String[] args) {
        System.out.println("abc".equals("abc"));
        System.out.println("abc".equals("BAC"));
        System.out.println("abc".equalsIgnoreCase("ABC"));
        System.out.println("abc".equals("BBB"));
    }
}
output:-
============
true
false
true
false
==============
package com.app.strings;

public class StringImMutableExample {
    /*
     * String ---------------> immutable obj StringBuffer ---------> mutable obj at
     * a time only one obj is allowed StringBuilder --------> mutable obj at a time
     * any number of obj's allowed StringTokenizer ------> divide the given str into
     * tokens
     * 
     * Reference and Content comparision ---------------------------------
     * primitives --------> == for content comparision, we dn't have reference
     * comparision objects -----------> == for ref comparision and .equals() for ref
     * comprision
     */
    public static void main(String[] args) {
        
        String s=new String("Srinivas");
        s.concat("Vadla");
        System.out.println(s);
    }
}
output:-
========
Srinivas
=============
package com.app.strings;

public class StringIndexOf {
    public static void main(String[] args) {
        String s1="welcome";
        System.out.println(s1.indexOf('e'));//1
        System.out.println(s1.lastIndexOf('e'));//6
    }
}
output:-
========
1
6
==============
package com.app.strings;

public class StringInternExample {
    public static void main(String[] args) {
        String s1=new String("abc");
        String s2=s1.concat("def");
        System.out.println(s2);//abcdef
        String s3=s2.intern();
        System.out.println(s3);//abcdef
        String s4="abcdef";
        System.out.println(s4);//abcdef
        System.out.println(s3==s4);//true
    }
}
output:-
===========
abcdef
abcdef
abcdef
true
==============
package com.app.strings;

public class StringLength {
    public static void main(String[] args) {
        String s1=new String();
        System.out.println(s1.length());
        System.out.println(s1.isEmpty());
    }
}
output:-
========
0
true
==========
package com.app.strings;

public class StringLengthWelcome {
    public static void main(String[] args) {
        String s=new String("welcome");
        System.out.println(s);
        System.out.println(s.length());
        System.out.println(s.isEmpty());
    }
}
output:-
==========
welcome
7
false
===========
package com.app.strings;

public class StringRefEqualComparison {
    public static void main(String[] args) {
        String s1=new String("Srinivas");
        String s2=new String("Srinivas");
        String s3=s1;
        System.out.println(s1==s2);//false
        System.out.println(s1.equals(s2)); //true
        System.out.println(s1==s3);//true
        System.out.println(s1.equals(s3));//true
    }
}

output:-
==========
false
true
true
true
==============
package com.app.strings;

public class StringReplaceExample {
    public static void main(String[] args) {
        String s="welcome java";
        System.out.println(s.replace('l', 'm'));
        System.out.println(s.replace('e', 'E'));
    }
}
output:-
=========
wemcome java
wElcomE java
===============
package com.app.strings;

import java.util.Arrays;

public class StringSplit {
    public static void main(String[] args) {
        String s="java is very simple";
        String ss[]=s.split(" ");
        System.out.println(Arrays.toString(ss));
    }
}

output:-
========
[java, is, very, simple]
==========================
package com.app.strings;

public class StringSubStringUsing {
    public static void main(String[] args) {
        String s="welcome to java and python";
        System.out.println(s);
        System.out.println(s.substring(11));
        System.out.println(s.substring(11, 15));
    }
}
output:-
=========
welcome to java and python
java and python
java
=============
package com.app.strings;

public class StringUppercaseAndLowerCase {
public static void main(String[] args) {
    String s="weLCome";
    System.out.println(s);
    System.out.println(s.toUpperCase());
    System.out.println(s.toLowerCase());
}
}
output:-
==========
weLCome
WELCOME
welcome
============
package com.app.strings;

public class StringUpperLowerCase {
    public static void main(String[] args) {
        String s1="abc";
        String s2=s1.toUpperCase();
        String s3=s1.toLowerCase();
        System.out.println(s1==s2);//false
        System.out.println(s1==s3);//true
    }
}
output:-
============
false
true
===========
package com.app.strings;

public class StringUpperLowerCaseCheck {
    public static void main(String[] args) {
        String s1=new String("i love my india");
        String s2=new String("i love my india");
        System.out.println(s1==s2);//false --1
        String s3="i love my india";
        System.out.println(s1==s3);//false--2
        String s4="i love my india";
        System.out.println(s3==s4);//true--3
        String s5="i love "+"my india";
        System.out.println(s4==s5);//true--4
        String s6="i love ";
        String s7=s6+"my india";
        System.out.println(s4==s7);//false--5
        final String s8="i love ";
        String s9=s8+"my india";
        System.out.println(s4==s9);//true--6
    }
}
output:-
==========
false
false
true
true
false
true
===========
package com.app.strings;

public class StringUpperLowerCaseNew {
public static void main(String[] args) {
    String s1=new String("abc");
    String s2=s1.toString();
    String s3=s1.toUpperCase();
    String s4=s1.toLowerCase();
    String s5=s1.toUpperCase();
    String s6=s3.toLowerCase();
    System.out.println(s1==s2);//true
    System.out.println(s1==s4);//true
    System.out.println(s3==s5);//false
    System.out.println(s1==s6);//false
}
}
output:-
=========
true
true
false
false
============
package com.app.strings;

public class UsingStringConcatHeapScp {
    public static void main(String[] args) {
        String s1=new String("abc");
        s1.concat("def");
        s1=s1.concat("wxyz");
        System.out.println(s1);//abcwxyz    
    }
}

output:-
==========
abcwxyz
===============
package com.app.strings;

public class UsingStringConcatHeapScpNew {
public static void main(String[] args) {
    String s1=new String("spring");
    s1.contains("fall");
    String s2=s1.concat("winter");
    s2.concat("summer");
    System.out.println(s1);//spring
    System.out.println(s2);//springwinter
}
}
output:-
==========
spring
springwinter
============
package com.app.strings;

public class UsingToStringScpHeap {
    public static void main(String[] args) {
        String s1="abc";
        String s2=s1.toString();
        System.out.println(s1==s2);//true
    }
}
output:-
==========
true
==========
package com.app.strings;

public class UsingToStringScpHeapNew {
public static void main(String[] args) {
    String s1=new String("abc");
    String s2=s1.toString();
    System.out.println(s1==s2);//true
}
}
output:-
========
true
============
package com.app.strings;

public class VowelCount {
public static void main(String[] args) {
    String s="welcome";
    System.out.println(s);
    int count=0;
    for(int i=0;i<s.length();i++)
    {
        char ch=s.charAt(i);
        if(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u')
        {
            count++;    
        }
    }
    System.out.println("No of vowels: "+count);
}
}
output:-
===========
welcome
No of vowels: 3
==================
package com.app.strings;

public class Vowels {
    public static void main(String[] args) {
        String s="welcome";
        System.out.println(s);
        for(int i=0;i<s.length();i++)
        {
            char ch=s.charAt(i);
            if(ch=='a'|| ch=='e'||ch=='i'||ch=='o'||ch=='u') {
                System.out.println(ch);
            }
        }
    }
}
output:-
===========
welcome
e
o
e
==============
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class EvenOddLength {
    public static void main(String[] args) 
    {
        String s = "the quick brown fox jumps over the lazy dog";
        StringBuffer sb = new StringBuffer();
        System.out.println(s);
        int i=0;
        StringTokenizer st = new StringTokenizer(s);
        while(st.hasMoreTokens()){
            String ss = st.nextToken();
            if(ss.length()%2==0) //ss.length()%2!=0
                sb.append(new StringBuffer(ss).reverse());
            else
                sb.append(ss);
            sb.append(" ");
            i++;
        }
        System.out.println(sb.toString());
    }
}
output:-
=========
the quick brown fox jumps over the lazy dog
the quick brown fox jumps revo the yzal dog 
=========================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class FirstCharLastCharExample {
    public static void main(String[] args) {
        String s = "the quick brown fox jumps over the lazy dog";
        StringBuffer sb=new StringBuffer();
        System.out.println(s);
        StringTokenizer st=new StringTokenizer(s);
        while(st.hasMoreTokens())
        {
            String ss=st.nextToken();
            int n=ss.length();
            sb.append(ss.substring(0, 1).toUpperCase()+ss.substring(1, n-1)+ss.substring(n-1, n).toUpperCase());
            sb.append(" ");
        }
        System.out.println(sb.toString());  
    }
}

output:-
==============
the quick brown fox jumps over the lazy dog
ThE QuicK BrowN FoX JumpS OveR ThE LazY DoG 
=================================================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class FirstCharLastCharRemainingUppercase {
    public static void main(String[] args) {
        String s="the quick brown fox jumps over the lazy dog";
        StringBuffer sb=new StringBuffer();
        System.out.println(s);
        StringTokenizer s1=new StringTokenizer(s);
        while(s1.hasMoreTokens())
        {
            String sss=s1.nextToken();
            int n=sss.length();
            
            sb.append(sss.substring(0, 1)+sss.substring(1, n-1).toUpperCase()+sss.substring(n-1,n));
            sb.append(" ");
            
        }
        System.out.println(sb.toString());
    }
}
output:-
===========
the quick brown fox jumps over the lazy dog
tHe qUICk bROWn fOx jUMPs oVEr tHe lAZy dOg 
===============================================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringtokenizerExample {
    public static void main(String[] args) {
        String s="java is very easy programming";
        StringTokenizer st=new StringTokenizer(s);
        System.out.println(st.countTokens());
        while(st.hasMoreElements())
        {
            System.out.println(st.nextToken());
        }
    }
}
output:-
==========
5
java
is
very
easy
programming
====================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringtokenizerExampleFour {
        public static void main(String[] args) 
        {
            String s = "8:04:34";
            StringTokenizer st = new StringTokenizer(s,":");
            System.out.println(st.countTokens());//3
            while(st.hasMoreTokens()){
                System.out.println(st.nextToken());
            }
        }
    }
output:-
============
3
8
04
34
======================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringtokenizerExampleNew {
public static void main(String[] args) {
    String s="java is very easy programming";
    StringTokenizer st=new StringTokenizer(s,"a");
    System.out.println(st.countTokens());
    while(st.hasMoreElements())
    {
        System.out.println(st.nextToken());
    }
}
}
output:-
==========
5
j
v
 is very e
sy progr
mming
====================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringtokenizerExamplethree {
    public static void main(String[] args) {
        String s = "3-1-2023";
        StringTokenizer st = new StringTokenizer(s,"-");
        System.out.println(st.countTokens());//3
        while(st.hasMoreTokens()){
            System.out.println(st.nextToken());
        }
    }
}
output:-
================
3
3
1
2023
================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringTokenizerReverse {
    public static void main(String[] args) {
        String str="the quick brown fox jumps over the lazy dog";
        System.out.println(str);
        StringTokenizer st = new StringTokenizer(str);
        while(st.hasMoreTokens()){
            System.out.print(new StringBuffer(st.nextToken()).reverse()+" ");
        }
    }
}
output:-
=============
the quick brown fox jumps over the lazy dog
eht kciuq nworb xof spmuj revo eht yzal god 
==================================================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class StringTokenizerReverseAlternativeword {
    public static void main(String[] args) {
        String s = "the quick brown fox jumps over the lazy dog";
        StringBuffer sb = new StringBuffer();
        System.out.println(s);
        int i=0;
        StringTokenizer st = new StringTokenizer(s);
        while(st.hasMoreTokens()){
            String ss = st.nextToken();
            if(i%2==0) //i%2!=0
                sb.append(ss);
            else
                sb.append(new StringBuffer(ss).reverse());
            sb.append(" ");
            i++;
        }
        System.out.println(sb.toString());
    }
}
output:-
============
the quick brown fox jumps over the lazy dog
the kciuq brown xof jumps revo the yzal dog 
================================================
package com.app.stringtokenizer;

import java.util.StringTokenizer;

public class ToUppercaseLetterString {
    public static void main(String[] args) {
        String s="the quick brown fox jumps over the lazy dog";
        StringBuffer sb=new StringBuffer();
        System.out.println(s);
        StringTokenizer st=new StringTokenizer(s);
        while(st.hasMoreTokens())
        {
            String sss=st.nextToken();
            sb.append(sss.substring(0, 1).toUpperCase()+sss.substring(1));
            sb.append(" ");
        }
        System.out.println(sb.toString());
    }
}

output:-
=============
the quick brown fox jumps over the lazy dog
The Quick Brown Fox Jumps Over The Lazy Dog 
======================================================
package com.app.swapuppercaselowercase;

import java.util.Scanner;

public class SwapUppercaseLowercase {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter String: ");
        String s=sc.nextLine();
        String ss="";
        for(int i=0;i<s.length();i++)
        {
            char ch=s.charAt(i);
            
            if(ch>='a' && ch<='z')
            {
                ss=ss+(char)(ch-32);
            }
            if(ch>='A' && ch<='Z') {
                ss=ss+(char)(ch+32);
            }
        }
        System.out.print(ss);
        sc.close();
    }
}
output:-
===========
Enter String: 
srinivas
SRINIVAS
Enter String: 
SRINIVAS
srinivas
========================
package com.app.train;

import java.util.Arrays;

public class TrainMinPlatform {
    public static int findMinPlaforms(double[] arrival,double[] departure)
    {
        // sort arrival time of trains
        Arrays.sort(arrival);
         // sort departure time of trains
        Arrays.sort(departure);
        //maintains the count of trains
        int count=0;
        // stores minimum platforms needed
        int platforms=0;
        // take two indices for arrival and departure time
        
        int i=0,j=0;
         // run till all trains have arrived
        while(i<arrival.length)
        {
             // if a train is scheduled to arrive next
            if(arrival[i]<departure[j])
            {
                // increase the count of trains and update minimum
                // platforms if required
                
            platforms=Integer.max(platforms, ++count);
                
             // move the pointer to the next arrival
            i++;
             // if the train is scheduled to depart next i.e.
            // `departure[j] < arrival[i]`, decrease trains' count
            // and move pointer `j` to the next departure.
 
            // If two trains are arriving and departing simultaneously,
            // i.e., `arrival[i] == departure[j]`, depart the train first
            }           
            else {
                count--;
                j++;
            }
            }
        return platforms;
        }
    public static void main(String[] args) {
        
        double[] arrival={ 2.00, 2.10, 3.00, 3.20, 3.50, 5.00 };
        double[] departure = { 2.30, 3.40, 3.20, 4.30, 4.00, 5.20 };
        System.out.println("The minimum platform needed is : "+findMinPlaforms(arrival,departure));
    }
}
output:-
===========
The minimum platform needed is : 2
===========================================
package com.app.updating;

import java.util.Arrays;

public class UpdateAtLocation {
    public static void updateAtLoc(int[] a,int loc,int element) {
        a[loc]=element;
    }
}
class Test66623{
    public static void main(String[] args) {
        int a[]= {10, 11, 12, 13, 12, 15, 12};
        System.out.println(Arrays.toString(a));//[10, 11, 12, 13, 12, 15, 12]
        UpdateAtLocation.updateAtLoc(a,1,999);
        System.out.println(Arrays.toString(a));//[10, 999, 12, 13, 12, 15, 12]
    }
}
output:-
============
[10, 11, 12, 13, 12, 15, 12]
[10, 999, 12, 13, 12, 15, 12]
================================
package com.app.updating;

import java.util.Arrays;

public class UpdateElement {
    public static void updateAllElement(int[] a,int olde,int newe)
    {
        for(int i=0;i<a.length;i++)
        {
            if(a[i]==olde)
            {
                a[i]=newe;
            }
        }   
    }
}
class Test56676{
    public static void main(String[] args) {
        
        int a[]= {10, 11, 12, 13, 12, 15, 12};
        System.out.println(Arrays.toString(a));
        UpdateElement.updateAllElement(a,12,888);
        System.out.println(Arrays.toString(a));
    }
}
output:-
============
[10, 11, 12, 13, 12, 15, 12]
[10, 11, 888, 13, 888, 15, 888]
======================================
package com.app.updating;

import java.util.Arrays;

public class UpdateFirstOccurance {
    public static void updateElement(int []a,int olde,int newe) {

        for(int i=0;i<a.length;i++)
        {
            if(a[i]==olde)
            {
                a[i]=newe;
                break;          
            }
        }
    }
}
class Test7773{
    public static void main(String[] args) {

        int a[]= {10,11,12,13,12,15,12};

        System.out.println(Arrays.toString(a));
        UpdateFirstOccurance.updateElement(a,12,888);
        System.out.println(Arrays.toString(a));
    }
}
output:-
============
[10, 11, 12, 13, 12, 15, 12]
[10, 11, 888, 13, 12, 15, 12]
=================================
package com.app.updating;

import java.util.Arrays;

public class UpdateFirstOccuranceAndLastOccurance {

    public static void updateFirstLastOccrance(int[] a,int olde,int newe)
    {

        int c=0;
        for(int i=0;i<a.length;i++)
        {
            if(a[i]==olde)
            {
                c++;
                a[i]=newe;
            }
            if(c==2)
            {
                break;
            }
        }       
    }
}
class Test66622{
    public static void main(String[] args) {
        int a[]= {10,11,12,13,12,15,12};
        System.out.println(Arrays.toString(a));
        UpdateFirstOccuranceAndLastOccurance.updateFirstLastOccrance(a,12,888);
        System.out.println(Arrays.toString(a));
    }
}

output:-
===========
[10, 11, 12, 13, 12, 15, 12]
[10, 11, 888, 13, 888, 15, 12]
====================================

LINKED LIST DATA STRUCTURE
~~~~~~~~~~~~~~~~~~~~~~~~~~~

the problems with arrays are:

1) size if always fixed.
2) inserting and deleting is very difficult.

to overcome these problems, we have linked list data structure.

linked list:
~~~~~~~~~~~~
it is collection of nodes which are growable. during execution we can insert and delete nodes from linked list. linked list is the best data structure for inserting and deleting elements.

accessing elements ----> best -----> arrays
insert/delete ---------> best -----> linked list

the following are the four types of linked lists are existed in java

1) single linked list
2) double linked list
3) circular single linked list
4) circular double linked list

single linked list
-------------------
01) Inserting the data first         ------------> ok
02) Inserting the data last        --------------> ok
03) Inserting the data at position        -------> ok
04) Sorted Insertion Asc/Desc        ------------> ok
05) Traversing or Displaying        -------------> ok
06) Size or Length of list     --------------> ok
07) Reverse List        -------------------------> ok
08) Searching        ----------------------------> ok
09) Deleting from first        ------------------> ok
10) Deleting from last        -------------------> ok
11) Delete from position        -----------------> ok
12) Deleting Element       - --------------------> ok
13) Deleting Elements        --------------------> ok
14) Deleting Duplicates        ------------------> ok
15) Copy the reversed list        ---------------> ok
16) Copy of original list        ----------------> ok
17) Comparing two list objects ------------------> ok
18) Finding nth node from begining and ending ---> ok

list ---> 111=>222=>333=>null 

reverse:
--------
list ---> 333=>222=>111=> null

copyreversed:
-------------
list -----> 111=>222=>333=>null
list1 ----> 333=>222=>111=>null

package lists;
public class SLL 
{
    Node head;
    int size;
    class Node{
        int data;
        Node next;
        Node(int data){
            this.data = data;
            this.next = null;
            size++;
        }
        Node(int data,Node next){
            this.data = data;
            this.next = next;
            size++;
        }
    }
    void traverse() {
        if(head==null) {
            System.out.println("SLL is empty");
            return;
        }
        Node currNode = head;
        while(currNode!=null) {
            System.out.print(currNode.data+" => ");
            currNode = currNode.next;
        }
        System.out.println("null");
    }
    void addFirst(int data) {
        Node newNode = new Node(data);
        if(head==null) {
            head = newNode;
            return;
        }
        newNode.next = head;
        head = newNode;
    }
    void addLast(int data) {
        Node newNode = new Node(data);
        if(head==null) {
            head = newNode;
            return;
        }
        Node currNode = head;
        while(currNode.next!=null){
            currNode = currNode.next;
        }
        currNode.next = newNode;
    }
    void addPos(int data, int pos) {
        int i=0;
        Node newNode = new Node(data);
        if(head==null) {
            head = newNode;
            return;
        }
        if(pos<0 || pos>=size-1) {
            System.out.println("out of range");
            return;
        }
        if(pos!=0) {
            Node currNode=head,temp=null;
            while(currNode.next!=null && i<pos) {
                temp = currNode;
                currNode = currNode.next;
                i++;
            }
            temp.next = newNode;
            newNode.next = currNode;
        }
        else {
            newNode.next = head;
            head = newNode;
        }
    }
    void sortedInsertAsc(int data) {
        Node newNode = new Node(data);
        Node currNode = head;
        if(currNode==null || currNode.data > data) {
            newNode.next = head;
            head = newNode;
            return;
        }
        while(currNode.next!=null && currNode.next.data<data)
            currNode = currNode.next;
        newNode.next = currNode.next;
        currNode.next = newNode;
    }
    void sortedInsertDesc(int data) {
        Node newNode = new Node(data);
        Node currNode = head;
        if(currNode==null || currNode.data < data) {
            newNode.next = head;
            head = newNode;
            return;
        }
        while(currNode.next!=null && currNode.next.data>data)
            currNode = currNode.next;
        newNode.next = currNode.next;
        currNode.next = newNode;
    }
    int getSize() {
        return this.size;
    }
    boolean search(int data) {
        Node currNode = head;
        while(currNode!=null) {
            if(currNode.data == data) 
                return true;
            currNode = currNode.next;
        }
        return false;
    }
    void deleteFirst() {
        if(head==null) {
            System.out.println("SLL is empty");
            return;
        }
        size--;
        head = head.next;
    }
    void deleteLast() {
        if(head==null) {
            System.out.println("SLL is empty");
            return;
        }
        if(head.next == null) {
            head = null;
            size--;
            return;
        }
        size--;
        Node temp1=head,temp2=head.next;
        while(temp2.next!=null) {
            temp2 = temp2.next;
            temp1 = temp1.next;
        }
        temp1.next = null;
    }
    void deletePos(int pos) {
        Node temp = head;
        int i=0;
        if(temp==null) {
            System.out.println("SLL is empty");
            return;
        }
        if(pos==0) {
            head=head.next;
            size--;
            return;
        }
        if(pos<0 || pos>=size-1) {
            System.out.println("out of range");
            return;
        }
        while(temp.next!=null && i<pos) {
            if(i==pos-1) {
                temp.next = temp.next.next;
                size--;
                return;
            }
            i++;
            temp=temp.next;
        }
    }
    void deleteElement(int value) {
        Node temp = head;
        if(temp==null) {
            System.out.println("SLL is empty");
            return;
        }
        if(temp.data==value) {
            head = head.next;
            size--;
            return;
        }
        while(temp.next!=null) {
            if(temp.next.data == value) {
                temp.next = temp.next.next;
                size--;
                return;
            }
            temp = temp.next;
        }
    }
    void deleteElements(int value) {
        Node temp = head;
        if(temp==null) {
            System.out.println("SLL is empty");
            return;
        }
        if(temp.data==value) {
            head = head.next;
            size--;
        }
        while(temp.next!=null) {
            if(temp.next.data == value) {
                temp.next = temp.next.next;
                size--;
            }
            if(temp.next!=null)
                temp = temp.next;
        }
    }
    void reverse() {
        Node currNode = head, prev = null, next = null;
        while(currNode!=null) {
            next = currNode.next;
            currNode.next = prev;
            prev = currNode;
            currNode = next;
        }
        head = prev;
    }
    //Note: compulsory the list must be in sorted form
    void removeDuplicates() {
        Node currNode = head;
        while(currNode!=null) {
            if(currNode.next!=null && currNode.data == currNode.next.data) 
                currNode.next = currNode.next.next;
            else
                currNode = currNode.next;
        }
    }
    SLL copyReversed() {
        Node temp1 = null, temp2=null, currNode = head;
        while(currNode!=null) {
            temp2 = new Node(currNode.data,temp1);
            currNode = currNode.next;
            temp1 = temp2;
        }
        SLL obj = new SLL();
        obj.head = temp1;
        return obj;
    }
    SLL copyList() {
        Node headNode = null, tailNode=null, tempNode = null, currNode = head;
        if(currNode==null)
            return null;
        headNode = new Node(currNode.data,null);
        tailNode = headNode;
        currNode = currNode.next;
        while(currNode!=null) {
            tempNode = new Node(currNode.data,null);
            tailNode.next = tempNode;
            tailNode = tempNode;
            currNode = currNode.next;
        }
        SLL obj = new SLL();
        obj.head = headNode;
        return obj;
    }
    boolean compareList(SLL list) {
        Node head1=head,head2=list.head;
        while(head1!=null && head2!=null) {
            if(head1.data!=head2.data)
                return false;
            head1 = head1.next;
            head2 = head2.next;
        }
        if(head1==null && head2==null)
            return true;
        return false;
    }
    int nthNodeFromBegin(int index) {
        if(index>getSize() || index<1)
            return -1;
        int count=0;
        Node currNode = head;
        while(currNode!=null && count<index-1) {
            count++;
            currNode = currNode.next;
        }
        return currNode.data;
    }
    int nthNodeFromEnd(int index) {
        int size = getSize(),sindex;
        if(size!=0 && size<index)
            return -1;
        sindex = size-index+1;
        return nthNodeFromBegin(sindex);
    }
}

double linked list
-------------------
01) Inserting the data first         ------------> ok
02) Inserting the data last        --------------> ok
03) Inserting the data at position        -------> ok
04) Sorted Insertion Asc/Desc        ------------> ok
05) Traversing or Displaying        -------------> ok
06) Size or Length of list     --------------> ok
07) Reverse List        -------------------------> ok
08) Searching        ----------------------------> ok
09) Deleting from first        ------------------> ok
10) Deleting from last        -------------------> ok
11) Delete from position        -----------------> ok
12) Deleting Element       - --------------------> ok
13) Deleting Elements        --------------------> ok
14) Deleting Duplicates        ------------------> ok
15) Copy the reversed list        ---------------> ok
16) Copy of original list        ----------------> ok
17) Comparing two list objects ------------------> ok

package lists;

public class DLL {
    Node head;
    int size = 0;
    class Node{
        int data;
        Node next;
        Node prev;
        Node(int data,Node next,Node prev){
            this.data = data;
            this.next = next;
            this.prev = prev;
            size++;
        }
    }
    void traverse() {
        if(head==null){
            System.out.println("DLL is empty");return;
        }
        Node currNode = head;
        while(currNode!=null) {
            System.out.print(currNode.data+" ==> ");
            currNode = currNode.next;
        }
        System.out.println("NULL");
    }
    void addFirst(int data) {
        Node newNode = new Node(data,null,null);
        if(head==null)
            head = newNode;
        else {
            head.prev = newNode;
            newNode.next = head;
            head = newNode;
        }
    }
    void addLast(int data) {
        Node newNode = new Node(data,null,null);
        if(head==null)
            head = newNode;
        else {
            Node currNode = head;
            while(currNode.next!=null)
                currNode = currNode.next;
            currNode.next = newNode;
            newNode.prev = currNode;
        }
    }
    void addPos(int data,int pos) {
        int i=0;
        if(pos<0 || pos>=size) {
            System.out.println("out of range");return;
        }
        Node newNode = new Node(data,null,null);
        if(head==null) {
            head = newNode;
            return;
        }
        if(pos!=0) {
            Node currNode = head;
            Node temp=null;
            while(currNode.next!=null && i<pos) {
                temp = currNode;
                currNode = currNode.next;
                i++;
            }
            temp.next = newNode;
            newNode.prev = temp;
            newNode.next = currNode;
            currNode.prev = newNode;
        }
        else {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        }
    }
    void sortedInsertAsc(int data) {
        Node newNode = new Node(data,null,null);
        Node currNode = head;
        if(currNode == null) {
            head = newNode;
            return;
        }
        if(currNode.data>data) {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
            return;
        }
        while(currNode.next!=null && currNode.next.data<data)
            currNode = currNode.next;
        if(currNode.next!=null) {
            newNode.next = currNode.next;
            currNode.next.prev = newNode;
            currNode.next = newNode;
            newNode.prev = currNode;
        }
        else {
            currNode.next = newNode;
            newNode.prev = currNode;
        }
    }
    void sortedInsertDesc(int data) {
        Node newNode = new Node(data,null,null);
        Node currNode = head;
        if(currNode == null) {
            head = newNode;
            return;
        }
        if(currNode.data<data) {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
            return;
        }
        while(currNode.next!=null && currNode.next.data>data)
            currNode = currNode.next;
        if(currNode.next!=null) {
            newNode.next = currNode.next;
            currNode.next.prev = newNode;
            currNode.next = newNode;
            newNode.prev = currNode;
        }
        else {
            currNode.next = newNode;
            newNode.prev = currNode;
        }
    }
    int getSize() {
        return this.size;
    }
    boolean search(int data) {
        Node temp = head;
        while(temp!=null)
        {
            if(temp.data == data)
                return true;
            temp = temp.next;
        }
        return false;
    }
    boolean compareList(DLL list) {
        Node head1 = head,head2 = list.head;
        while(head1!=null && head2!=null) {
            if(head1.data!=head2.data)
                return false;
            head1 = head1.next;
            head2 = head2.next;
        }
        if(head1==null && head2==null)
            return true;
        return false;
    }
    void reverse() {
        Node temp=null,currNode=head;
        while(currNode!=null) {
            temp = currNode.prev;
            currNode.prev = currNode.next;
            currNode.next = temp;
            currNode = currNode.prev;
        }
        if(temp!=null)
            head = temp.prev;
    }
    void deleteFirst() {
        if(head==null) {
            System.out.println("DLL is empty");
            return;
        }
        size--;
        head = head.next;
        if(head!=null)
            head.prev = null;
    }
    void deleteLast() {
        if(head==null) {
            System.out.println("DLL is empty");return;
        }
        if(head.next==null) {
            head = null;
            size--;
            return;
        }
        size--;
        Node temp = head;
        Node currNode = head.next;
        while(currNode.next!=null) {
            currNode = currNode.next;
            temp = temp.next;
        }
        temp.next = null;   
    }
    void deleteAtPos(int pos) {
        Node temp = head,tempp;
        int i=0;
        if(temp==null) {
            System.out.println("DLL is empty");return;
        }
        if(pos<0 || pos>=getSize()) {
            System.out.println("out of range");return;
        }
        if(pos==0) {
            head=head.next;
            if(head!=null)
                head.prev=null;
            size--;
            return;
        }
        while(temp.next!=null && i<pos) {
            if(i==pos-1) {
                temp.next = temp.next.next;
                tempp = temp.next;
                if(tempp!=null)
                    tempp.prev = temp;
                size--;
                return;
            }
            i++;
            temp=temp.next;
        }       
    }
    void deleteElement(int data) {
        Node temp = head,tempp;
        if(temp==null) {
            System.out.println("DLL is empty");return;
        }
        if(temp.data == data) {
            head = head.next;
            if(head!=null)
                head.prev = null;
            size--;
            return;
        }
        while(temp.next!=null) {
            if(temp.next.data == data) {
                temp.next = temp.next.next;
                tempp = temp.next;
                if(tempp!=null)
                    tempp.prev = temp;
                size--;
                return;
            }
            temp = temp.next;
        }
    }
    void deleteElements(int data) {
        Node temp = head,tempp;
        if(temp==null) {
            System.out.println("DLL is empty");return;
        }
        if(temp.data == data) {
            head = head.next;
            if(head!=null)
                head.prev = null;
            size--;
            return;
        }
        while(temp.next!=null) {
            if(temp.next.data == data) {
                temp.next = temp.next.next;
                tempp = temp.next;
                if(tempp!=null)
                    tempp.prev = temp;
                size--;
            }
            temp = temp.next;
        }
    }
    //list should in order
    void removeDuplicates() {
        Node currNode = head,temp;
        while(currNode!=null) {
            if(currNode.next!=null && currNode.data == currNode.next.data) {
                currNode.next = currNode.next.next;
                temp = currNode.next;
                if(temp!=null)
                    temp.prev = currNode;
            }else {
                currNode = currNode.next;
            }
        }
    }
    DLL copyList() {
        Node headNode=null,tailNode,tempNode,currNode=head;
        tailNode = null;
        tempNode = null;
        if(currNode==null)
            return null;
        headNode = new Node(currNode.data,null,null);
        tailNode = headNode;
        currNode = currNode.next;
        while(currNode!=null) {
            tempNode = new Node(currNode.data,null,null);
            tailNode.next = tempNode;
            tempNode.prev = tailNode;
            tailNode = tempNode;
            currNode = currNode.next;
        }
        DLL obj = new DLL();
        obj.head = headNode;
        return obj;
    }
    DLL copyReverse() {
        Node temp1=null,temp2=null,currNode=head;
        while(currNode!=null) {
            temp2 = new Node(currNode.data,temp1,null);
            currNode = currNode.next;
            if(temp1!=null)
                temp1.prev = temp2;
            temp1 = temp2;
        }
        DLL obj = new DLL();
        obj.head = temp1;
        return obj;
    }
}

Circular Single Linked List
~~~~~~~~~~~~~~~~~~~~~~~~~~~
package lists;

public class CSLL {
    Node tail;
    int size = 0;
    class Node{
        int value;
        Node next;
        Node(int value,Node next){
            this.value = value;
            this.next = next;
        }
    }
    void print() {
        if(size==0) {
            System.out.println("CSLL is empty");
            return;
        }
        Node temp = tail.next;
        while(temp!=tail) {
            System.out.print(temp.value+" => ");
            temp = temp.next;
        }
        System.out.println(temp.value);
    }
    void addHead(int value) {
        Node temp = new Node(value,null);
        if(size==0) {
            tail = temp;
            temp.next = temp;
        }
        else {
            temp.next = tail.next;
            tail.next = temp;
        }
        size++;
    }
    void addTail(int value) {
        Node temp = new Node(value,null);
        if(size==0) {
            tail = temp;
            temp.next = temp;
        }
        else {
            temp.next = tail.next;
            tail.next = temp;
            tail = temp;
        }
        size++;
    }
    void addPos(int pos,int value) {
        Node newNode = new Node(value,null);
        if(size==0) {
            tail = newNode;
            newNode.next = newNode;
        }
        else {
            if(pos==0) {
                Node temp = tail.next;
                newNode.next = temp;
                tail.next = newNode;
            }
            else {
                Node temp = tail.next;
                int i=0;
                while(temp.next!=tail && i<pos-1) {
                    temp = temp.next;
                    i++;
                }
                newNode.next = temp.next;
                temp.next = newNode;
            }           
        }
        size++;
    }
    void removeHead() {
        if(size==0) {
            System.out.println("CSLL is empty");
            return;
        }
        if(tail==tail.next)
            tail = null;
        else 
            tail.next = tail.next.next;
        size--;         
    }
    void removeTail() {
        if(size==0) {
            System.out.println("CSLL is empty");
            return;
        }
        if(tail == tail.next)
            tail = null;
        else {
            Node temp = tail.next;
            while(temp.next!=tail)
                temp = temp.next;
            temp.next = tail.next;
            tail = temp;
        }
        size--;
    }
    void deleteElement(int value) {
        if(size==0) {
            System.out.println("CSLL is empty");
            return;
        }
        Node prev=tail,currNode=tail.next,head=tail.next;
        if(currNode.value == value) {
            if(currNode==currNode.next)
                tail=null;
            else
                tail.next = tail.next.next;
            size--;
            return;
        }
        
            prev = currNode;
            currNode = currNode.next;
            while(currNode!=head) {
                if(currNode.value==value) {
                    if(currNode==tail)
                        tail=prev;
                prev.next = currNode.next;
                return;
                }
                prev = currNode;
                currNode = currNode.next;
            }
    }
}

Circular Double Linked List
~~~~~~~~~~~~~~~~~~~~~~~~~~~
package lists;

public class CDLL {
    Node head = null;
    Node tail = null;
    int size = 0;
    class Node{
        int value;
        Node next,prev;
        Node(int value,Node next,Node prev){
            this.value = value;
            this.next = next;
            this.prev = prev;
        }
    }
    void print() {
        if(size==0) {
            System.out.println("CDLL is empty");
            return;
        }
        Node temp = tail.next;
        while(temp!=tail) {
            System.out.print(temp.value+" ==> ");
            temp = temp.next;
        }
        System.out.println(temp.value);
    }
    void addHead(int value) {
        Node newNode = new Node(value,null,null);
        if(size==0) {
            tail = head = newNode;
            newNode.next = newNode;
            newNode.prev = newNode;
        }
        else {
            newNode.next = head;
            newNode.prev = head.prev;
            head.prev = newNode;
            newNode.prev.next = newNode;
            head = newNode;
        }
        size++;
    }
    void addTail(int value) {
        Node newNode =new Node(value,null,null);
        if(size==0) {
            tail = head = newNode;
            newNode.next = newNode;
            newNode.prev = newNode;
        }
        else {
            newNode.next = tail.next;
            newNode.prev = tail;
            tail.next = newNode;
            newNode.next.prev = newNode;
            tail=newNode;
        }
        size++;
    }
    void removeHead() {
        if(size==0) {
            System.out.println("CDLL is empty");
            return;
        }
        size--;
        if(size==0) {
            head = null;
            tail=null;
            return;
        }
        Node temp = head.next;
        temp.prev = tail;
        tail.next = temp;
        head = temp;
    }
    void removeTail() {
        if(size==0) {
            System.out.println("CDLL is empty");
            return;
        }
        size--;
        if(size==0) {
            head= null;
            tail = null;
        }
        Node temp = tail.prev;
        temp.next = head;
        head.prev = temp;
        tail = temp;
    }
}

package lists;

public class Test 
{
    public static void main(String[] args) 
    {
        CDLL list = new CDLL();
        list.addHead(111);
        list.addTail(222);
        list.addTail(333);
        list.addTail(444);
        list.addTail(555);
        list.addHead(888);
        list.addHead(999);
        list.print();
        list.removeHead();
        list.print();
        list.removeTail();
        list.print();
    }
}



stack data structures:
~~~~~~~~~~~~~~~~~~~~~~
01. Introduction
02. Operations on stack
03. implementation of stack using arrays
04. implementation of stack using linked list
05. predefined implementation stack class (java.util.Stack)
06. toString() method implementation for stack class
07. sorted insertion into stack
08. sorting stack elements
09. bottom insertion 
10. reverse of the stack
11. balanced parethesis application
12. infix, prefix and postfix expressions
13. infix to postfix conversion
14. infix to prefix conversion
15. postfix evaluation

stack data structure:
~~~~~~~~~~~~~~~~~~~~~
a stack is a basic data structure, it follows LIFO i.e. last-in-first-out. The item which is inserted at last will be the item removed first.

Ex:
---
    Recursion
    Stack of plats
    books in a box
    etc

the following are the common operations that we can perform on stack

push ------> add element 
pop -------> remove element
peek ------> top most element
size ------> number of elements
isempty ---> returns True if stack is empty
display ---> travese each element 
search ----> find for an object

Q) What is the difference between pop() and peek() method?
----------------------------------------------------------
pop() method will remove and return top element in the stack, but peek() method will return top element in the stack, it wn't perform delete operation.

representation of stack
-----------------------
1) Implementation of stack by using arrays
2) Implementation of stack by using linked list (SLL)
3) Implementation of stack by using predefined class (java.util.Stack)

Implementation of stack by using arrays
---------------------------------------
import java.util.*;

class StackArray
{
    int capacity = 5;
    int[] data;
    int top = -1;

    StackArray(){
        data = new int[capacity];
    }

    boolean isEmpty(){
        return top==-1;
    }

    int getSize(){
        return (top+1);
    }

    void print(){
        if(isEmpty()){
            System.out.println("Stack Under Flow");
            return;
        }
        else{
            for(int i=0;i<=top;i++)
                System.out.print(data[i]+" ");
            System.out.println();
        }
    }

    void push(int value){
        if(getSize()==data.length){
            System.out.println("Stack Over Flow");
            return;
        }
        top++;
        data[top] = value;
    }
    int pop(){
        if(isEmpty()){
            System.out.println("Under Flow");
            return -1;
        }
        int delvalue = data[top];
        top--;
        return delvalue;
    }
    int peek(){
        if(isEmpty()){
            System.out.println("under flow");
            return -1;
        }
        return data[top];
    }
    boolean search(int value){
        if(isEmpty()){
            System.out.println("under flow");
            return false;
        }
        for(int i=0;i<=top;i++){
            if(data[i]==value)
                return true;
        }
        return false;
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        StackArray s = new StackArray();
        s.push(111);
        s.push(222);
        s.push(333);
        s.push(444);
        s.push(555);
        s.print();//111 222 333 444 555
        System.out.println(s.search(333));//true
        System.out.println(s.search(999));//false
    }
}

Implementation of stack by using linked list (SLL):
---------------------------------------------------
import java.util.*;

class StackLL
{
    Node head = null;
    int size = 0;
    class Node{
        int value;
        Node next;
        Node(int value,Node next){
            this.value = value;
            this.next = next;
        }
    }
    int getSize(){
        return size;
    }
    boolean isEmpty(){
        return size==0;
    }
    void print(){
        Node temp = head;
        while(temp!=null){
            System.out.print(temp.value+"==>");
            temp = temp.next;
        }
        System.out.println("null");
    }
    void push(int value){
        head = new Node(value,head);
        size++;
    }
    int peek(){
        if(isEmpty())
            return -1;
        else
            return head.value;
    }
    int pop(){
        if(isEmpty())
        {
            System.out.println("stack is underflow");
            return -1;
        }
        else{
            int temp = head.value;
            head = head.next;
            return temp;
        }
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        StackLL s = new StackLL();
        s.push(777);
        s.push(888);
        s.push(333);
        s.push(222);
        s.print();//222=>333=>888=>777=>null
        System.out.println(s.peek());//222
        s.push(555);
        s.print();//555=>222=>333=>888=>777=>null
        System.out.println(s.peek());//555
        System.out.println(s.pop());//555
        System.out.println(s.pop());//222
        s.print();//333=>888=>777=>null
        System.out.println(s.peek());//333
    }
}

toString() method implementation for stack
------------------------------------------
    public String toString(){
        Node temp = head;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        while(temp!=null){
            if(temp.next!=null)
                sb.append(temp.value+", ");
            else
                sb.append(temp.value);
            temp=temp.next;
        }
        sb.append("]");
        return sb.toString();
    }

Ex:
---
import java.util.*;

class StackLL
{
    Node head = null;
    int size = 0;
    class Node{
        int value;
        Node next;
        Node(int value,Node next){
            this.value = value;
            this.next = next;
        }
    }
    int getSize(){
        return size;
    }
    boolean isEmpty(){
        return size==0;
    }
    void print(){
        Node temp = head;
        while(temp!=null){
            System.out.print(temp.value+"==>");
            temp = temp.next;
        }
        System.out.println("null");
    }
    void push(int value){
        head = new Node(value,head);
        size++;
    }
    int peek(){
        if(isEmpty())
            return -1;
        else
            return head.value;
    }
    int pop(){
        if(isEmpty())
        {
            System.out.println("stack is underflow");
            return -1;
        }
        else{
            int temp = head.value;
            head = head.next;
            return temp;
        }
    }
    public String toString(){
        Node temp = head;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        while(temp!=null){
            if(temp.next!=null)
                sb.append(temp.value+", ");
            else
                sb.append(temp.value);
            temp=temp.next;
        }
        sb.append("]");
        return sb.toString();
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        StackLL s = new StackLL();
        s.push(777);
        s.push(888);
        s.push(333);
        s.push(222);
        s.print();//222==>333==>888==>777==>null
        System.out.println(s);//[222, 333, 888, 777]
    }
}

Implementation of stack by using predefined class (java.util.Stack)
-------------------------------------------------------------------
case1: General implementation
------------------------------
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Stack s = new Stack();
        System.out.println(s);//[]
        System.out.println(s.empty());//true
        s.push(111);
        s.push(222);
        s.push(333);
        s.push(444);
        s.push(555);
        System.out.println(s.empty());//false
        System.out.println(s);//[111,222,333,444,555]
        System.out.println(s.peek());//555
        System.out.println(s.search(111));//5
        System.out.println(s.search(555));//1
        System.out.println(s.search(999));//-1
        System.out.println(s.pop());//555
        System.out.println(s);//[111,222,333,444]
    }
}

output:
-------
[]
true
false
[111, 222, 333, 444, 555]
555
5
1
-1
555
[111, 222, 333, 444]


case2: Stack class to hold String object
----------------------------------------
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Stack<String> s = new Stack<String>();
        s.push("AAA");
        s.push("BBB");
        s.push("CCC");
        s.push("DDD");
        s.push("EEE");
        System.out.println(s);//[AAA,BBB,CCC,DDD,EEE]
        System.out.println(s.peek());//EEE
        System.out.println(s.search("AAA"));//5
        System.out.println(s.search("CCC"));//3
        System.out.println(s.search("FFF"));//-1
        System.out.println(s.pop());//EEE
        System.out.println(s);//[AAA,BBB,CCC,DDD]
    }
}

output:
-------
[AAA, BBB, CCC, DDD, EEE]
EEE
5
3
-1
EEE
[AAA, BBB, CCC, DDD]

case3: Stack to hold student objects
------------------------------------
import java.util.*;

class Student{
    int sid;
    String sname;
    Student(int sid,String sname){
        this.sid = sid;
        this.sname = sname;
    }
    public String toString(){
        return "("+this.sid+","+this.sname+")"; 
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        Student s1 = new Student(444,"BBB");
        Student s2 = new Student(111,"AAA");
        Student s3 = new Student(555,"EEE");
        Student s4 = new Student(333,"DDD");
        Student s5 = new Student(222,"XXX");

        Stack<Student> s = new Stack<Student>();
        s.push(s1);
        s.push(s2);
        s.push(s3);
        s.push(s4);
        s.push(s5);
        System.out.println(s);
        //[(444,"BBB"),(111,"AAA"),(555,"EEE"),(333,"DDD"),(222,"XXX")]
        System.out.println(s.pop());//(222,"XXX")
        System.out.println(s);
        //[(444,"BBB"),(111,"AAA"),(555,"EEE"),(333,"DDD")]
    }
}

output:
-------
[(444,BBB), (111,AAA), (555,EEE), (333,DDD), (222,XXX)]
(222,XXX)
[(444,BBB), (111,AAA), (555,EEE), (333,DDD)]

Sorted Insertion:
-----------------
import java.util.*;

class Test 
{
    public static void sortedInsert(Stack<Integer> s,int value){
        int temp;
        if(s.empty() || value>s.peek())
            s.push(value);
        else
        {
            temp = s.pop();
            sortedInsert(s,value);
            s.push(temp);
        }
    }
    public static void main(String[] args) 
    {
        Stack<Integer> s = new Stack<Integer>();
        s.push(1);
        s.push(3);
        s.push(4);
        System.out.println(s);//[1,3,4]
        sortedInsert(s,2);
        System.out.println(s);//[1,2,3,4]
    }
}

output:
-------
[1,3,4]
[1,2,3,4]

Sorting of Stack
-----------------
import java.util.*;

class Test 
{
    public static void sortedInsert(Stack<Integer> s,int value){
        int temp;
        if(s.empty() || value>s.peek())
            s.push(value);
        else
        {
            temp = s.pop();
            sortedInsert(s,value);
            s.push(temp);
        }
    }
    public static void sortStack(Stack<Integer> s){
        int temp;
        if(s.empty()==false){
            temp = s.pop();
            sortStack(s);
            sortedInsert(s,temp);
        }
    }
    public static void main(String[] args) 
    {
        Stack<Integer> s = new Stack<Integer>();
        s.push(5);
        s.push(3);
        s.push(1);
        s.push(4);
        s.push(2);
        System.out.println(s);//[5,3,1,4,2]
        sortStack(s);
        System.out.println(s);//[1,2,3,4,5]
    }
}

output:
------
[5, 3, 1, 4, 2]
[1, 2, 3, 4, 5]

Bottom Insert
-------------
import java.util.*;

class Test 
{
    public static void bottomInsert(Stack<Integer> s,int value){
        int temp;
        if(s.empty())
            s.push(value);
        else{
            temp = s.pop();
            bottomInsert(s,value);
            s.push(temp);
        }
    }

    public static void main(String[] args) 
    {
        Stack<Integer> s = new Stack<Integer>();
        s.push(1);
        s.push(2);
        s.push(3);
        System.out.println(s);//[1,2,3]
        bottomInsert(s,888);
        System.out.println(s);//[888,1,2,3]
        bottomInsert(s,999);
        System.out.println(s);//[999,888,1,2,3]
    }
}

output:
------
[1, 2, 3]
[888, 1, 2, 3]
[999, 888, 1, 2, 3]

Reverse The Stack
-----------------
import java.util.*;

class Test 
{
    public static void bottomInsert(Stack<Integer> s,int value){
        int temp;
        if(s.empty())
            s.push(value);
        else{
            temp = s.pop();
            bottomInsert(s,value);
            s.push(temp);
        }
    }
    public static void reverseStack(Stack<Integer> s){
        if(s.empty())
            return;
        else{
            int temp = s.pop();
            reverseStack(s);
            bottomInsert(s,temp);
        }
    }
    public static void main(String[] args) 
    {
        Stack<Integer> s = new Stack<Integer>();
        s.push(1);
        s.push(2);
        s.push(3);
        s.push(4);
        s.push(5);
        System.out.println(s);//[1,2,3,4,5]
        reverseStack(s);
        System.out.println(s);//[5,4,3,2,1]
    }
}

output:
-------
[1, 2, 3, 4, 5]
[5, 4, 3, 2, 1]

Balanced Parenthesis
--------------------
() -------> true
(()) -----> true
([) ------> false
([{}]) ---> true


import java.util.*;
class Test 
{
    public static boolean isBalancedParenthesis(String ss){
        Stack<Character> s = new Stack<Character>();
        for(char ch:ss.toCharArray()){
            switch(ch){
            case '{':
            case '[':
            case '(': s.push(ch);break;

            case '}': 
                       if(s.pop()!='{')
                        return false;
                       break;
            case ']':
                       if(s.pop()!='[')
                        return false;
                       break;
            case ')':
                       if(s.pop()!='(')
                        return false;
                       break;
            }
        }
        return s.empty();
    }
    public static void main(String[] args) 
    {
        System.out.println(isBalancedParenthesis("[]"));//true
        System.out.println(isBalancedParenthesis("[()]"));//true
        System.out.println(isBalancedParenthesis("{[()]}"));//true
        System.out.println(isBalancedParenthesis("[{(}]"));//false
        System.out.println(isBalancedParenthesis("["));//false
    }
}

output:
-------
true
true
true
false
false

Infix, Postfix and Prefix
-------------------------
Expression is a combination of operators and operands. All the expressions in programming are divided into the following three types.

1) infix expressions ------> operand operator operand
2) postfix expressions ----> operand operand operator 
3) prefix expressions -----> operator operand operand

Ex:
---
    expression: a+b

    infix ===> a+b
    postfix => ab+
    prefix ==> +ab

Infix to Postfix Conversion
---------------------------
1) read expression from left to right.
2) if the input symbol is '(' then push it into the stack.
3) if the input symbol is an operand then push it into the output.
4) if the input symbol is an operator then,
   a. check the precedence of an operator which is existed inside stack is having greater precedence then the precedence of incoming symbol, then remove that operator from stack and then push it into output. repeat this process till you get the operator which is having less priority.
   b. otherwise push that incoming symbol into stack.
5) if the input symbol is closing parenthsis ')' then pop all the operators from stack, place them in to output till the opening parenthesis is encountered. dnt push parenthesis into output expressions.
6) if all symbols are extracted then pop all items from stack and push it output.
7) finally print output.

Ex1: a+b -----> ab+
Ex2: a*b+c ---> ab*c+
Ex3: a+b/c-d--> abc/+d-
Ex4: (a+b)*(c-d) ---> ab+cd-*

Ex:
---
import java.util.*;

class Test 
{
    static int precedence(char ch){
        if(ch=='+' || ch=='-')
            return 1;
        if(ch=='*' || ch=='/')
            return 2;
        return -1;
    }
    static String infixToPostfix(String s){
        String output = "";
        Stack<Character> stk = new Stack<Character>();
        for(int i=0;i<s.length();i++)
        {
            char ch = s.charAt(i);
            if(Character.isLetterOrDigit(ch))
                output=output+ch;
            else if(ch=='(')
                stk.push(ch);
            else if(ch==')'){
                while(!stk.empty() && stk.peek()!='('){
                    output=output+stk.peek();
                    stk.pop();
                }
                stk.pop();
            }
            else{
                while(!stk.empty() && precedence(ch)<=precedence(stk.peek())){
                    output = output+stk.peek();
                    stk.pop();
                }
                stk.push(ch);
            }
        }
        while(!stk.empty()){
                    output = output+stk.peek();
                    stk.pop();
        }
        return output;
    }
    public static void main(String[] args) 
    {
        System.out.println(infixToPostfix("a+b"));
        System.out.println(infixToPostfix("a*b+c"));
        System.out.println(infixToPostfix("a+b/c-d"));
        System.out.println(infixToPostfix("(a+b)*(c-d)"));
    }
}


C:\DSAJ>java Test
ab+
ab*c+
abc/+d-
ab+cd-*

infix to prefix conversion:
---------------------------

expr:     a+b
infix:    a+b
postfix:  ab+
prefix :  +ab

Algorithm:
----------
1) reverse the given input expression.
2) apply infix to post fix conversion method.
3) reverse the output expression.

implementaion:
--------------
import java.util.*;

class Test 
{
    static int precedence(char ch){
        if(ch=='+' || ch=='-')
            return 1;
        if(ch=='*' || ch=='/')
            return 2;
        return -1;
    }
    static String infixToPostfix(String s){
        String output = "";
        Stack<Character> stk = new Stack<Character>();
        for(int i=0;i<s.length();i++)
        {
            char ch = s.charAt(i);
            if(Character.isLetterOrDigit(ch))
                output=output+ch;
            else if(ch=='(')
                stk.push(ch);
            else if(ch==')'){
                while(!stk.empty() && stk.peek()!='('){
                    output=output+stk.peek();
                    stk.pop();
                }
                stk.pop();
            }
            else{
                while(!stk.empty() && precedence(ch)<=precedence(stk.peek())){
                    output = output+stk.peek();
                    stk.pop();
                }
                stk.push(ch);
            }
        }
        while(!stk.empty()){
                    output = output+stk.peek();
                    stk.pop();
        }
        return output;
    }
    static String infixToPrefix(String expr){
        String output=new String();
        output = new StringBuffer(expr).reverse().toString();
        output = infixToPostfix(output);
        output = new StringBuffer(output).reverse().toString();
        return output;
    }
    public static void main(String[] args) 
    {
        String s = "a+b";
        System.out.println(s);
        System.out.println(infixToPostfix(s));
        System.out.println(infixToPrefix(s));

        s = "a*b+c";
        System.out.println(s);
        System.out.println(infixToPostfix(s));
        System.out.println(infixToPrefix(s));

        s = "(a+b)*(c-d)";
        System.out.println(s);
        System.out.println(infixToPostfix(s));
        //System.out.println(infixToPrefix(s));
    }
}

C:\prakash>java Test
a+b
ab+
+ab
a*b+c
ab*c+
+*abc
(a+b)*(c-d)
ab+cd-*

Evaluation of Postfix Expression:-
---------------------------------
The postfix notation is used to represent algebric expressions. The expressions written in postfix form are evaluated fater compared to infix notation as parenthesis is not required in postfix.

steps:
------
1) create a stack to store operands or values.
2) we have to scan the input symbols one by one from the given expression.
   i) if the element is a number, push it into the stack.
   ii) if the element is an operator, pop operands for the operation. Eval the operator and push the result back to the stack.
3) when the expression is ended, the number in the stack is the final output.

implementation:
---------------
import java.util.*;

class Test 
{
    static int precedence(char ch){
        if(ch=='+' || ch=='-')
            return 1;
        if(ch=='*' || ch=='/')
            return 2;
        return -1;
    }
    static String infixToPostfix(String s){
        String output = "";
        Stack<Character> stk = new Stack<Character>();
        for(int i=0;i<s.length();i++)
        {
            char ch = s.charAt(i);
            if(Character.isLetterOrDigit(ch))
                output=output+ch;
            else if(ch=='(')
                stk.push(ch);
            else if(ch==')'){
                while(!stk.empty() && stk.peek()!='('){
                    output=output+stk.peek();
                    stk.pop();
                }
                stk.pop();
            }
            else{
                while(!stk.empty() && precedence(ch)<=precedence(stk.peek())){
                    output = output+stk.peek();
                    stk.pop();
                }
                stk.push(ch);
            }
        }
        while(!stk.empty()){
                    output = output+stk.peek();
                    stk.pop();
        }
        return output;
    }
    static String infixToPrefix(String expr){
        String output=new String();
        output = new StringBuffer(expr).reverse().toString();
        output = infixToPostfix(output);
        output = new StringBuffer(output).reverse().toString();
        return output;
    }
    static int evalPostfix(String expr){
        Stack<Integer> stack = new Stack<Integer>();
        for(int i=0;i<expr.length();i++){
            char ch = expr.charAt(i);
            if(Character.isDigit(ch))
                stack.push(ch-'0');
            else{
                int v1 = stack.pop();
                int v2 = stack.pop();
                switch(ch){
                    case '+': stack.push(v2+v1);break;
                    case '-': stack.push(v2-v1);break;
                    case '/': stack.push(v2/v1);break;
                    case '*': stack.push(v2*v1);break;
                }
            }
        }
        return stack.pop();
    }
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        String s = obj.nextLine();
        System.out.println(evalPostfix(infixToPostfix(s)));
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
2+3
5

C:\prakash>java Test
2+3-1
4

C:\prakash>java Test
2*3-1
5

Chapter: Queue
~~~~~~~~~~~~~~
01. Introduction to Queue
02. Types of Queues
03. Implementation of Normal Queue by using Arrays
04. Implementation of Normal Queue by using Linked List
05. Implementation of Circular Queue by using Arrays
06. Implementation of circular Queue by using Linked List
07. Implementation of Deque by using Arrays
08. Implementation of Deque by using Linked List
09. Predefined Queue interface and its implementation classes

Queue:
------
it is a linear data structure, which follows FIFO (first-in-first-out), the element which is inserted first will be item which is accessed/removed/processed. In stack data structure we are using only one pointer(top) because it is unidirectional. But in queue data structure we have to maintain two pointers one pointer is pointing at beggingin and anohter pointer is pointing at ending. the following are the different types of queues existed.

1. Normal Queues
2. Circular Queues
3. Dequeue Queues

The following are the various operations that we can able to perform on queues

1) inserting an element into queue
2) deleting an element from queue
3) displaying elements in a queue

normal queue:
~~~~~~~~~~~~~
normal queue implementation by using arrays
normal queue implementation by using linked list

Ex:
---
import java.util.*;

class NQ{
    int front,rear,size;
    int Q[];
    NQ(){
        front = -1;
        rear = -1;
        size = 6;
        Q = new int[size];
    }
    void insert(int value){
        if(rear==size){
            System.out.println("Q is full");
            return;
        }
        if(front==rear)
            front=rear=0;
        Q[rear++]=value;
    }
    void delete(){
        if(front==rear){
            System.out.println("Q is empty");
            return;
        }
        System.out.println("deleted obj:"+Q[front]);
        front++;
        if(front==rear)
            front=rear=-1;
    }
    void display(){
        if(front==rear){
            System.out.println("Q is empty");
            return;
        }
        for(int i=front;i<rear;i++){
            System.out.print(Q[i]+" ");
        }
        System.out.println();
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        NQ q = new NQ();
        q.insert(111);
        q.insert(222);
        q.insert(333);
        q.insert(444);
        q.insert(555);
        q.insert(666);
        q.display();
        q.delete();//111
        q.display();
    }
}

output:
-------
111 222 333 444 555 666
deleted obj:111
222 333 444 555 666


normal queue implementation by using linked list
------------------------------------------------
import java.util.*;

class NQList{
    Node front,rear;
    int size;
    class Node{
        int data;
        Node next;
        Node(int data,Node next){
            this.data = data;
            this.next = next;
        }
    }
    NQList(){
        front = null;
        rear = null;
        size = 0;
    }
    boolean isEmpty(){
        return size==0;
    }
    void display(){
        if(isEmpty()){
            System.out.println("Q is empty");
            return;
        }
        Node temp=front;
        while(temp!=null){
            System.out.print(temp.data+" ");
            temp = temp.next;
        }
        System.out.println();
    }
    void insert(int value){
        Node newNode = new Node(value,null);
        if(front==null && rear==null){
            front = newNode;
            rear = newNode;
        }
        else{
            rear.next = newNode;
            rear = newNode;
        }
        size++;
    }
    void delete(){
        if(isEmpty()){
            System.out.println("Q is empty");
            return;
        }
        System.out.println("deleted object content: "+front.data);
        front = front.next;
        size--;
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        NQList q = new NQList();
        q.insert(111);
        q.insert(222);
        q.insert(333);
        q.insert(444);
        q.insert(555);
        q.display();
        q.delete();
        q.delete();
        q.display();
    }
}

C:\prakashclasses>javac Test.java

C:\prakashclasses>java Test
111 222 333 444 555
deleted object content: 111
deleted object content: 222
333 444 555

circular queue:
~~~~~~~~~~~~~~~

circular queue implementation by using arrays:
----------------------------------------------
import java.util.*;

class CQArrays{
    int front,rear,c,size,Q[];
    CQArrays(){
        front=-1;
        rear=-1;
        size=5;
        c=0;
        Q=new int[5];
    }
    void insert(int value){
        if(c==size){
            System.out.println("Queue is full");
            return;
        }
        if(c==0)
            front=rear=0;
        else
            rear=(rear+1)%size;
        Q[rear] = value;
        c++;
    }
    void remove(){
        if(c==0){
            System.out.println("Queue is empty");
            return;
        }
        System.out.println("Deleted item : "+Q[front]);
        if(front==rear)
            front=rear=-1;
        else
            front=(front+1)%size;
        c--;
    }
    void display(){
        if(c==0){
            System.out.println("Q is empty");
            return;
        }
        int i=front;
        if(front<rear){
            while(i<=rear){
                System.out.print(Q[i]+" ");
                i++;
            }
        }
        else{
            while(i!=rear){
                System.out.print(Q[i]+" ");
                i=(i+1)%size;
            }
            System.out.print(Q[i]);
        }
        System.out.println();
    }
}

class Test 
{
    public static void main(String[] args) 
    {
        CQArrays q = new CQArrays();
        q.insert(111);
        q.insert(222);
        q.insert(333);
        q.insert(444);
        q.insert(555);
        q.display();
        q.insert(666);
        q.remove();
        q.insert(666);
        q.display();
    }
}

C:\prakashclasses>javac Test.java

C:\prakashclasses>java Test
111 222 333 444 555
Queue is full
Deleted item : 111
222 333 444 555 666

circular queue implementation by using linked list
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import java.util.*;

class CQList{
    Node front,rear;
    class Node{
        int value;
        Node next;
        Node(int value,Node next){
            this.value = value;
            this.next = next;
        }
    }
    void insert(int value){
        Node newNode = new Node(value,null);
        if(front==null)
            front = newNode;
        else
            rear.next = newNode;
        rear = newNode;
        rear.next = front;
    }
    void delete(){
        if(front==null){
            System.out.println("circurlar queue is empty");
        }
        else{
            if(front==rear){
                front=null;
                rear=null;
            }
            else{
                front = front.next;
                rear.next = front;
            }
        }
    }
    void display(){
        Node temp = front;
        if(temp==null){
            System.out.println("CQ is empty");
        }
        else{
            while(temp.next!=front){
                System.out.print(temp.value+" ==> ");
                temp = temp.next;
            }
            System.out.println(temp.value);
        }
    }
}

class Test 
{   
    public static void main(String[] args) 
    {
        CQList q = new CQList();
        q.insert(111);
        q.insert(222);
        q.insert(333);
        q.insert(444);
        q.insert(555);
        q.display();
        q.delete();
        q.display();
    }
}

C:\prakashclasses>javac Test.java

C:\prakashclasses>java Test
111 ==> 222 ==> 333 ==> 444 ==> 555
222 ==> 333 ==> 444 ==> 555

normal queue implementation by using arrays
normal queue implementation by using linked list
circular queue implementation by using arrays
circular queue implementation by using linked list

Deque:
~~~~~~
Deque stands for double ended queue

we can perform insertion and deletion from both sides.

void insert() and delete()

insertAtFront()
insertAtRear()
deleteAtFront()
deleteAtRear()
display()

case1: input restricted queue ---> insert, deleteAtFront and deleteRear
case2: output restricted queue --> delete, insertAtFront and insertionAtRear

deque implementation by using arrays:
-------------------------------------
import java.util.*;

class DequeArray{
    int DQ[],front,rear,size;
    DequeArray(){
        front=-1;
        rear=-1;
        size=5;
        DQ = new int[size];
    }
    void insertAtFront(int value){
        if((front==0 && rear==size-1)||(front==rear+1)){
            System.out.println("Q is full");
            return;
        }
        if(front==-1)
            front=rear=0;
        else if(front==0)
            front=size-1;
        else
            front = front-1;
        DQ[front]=value;
    }
    void insertAtRear(int value){
        if((front==0 && rear==size-1)||(front==rear+1)){
            System.out.println("DQ is full");
            return;
        }
        if(front==-1)
            front=rear=0;
        else if(rear==size-1)
            rear=0;
        else
            rear=rear+1;
        DQ[rear] = value;
    }
    void deleteAtFront(){
        if(front==-1){
            System.out.println("DQ is empty");
            return;
        }
        System.out.println("Deleted item is: "+DQ[front]);
        if(front==rear)
            front=rear=-1;
        else{
            if(front==size-1)
                front=0;
            else
                front=front+1;
        }
    }
    void deleteAtRear(){
        if(front==-1){
            System.out.println("DQ is empty");
            return;
        }
        System.out.println("Deleted item is: "+DQ[rear]);
        if(front==rear)
            front=rear=-1;
        else{
            if(rear==0)
                rear=size-1;
            else
                rear=rear-1;
        }
    }
    void display(){
        if(front==-1)
        {
            System.out.println("DQ is empty");
            return;
        }
        int left=front,right=rear;
        if(left<=right){
            while(left<=right)
                Syste
m.out.print(DQ[left++]+" ");
        }
        else{
            while(left<=size-1)
                System.out.print(DQ[left++]+" ");
            left=0;
            while(left<=right)
                System.out.print(DQ[left++]+" ");
        }
        System.out.println();
    }
}

class Test 
{       
    public static void main(String[] args) 
    {
        DequeArray q = new DequeArray();
        q.insertAtRear(111);
        q.insertAtRear(222);
        q.insertAtRear(333);
        q.insertAtRear(444);
        q.insertAtRear(555);
        q.display();
        q.deleteAtFront();//111
        q.display();
        q.deleteAtRear();//555
        q.display();
        //q.insertAtFront(999);
        q.insertAtRear(999);
        q.display();
    }
}


C:\prakashclasses>java Test
111 222 333 444 555
Deleted item is: 111
222 333 444 555
Deleted item is: 555
222 333 444
222 333 444 999

deque implementation by using linked list
-----------------------------------------
import java.util.*;

class DequeList{
    Node front;
    Node rear;
    int size;
    DequeList(){
        front = null;
        rear = null;
        size = 0;
    }
    class Node{
        int data;
        Node next;
        Node(int data,Node next){
            this.data = data;
            this.next = next;
        }
    }
    void insertAtFront(int value){
        Node newNode = new Node(value,null);
        if(front==null){
            front = newNode;
            rear = newNode;
            return;
        }
        newNode.next = front;
        front = newNode;
    }
    void insertAtRear(int value){
        Node newNode = new Node(value,null);
        if(front==null){
            front = newNode;
            rear = newNode;
            return;
        }
        rear.next = newNode;
        rear = newNode;
    }
    void deleteAtFront(){
        if(front==null){
            System.out.println("q is empty");
            return;
        }
        System.out.println("Deleted item is: "+front.data);
        front = front.next;
    }
    void deleteAtRear(){
        if(front==null){
            System.out.println("q is empty");
            return;
        }
        System.out.println("Deleted item is: "+rear.data);
        if(front==rear){
            front = null;
            rear = null;
            return;
        }
        Node temp = front;
        while(temp.next!=rear)
            temp = temp.next;
        rear = temp;
        rear.next = null;
    }
    void display(){
        if(front==null){
            System.out.println("Q is empty");
            return;
        }
        Node temp = front;
        while(temp!=null){
            System.out.print(temp.data+" ");
            temp = temp.next;
        }
        System.out.println();
    }
}

class Test 
{       
    public static void main(String[] args) 
    {
        DequeList q = new DequeList();
        q.insertAtRear(222);
        q.insertAtRear(333);
        q.insertAtRear(444);
        q.insertAtFront(111);
        q.display();
        q.deleteAtFront();
        q.display();
        q.deleteAtRear();
        q.display();
    }   
}


C:\prakashclasses>javac Test.java

C:\prakashclasses>java Test
111 222 333 444
Deleted item is: 111
222 333 444
Deleted item is: 444
222 333


queue
types of queues ---> nq, cq, deque

normal queue implementation by using arrays
normal queue implementation by using linked list
circular queue implementation by using arrays
circular queue implementation by using linked list
deque implementation by using arrays
deque implementation by using linked list

java.util package contains queue and its implementation classes

Queue interface
----------------
public boolean offer(Object) -----> add an object into queue

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Queue q = new PriorityQueue();
        System.out.println(q);//[]
        q.offer(111);
        q.offer(222);
        q.offer(333);
        System.out.println(q);//[111, 222, 333]
    }
}

public Object peek() ------> return head element
public Object element() ---> return head element

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Queue q = new PriorityQueue();
        System.out.println(q);//[]
        q.offer(111);
        q.offer(222);
        q.offer(333);
        System.out.println(q);//[111, 222, 333]
        System.out.println(q.peek());//111
        System.out.println(q.element());//111
    }
}

What is the differece between peek() and element()?
---------------------------------------------------
When queue contains some objects, then both are same. on empty queue if we call peek() method it returns null. on empty queue object if we call element() method then it returns NoSuchElementException.

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Queue q = new PriorityQueue();
        System.out.println(q);//[]
        System.out.println(q.peek());//null
        System.out.println(q.element());//RE: java.util.NoSuchElementException
    }
}

public Object poll() ----> remove and return head element from queue
public Object remove() --> remove and return head element from queue

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Queue q = new PriorityQueue();
        System.out.println(q);//[]
        q.offer(111);
        q.offer(222);
        q.offer(333);
        q.offer(444);
        System.out.println(q);//[111, 222, 333, 444]
        System.out.println(q.poll());//111
        System.out.println(q);//[222, 333, 444]
        System.out.println(q.remove());//222
        System.out.println(q);//[333, 444]
    }
}

What is the differece between poll() and remove()?
---------------------------------------------------
When queue contains some objects, then both are same. on empty queue if we call poll() method it returns null. on empty queue object if we call remove() method then it returns NoSuchElementException.

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Queue q = new PriorityQueue();
        System.out.println(q);//[]
        System.out.println(q.poll());//null
        System.out.println(q.remove());//RE: java.util.NoSuchElementException
    }
}

How internally PriorityQueue will work?
---------------------------------------
Based on Heap concept.

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Queue q = new PriorityQueue();
        System.out.println(q);//[]
        q.add(5);
        q.add(2);
        q.add(4);
        q.add(1);
        q.add(7);
        q.add(9);
        q.add(3);
        System.out.println(q);//[1, 2, 3, 5, 7, 9, 4]
    }
}

Deque:
------

public addFirst(obj) ----> add the given object into q
public offerFirst(obj) --> add the given object into q

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addFirst(111);
        q.addFirst(333);
        q.addFirst(555);
        System.out.println(q);//[555, 333, 111]
        q.offerFirst(777);
        q.offerFirst(888);
        System.out.println(q);//[888,777,555, 333, 111]
    }
}

public addLast(obj) -----> it will add the given object at the end of q
public offerLast(obj) ---> it will add the given object at the end of q

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addLast(111);
        q.addLast(333);
        q.addLast(555);
        System.out.println(q);//[111,333,555]
        q.offerLast(777);
        q.offerLast(888);
        System.out.println(q);//[111,333,555,777,888]
    }
}

public object getFirst() ---> return first object from queue
public object peekFirst() --> return first obejct from queue

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addLast(111);
        q.addLast(333);
        q.addLast(555);
        q.addLast(999);
        System.out.println(q);//[111,333,555,999]
        System.out.println(q.getFirst());//111
        System.out.println(q.peekFirst());//111

        Deque qq = new ArrayDeque();
        System.out.println(qq);//[]
        System.out.println(qq.peekFirst());//null
        System.out.println(qq.getFirst());//java.util.NoSuchElementException
    }
}

public object getLast() ---> return first object from queue
public object peekLast() --> return first obejct from queue

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addLast(111);
        q.addLast(333);
        q.addLast(555);
        q.addLast(999);
        System.out.println(q);//[111,333,555,999]
        System.out.println(q.getLast());//999
        System.out.println(q.peekLast());//999
        Deque qq = new ArrayDeque();
        System.out.println(qq);//[]
        System.out.println(qq.peekLast());//null
        System.out.println(qq.getLast());//java.util.NoSuchElementException
    }
}


Note: 

1) on empty queue if we call getFirst() ----> NSEE
2) on empty queue if we call peekFirst() ---> null
3) on empty queue if we call getLast() -----> NSEE
4) on empty queue if we cann peekLast() ----> null

public Object removeLast() ----> remove last element from queue
public Object pollLast() ------> remove last element from queue

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addLast(111);
        q.addLast(333);
        q.addLast(555);
        q.addLast(999);
        System.out.println(q);//[111,333,555,999]
        System.out.println(q.removeLast());//999
        System.out.println(q);//[111,333,555]
        System.out.println(q.pollLast());//555

        Deque qq = new ArrayDeque();
        System.out.println(qq);//[]
        System.out.println(qq.pollLast());//null
        System.out.println(qq.removeLast());//java.util.NoSuchElementException
    }
}


public Object removeFirst() -----> remove the object from first
public Object pollFirst()     ---> remove the object from first

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addLast(111);
        q.addLast(333);
        q.addLast(555);
        q.addLast(999);
        System.out.println(q);//[111,333,555,999]
        System.out.println(q.removeFirst());//111
        System.out.println(q);//[333,555,999]
        System.out.println(q.pollFirst());//333
        System.out.println(q);//[555,999]

        Deque qq = new ArrayDeque();
        System.out.println(qq);//[]
        System.out.println(qq.pollFirst());//null
        System.out.println(qq.removeFirst());//java.util.NoSuchElementException
    }
}


Note: 

1) on empty queue if we call removeFirst() ----> NSEE
2) on empty queue if we call pollFirst()    ---> null
3) on empty queue if we call removeLast() -----> NSEE
4) on empty queue if we cann pollLast()    ----> null

public boolean removeFirstOccurrece(obj) ------> it will remove first occurrence of obj
public boolean removeLastOccurrece(obj) ------> it will remove first occurrence of obj

Ex:
---
import java.util.*;

class Test 
{
    public static void main(String[] args) 
    {
        Deque q = new ArrayDeque();
        System.out.println(q);//[]
        q.addLast(111);
        q.addLast(333);
        q.addLast(111);
        q.addLast(555);
        q.addLast(111);
        System.out.println(q);//[111, 333, 111, 555, 111]
        System.out.println(q.removeFirstOccurrence(111));//true
        System.out.println(q.removeLastOccurrence(111));//true
        System.out.println(q);//[333, 111, 555]
        System.out.println(q.removeFirstOccurrence(888));//false
        System.out.println(q.removeLastOccurrence(888));//false
    }
}

Bitmanipulations:
~~~~~~~~~~~~~~~~~
number system: the way of representing numbers is called as number system. we have the following types of number systems are existed in the programming.

1) decimal number system
2) octal number system
3) hexa decimal number system
4) binary number system

decimal -----> base: 10 ------> symbols: 0,1,2,3,4,5,6,7,8,9
octal -------> base: 8 -------> symbols: 0,1,2,3,4,5,6,7
hexadecimal -> base: 16 ------> symbols: 0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f
binary ------> base: 2 -------> symbols: 0,1

10 ----> 10101010101
45.3 --> 10101011101
'f' ---> 10111010101
"abc" -> 10000010001

each and data in machine is located inside memory unit by using binary number sys

bit ---> binary digit

bit ---> 0 or 1

nibble ---> 4-bits
byte -----> 8-bits

conversion from decimal number system into binary:
--------------------------------------------------
divide the given number with 2 and catch all the remainders and reverse remainders.

Ex:
---
    5 ---> 101

    5%2=1, 5/2=2
    2%2=0, 2/2=1
    1%2=1, 1/2=0

    101 ---> 101

conversion from binary number system into decimal:
--------------------------------------------------
multiply the given bits with 2 power 0 to n-1, and find the sum

Ex:
---
    1011 ---> 11

    1x2^0 + 1x2^1 + 0x2^2 + 1x2^3 = 1+2+0+8=11


decimal     binary
-------     ------
0       0000
1       0001
2       0010
3       0011
4       0100
5       0101
6       0110
7       0111
8       1000
9       1001
10      1010

bitwise operators:
------------------
bitwise and &
bitwise or |
bitwise x-or ^
bitwise leftshift <<
bitwise rightshift >>
bitwise complement ~

bitwise and &
-------------
0 & 0 = 0
0 & 1 = 0
1 & 0 = 0
1 & 1 = 1

if both bits are 1 then only the result is 1, otherwise result is 0

Ex:
    a=5
    b=9

    A=0101
    B=1001
    ------
      0001
    ------

    a&b = 1

Ex:
---
class Test 
{
    public static void main(String[] args) 
    {
        int a = 5, b= 9;
        System.out.println(a&b);//1
    }
}

bitwise or |
------------
0 | 0 = 0
0 | 1 = 1
1 | 0 = 1
1 | 1 = 1

if any one bit is 1 then result is 1 else 0.

Ex:
    a = 5
    b = 9

    A = 0101
    B = 1001
    --------
        1101 ---> 8+4+1=13
    --------

    a | b = 13

Ex:
---
class Test 
{
    public static void main(String[] args) 
    {
        int a = 5, b= 9;
        System.out.println(a|b);//13
    }
}

bitwise x-or
------------
0 ^ 0 = 0
0 ^ 1 = 1
1 ^ 0 = 1
1 ^ 1 = 0

if both bits are in different state, then result is 1 else 0

Ex:
    a = 5
    b = 9

    A = 0101
    B = 1001
    --------
        1100 ---> 8+4=12 
    --------

Ex:
---
class Test 
{
    public static void main(String[] args) 
    {
        int a = 5, b= 9;
        System.out.println(a^b);//12
    }
}

bitwise left shift operation <<
-------------------------------
moving the bits towards left direction. 

a<<b = ax2 power b

Ex:
    5<<1 = 5x2^1 = 5x2 = 10
    5<<2 = 5x2^2 = 5x4 = 20
    5<<3 = 5x2^3 = 5x8 = 40

Ex:
---
class Test 
{
    public static void main(String[] args) 
    {
        int a = 5;
        System.out.println(a);//5
        System.out.println(a<<1); //5x2^1=10
        System.out.println(a<<2); //5x2^2=20
        System.out.println(a<<3); //5x2^3=40
    }
}

bitwise right sift >>
---------------------
moving the bits towards right direction

a>>b = a/2^b

Ex:
    5>>1 = 5/2^1=5/2=2
    5>>2 = 5/2^2=5/4=1
    5>>3 = 5/2^3=5/8=0

Ex:
class Test 
{
    public static void main(String[] args) 
    {
        int a = 5;
        System.out.println(a);//5
        System.out.println(a>>1); //5/2^1=2
        System.out.println(a>>2); //5/2^2=1
        System.out.println(a>>3); //5/2^3=0
    }
}

bitwise one's complement ~
--------------------------
we have to convert 0 into 1 and 1 into 0, if most significant bit is 0 then result is +ve number direclty we can take, if most significant bit is 1 then result is -ve number we have to take 2's complement for that number.

~n = -(n+1)

Ex:
---
    ~5 = -(5+1) = -6
    ~1 = -(1+1) = -2
    ~-8 = -(-8+1) = -(-7) = 7

Ex:
---
class Test 
{
    public static void main(String[] args) 
    {
        System.out.println(~5); //-(5+1)=-6
        System.out.println(~7); //-(7+1)=-8
        System.out.println(~1); //-(1+1)=-2
        System.out.println(~-2); //-(-2+1)=-(-1)=1
    }
}

swaping of two numbers:
-----------------------
import java.util.Scanner;
class Test 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        int a = obj.nextInt();
        int b = obj.nextInt();
        System.out.println("Before swaping   a: "+a+" and b: "+b);
        a=a^b;
        b=a^b;
        a=a^b;
        System.out.println("After swaping    a: "+a+" and b: "+b);
    }
}

output:
-------
10
13
Before swaping   a: 10 and b: 13
After swaping    a: 13 and b: 10

even or odd:
------------
0       0000
1       0001
2       0010
3       0011
4       0100
5       0101
6       0110
7       0111
8       1000
9       1001
10      1010
11      1011

if LSB (least significant bit) is 0 then that number is:  EVEN NUMBER
if LSB (least significant bit) is 1 then that number is:  ODD

Ex:
---
import java.util.Scanner;
class Test 
{
    public static void main(String[] args) 
    {
        Scanner obj = new Scanner(System.in);
        System.out.println("Enter any number:");
        int n = obj.nextInt();
        int bitMask = 1;
        if((n&bitMask)==0){
            System.out.println("EVEN number");
        }
        else{
            System.out.println("ODD number");
        }
    }
}

C:\prakash>java Test
Enter any number:
5
ODD number

C:\prakash>java Test
Enter any number:
6
EVEN number

C:\prakash>java Test
Enter any number:
7
ODD number

C:\prakash>java Test
Enter any number:
100
EVEN number

C:\prakash>java Test
Enter any number:
555
ODD number

bit level operations:
---------------------
get ith bit:
------------
Ex:
    19 ----> 10011

    0th ---> 1
    1st ---> 1
    2nd ---> 0
    3rd ---> 0
    4th ---> 1

bitmask = 1<<i

if n&bm==0 then 0 else 1

Ex:
---
import java.util.*;
class Test 
{
    public static int getIthBit(int n,int i){
        int bitMask = 1<<i;
        if((n & bitMask)==0)
            return 0;
        else
            return 1;
    }
    public static void main(String[] args) 
    {
        int n=19;//10011
        System.out.println(getIthBit(n,0));//1
        System.out.println(getIthBit(n,1));//1
        System.out.println(getIthBit(n,2));//0
        System.out.println(getIthBit(n,3));//0
        System.out.println(getIthBit(n,4));//1
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
1
1
0
0
1

set ith bit:
------------
Ex:
---
    19 ----> 10011

    set 1st bit to 0

    10011 ---> 10001 ---> 17

19,2 ---> 23
19,0 ---> 18
19,1 ---> 17


bitmask = 1<<i

formula: n^bitMask

Ex:
---
import java.util.*;
class Test 
{
    public static int setIthBit(int n,int i){
        int bitMask = 1<<i;
        return n^bitMask;
    }
    public static void main(String[] args) 
    {
        int n=19;//10011
        System.out.println(setIthBit(n,0));//18
        System.out.println(setIthBit(n,1));//17
        System.out.println(setIthBit(n,2));//23
        System.out.println(setIthBit(n,3));//27
        System.out.println(setIthBit(n,4));//3
    }
}

clear ith bit:
--------------
Ex:
    19 ----> 10011
    0th ---> 10010 --> 18
    1st ---> 10001 --> 17
    2nd ---> 10011 --> 19
    3rd ---> 10011 --> 19
    4th ---> 00011 --> 3

bitMask = ~(1<<i)

formula: n&bitMask

Ex:
---
import java.util.*;
class Test 
{
    public static int getIthBit(int n,int i){
        int bitMask = 1<<i;
        if((n & bitMask)==0)
            return 0;
        else
            return 1;
    }
    public static int setIthBit(int n,int i){
        int bitMask = 1<<i;
        return n^bitMask;
    }
    public static int clearIthBit(int n,int i){
        int bitMask = ~(1<<i);
        return n&bitMask;
    }
    public static void main(String[] args) 
    {
        int n=19;//10011
        System.out.println(clearIthBit(n,0));//18
        System.out.println(clearIthBit(n,1));//17
        System.out.println(clearIthBit(n,2));//19
        System.out.println(clearIthBit(n,3));//19
        System.out.println(clearIthBit(n,4));//3
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
18
17
19
19
3

update ith bit:
---------------
Ex:
    19 ----> 10011

    update 19, 0, 0 ----> 10010 ---> 18
    update 19, 0, 1 ----> 10011 ---> 19
    update 19, 1, 0 ----> 10001 ---> 17
    update 19, 1, 1 ----> 10010 ---> 18

    if newbit==0 then clearIthBit(n,i)
    else then setIthBit(n,i)

Ex:
---
import java.util.*;
class Test 
{
    public static int setIthBit(int n,int i){
        int bitMask = 1<<i;
        return n^bitMask;
    }
    public static int clearIthBit(int n,int i){
        int bitMask = ~(1<<i);
        return n&bitMask;
    }
    public static int updateIthBit(int n,int i,int newBit){
        if(newBit==0)
            return clearIthBit(n,i);
        else
            return setIthBit(n,i);
    }
    public static void main(String[] args) 
    {
        int n=19;//10011
        System.out.println(updateIthBit(n,0,0));//18
        System.out.println(updateIthBit(n,0,1));//
        System.out.println(updateIthBit(n,1,0));//17
        System.out.println(updateIthBit(n,1,1));//
    }
}


clear last i bits:
------------------
Ex:
    23 ----> 10111

    clear last 1 bits ---> 10110 ---> 22
    clear last 2 bits ---> 10100 ---> 20
    clear last 3 bits ---> 10000 ---> 16

    bitMask = (-1)<<i
    formula: n & bitMask

import java.util.*;
class Test 
{
    public static int clearLastIBits(int n,int i){
        int bitMask = (-1)<<i;
        return n & bitMask;
    }
    public static void main(String[] args) 
    {
        int n=19;//10011
        System.out.println(clearLastIBits(n,1));//10010=18
        System.out.println(clearLastIBits(n,2));//10000=16
    }
}

clear range of bits:
--------------------
Ex:
    157 ---> 10011101

    clear 2 to 4 ----> 10010001 --> 145
    clear 3 to 6 ----> 10000101 --> 133


number is power of 2 or not:
----------------------------
Ex:
    4 ----> yes
    5 ----> no
    6 ----> no
    7 ----> no
    8 ----> yes

formula: if n&(n-1)==0 then true else false

Ex:
---
import java.util.*;
class Test 
{
    public static boolean powerOf2(int n){
        return (n&(n-1))==0;
    }
    public static void main(String[] args) 
    {
        for(int i=0;i<=10;i++)
            System.out.println(i+"\t"+powerOf2(i));
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
0       true
1       true
2       true
3       false
4       true
5       false
6       false
7       false
8       true
9       false
10      false

count set bits in a number
--------------------------
Ex:
    n=10 ---> 1010 ---> 2


import java.util.*;
class Test 
{
    public static int countSetBits(int n){
        int c=0;
        while(n!=0){
            if((n&1)!=0)
                c++;
            n=n>>1;
        }
        return c;
    }
    public static void main(String[] args) 
    {
        for(int i=0;i<=10;i++)
            System.out.println(i+"\t"+countSetBits(i));
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
0       0
1       1
2       1
3       2
4       1
5       2
6       2
7       3
8       1
9       2
10      2

fast exponentiation:
-------------------
import java.util.*;
class Test 
{
    public static int fastExpo(int a,int n){
        int res=1;
        while(n!=0){
            if((n&1)!=0)
                res = res * a;
            a = a * a;
            n = n>>1;
        }
        return res;
    }
    public static void main(String[] args) 
    {
        System.out.println(fastExpo(2,8));//256
    }
}

clear range of bits
-------------------
clear the bits from ith location to jth location.

Ex:
    10     ----> 00001010 ---> 10
    i=2,j=4 ---> 00000010 ---> 2


import java.util.*;
class Test 
{
    static int clearBits(int n,int i,int j){
        int a = (~0)<<(j+1);
        int b = (1<<i)-1;
        int bitMask = a|b;
        return n&bitMask;
    }
    public static void main(String[] args) 
    {
        System.out.println(clearBits(10,2,7));//2
    }
}

increment an integer value by one unit
--------------------------------------
Ex:
    4 -----> 5
    10 ----> 11
    -2 ----> -1
    n -----> n+1

formula: -~n

import java.util.*;
class Test 
{
    static int increment(int n){
        return -~n;
    }
    public static void main(String[] args) 
    {
        System.out.println(increment(2));//3
        System.out.println(increment(-3));//-2
    }
}

convert given string from lower case to upper case
--------------------------------------------------
Ex:
    "abc" ----> "ABC"

import java.util.*;
class Test 
{
    static String convertToUpperCase(String s){
        String ss="";
        for(int i=0;i<s.length();i++){
            ss=ss+(char)(s.charAt(i)^32);
        }
        return ss;
    }
    public static void main(String[] args) 
    {
        System.out.println(convertToUpperCase("abc"));
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
ABC

convert upper case characters into lower case
---------------------------------------------
import java.util.*;
class Test 
{
    static String convertToLowerCase(String s){
        String ss="";
        for(int i=0;i<s.length();i++){
            ss=ss+(char)(s.charAt(i)|32);
        }
        return ss;
    }
    public static void main(String[] args) 
    {
        System.out.println(convertToLowerCase("PRAKASH"));
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
prakash


Tree data structure
graph data structure
backtracking
dynamic programming
time & space complexities


Tree data structure:
~~~~~~~~~~~~~~~~~~~~

introduction:
~~~~~~~~~~~~~
==> It is a non-linear data structure.
==> Best choice for search operations.
==> Represent hierarchical relationships (parent and child)

Ex1: Banking System Administration
Ex2: It Company Administration
Ex3: File System in every OS

A tree is a finate set of one or more nodes, such that

1) there is a specially designed node called ROOT.
2) Remaining nodes are partitioned into n>=0 disjoint sets called as SUB-TREES


Root:
-----
It is a unique node, which is not having any incoming edges.

Ex:
---
A

Node:
-----
Fundamental element of tree. Each node has data and two pointers that may point to null or its children.

Ex: A, B, C, D, E, F, G, H, I

Edge:
-----
It is fundamental part of tree, used to connect two nodes.

Ex: AB, AC, AD, BE, BF, BG, EI

Path:
-----
an ordered list of nodes, that are connected by edges is called as path.

Ex:
---

AI ---> AB, BE, EI
AH ---> AD, DH

Leaf Nodes:
-----------
A nodes which is not having any outgoing edges or any children.

Ex: I, F, G, C, H

Height of the tree:
-------------------
height of the tree is the number of edges on the longest path between root and leaf

Ex:
    AI (4)

level of the Node:
------------------
The level of node is the number of edges on the path from root to that node.


parent:
-------
node is a parent of all the child nodes that are linked by outgoing edges.

Ex:
    B, D, E

children:
---------
Nodes that are having incoming edges from same node.

Ex:
    B, C, D, E

Siblings:
---------
Nodes in the tree that are children of the same parent are called as siblings.

Ex:
    (B,C,D)
    (E,F,G)

Ancestor:
---------
A node reachable through repeated moving from child to parent.

Ex:
    I --> (E, B, A)

degree of node:
---------------
The total number of sub-trees attached to the node is called as the degree of the node.

degree of the tree:
-------------------
max number of sub-trees attached to the nodes is called as degree of the tree.


predecesor:
-----------
while displaying a tree, if a node occurrs previous to some other node, then that node is called as predecesor.

sucessor:
---------
while displaying a tree, if a node occurrs next to some other node, then that node is called as predecesor.


Ex:
    T5 T3 T6

    T3 predecesor: T5
    T3 sucessor: T6

Binary Tree:
~~~~~~~~~~~~
A binary tree is a type of tree in which each node has atmost two children(0,1 or 2). which are referred to as the left child and right child.

In binary tree each node will have one data filed and two pointer fields for representing the sub-braches. The degree of each node in the binary tree will be 2.

properties of binary trees:
---------------------------
1) max number of nodes on level i of a binary tree is 2^i.

i=0 ---> 2^0 = 1
i=1 ---> 2^1 = 2
i=2 ---> 2^2 = 4

2) there should be exatcly one path should be there from root to any node.
3) tree with 'N' nodes has exatcly "N-1" edges connecting these nodes.
4) the height of complete binary tree of N nodes is logN

Binary Tree Represenation
~~~~~~~~~~~~~~~~~~~~~~~~~
There are two wasy are there to represent binary trees.

1) sequential representation
2) linked list representation

1) sequential representation
----------------------------
Each node is sequentially arranged from top to bottom and from left to right. Let us understand this concept by numering the nodes. the number will start from root node and then remaining nodes will given with increasing numbers in level wise direction. the nodes which are in the same level will be numered from left to right.

Root  ----> 0th location
Left(n)  -> 2n+1
Right(n) -> 2n+2

2) linked list representation
-----------------------------
In linked list representation each node will be like left pointer, data of root node, right pointer.

build tree by using preorder
~~~~~~~~~~~~~~~~~~~~~~~~~~~
nodes[] = {1, 2, 4, -1, -1, 5, -1, -1, 3, -1, 6, -1, -1}


Ex:
---
import java.util.*;
class BT{
    static int index = -1;
    class Node{
        int data;
        Node left,right;
        Node(int data){
            this.data = data;
            this.left = null;
            this.right = null;
        }
    }
    Node buildTree(int nodes[]){
        index++;
        if(nodes[index]==-1)
            return null;
        Node newNode = new Node(nodes[index]);
        newNode.left = buildTree(nodes);
        newNode.right = buildTree(nodes);
        return newNode;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        int[] nodes = {1, 2, 4, -1, -1, 5, -1, -1, 3, -1, 6, -1, -1};
        BT obj = new BT();
        System.out.println(obj.buildTree(nodes).data);//1   
    }
}

Binary Tree Traversals:
~~~~~~~~~~~~~~~~~~~~~~~
Traversing the tree means visiting each node exactly once. we have three traversal methods are there.

1) Inoder traversal
2) Preorder traversal
3) Postorder traversal

1) Inoder traversal
-------------------
==> Left Node [L]
==> Root Node [D]
==> Right Node [R]

LDR

2) Preorder traversal
---------------------
==> Root Node [D]
==> Left Node [L]
==> Right Node [R]

DLR

3) Postorder traversal
----------------------
==> Left Node [L]
==> Right Node [R]
==> Root Node [D]

LRD

Ex1: diagram



import java.util.*;
class BT{
    static int index = -1;
    class Node{
        int data;
        Node left,right;
        Node(int data){
            this.data = data;
            this.left = null;
            this.right = null;
        }
    }
    Node buildTree(int nodes[]){
        index++;
        if(nodes[index]==-1)
            return null;
        Node newNode = new Node(nodes[index]);
        newNode.left = buildTree(nodes);
        newNode.right = buildTree(nodes);
        return newNode;
    }
    void inOrder(Node root){
        if(root==null)
            return;
        inOrder(root.left);
        System.out.print(root.data+" ");
        inOrder(root.right);
    }
    void preOrder(Node root){
        if(root==null)
            return;
        System.out.print(root.data+" ");
        preOrder(root.left);        
        preOrder(root.right);
    }
    void postOrder(Node root){
        if(root==null)
            return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.data+" ");        
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        int[] nodes = {1, 2, 4, -1, -1, 5, -1, -1, 3, -1, 6, -1, -1};
        BT obj = new BT();
        //obj.inOrder(obj.buildTree(nodes));
        //obj.preOrder(obj.buildTree(nodes));
        obj.postOrder(obj.buildTree(nodes));
    }
}

Ex:
---
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.right = new Node(6);

        obj.preOrder(obj.root);
        System.out.println();
        obj.postOrder(obj.root);
        System.out.println();
        obj.inOrder(obj.root);
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
1 2 3
2 3 1
2 1 3
C:\prakash>javac Test.java

C:\prakash>java Test
1 2 4 5 3 6
4 5 2 6 3 1
4 2 5 1 3 6

level order traversal:
~~~~~~~~~~~~~~~~~~~~~~
we are displaying nodes content level by level

Ex:
        1
    2       3
4      5  6    7

output:
        1
        2 3
        4 5 6 7

        pre ---> 1-2-4-5-3-6-7
        post---> 4-5-2-6-7-3-1
        in- ---> 4-2-5-1-6-3-7

Ex:
---
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static void levelOrder(Node node){
        if(node==null)
            return;
        Queue<Node> q = new LinkedList<>();
        q.add(node);
        q.add(null);

        while(!q.isEmpty()){
            Node currNode = q.remove();
            if(currNode==null){
                    System.out.println();
                    if(q.isEmpty())
                        break;
                    else
                        q.add(null);
            }
            else{
                System.out.print(currNode.data+" ");
                if(currNode.left!=null)
                    q.add(currNode.left);
                if(currNode.right!=null)
                    q.add(currNode.right);
            }
        }
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.left = new Node(6);
        obj.root.right.right = new Node(7);

        BT.levelOrder(obj.root);
    }
}

C:\prakash>javac Test.java

C:\prakash>java Test
1
2 3
4 5 6 7

count nodes
-----------
Return number of nodes present in the binary tree.

import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int countNodes(Node node){
        if(node==null)
            return 0;
        int ln = countNodes(node.left);
        int rn = countNodes(node.right);
        return ln+rn+1;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.left = new Node(6);
        obj.root.right.right = new Node(7);

        System.out.println(BT.countNodes(obj.root));//7
    }
}

C:\test>javac Test.java

C:\test>java Test
7

sum of nodes
------------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int countNodes(Node node){
        if(node==null)
            return 0;
        int ln = countNodes(node.left);
        int rn = countNodes(node.right);
        return ln+rn+1;
    }
    public static int sumOfNodes(Node node){
        if(node==null)
            return 0;
        int ls = sumOfNodes(node.left);
        int rs = sumOfNodes(node.right);
        return ls+rs+node.data;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.right = new Node(6);

        System.out.println(BT.sumOfNodes(obj.root));//21
    }
}

C:\test>javac Test.java

C:\test>java Test
21

height of tree
--------------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int countNodes(Node node){
        if(node==null)
            return 0;
        int ln = countNodes(node.left);
        int rn = countNodes(node.right);
        return ln+rn+1;
    }
    public static int sumOfNodes(Node node){
        if(node==null)
            return 0;
        int ls = sumOfNodes(node.left);
        int rs = sumOfNodes(node.right);
        return ls+rs+node.data;
    }
    public static int heightOfTree(Node node){
        if(node==null)
            return 0;
        int lh = heightOfTree(node.left);
        int rh = heightOfTree(node.right);
        int mh = Math.max(lh,rh)+1;
        return mh;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.right = new Node(6);

        System.out.println(BT.heightOfTree(obj.root));//3
    }
}

C:\test>javac Test.java

C:\test>java Test
3

search:
-------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int countNodes(Node node){
        if(node==null)
            return 0;
        int ln = countNodes(node.left);
        int rn = countNodes(node.right);
        return ln+rn+1;
    }
    public static int sumOfNodes(Node node){
        if(node==null)
            return 0;
        int ls = sumOfNodes(node.left);
        int rs = sumOfNodes(node.right);
        return ls+rs+node.data;
    }
    public static int heightOfTree(Node node){
        if(node==null)
            return 0;
        int lh = heightOfTree(node.left);
        int rh = heightOfTree(node.right);
        int mh = Math.max(lh,rh)+1;
        return mh;
    }
    public static boolean search(Node node,int value){
        if(node==null)
            return false;
        if(node.data == value)
            return true;
        if(search(node.left,value))
            return true;
        if(search(node.right,value))
            return true;
        return false;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.right = new Node(6);

        System.out.println(BT.search(obj.root,4));//true
        System.out.println(BT.search(obj.root,7));//false
    }
}

C:\test>javac Test.java

C:\test>java Test
true
false

max element
-----------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int maxElement(Node node){
        int max,left,right;
        if(node==null)
            return Integer.MIN_VALUE;
        max = node.data;
        left=maxElement(node.left);
        right=maxElement(node.right);
        if(left>max)
            max=left;
        if(right>max)
            max=right;
        return max;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.right = new Node(6);

        System.out.println(BT.maxElement(obj.root));//6
    }
}

min element
-----------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int maxElement(Node node){
        int max,left,right;
        if(node==null)
            return Integer.MIN_VALUE;
        max = node.data;
        left=maxElement(node.left);
        right=maxElement(node.right);
        if(left>max)
            max=left;
        if(right>max)
            max=right;
        return max;
    }
    public static int minElement(Node node){
        int min,left,right;
        if(node==null)
            return Integer.MAX_VALUE;
        min = node.data;
        left=minElement(node.left);
        right=minElement(node.right);
        if(left<min)
            min=left;
        if(right<min)
            min=right;
        return min;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj = new BT();
        obj.root = new Node(1);
        obj.root.left = new Node(2);
        obj.root.right = new Node(3);

        obj.root.left.left = new Node(4);
        obj.root.left.right = new Node(5);

        obj.root.right.right = new Node(6);

        System.out.println(BT.maxElement(obj.root));//6
        System.out.println(BT.minElement(obj.root));//1
    }
}

equal or not
------------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int maxElement(Node node){
        int max,left,right;
        if(node==null)
            return Integer.MIN_VALUE;
        max = node.data;
        left=maxElement(node.left);
        right=maxElement(node.right);
        if(left>max)
            max=left;
        if(right>max)
            max=right;
        return max;
    }
    public static int minElement(Node node){
        int min,left,right;
        if(node==null)
            return Integer.MAX_VALUE;
        min = node.data;
        left=minElement(node.left);
        right=minElement(node.right);
        if(left<min)
            min=left;
        if(right<min)
            min=right;
        return min;
    }
    public static boolean isEqual(Node node1,Node node2){
        if(node1==null && node2==null)
            return true;
        if(node1==null || node2==null)
            return false;
        else
            return isEqual(node1.left,node2.left) 
            && 
            isEqual(node1.right,node2.right) && node1.data == node2.data;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj1 = new BT();
        obj1.root = new Node(1);
        obj1.root.left = new Node(2);
        obj1.root.right = new Node(3);

        BT obj2 = new BT();
        obj2.root = new Node(1);
        obj2.root.left = new Node(2);
        obj2.root.right = new Node(33);

        System.out.println(BT.isEqual(obj1.root,obj2.root));//true
    }
}

copy of a tree:
---------------
import java.util.*;
class Node{
    int data;
    Node left,right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BT{
    Node root;
    BT(){
        root = null;
    }
    void preOrder(Node node){//DLR
        if(node==null)
            return;
        System.out.print(node.data+" ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){//LRD
        if(node==null)
            return;
        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.data+" ");
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    public static int maxElement(Node node){
        int max,left,right;
        if(node==null)
            return Integer.MIN_VALUE;
        max = node.data;
        left=maxElement(node.left);
        right=maxElement(node.right);
        if(left>max)
            max=left;
        if(right>max)
            max=right;
        return max;
    }
    public static int minElement(Node node){
        int min,left,right;
        if(node==null)
            return Integer.MAX_VALUE;
        min = node.data;
        left=minElement(node.left);
        right=minElement(node.right);
        if(left<min)
            min=left;
        if(right<min)
            min=right;
        return min;
    }
    public static boolean isEqual(Node node1,Node node2){
        if(node1==null && node2==null)
            return true;
        if(node1==null || node2==null)
            return false;
        else
            return isEqual(node1.left,node2.left) 
            && 
            isEqual(node1.right,node2.right) && node1.data == node2.data;
    }
    public static Node copyTree(Node node){
        Node temp;
        if(node!=null){
            temp = new Node(node.data);
            temp.left = copyTree(node.left);
            temp.right = copyTree(node.right);
            return temp;
        }
        return null;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BT obj1 = new BT();
        obj1.root = new Node(1);
        obj1.root.left = new Node(2);
        obj1.root.right = new Node(3);

        BT obj2 = new BT();
        obj2.root = BT.copyTree(obj1.root);

        System.out.println(obj1.root.data);
        System.out.println(obj2.root.data);

        System.out.println(obj1.root.left.data);
        System.out.println(obj2.root.left.data);

        System.out.println(obj1.root.right.data);
        System.out.println(obj2.root.right.data);


    }
}

C:\test>javac Test.java

C:\test>java Test
1
1
2
2
3
3

Binary Search Tree
~~~~~~~~~~~~~~~~~~
introduction to bst
-------------------
A binary search tree (BST) is a binary tree on which the nodes are are ordered in the following way.

=> The key in the left subtree is less than the key in its parent
=> the key in the right substree is greater than the key in its parent
=> no duplicates in the tree

creation of binary search tree by using arrays
----------------------------------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    Node root;
    BST(){
        root = null;
    }
    static Node createBST(int[] nodes,int start,int end){
        Node node = null;
        if(start>end)
            return null;
        int mid = (start+end)/2;
        node = new Node(nodes[mid]);
        node.left = createBST(nodes,start,mid-1);
        node.right = createBST(nodes,mid+1,end);
        return node;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        int[] nodes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        obj.root = BST.createBST(nodes,0,nodes.length-1);
        System.out.println(obj.root.data);//5
        //System.out.println(obj.root.left.data);//2
        //System.out.println(obj.root.right.data);//8
    }
}

Inserting Nodes into BST
------------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    Node root;
    BST(){
        root = null;
    }
    void inOrder(Node node){
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    void insertNode(int value){
        root = insertNode(root,value);
    }
    Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.insertNode(2);
        obj.insertNode(1);
        obj.insertNode(3);
        obj.insertNode(4);
        System.out.println();
        obj.inOrder(obj.root);//1 2 3 4
    }
}

C:\test>javac Test.java

C:\test>java Test

1 2 3 4
C:\test>

Ex:
---
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    Node root;
    BST(){
        root = null;
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    void insertNode(int value){
        root = insertNode(root,value);
    }
    Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.insertNode(6);
        obj.insertNode(4);
        obj.insertNode(2);
        obj.insertNode(5);
        obj.insertNode(1);
        obj.insertNode(3);
        obj.insertNode(8);
        obj.insertNode(7);
        obj.insertNode(9);
        obj.insertNode(10);
        obj.inOrder(obj.root);//1 2 3 4 5 6 7 8 9 10
    }
}

C:\test>java Test
1 2 3 4 5 6 7 8 9 10
C:\test>

searching for an element in BST
-------------------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    Node root;
    BST(){
        root = null;
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    void insertNode(int value){
        root = insertNode(root,value);
    }
    Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    boolean search(int value){
        Node curr = root;
        while(curr!=null){
            if(curr.data == value)
                return true;
            if(value<curr.data)
                curr=curr.left;
            else
                curr=curr.right;
        }
        return false;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.insertNode(6);
        obj.insertNode(4);
        obj.insertNode(2);
        obj.insertNode(5);
        obj.insertNode(1);
        obj.insertNode(3);
        obj.insertNode(8);
        obj.insertNode(7);
        obj.insertNode(9);
        obj.insertNode(10);
        obj.inOrder(obj.root);//1 2 3 4 5 6 7 8 9 10
        System.out.println();
        System.out.println(obj.search(9));//true
        System.out.println(obj.search(13));//false
    }
}

C:\test>javac Test.java

C:\test>java Test
1 2 3 4 5 6 7 8 9 10
true
false

min element present in the BST
------------------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    Node root;
    BST(){
        root = null;
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    void insertNode(int value){
        root = insertNode(root,value);
    }
    Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    int findMinElement(Node node){
        if(node==null)
            return -1;
        while(node.left!=null)
            node=node.left;
        return node.data;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.insertNode(6);
        obj.insertNode(4);
        obj.insertNode(2);
        obj.insertNode(5);
        obj.insertNode(1);
        obj.insertNode(3);
        obj.insertNode(8);
        obj.insertNode(7);
        obj.insertNode(9);
        obj.insertNode(10);
        obj.inOrder(obj.root);//1 2 3 4 5 6 7 8 9 10
        System.out.println();
        System.out.println(obj.findMinElement(obj.root));//1
    }
}

C:\test>javac Test.java

C:\test>java Test
1 2 3 4 5 6 7 8 9 10
1


max element in BST:
-------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    Node root;
    BST(){
        root = null;
    }
    void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    void insertNode(int value){
        root = insertNode(root,value);
    }
    Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    int findMinElement(Node node){
        if(node==null)
            return -1;
        while(node.left!=null)
            node=node.left;
        return node.data;
    }
    int findMaxElement(Node node){
        if(node==null)
            return -1;
        while(node.right!=null)
            node=node.right;
        return node.data;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.insertNode(6);
        obj.insertNode(4);
        obj.insertNode(2);
        obj.insertNode(5);
        obj.insertNode(1);
        obj.insertNode(3);
        obj.insertNode(8);
        obj.insertNode(7);
        obj.insertNode(9);
        obj.insertNode(10);
        obj.inOrder(obj.root);//1 2 3 4 5 6 7 8 9 10
        System.out.println();
        System.out.println(obj.findMaxElement(obj.root));//10
    }
}

C:\test>javac Test.java

C:\test>java Test
1 2 3 4 5 6 7 8 9 10
10

given tree is BST or not?
-------------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    static Node root;
    BST(){
        root = null;
    }
    static void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    static void insertNode(int value){
        root = insertNode(root,value);
    }
    static Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    static Node findMinElement(Node node){
        if(node==null)
            return null;
        while(node.left!=null)
            node=node.left;
        return node;
    }
    static Node findMaxElement(Node node){
        if(node==null)
            return null;
        while(node.right!=null)
            node=node.right;
        return node;
    }
    public static boolean isBST(Node node){
        if(node==null)
            return true;
        if(node.left!=null && findMaxElement(node.left).data > node.data)
            return false;
        if(node.right!=null && findMinElement(node.right).data < node.data)
            return false;
        return isBST(node.left) && isBST(node.right);
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.root = new Node(3);
        obj.root.left = new Node(2);
        obj.root.right = new Node(5);
        obj.root.left.left = new Node(1);
        obj.root.right.left = new Node(4);
        obj.root.right.right = new Node(9);
        BST.inOrder(obj.root);
        System.out.println();
        System.out.println(BST.isBST(obj.root));
    }
}

C:\test>java Test
1 2 3 4 5 9
true

C:\test>javac Test.java

C:\test>java Test
6 2 3 4 5 9
false


Deleting node from BST
----------------------
There are three cases in delete node from BST

case1: node 'x' has no children
case2: node 'x' has one child
case3: node 'x' has two children

import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    static Node root;
    BST(){
        root = null;
    }
    static void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    static void insertNode(int value){
        root = insertNode(root,value);
    }
    static Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    static Node findMinElement(Node node){
        if(node==null)
            return null;
        while(node.left!=null)
            node=node.left;
        return node;
    }
    static Node findMaxElement(Node node){
        if(node==null)
            return null;
        while(node.right!=null)
            node=node.right;
        return node;
    }
    public static boolean isBST(Node node){
        if(node==null)
            return true;
        if(node.left!=null && findMaxElement(node.left).data > node.data)
            return false;
        if(node.right!=null && findMinElement(node.right).data < node.data)
            return false;
        return isBST(node.left) && isBST(node.right);
    }
    static Node delete(Node node,int value){
        if(node.data < value)
            node.right = delete(node.right,value);
        else if(node.data > value)
            node.left = delete(node.left,value);
        else{
            if(node.left==null && node.right==null)//case1:no children
                return null;
            if(node.left==null)//case2: one child (right)
                return node.right;
            else if(node.right==null) //case2: one child (left)
                return node.left;
            Node is = findInOrderSuccessor(node.right);
            node.data = is.data;
            node.right = delete(node.right,is.data);
        }
        return node;
    }
    static Node findInOrderSuccessor(Node node){
        while(node.left!=null)
            node = node.left;
        return node;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.root = new Node(6);
        obj.root.left = new Node(4);
        obj.root.right = new Node(8);
        obj.root.left.left = new Node(2);
        obj.root.left.right = new Node(5);
        obj.root.left.left.left = new Node(1);
        obj.root.left.left.right = new Node(3);

        obj.root.right.left = new Node(7);
        obj.root.right.right = new Node(9);
        obj.root.right.right.right = new Node(10);
        BST.inOrder(obj.root);
        System.out.println();
        BST.delete(obj.root,6);
        BST.inOrder(obj.root);
        System.out.println();
    }
}

C:\test>javac Test.java

C:\test>java Test
1 2 3 4 5 6 7 8 9 10
1 2 3 4 5 7 8 9 10

remove all leaf nodes from BST
------------------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    static Node root;
    BST(){
        root = null;
    }
    static void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    static void insertNode(int value){
        root = insertNode(root,value);
    }
    static Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    public static Node leafDelete(Node node){
        if(node==null)
            return null;
        if(node.left==null && node.right==null)
            return null;
        node.left = leafDelete(node.left);
        node.right = leafDelete(node.right);
        return node;
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.root = new Node(4);
        obj.root.left = new Node(2);
        obj.root.right = new Node(6);
        obj.root.left.left = new Node(1);
        obj.root.left.right = new Node(3);

        obj.root.right.left = new Node(5);
        obj.root.right.right = new Node(7);
        BST.inOrder(obj.root);
        System.out.println();
        BST.leafDelete(obj.root);
        BST.inOrder(obj.root);
        System.out.println();
    }
}

C:\test>javac Test.java

C:\test>java Test
1 2 3 4 5 6 7
2 4 6

print in range
--------------
print the list of nodes whose values are in between k1 and k2.

import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    static Node root;
    BST(){
        root = null;
    }
    static void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    static void insertNode(int value){
        root = insertNode(root,value);
    }
    static Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    public static void printInRange(Node node,int k1,int k2){
        if(node==null)
            return;
        if(node.data>=k1 && node.data<=k2){
            printInRange(node.left,k1,k2);
            System.out.print(node.data+" ");
            printInRange(node.right,k1,k2);
        }
        else if(node.data<k1)
            printInRange(node.left,k1,k2);
        else
            printInRange(node.right,k1,k2);
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.root = new Node(8);
        obj.root.left = new Node(5);
        obj.root.right = new Node(10);
        obj.root.left.left = new Node(3);
        obj.root.left.right = new Node(6);
        obj.root.left.left.left = new Node(1);
        obj.root.left.left.right = new Node(4);

        obj.root.right.right = new Node(11);
        obj.root.right.right.right = new Node(14);
        BST.inOrder(obj.root);
        System.out.println();
        BST.printInRange(obj.root,5,12);
        System.out.println();
    }
}

C:\test>javac Test.java

C:\test>java Test
1 3 4 5 6 8 10 11 14
5 6 8 10 11

root to leaf node paths
-----------------------
import java.util.*;
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BST{
    static Node root;
    BST(){
        root = null;
    }
    static void inOrder(Node node){//LDR
        if(node==null)
            return;
        inOrder(node.left);
        System.out.print(node.data+" ");
        inOrder(node.right);
    }
    static void insertNode(int value){
        root = insertNode(root,value);
    }
    static Node insertNode(Node node,int value){
        if(node == null)
            node = new Node(value);
        else{
            if(node.data > value)
                node.left = insertNode(node.left,value);
            else
                node.right = insertNode(node.right,value);
        }
        return node;
    }
    public static void printRoot2Leaf(Node node,ArrayList<Integer> path){
        if(node==null)
            return;
        path.add(node.data);    
        if(node.left==null && node.right==null)
            printPath(path);    
        printRoot2Leaf(node.left,path);
        printRoot2Leaf(node.right,path);
        path.remove(path.size()-1);
    }
    public static void printPath(ArrayList<Integer> path){
        for(int i=0;i<path.size();i++)
            System.out.print(path.get(i)+" => ");
        System.out.println("null");
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        BST obj = new BST();
        obj.root = new Node(8);
        obj.root.left = new Node(5);
        obj.root.right = new Node(10);
        obj.root.left.left = new Node(3);
        obj.root.left.right = new Node(6);
        obj.root.left.left.left = new Node(1);
        obj.root.left.left.right = new Node(4);

        obj.root.right.right = new Node(11);
        obj.root.right.right.right = new Node(14);
        BST.inOrder(obj.root);
        System.out.println();
        BST.printRoot2Leaf(obj.root,new ArrayList<Integer>());
        System.out.println();
    }
}

C:\test>javac Test.java

C:\test>java Test
1 3 4 5 6 8 10 11 14
8 => 5 => 3 => 1 => null
8 => 5 => 3 => 4 => null
8 => 5 => 6 => null
8 => 10 => 11 => 14 => null

Backtracking:
~~~~~~~~~~~~~
backtracking a method by which a solution is found by exahaustively searching through a large volume but finate number of state with some boundary condition.

Ex:
    Rat in maze
    N-Queen
    subsets
    permutations
    towers of hanoi
    grid based problems etc

Types of backtracking problems:
-------------------------------
1) Decision --------------------> Yes / No
2) Optimized Solution ----------> One sol
3) Enumerations (all sols) -----> all sols

Backtracking with Arrays
------------------------
import java.util.*;
class Test 
{
    public static void changeArray(int[] a,int index,int value){
        if(index==a.length){//base condition
            System.out.println(Arrays.toString(a));
            return;
        }
        a[index] = value;
        changeArray(a,index+1,value+1);//recursion
        a[index] = a[index]-2;  //backtracking
    }
    public static void main(String[] args)
    {
        int[] a = new int[5];
        System.out.println(Arrays.toString(a));
        changeArray(a,0,1);
        System.out.println(Arrays.toString(a));
    }
}

C:\test>javac Test.java

C:\test>java Test
[0, 0, 0, 0, 0]
[1, 2, 3, 4, 5]
[-1, 0, 1, 2, 3]

Find Subsets:
-------------
find all subsets of the given string.

Ex: abc ----> "",a,b,c,ab,bc,ac,abc

3 char ----> 2^n ---> 2^3 = 8

abcd ----> 2^4 = 16 

import java.util.*;
class Test 
{
    public static void findSubsets(String s,String ans,int index){
        //base condition
        if(index==s.length()){
            if(ans.length()==0)
                System.out.println("null");
            else
                System.out.println(ans);
            return;
        }
        //yes condition
        findSubsets(s,ans+s.charAt(index),index+1);
        //no condition
        findSubsets(s,ans,index+1);
    }
    public static void main(String[] args)
    {
        String s = "abc";
        String ans = "";
        findSubsets(s,ans,0);
    }
}

C:\test>javac Test.java

C:\test>java Test
abc
ab
ac
a
bc
b
c
null

find permutations
-----------------
"abc" ----> abc, acb, bac, bca, cab, cba

n!  number of permutations

3! = 6

import java.util.*;
class Test 
{
    static void findPermutations(String s,String ans){
        //base condition
        if(s.length()==0){
            System.out.println(ans);
            return;
        }
        //recursion
        for(int i=0;i<s.length();i++){
            char cur = s.charAt(i);
            String ns = s.substring(0,i)+s.substring(i+1);
            findPermutations(ns,ans+cur);
        }
    }
    public static void main(String[] args)
    {
        String s = "abc";
        findPermutations(s,"");
    }
}

C:\test>javac Test.java

C:\test>java Test
abc
acb
bac
bca
cab
cba


N-queens problem:
-----------------
We have to place N-queens on NxN chess board such that no 2 queens can attack each other

import java.util.*;
class Test 
{
    static int counter;
    public static void printBoard(char[][] board){
        System.out.println("---chess board---");
        for(int i=0;i<board.length;i++){
            for(int j=0;j<board.length;j++){
                System.out.print(board[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static boolean isSafe(char[][] board,int row,int col){
        //vertical up
        for(int i=row-1;i>=0;i--){
            if(board[i][col]=='Q')
                return false;
        }
        //dia left up
        for(int i=row-1,j=col-1;i>=0 && j>=0;i--,j--){
            if(board[i][j]=='Q')
                return false;
        }
        //diag right up
        for(int i=row-1,j=col+1;i>=0 && j<board.length;i--,j++){
            if(board[i][j]=='Q')
                return false;
        }
        return true;
    }
    public static void nQueens(char[][] board,int row){
        //base condition
        if(row==board.length){
            printBoard(board);
            counter++;
            return;
        }
        //main logic (recursion and backtracking)
        for(int j=0;j<board.length;j++){//col loop
            if(isSafe(board,row,j)){
                board[row][j] = 'Q';
                nQueens(board,row+1);//recursion
                board[row][j] = 'x';
            }           
        }
    }
    public static void main(String[] args) 
    {
        int n = 5;
        char board[][] = new char[n][n];
        for(int i=0;i<board.length;i++){
            for(int j=0;j<board.length;j++){
                board[i][j]='x';
            }
        }
        nQueens(board,0);
        System.out.println("Number of solutions: "+counter);
    }
}

C:\test>javac Test.java

C:\test>java Test
---chess board---
Q x x x x
x x Q x x
x x x x Q
x Q x x x
x x x Q x
---chess board---
Q x x x x
x x x Q x
x Q x x x
x x x x Q
x x Q x x
---chess board---
x Q x x x
x x x Q x
Q x x x x
x x Q x x
x x x x Q
---chess board---
x Q x x x
x x x x Q
x x Q x x
Q x x x x
x x x Q x
---chess board---
x x Q x x
Q x x x x
x x x Q x
x Q x x x
x x x x Q
---chess board---
x x Q x x
x x x x Q
x Q x x x
x x x Q x
Q x x x x
---chess board---
x x x Q x
Q x x x x
x x Q x x
x x x x Q
x Q x x x
---chess board---
x x x Q x
x Q x x x
x x x x Q
x x Q x x
Q x x x x
---chess board---
x x x x Q
x Q x x x
x x x Q x
Q x x x x
x x Q x x
---chess board---
x x x x Q
x x Q x x
Q x x x x
x x x Q x
x Q x x x
Number of solutions: 10

Grid ways:
----------
Find number of ways to reach from (0,0) to (N-1,N-1) in a NxN grid matrix

allowed moves are ----> right or down

import java.util.*;
class Test 
{
    public static int gridWays(int i,int j,int n,int m){
        //base
        if(i==n-1 && j==m-1)
            return 1;
        else if(i==n || j==m)
            return 0;
        int value1 = gridWays(i+1,j,n,m);
        int value2 = gridWays(i,j+1,n,m);
        return value1+value2;
    }
    public static void main(String[] args) 
    {
        int n=4,m=4;
        System.out.println(gridWays(0,0,n,m));
    }
}

C:\test>javac Test.java

C:\test>java Test
6

C:\test>javac Test.java

C:\test>java Test
20

sudoku problem solver
---------------------

9x9

{
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0}
}

problem and solution

import java.util.*;
class Test 
{
    public static boolean isSafe(int sudoku[][],int row,int col,int digit){
        //column
        for(int i=0;i<=8;i++){
            if(sudoku[i][col]==digit)
                return false;
        }
        //row
        for(int j=0;j<=8;j++){
            if(sudoku[row][j]==digit)
                return false;
        }
        //grid
        int sr = (row/3)*3;
        int sc = (col/3)*3;
        //3x3 grid
        for(int i=sr;i<sr+3;i++){
            for(int j=sc;j<sc+3;j++){
                if(sudoku[i][j]==digit)
                    return false;
            }
        }
        return true;
    }
    public static boolean sudokuSolver(int sudoku[][],int row,int col){
        //base
        if(row==9 && col==0)
            return true;
        //recursion
        int nextRow = row, nextCol = col+1;
        if(col+1==9){
            nextRow = row+1;
            nextCol = 0;
        }
        if(sudoku[row][col]!=0)
            return sudokuSolver(sudoku,nextRow,nextCol);
        for(int digit=1;digit<=9;digit++){
            if(isSafe(sudoku,row,col,digit)){
                sudoku[row][col] = digit;
                if(sudokuSolver(sudoku,nextRow,nextCol)){
                    return true;
                }
                sudoku[row][col] = 0;
            }
        }
        return false;
    }
    public static void printSudoku(int sudoku[][]){
        for(int i=0;i<9;i++){
            for(int j=0;j<9;j++){
                System.out.print(sudoku[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) 
    {
        int sudoku[][]={
            {0,0,8,0,0,0,0,0,0},
            {4,9,0,1,5,7,0,0,2},
            {0,0,3,0,0,4,1,9,0},
            {1,8,5,0,6,0,0,2,0},
            {0,0,0,0,2,0,0,6,0},
            {9,6,0,4,0,5,3,0,0},
            {0,3,0,0,7,2,0,0,4},
            {0,4,9,0,3,0,0,5,7},
            {8,2,7,0,0,9,0,1,3}
        };
        printSudoku(sudoku);
        if(sudokuSolver(sudoku,0,0)){
            System.out.println("solution exists");
            printSudoku(sudoku);
        }
        else
            System.out.println("solution not exists");
    }
}


C:\test>javac Test.java

C:\test>java Test
0 0 8 0 0 0 0 0 0
4 9 0 1 5 7 0 0 2
0 0 3 0 0 4 1 9 0
1 8 5 0 6 0 0 2 0
0 0 0 0 2 0 0 6 0
9 6 0 4 0 5 3 0 0
0 3 0 0 7 2 0 0 4
0 4 9 0 3 0 0 5 7
8 2 7 0 0 9 0 1 3
solution exists
2 1 8 3 9 6 7 4 5
4 9 6 1 5 7 8 3 2
7 5 3 2 8 4 1 9 6
1 8 5 7 6 3 4 2 9
3 7 4 9 2 8 5 6 1
9 6 2 4 1 5 3 7 8
5 3 1 6 7 2 9 8 4
6 4 9 8 3 1 2 5 7
8 2 7 5 4 9 6 1 3


another:
--------
C:\test>javac Test.java

C:\test>java Test
5 4 0 0 2 0 8 0 6
0 1 9 0 0 7 0 0 3
0 0 0 3 0 0 2 1 0
9 0 0 4 0 5 0 2 0
0 0 1 0 0 0 6 0 4
6 0 4 0 3 2 0 8 0
0 6 0 0 0 0 1 9 0
4 0 2 0 0 9 0 0 5
0 9 0 0 7 0 4 0 2
solution exists
5 4 3 9 2 1 8 7 6
2 1 9 6 8 7 5 4 3
8 7 6 3 5 4 2 1 9
9 8 7 4 6 5 3 2 1
3 2 1 7 9 8 6 5 4
6 5 4 1 3 2 9 8 7
7 6 5 2 4 3 1 9 8
4 3 2 8 1 9 7 6 5
1 9 8 5 7 6 4 3 2


Graphs Data Structure:
~~~~~~~~~~~~~~~~~~~~~~
introduction:
-------------
==> collection of nodes
==> nodes or vertex or vertices
==> network of nodes
==> connections --> edges

edge
node or vertex
directed graph
undirected graph
weighted graph
unweighted graph


Representation of graph:
~~~~~~~~~~~~~~~~~~~~~~~~
1) Adjacency list (list of lists)
2) Adjacency matrix (2-d array)

import java.util.*;
class Edge{
    int s,d,w;
    Edge(int s,int d,int w){
        this.s = s;
        this.d = d;
        this.w = w;
    }
    public String toString(){
        return "{"+this.s+","+this.d+","+this.w+"}";
    }
}
class Test 
{
    public static void main(String[] args) 
    {
        int v = 5;
        ArrayList<Edge>[] graph = new ArrayList[v];
        for(int i =0;i<v;i++)
            graph[i] = new ArrayList<Edge>();

        //0th vertex
        graph[0].add(new Edge(0,1,5));

        //1st vertex
        graph[1].add(new Edge(1,0,5));
        graph[1].add(new Edge(1,2,1));
        graph[1].add(new Edge(1,3,3));

        //2nd vertex
        graph[2].add(new Edge(2,1,1));
        graph[2].add(new Edge(2,3,1));
        graph[2].add(new Edge(2,4,2));

        //3rd vertex
        graph[3].add(new Edge(3,1,3));
        graph[3].add(new Edge(3,2,1));

        //4th vertex
        graph[4].add(new Edge(4,2,2));

        /*
        //all dest verteces for node 2
        for(int i=0;i<graph[2].size();i++){
            Edge e = graph[2].get(i);
            System.out.println(e.d);
        }*/

        for(ArrayList L:graph){
            System.out.println(L);
        }
    }
}


C:\test>java Test
[{0,1,5}]
[{1,0,5}, {1,2,1}, {1,3,3}]
[{2,1,1}, {2,3,1}, {2,4,2}]
[{3,1,3}, {3,2,1}]
[{4,2,2}]





Adjacency Matrix Representation of a graph
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import java.util.*;
class Graph{
    int v;
    int[][] adj;
    Graph(int v){
        this.v = v;
        adj = new int[v][v];
    }
    void addDirectedEdge(int src,int dest,int cost){
        adj[src][dest] = cost;
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            System.out.print("vertex "+i+" is connected to: ");
            for(int j=0;j<v;j++){
                if(adj[i][j]!=0)
                    System.out.print("("+j+", "+adj[i][j]+") ");
            }
            System.out.println();
        }
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        //from vertex 0
        g.addUnDirectedEdge(0,1,1);
        g.addUnDirectedEdge(0,2,1);
        //from vertext 1
        g.addUnDirectedEdge(1,2,1);
        g.addUnDirectedEdge(1,3,1);
        //from vertext 2
        g.addUnDirectedEdge(2,3,1);

        g.printGraph();
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to: (1, 1) (2, 1)
vertex 1 is connected to: (0, 1) (2, 1) (3, 1)
vertex 2 is connected to: (0, 1) (1, 1) (3, 1)
vertex 3 is connected to: (1, 1) (2, 1)


Adjacency List Representation:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        //from vertex 0
        g.addUnDirectedEdge(0,1,1);
        g.addUnDirectedEdge(0,2,1);
        //from vertext 1
        g.addUnDirectedEdge(1,2,1);
        g.addUnDirectedEdge(1,3,1);
        //from vertext 2
        g.addUnDirectedEdge(2,3,1);

        g.printGraph();
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=> (0, 1) (2, 1) (3, 1)
vertex 2 is connected to=> (0, 1) (1, 1) (3, 1)
vertex 3 is connected to=> (1, 1) (2, 1)

Graph Traversal 
---------------
visiting every vertex is called as graph traversal

DFS and BFS

import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static void bfs(Graph g,int source){
        int v = g.v;
        boolean[] visited = new boolean[v];
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.add(source);
        visited[source]=true;
        while(!q.isEmpty()){
            int curr = q.remove();
            System.out.print(curr+" ");
            LinkedList<Edge> temp = g.adj.get(curr);
            for(Edge e:temp){
                if(visited[e.dest]==false){
                    visited[e.dest] = true;
                    q.add(e.dest);
                }
            }
        }
    }
    static void dfs(Graph g,int curr,boolean[] visited){
        System.out.print(curr+" ");
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp){
            if(visited[e.dest]==false){
                dfs(g,e.dest,visited);
            }
        }
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(5);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(1,2);
        g.addUnDirectedEdge(1,4);
        g.addUnDirectedEdge(2,3);
        g.addUnDirectedEdge(3,4);
        g.printGraph();
        System.out.print("BFS: ");
        Graph.bfs(g,0);//BFS: 0 1 2 4 3
        System.out.println();
        System.out.print("DFS: ");
        Graph.dfs(g,0,new boolean[g.v]);//DFS: 0 1 2 3 4
        /*
        Graph g = new Graph(4);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(0,2);
        g.addUnDirectedEdge(1,2);
        g.addUnDirectedEdge(2,3);
        g.printGraph();
        System.out.print("BFS: ");
        Graph.bfs(g,0);//BFS: 0 1 2 3
        System.out.println();
        System.out.print("DFS: ");
        Graph.dfs(g,0,new boolean[g.v]);//DFS: 0 1 2 3
        */
    }
}

determineing a path from vertex 'u' to vertex 'v'.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
for given source and destination, determine if a path is existed from source to dest or not.

import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static void bfs(Graph g,int source){
        int v = g.v;
        boolean[] visited = new boolean[v];
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.add(source);
        visited[source]=true;
        while(!q.isEmpty()){
            int curr = q.remove();
            System.out.print(curr+" ");
            LinkedList<Edge> temp = g.adj.get(curr);
            for(Edge e:temp){
                if(visited[e.dest]==false){
                    visited[e.dest] = true;
                    q.add(e.dest);
                }
            }
        }
    }
    static void dfs(Graph g,int curr,boolean[] visited){
        System.out.print(curr+" ");
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp){
            if(visited[e.dest]==false){
                dfs(g,e.dest,visited);
            }
        }
    }
    static boolean hasPath(Graph g,int source,int dest){
        boolean[] visited = new boolean[g.v];
        dfsUtil(g,source,visited);
        return visited[dest];
    }
    static void dfsUtil(Graph g,int curr,boolean visited[])
    {
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp)
        {
            if(visited[e.dest]==false)
                dfsUtil(g,e.dest,visited);
        }
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(6);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(0,2);
        g.addUnDirectedEdge(1,3);
        g.addUnDirectedEdge(1,4);
        g.addUnDirectedEdge(2,3);
        g.addUnDirectedEdge(3,4);
        g.printGraph();
        System.out.println(Graph.hasPath(g,0,4));//true
        System.out.println(Graph.hasPath(g,0,5));//false
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=> (0, 1) (3, 1) (4, 1)
vertex 2 is connected to=> (0, 1) (3, 1)
vertex 3 is connected to=> (1, 1) (2, 1) (4, 1)
vertex 4 is connected to=> (1, 1) (3, 1)
vertex 5 is connected to=>
true
false

Count all paths from source to destination
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Given source and destination verteces. Count of all paths from source to destination.


import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static void bfs(Graph g,int source){
        int v = g.v;
        boolean[] visited = new boolean[v];
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.add(source);
        visited[source]=true;
        while(!q.isEmpty()){
            int curr = q.remove();
            System.out.print(curr+" ");
            LinkedList<Edge> temp = g.adj.get(curr);
            for(Edge e:temp){
                if(visited[e.dest]==false){
                    visited[e.dest] = true;
                    q.add(e.dest);
                }
            }
        }
    }
    static void dfs(Graph g,int curr,boolean[] visited){
        System.out.print(curr+" ");
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp){
            if(visited[e.dest]==false){
                dfs(g,e.dest,visited);
            }
        }
    }
    static int countAllPaths(Graph g,int source,int dest)
    {
        boolean[] visited = new boolean[g.v];
        return countAllPaths(g,visited,source,dest);
    }
    static int countAllPaths(Graph g,boolean[] visited,int source,int dest)
    {
        if(source==dest)
            return 1;
        int c = 0;
        visited[source] = true;
        //recursion logic
        LinkedList<Edge> temp = g.adj.get(source);
        for(Edge e:temp)
        {
            if(visited[e.dest]==false)
                c=c+countAllPaths(g,visited,e.dest,dest);
        }
        //backtracking
        visited[source] = false;
        return c;
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(5);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(0,2);
        g.addUnDirectedEdge(1,3);
        g.addUnDirectedEdge(1,4);
        g.addUnDirectedEdge(2,3);
        g.addUnDirectedEdge(3,4);
        g.printGraph();
        System.out.println(Graph.countAllPaths(g,0,4));//4
        System.out.println(Graph.countAllPaths(g,0,2));//4
    }
}

print all paths from source to destination
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static void bfs(Graph g,int source){
        int v = g.v;
        boolean[] visited = new boolean[v];
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.add(source);
        visited[source]=true;
        while(!q.isEmpty()){
            int curr = q.remove();
            System.out.print(curr+" ");
            LinkedList<Edge> temp = g.adj.get(curr);
            for(Edge e:temp){
                if(visited[e.dest]==false){
                    visited[e.dest] = true;
                    q.add(e.dest);
                }
            }
        }
    }
    static void dfs(Graph g,int curr,boolean[] visited){
        System.out.print(curr+" ");
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp){
            if(visited[e.dest]==false){
                dfs(g,e.dest,visited);
            }
        }
    }
    static void printAllPaths(Graph g,int source,int dest)
    {
        boolean[] visited = new boolean[g.v];
        Stack<Integer> path = new Stack<Integer>();
        printAllPaths(g,visited,source,dest,path);
    }
    static void printAllPaths(Graph g,boolean[] visited,int source,int dest,Stack<Integer> path)
    {
        path.push(source);
        if(source==dest)
        {
            System.out.println(path);
            path.pop();
            return;
        }
        visited[source] = true;
        //recursion logic
        LinkedList<Edge> temp = g.adj.get(source);
        for(Edge e:temp)
        {
            if(visited[e.dest]==false)
                printAllPaths(g,visited,e.dest,dest,path);
        }
        //backtracking
        visited[source] = false;
        path.pop();
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(5);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(0,2);
        g.addUnDirectedEdge(1,3);
        g.addUnDirectedEdge(1,4);
        g.addUnDirectedEdge(2,3);
        g.addUnDirectedEdge(3,4);
        g.printGraph();
        Graph.printAllPaths(g,0,4);
        Graph.printAllPaths(g,0,2);
    }
}

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=> (0, 1) (3, 1) (4, 1)
vertex 2 is connected to=> (0, 1) (3, 1)
vertex 3 is connected to=> (1, 1) (2, 1) (4, 1)
vertex 4 is connected to=> (1, 1) (3, 1)
[0, 1, 3, 4]
[0, 1, 4]
[0, 2, 3, 1, 4]
[0, 2, 3, 4]
[0, 1, 3, 2]
[0, 1, 4, 3, 2]
[0, 2]

Detect cycle in an undirected graph
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ex1:
---
import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static boolean detectCycle(Graph g){
        boolean[] visited = new boolean[g.v];
        for(int i=0;i<g.v;i++){
            if(!visited[i]){
                if(detectCycleUtil(g,visited,i,-1))
                    return true;
            }
        }
        return false;
    }
    static boolean detectCycleUtil(Graph g,boolean[] visited,int curr,int parent)
    {
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e : temp){
            if(!visited[e.dest]){//case1
                if(detectCycleUtil(g,visited,e.dest,curr))
                    return true;
            }
            else if(visited[e.dest] && e.dest!=parent)//case2
                return true;
            //case3: do nothing
        }
        return false;
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(0,2);
        g.addUnDirectedEdge(1,2);
        g.addUnDirectedEdge(2,3);
        g.printGraph();
        System.out.println(Graph.detectCycle(g));//true
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=> (0, 1) (2, 1)
vertex 2 is connected to=> (0, 1) (1, 1) (3, 1)
vertex 3 is connected to=> (2, 1)
true

Ex2:
----
import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static boolean detectCycle(Graph g){
        boolean[] visited = new boolean[g.v];
        for(int i=0;i<g.v;i++){
            if(!visited[i]){
                if(detectCycleUtil(g,visited,i,-1))
                    return true;
            }
        }
        return false;
    }
    static boolean detectCycleUtil(Graph g,boolean[] visited,int curr,int parent)
    {
        visited[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e : temp){
            if(!visited[e.dest]){//case1
                if(detectCycleUtil(g,visited,e.dest,curr))
                    return true;
            }
            else if(visited[e.dest] && e.dest!=parent)//case2
                return true;
            //case3: do nothing
        }
        return false;
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        g.addUnDirectedEdge(0,1);
        g.addUnDirectedEdge(0,2);
        g.addUnDirectedEdge(2,3);
        g.printGraph();
        System.out.println(Graph.detectCycle(g));//false
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=> (0, 1)
vertex 2 is connected to=> (0, 1) (3, 1)
vertex 3 is connected to=> (2, 1)
false

Detect cycle in an directed graph
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ex1:
----
import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static boolean isCycle(Graph g){
        boolean[] visited = new boolean[g.v];
        boolean[] stack = new boolean[g.v];
        for(int i=0;i<g.v;i++){
            if(!visited[i]){
                if(isCycleUtil(g,visited,stack,i))
                    return true;
            }
        }
        return false;
    }
    static boolean isCycleUtil(Graph g,boolean[] visited,boolean[] stack,int curr)
    {
        visited[curr] = true;
        stack[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp)
        {
            if(stack[e.dest])
                return true;
            if(!visited[e.dest] && isCycleUtil(g,visited,stack,e.dest))
                return true;
        }
        stack[curr] = false;//backtracking
        return false;
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        g.addDirectedEdge(0,1);
        g.addDirectedEdge(0,2);
        g.addDirectedEdge(2,3);
        g.addDirectedEdge(1,3);
        g.printGraph();
        System.out.println(Graph.isCycle(g));//false
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=> (3, 1)
vertex 2 is connected to=> (3, 1)
vertex 3 is connected to=>
false

Ex2:
----
import java.util.*;
class Graph{
    static class Edge{
        int dest;
        int cost;
        Edge(int dest,int cost){
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static boolean isCycle(Graph g){
        boolean[] visited = new boolean[g.v];
        boolean[] stack = new boolean[g.v];
        for(int i=0;i<g.v;i++){
            if(!visited[i]){
                if(isCycleUtil(g,visited,stack,i))
                    return true;
            }
        }
        return false;
    }
    static boolean isCycleUtil(Graph g,boolean[] visited,boolean[] stack,int curr)
    {
        visited[curr] = true;
        stack[curr] = true;
        LinkedList<Edge> temp = g.adj.get(curr);
        for(Edge e:temp)
        {
            if(stack[e.dest])
                return true;
            if(!visited[e.dest] && isCycleUtil(g,visited,stack,e.dest))
                return true;
        }
        stack[curr] = false;//backtracking
        return false;
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        g.addDirectedEdge(0,1);
        g.addDirectedEdge(0,2);
        g.addDirectedEdge(2,3);
        g.addDirectedEdge(3,0);
        g.printGraph();
        System.out.println(Graph.isCycle(g));//true
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 1) (2, 1)
vertex 1 is connected to=>
vertex 2 is connected to=> (3, 1)
vertex 3 is connected to=> (0, 1)
true

Shortest Path from source to all vertices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import java.util.*;
class Graph{
    static class Edge{
        int source;
        int dest;
        int cost;
        Edge(int source,int dest,int cost){
            this.source = source;
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(src,dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(src,dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static class Pair implements Comparable<Pair>
    {
        int n;
        int path;
        public Pair(int n,int path){
            this.n = n;
            this.path = path;
        }
        public int compareTo(Pair p){
            return this.path - p.path;
        }
    }
    static void dijkstra(Graph g,int source){
        int[] dist = new int[g.v];
        for(int i=0;i<g.v;i++){
            if(i!=source)
                dist[i] = Integer.MAX_VALUE;
        }
        //logic
        boolean[] visited = new boolean[g.v];
        PriorityQueue<Pair> pq = new PriorityQueue<>();
        pq.add(new Pair(source,0));
        while(!pq.isEmpty()){
            Pair curr = pq.remove();
            if(!visited[curr.n]){
                visited[curr.n] = true;
                LinkedList<Edge> temp = g.adj.get(curr.n);
                for(Edge e:temp){
                    int u = e.source;
                    int v = e.dest;
                    int wt = e.cost;
                    if(dist[u]+wt <dist[v]){
                        dist[v] = dist[u]+wt;
                        pq.add(new Pair(v,dist[v]));
                    }
                }
            }
        }

        for(int i=0;i<g.v;i++)
            System.out.print(dist[i]+" ");
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(6);
        g.addDirectedEdge(0,1,2);
        g.addDirectedEdge(0,2,4);
        g.addDirectedEdge(1,2,1);
        g.addDirectedEdge(1,3,7);
        g.addDirectedEdge(2,4,3);
        g.addDirectedEdge(3,5,1);
        g.addDirectedEdge(4,3,2);
        g.addDirectedEdge(4,5,5);
        g.printGraph();
        Graph.dijkstra(g,0);
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 2) (2, 4)
vertex 1 is connected to=> (2, 1) (3, 7)
vertex 2 is connected to=> (4, 3)
vertex 3 is connected to=> (5, 1)
vertex 4 is connected to=> (3, 2) (5, 5)
vertex 5 is connected to=>
0 2 3 8 6 9

Bellman Ford Algorithm:
~~~~~~~~~~~~~~~~~~~~~~~
Shortest path from the source vertex to all vertices (negative edges/weights).


import java.util.*;
class Graph{
    static class Edge{
        int source;
        int dest;
        int cost;
        Edge(int source,int dest,int cost){
            this.source = source;
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(src,dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(src,dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static class Pair implements Comparable<Pair>
    {
        int n;
        int path;
        public Pair(int n,int path){
            this.n = n;
            this.path = path;
        }
        public int compareTo(Pair p){
            return this.path - p.path;
        }
    }
    static void bellManFord(Graph g,int source){
        int dist[] = new int[g.v];
        for(int i=0;i<dist.length;i++){
            if(i!=source)
                dist[i] = Integer.MAX_VALUE;
        }
        for(int i=0;i<g.v-1;i++){
            for(int j=0;j<g.v;j++){
                LinkedList<Edge> temp = g.adj.get(j);
                for(Edge e :temp){
                    int u = e.source;
                    int v = e.dest;
                    int wt = e.cost;
                    if(dist[u] != Integer.MAX_VALUE && dist[u]+wt < dist[v])
                        dist[v] = dist[u] + wt;
                }
            }
        }
        for(int i=0;i<dist.length;i++)
            System.out.print(dist[i]+" ");
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(5);
        g.addDirectedEdge(0,1,2);
        g.addDirectedEdge(0,2,4);
        g.addDirectedEdge(1,2,-4);
        g.addDirectedEdge(2,3,2);
        g.addDirectedEdge(3,4,4);
        g.addDirectedEdge(4,1,-1);
        g.printGraph();
        Graph.bellManFord(g,0);
    }
}

C:\test>java Test
vertex 0 is connected to=> (1, 2) (2, 4)
vertex 1 is connected to=> (2, -4)
vertex 2 is connected to=> (3, 2)
vertex 3 is connected to=> (4, 4)
vertex 4 is connected to=> (1, -1)
0 2 -2 0 4

Minimum spanning tree (MST)
~~~~~~~~~~~~~~~~~~~~~~~~~~~
A minimum cost spanning tree or minimum weight spanning tree MST is asubset of the edges of a connected edge weighted undirected graph that connects all the vertices together without any cycle and min cost should be there.


Ex:
---
import java.util.*;
class Graph{
    static class Edge{
        int source;
        int dest;
        int cost;
        Edge(int source,int dest,int cost){
            this.source = source;
            this.dest = dest;
            this.cost = cost;
        }
    }
    int v;
    static LinkedList<LinkedList<Edge>> adj;
    Graph(int v){
        this.v = v;
        adj = new LinkedList<LinkedList<Edge>>();
        for(int i=0;i<v;i++)
            adj.add(new LinkedList<Edge>());
    }
    void addDirectedEdge(int src,int dest,int cost){
        Edge edge = new Edge(src,dest,cost);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest,int cost){
        addDirectedEdge(src,dest,cost);
        addDirectedEdge(dest,src,cost);
    }
    void addDirectedEdge(int src,int dest){
        Edge edge = new Edge(src,dest,1);
        adj.get(src).add(edge);
    }
    void addUnDirectedEdge(int src,int dest){
        addDirectedEdge(src,dest,1);
        addDirectedEdge(dest,src,1);
    }
    void printGraph(){
        for(int i=0;i<v;i++){
            LinkedList<Edge> temp = adj.get(i);
            System.out.print("vertex "+i+" is connected to=> ");
            for(Edge e : temp){
                System.out.print("("+e.dest+", "+e.cost+") ");
            }
            System.out.println();
        }
    }
    static class Pair implements Comparable<Pair>
    {
        int n;
        int cost;
        public Pair(int n,int cost){
            this.n = n;
            this.cost = cost;
        }
        public int compareTo(Pair p){
            return this.cost - p.cost;
        }
    }
    static void primsMcst(Graph g)
    {
        boolean[] visited = new boolean[g.v];
        PriorityQueue<Pair> pq = new PriorityQueue<>();
        pq.add(new Pair(0,0));
        int finalcost = 0;
        while(!pq.isEmpty()){
            Pair curr = pq.remove();
            if(!visited[curr.n]){
                visited[curr.n] = true;
                finalcost = finalcost+curr.cost;
                LinkedList<Edge> temp = g.adj.get(curr.n);
                for(Edge e:temp){
                    pq.add(new Pair(e.dest,e.cost));
                }
            }
        }
        System.out.println(finalcost);
    }
}
class Test
{
    public static void main(String[] args)
    {
        Graph g = new Graph(4);
        g.addDirectedEdge(0,1,10);
        g.addDirectedEdge(0,2,15);
        g.addDirectedEdge(0,3,30);
        g.addDirectedEdge(1,3,40);
        g.addDirectedEdge(2,3,50);
        g.printGraph();
        Graph.primsMcst(g);
    }
}

C:\test>javac Test.java

C:\test>java Test
vertex 0 is connected to=> (1, 10) (2, 15) (3, 30)
vertex 1 is connected to=> (3, 40)
vertex 2 is connected to=> (3, 50)
vertex 3 is connected to=>
55

Krushkals algorithm:
--------------------
import java.util.*;
class Test
{
    static class Edge implements Comparable<Edge>{
        int source;
        int dest;
        int cost;
        Edge(int source,int dest,int cost){
            this.source = source;
            this.dest = dest;
            this.cost = cost;
        }
        public int compareTo(Edge e2){
            return this.cost - e2.cost;
        }
    }
    static void createGraph(ArrayList<Edge> edges)
    {
        edges.add(new Edge(0,1,10));
        edges.add(new Edge(0,2,15));
        edges.add(new Edge(0,3,30));
        edges.add(new Edge(1,3,40));
        edges.add(new Edge(2,3,50));
    }
    static int n = 4;
    static int par[] = new int[n];
    static int rank[] = new int[n];
    public static void init(){
        for(int i=0;i<n;i++)
            par[i] = i;
    }
    public static int find(int x){
        if(par[x] == x)
            return x;
        return par[x] = find(par[x]);
    }
    public static void union(int a,int b){
        int parA = find(a);
        int parB = find(b);
        if(rank[parA]==rank[parB]){
            par[parB]=parA;
            rank[parA]++;
        }
        else if(rank[parA]<rank[parB])
            par[parA] = parB;
        else
            par[parB] = parA;
    }
    public static void krushkals(ArrayList<Edge> edges,int v)
    {
        init();
        Collections.sort(edges);
        int mstCost = 0;
        int count = 0;
        for(int i=0;count<v-1;i++){
            Edge e = edges.get(i);
            int parA = find(e.source);
            int parB = find(e.dest);
            if(parA!=parB)
            {
                union(e.source,e.dest);
                mstCost = mstCost + e.cost;
                count++;
            }
        }
        System.out.println(mstCost);
    }
    public static void main(String[] args)
    {
        int v = 4;
        ArrayList<Edge> edges = new ArrayList<>();
        createGraph(edges);
        krushkals(edges,v);
    }
}

C:\test>javac Test.java

C:\test>java Test
55

Priority Queues / Heaps
~~~~~~~~~~~~~~~~~~~~~~~~

introduction:
-------------
1) it is also know as Heaps
2) items are inserted at end and removed from begining.
3) PQ is logical ordering of objects based on priority.
4) when we add an object in to PQ, reordering must be required (heapify).
5) when we remove an object from PQ, reordering must be required (heapify).

Ex:
    Prims algorithm
    Rank processing
    student admission based on percentage
    etc

6) PQ is implemented based on heaps
7) It is implemented by using arrays only.

predefined implementation of PQ:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Ex1:
----
Sort the given integer values based on asc order

import java.util.*;

class Test
{
    public static void main(String[] args)
    {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.add(3);
        pq.add(4);
        pq.add(1);
        pq.add(7);
        while(!pq.isEmpty()){
            System.out.println(pq.peek());//first most element
            pq.remove();//first element will be removed
        }
    }
}
C:\test>javac Test.java

C:\test>java Test
1
3
4
7

Ex2:
----
sort integer values based on desc order using PQ

import java.util.*;

class Test
{
    public static void main(String[] args)
    {
        PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
        pq.add(3);
        pq.add(4);
        pq.add(1);
        pq.add(7);
        while(!pq.isEmpty()){
            System.out.println(pq.peek());//first most element
            pq.remove();//first element will be removed
        }
    }
}

C:\test>javac Test.java

C:\test>java Test
7
4
3
1

Ex3: sort the students based on htno in asc order
-------------------------------------------------
import java.util.*;

class Student implements Comparable<Student>{
    int htno;
    String name;
    Student(int htno,String name){
        this.htno = htno;
        this.name = name;
    }
    public String toString(){
        return "["+this.htno+", "+this.name+"]";
    }
    public int compareTo(Student temp){
        return this.htno - temp.htno;
    }
}
class Test
{
    public static void main(String[] args)
    {
        PriorityQueue<Student> pq = new PriorityQueue<>();
        pq.add(new Student(1,"Prakash"));
        pq.add(new Student(4,"Raju"));
        pq.add(new Student(2,"Kiran"));
        pq.add(new Student(3,"Karan"));
        pq.add(new Student(9,"Abhi"));
        while(!pq.isEmpty()){
            System.out.println(pq.peek());//first most element
            pq.remove();//first element will be removed
        }
    }
}

C:\test>javac Test.java

C:\test>java Test
[1, Prakash]
[2, Kiran]
[3, Karan]
[4, Raju]
[9, Abhi]


Ex4: sort the students based on htno in desc order
-------------------------------------------------
import java.util.*;

class Student implements Comparable<Student>{
    int htno;
    String name;
    Student(int htno,String name){
        this.htno = htno;
        this.name = name;
    }
    public String toString(){
        return "["+this.htno+", "+this.name+"]";
    }
    public int compareTo(Student temp){
        return this.htno - temp.htno;
    }
}
class Test
{
    public static void main(String[] args)
    {
        PriorityQueue<Student> pq = new PriorityQueue<>(Collections.reverseOrder());
        pq.add(new Student(1,"Prakash"));
        pq.add(new Student(4,"Raju"));
        pq.add(new Student(2,"Kiran"));
        pq.add(new Student(3,"Karan"));
        pq.add(new Student(9,"Abhi"));
        while(!pq.isEmpty()){
            System.out.println(pq.peek());//first most element
            pq.remove();//first element will be removed
        }
    }
}

C:\test>javac Test.java

C:\test>java Test
[9, Abhi]
[4, Raju]
[3, Karan]
[2, Kiran]
[1, Prakash]

Heap
----
* It is a binary tree (at most two children)
* it is complete binary tree
  ==> all levels are completely filled except last level
  ==> filling should be done from left to right
* heap order property
  ==> children values >= parent values ---> minHeap ASC
  ==> children values <= parent values ---> maxHeap DESC


we can't represent heap tree in class list format ---> we will use array

Node at position x

left(x) ----> 2x+1
right(x) ---> 2x+2

min heap ---> Root Node must be smallest value
max heap ---> Root Node must be larger value

Insertion of an element in heap
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
step1: add the element at end
step3: fix heap (normal method)

Ex:
---
        public void add(int data){
            //1. add the data at end
            arr.add(data);
            //2. fix heap
            int ci = arr.size()-1;
            int pi = (ci-1)/2;
            while(arr.get(ci)<arr.get(pi)){
                int temp = arr.get(ci);
                arr.set(ci,arr.get(pi));
                arr.set(pi,temp);
                ci = pi;
                pi = (ci-1)/2;
            }
        }

deletetion in heap
------------------

import java.util.*;

class Test
{
    static class Heap{
        ArrayList<Integer> arr = new ArrayList<>();
        public void add(int data){
            //1. add the data at end
            arr.add(data);
            //2. fix heap
            int ci = arr.size()-1;
            int pi = (ci-1)/2;
            while(arr.get(ci)<arr.get(pi)){
                int temp = arr.get(ci);
                arr.set(ci,arr.get(pi));
                arr.set(pi,temp);
                ci = pi;
                pi = (ci-1)/2;
            }
        }
        public int remove(){
            int data = arr.get(0);
            //step1: swap first and last
            int temp = arr.get(0);
            arr.set(0,arr.get(arr.size()-1));
            arr.set(arr.size()-1,temp);
            //step2: delete last
            arr.remove(arr.size()-1);
            //step3: heapify
            heapify(0);
            return data;
        }
        public void heapify(int index)
        {
            int left = 2*index + 1;
            int right = 2*index + 2;
            int mi = index;
            if(left<arr.size() && arr.get(mi)>arr.get(left))
                mi = left;
            if(right<arr.size() && arr.get(mi)>arr.get(right))
                mi = right;
            if(index!=mi){
                int temp = arr.get(index);
                arr.set(index,arr.get(mi));
                arr.set(mi,temp);
                heapify(mi);
            }
        }
        public int peek(){
            return arr.get(0);
        }
        public boolean isEmpty(){
            return arr.size()==0;
        }
    }       
    public static void main(String[] args)
    {
        Heap h = new Heap();
        h.add(3);
        h.add(4);
        h.add(1);
        h.add(5);
        while(!h.isEmpty()){
            System.out.println(h.peek());
            h.remove();
        }
    }
}

C:\test>javac Test.java

C:\test>java Test
1
3
4
5

heap sort:
----------
==> ascending order -----> max heap
==> descending order ----> min heap

import java.util.*;

class Test
{
    public static void heapSortAsc(int[] a){
        //step1: to build max heap
        int n=a.length;
        for(int i=n/2;i>=0;i--)
            heapify(a,i,n);
        //step2: push largest element at end
        for(int i=n-1;i>0;i--){
            //swap
            int temp;
            temp=a[0];
            a[0] = a[i];
            a[i] = temp;
            heapify(a,0,i);
        }
    }
    public static void heapify(int[] a,int i,int n){
        int left = 2*i+1;
        int right = 2*i+2;
        int maxIndex = i;
        if(left<n && a[left] > a[maxIndex])
            maxIndex = left;
        if(right<n && a[right] > a[maxIndex])
            maxIndex = right;
        if(maxIndex!=i)
        {
            //swap
            int temp = a[i];
            a[i] = a[maxIndex];
            a[maxIndex] = temp;
            heapify(a,0,i);
        }
    }
    public static void main(String[] args)
    {
        int[] a = {1,2,4,5,3};
        System.out.println(Arrays.toString(a));//[1,2,4,5,3]
        heapSortAsc(a);
        System.out.println(Arrays.toString(a));//[1,2,3,4,5]
    }
}

C:\test>javac Test.java

C:\test>java Test
[1, 2, 4, 5, 3]
[1, 2, 3, 4, 5]

Ex:
---
import java.util.*;

class Test
{
    public static void heapSortDesc(int[] a){
        //step1: to build min heap
        int n=a.length;
        for(int i=n/2;i>=0;i--)
            heapify(a,i,n);
        //step2: push smallest element at end
        for(int i=n-1;i>0;i--){
            //swap
            int temp;
            temp=a[0];
            a[0] = a[i];
            a[i] = temp;
            heapify(a,0,i);
        }
    }
    public static void heapify(int[] a,int i,int n){
        int left = 2*i+1;
        int right = 2*i+2;
        int minIndex = i;
        if(left<n && a[left] < a[minIndex])
            minIndex = left;
        if(right<n && a[right] < a[minIndex])
            minIndex = right;
        if(minIndex!=i)
        {
            //swap
            int temp = a[i];
            a[i] = a[minIndex];
            a[minIndex] = temp;
            heapify(a,0,i);
        }
    }
    public static void main(String[] args)
    {
        int[] a = {1,2,4,5,3};
        System.out.println(Arrays.toString(a));//[1,2,4,5,3]
        heapSortDesc(a);
        System.out.println(Arrays.toString(a));//[1,2,3,4,5]
    }
}

C:\test>javac Test.java

C:\test>java Test
[1, 2, 4, 5, 3]
[5, 4, 3, 2, 1]

Divide and Conquer Algs:
~~~~~~~~~~~~~~~~~~~~~~~~
==> In D and C, we will break the problem into sub-problems.
==> Then solve sub-problems independently.
==> Combine the solutions to get solution for main problems.
==> we have to solve this sub-problems by using recursion.
==> This D and C has three forms

    1) Divide -----> smaller instances
    2) Conquer ----> solve sub problems recursively
    3) Combine ----> combine the solutions

Ex:
---
    Merge Sort
    Binary Search
    Quick sort
    Towers of Hanoi etc



Dynamic Programming:
~~~~~~~~~~~~~~~~~~~~
=> almost it is same like recursion.
=> recursion with optimized solution is nothing but DP.

Ex: Fibonacci Sequence General Method
--------------------------------------
import java.util.*;

class Test
{
    static int c1 = 0;
    public static int fibGeneral(int n){
        c1++;
        if(n==0||n==1)
            return n;
        else
            return fibGeneral(n-1)+fibGeneral(n-2);
    }
    public static void main(String[] args)
    {
        //0, 1, 1, 2, 3, 5, 8, .....
        int n = 6;
        System.out.println(fibGeneral(n));//8
        System.out.println("Number of calls: "+c1);
    }
}

C:\test>javac Test.java

C:\test>java Test
8
Number of calls: 25


Ex: Fibonacci Sequence Modified Method
--------------------------------------
import java.util.*;

class Test
{
    static int c1 = 0;
    static int c2 = 0;
    public static int fibGeneral(int n){
        c1++;
        if(n==0||n==1)
            return n;
        else
            return fibGeneral(n-1)+fibGeneral(n-2);
    }
    public static int fibModified(int n,int[] f){
        c2++;
        if(n==0||n==1)
            return n;
        if(f[n]!=0)
            return f[n];
        f[n] = fibModified(n-1,f)+fibModified(n-2,f);
        return f[n];
    }
    public static void main(String[] args)
    {
        //0, 1, 1, 2, 3, 5, 8, .....
        int n = 6;
        System.out.println(fibGeneral(n));//8
        System.out.println("Number of calls: "+c1);//25
        int[] f = new int[n+1];
        System.out.println(fibModified(n,f));//8
        System.out.println("Number of calls: "+c2);//
    }
}

C:\test>javac Test.java

C:\test>java Test
8
Number of calls: 25
8
Number of calls: 11

What is dynamic programming?
----------------------------
Dynamic programming is optimized recursion process.

How to identify dynamic programming is applicable for a problem?

1) optimal problem
2) some choice is given (multiple branches)

there are two ways are there to solve dynamic programming

1) memoization (top down) (recursion, subproblems (reuse))
2) tabulation (bottom up) (intialization, filling an array)

application11: fibanocci of the given number
--------------------------------------------
import java.util.*;

class Test
{
    public static int fibGeneral(int n){
        if(n==0||n==1)
            return n;
        else
            return fibGeneral(n-1)+fibGeneral(n-2);
    }
    public static int fibMemoization(int n,int[] f){
        if(n==0||n==1)
            return n;
        if(f[n]!=0)
            return f[n];
        f[n] = fibMemoization(n-1,f)+fibMemoization(n-2,f);
        return f[n];
    }
    public static int fibTabulation(int n){
        int dp[] = new int[n+1];
        dp[0] = 0;
        dp[1] = 1;
        for(int i=2;i<=n;i++){
            dp[i] = dp[i-1]+dp[i-2];
        }
        return dp[n];
    }
    public static void main(String[] args)
    {
        //0, 1, 1, 2, 3, 5, 8, .....
        int n = 6;
        System.out.println(fibGeneral(n));//8
        int[] f = new int[n+1];
        System.out.println(fibMemoization(n,f));//8
        System.out.println(fibTabulation(n));//8
    }
}

C:\test>javac Test.java

C:\test>java Test
8
8
8

application2: climbing stairs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
count ways to reach the nth stair, the person can climb either 1 stair or 2 stair at a time.

import java.util.*;

class Test
{
    public static int countWaysGeneral(int n){
        if(n==0)
            return 1;
        if(n<0)
            return 0;
        return countWaysGeneral(n-1)+countWaysGeneral(n-2);
    }
    public static int countWaysMemoization(int n,int[] ways)
    {
        if(n==0)
            return 1;
        if(n<0)
            return 0;
        if(ways[n]!=-1)//already calculated
            return ways[n];
        ways[n] = countWaysMemoization(n-1,ways)+countWaysMemoization(n-2,ways);
        return ways[n];
    }
    public static int countWaysTabulation(int n){
        int[] dp = new int[n+1];
        dp[0] = 1;
        for(int i=1;i<=n;i++){
            if(i==1)
                dp[i] = dp[i-1];
            else
                dp[i] = dp[i-1]+dp[i-2];
        }
        return dp[n];
    }
    public static void main(String[] args)
    {
        //1, 1, 2, 3, 5,.....
        int n = 6;
        System.out.println(countWaysGeneral(n));//13
        int[] ways = new int[n+1];
        Arrays.fill(ways,-1);
        System.out.println(countWaysMemoization(n,ways));//13
        System.out.println(countWaysTabulation(n));//13
    }
}

C:\test>javac Test.java

C:\test>java Test
13
13
13

Time & Space complexities
~~~~~~~~~~~~~~~~~~~~~~~~~
complexity of an algorithm is the amount of time and space required to complete its execution. 

Time Complexity T(n) -----> amount of time taken
Space Complexity S(n) ----> amount of space taken

Note: Space Complexity we are not required to consider. (High Level Programming->GC)

Asymptotic Notations or Asymptotic Analysis:
--------------------------------------------
calculating running time of any algorithm in mathmatical units of computation is know as aymptotic notations.

Big-Oh Notation ----> O
Omega Notation -----> w
Theta Notation -----> 0

complexity analysis of an algorithm:
------------------------------------
worst case complexity ------> Bigoh (O) ---> most commonly used notation
best case complexity -------> Omega (w)
average case complexity ----> Theta (0)

Growth of functions:
~~~~~~~~~~~~~~~~~~~~

O(1) constant time:
-------------------
Algorithm will return on constant time.

Ex:
    Access nth element from an array
    Push and Pop operations on stack
    add and remove operations from queue
    accessing element from hashtable etc

O(n) linear time:
-----------------
Execution time is directly proportional to input size.

Ex:
    search operation
    min element in an array
    max element in an array
    traversal of a tree
    etc

Logarithmic Time O(logn):
-------------------------
Algorithm is said to run in logarithm time. if the execution time of an alg is proportional to logarithm of input size.

Ex:
    binary search

Logarithmic Time O(nlogn):
--------------------------
Algorithm will run in n*logn time, if the execution of an algorithm is proportional to the product of input size and logarithm value of input size.

Ex:
    Merge Sort
    Quick Sort
    Heap Sort
    All Divide and Conquer etc

Quadratic Time O(n2):
---------------------
An algorithm is said to run in quadratic time of an algorithm is proportional to square of input size.

Ex:
    Bubble Sort
    Selection Sort
    Insertion Sort
    etc

Ex1:
----
import java.util.*;

class Test 
{
    public static int m(int n){
        int c=0;
        for(int i=0;i<n;i++)
            c++;
        return c;
    }
    public static void main(String[] args) 
    {
        System.out.println("N=100, number of instructions is O(n): "+m(100));
    }
}

C:\9pm>javac Test.java

C:\9pm>java Test
N=100, number of instructions is O(n): 100

Ex2:
----
import java.util.*;

class Test 
{
    public static int m(int n){
        int c=0;
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++){
                c++;
            }
        }
        return c;
    }
    public static void main(String[] args) 
    {
        System.out.println("N=100, number of instructions is O(n2): "+m(100));//10000
    }
}

C:\9pm>java Test
N=100, number of instructions is O(n2): 10000

Ex3:
----
import java.util.*;

class Test 
{
    public static int m(int n){
        int c=0;
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++){
                for(int k=0;k<n;k++){
                    c++;
                }
            }
        }
        return c;
    }
    public static void main(String[] args) 
    {
        System.out.println("N=100, number of instructions is O(n3): "+m(100));//1000000
    }
}

Ex4:
----
import java.util.*;

class Test 
{
    public static int m(int n){
        int c=0;
        for(int i=n;i>0;i=i/2)
        {
            c++;
        }
        return c;
    }
    public static void main(String[] args) 
    {
        System.out.println("N=100, number of instructions is O(logn): "+m(100));//7
    }
}

Ex5:
---
import java.util.*;

class Test 
{
    public static int m(int n){
        int c=0;
        for(int i=1;i<=n;i=i*2)
        {
            c++;
        }
        return c;
    }
    public static void main(String[] args) 
    {
        System.out.println("N=100, number of instructions is O(logn): "+m(100));//7
    }
}
Greedy Method:
--------------
The main purpose of this method is to find optimal solution for the given problem

Ex1: coins
----------

coins ---> [10, 5, 2, 1]
amount --> 52

solution1: 52x1=52      ----> 52 coins
solution2: 25x2+2x1=50+2 =52    ----> 27 coins
.
.
.
solutionx: 5x10+1x2 = 50+2 = 52 ----> 6 coins

import java.util.*;
class Test 
{
    public static void sortDesc(int[] a){
        for(int i=0;i<a.length;i++){
            for(int j=i+1;j<a.length;j++){
                if(a[i]<a[j]){
                    int t=a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
        }
    }
    public static int minCoins(int[] coins,int amount)
    {
        int res = 0,j;
        sortDesc(coins);
        for(int i:coins){
            if(i<=amount){
                j = amount/i;
                res = res + j;
                amount = amount - (j*i);
            }
            if(amount==0)
                break;
        }
        return res;
    }
    public static void main(String[] args) 
    {
        int[] coins = {5, 10, 2, 1};
        int amount = 52;
        System.out.println(minCoins(coins,amount));
    }
}

General Method for solving greedy method
----------------------------------------
void getOptimalSol()
{
    res = 0;
    while(all items are not considered)
    {
         i = selectAnItem;
         if(feasible(i))
            res = res + i;
    }
    return res;
}


Knapsack(bag) problem
---------------------
items = [60, 100, 120]
weight = [10, 20, 30]
W = 50

AVL Trees
RED & BLACK Trees

syllabus:
---------
01. introduction to dsa
02. sample algorithms and implementations
03. arrays data structure
04. CRUD operations on arrays (insert, access, update & delete)
05. One-D, Two-D(matrix) and Three-D array programs
06. string data structure
07. recursion and applications
08. sorting and searching algorithms
09. list data structures (SLL, DLL, CSLL & CDLL)
10. stack data structure
11. queue data structure
12. hashtable data strcture
13. tree data structure
14. priority queues and heaps
15. graphs
16. dynamic programming
17. divide and conquer
18. greedy algorithms
19. backtracking applications
20. complexities
=================================================================


Exception:-
===========
package com.app;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ExceptionFirstExample {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter out=new PrintWriter("abc.txt");
        out.println("hello");
        System.out.println(10/0);
    }
}
output:-
===========
Exception in thread "main" java.lang.ArithmeticException: / by zero
    at com.app.ExceptionFirstExample.main(ExceptionFirstExample.java:10)

=======================================================================

Logic Based Programming(LBP):-
===============================
P001 to P050 ---> Basic Formula
P051 to P100 ---> while loop
P101 to P150 ---> for loop
P151 to P225 ---> arrays (1-D)
P226 to P275 ---> matrix (2-D)
P276 to P350 ---> strings
P351 to P360 ---> FA Coding Quenstions

P001: WELCOME MESSAGE
P002: ADDITION
P003: NEXT INTEGER
P004: DIVISIBLE BY 3
P005: NUMBER OF HOURS
P006: NUMBER OF MONTHS
P007: THREE NUMBERS ADDITION
P008: COFFEE CUPS
P009: SUM OF LAST THREE DIGIT NUMBERS IN FOUR DIGIT NUMBER
P010: MULTIPLICATION OF CURRENT AND NEXT NUMBER FROM THE GIVEN INTEGER
P011: DIVISION AND MODULO DIVISION
P012: MULTIPLICATION
P013: ARE THE NUMBERS ARE EQUAL?
P014: POWER CALCULATOR
P015: CONVERT AGE TO DAYS
P016: FOOTBALL POINTS
P017: TWO MAKES TEN
P018: CHARACTER TO ASCII
P019: ASCII TO CHARACTER
P020: AREA OF CIRCLE
P021: CELSIUS TO FAHRENHEIT
P022: FAHRENHEIT TO CELSIUS
P023: ACTUAL MEMORY SIZE OF USB DRIVE
P024: FLIGHT LUGGAGE WEIGHT
P025: SAVINGS
P026: MAXIMUM OF TWO NUMBERS
P027: MINIMUM OF TWO NUMBERS
P028: MAXIMUM OF THREE NUMBERS
P029: MINIMUM OF THREE NUMBERS
P030: SAME DIGIT
P031: NEAREST TO 10
P032: BOTH ARE IN RANGE 30 TO 40
P033: TEEN NUMBER
P034: MULTIPLE OF 3 OR 5
P035: INVERT COLORS
P036: EVEN OR ODD
P037: BIGGEST OF FOUR NUMBERS
P038: SMALLEST OF FIVE NUMBERS
P039: WEIRD OR NOT WEIRD
P040: NEON NUMBER
P041: COMPARE STRING BY COUNT OF CHARACTERS
P042: CONVERT NUMBER TO CORRESPONDING MONTH NAME
P043: SAY "HELLO", SAY "BYE"
P044: ALONE SUM
P045: LOWER CASE ENGLISH WORD
P046: FIZZBUZZ
P047: VACCINATION DRIVE LIST PREPARATION
P048: EMPLOYEES RATING POINT
P049: PUZZLE
P050: POWER FUNCTION
P101: N NATURAL NUMBERS
P102: N EVEN NUMBERS
P103: N ODD NUMBERS
P104: N NATURAL NUMBERS IN REVERSE
P105: NUMBER FOLLOWED BY ITS SQUARE
P106: NUMBER FOLLOWED BY ITS CUBE
P107: PRINT THE SEQUENCE 1,4,7,10,13,.....
P108: PRINT THE SEQUENCE 1,5,9,13,17,.....
P109: PRINT THE SEQUENCE 10,20,30,40,.....
P110: PRINT THE SEQUENCE 7, 14, 21, 28,....
P111: SUM OF 'N' NATURAL NUMBERS
P112: SUM OF 'N' EVEN NUMBERS
P113: SUM OF 'N' ODD NUMBERS
P114: SUM OF NUMBERS DIVISIBLE BY 3
P115: SUM OF NUMBERS DIVISIBLE BY 5
P116: SUM OF NUMBERS DIVISIBLE BY BOTH 2 AND 3
P117: PRINT ALL THE EVEN NUMBERS BETWEEN THE INTERVALS
P118: PRINT ALL THE ODD NUMBERS BETWEEN THE INTERVALS
P119: PRINT ALL THE FACTORS OF THE GIVEN NUMBER
P120: PRIME NUMBER OR NOT
P121: FACTORIAL OF THE NUMBER
P122: FIBANOCCI SEQUENCE
P123: TRIBANOCCI SEQUENCE
P124: STRONG NUMBER OR NOT
P125: LIST OF PRIME NUMBER BETWEEN THE GIVEN INTERVALS
P126: LUCKY CUSTOMER
P127: CLIMBING STAIR
P128: PIN
P129: PRIME NUMBER APPLICATION
P130: PERFECT NUMBER
P131: SUM OF DIGITS
P132: PARITY BITS
P133: FUN GAMES
P134: FACTORIAL GAME
P135: PARITY QUIZ
P136: PRIME NUMBER BUSSES
P137: SUM OF ALL INTEGERS
P138: COMPOSITE NUMBER
P139: DIVISIBLE BY 5 OR 7
P140: ENDING 3 
P141: MIN AND MAX
P142: SUM OF ALL PRIME NUMBERS
P143: SUM OF ALL ARMSTRONG NUMBERS
P144: SUM OF ALL STRONG NUMBERS
P145: SUM OF ALL PERFECT NUMBERS
P146: MULTIPLICATION TABLE
P147: ENDING WITH 3
P148: LAST BEFORE DIGITI IS 3
P149: BEGINGIN AND ENDING WITH 3
P150: SUM OF FIRST AND LAST DIGIT IS 3
P151: READING AND WRITING AN ARRAY
P152: SUM OF ALL ELEMENTS IN AN ARRAY
P153: SUM OF EVEN ELEMENTS IN AN ARRAY
P154: SUM OF ODD ELEMENTS IN AN ARRAY
P155: SUM OF PRIME ELEMENTS IN AN ARRAY
P156: SUM OF PALINDROME  ELEMENTS
P157: SUM OF ELEMENTS ENDING WITH 3
P158: SUM OF ELEMENTS AT EVEN INDEX
P159: SUM OF ELEMENTS AT ODD INDEX
P160: SUM OF TWO ARRAYS
P161: SUBTRACTION OF TWO ARRAYS
P162: MULTIPLICATION OF TWO ARRAYS
P163: INCREMENT EACH ELEMENT BY ONE UNIT
P164: ARRAY REVERSE
P165: REVERSE EACH ELEMENT IN ARRAY
P166: SORTING IN ASC
P167: SORTING IN DESC
P168: LINEAR SEARCH
P169: BINARY SEARCH
P170: MIN ELEMENT IN ARRAY
P171: MAX ELEMENT IN ARRAY
P172: 2nd MIN ELEMENT IN ARRAY
P173: 2nd MAX ELEMENT IN ARRAY
P174: DIFFERENCE BETWEEN MAX AND MIN ELEMENT
P175: SORTING 0's, 1's and 2's
P176: NUMBER OF OCCURRENCES
P177: NUMBER OF DUPLICATES
P178: PRINT UNIQUE ELEMENTS
P179: REPLACE WITH GREATER ELEMENT
P180: SUM OF FIRST AND LAST, SECOND AND SECOND LAST
P181: NUMBER OF EVEN AND ODD ELEMENTS
P182: SORT FIRST HALF OF ARRAY
P183: REARRANGE AN ARRAY
P184: ARRAY OF MULTIPLES
P185: INCLUSIVE ARRAY RANGE
P186: ELIMINATE ODD ELEMENTS
P187: POSITIVE COUNT AND NEGATIVE SUM
P188: SUM OF TWO SMALLEST ELEMENTS
P189: LAST N ELEMENTS
P190: MINI PEACKS
P191: ALL PRIME
P192: SUM OF ADJACENTS
P193: INSERT ELEMENT AT FIRST LOCATION
P194: INSERT ELEMENT AT LAST LOCATION
P195: DELETE FIRST ELEMENT
P196: DELETE LAST ELEMENT
P197: DELETE ELEMENT FROM LOCATION
P198: DELETE ELEMENT FROM ARRAY
P199: UPDATE ELEMENT IN ARRAY
P200: UPDATE ELEMENT PRESENT AT LOCATION
P201: ODD EVEN ONLINE GAME
P202: GARMENTS COMPANY APPAREL
P203: POLLED CAB SERVICE
P204: KTH PROCESSING QUEUE
P205: SEVEN BOOM
P206: POSITIVES AND NEGATIVES
P207: ALL VALUES ARE TRUE
P208: THIRD LARGEST AND SECOND SMALLEST
P209: ARRAY PALIANDROME
P210: PRODUCT WITH SUCCESSOR 
P211: PRE-SORTED INTEGER ARRAY
P212: HALF ASCENDING AND HALF DESCENDING
P213: LUCKY DRAW
P214: SCORE OF PLAYER
P215: PERFECT MATH
P216: MULTIPLES OF 10
P217: ENCRYPTED DIGITS
P218: EASY SHOPPING
P219: CALCULATE MEAN
P220: TODAYS APPAREL
P221: BUCKET ID
P222: DEC TO OCTAL
P223: SUM OF ADJACENT ELEMENTS
P224: AIRPORT SECURITY
P225: CHOCOLATE FACTORY
P226: READING AND WRITING MATRIX ELEMENTS
P227: SUM OF ALL MATRIX ELEMENTS
P228: SUM OF ALL EVEN ELEMENTS
P229: SUM OF ALL ODD ELEMENTS
P230: SUM OF ALL PRIME ELEMENTS
P231: ROW WISE SUM IN MATRIX
P232: COLUMN WISE SUM IN MATRIX
P233: SUM OF DIAGONAL ELEMENTS IN MATRIX
P234: SUM OF OPPOSITE DIAGONAL ELEMENTS IN MATRIX
P235: SUM OF FIRST AND LAST ELEMENT IN THE MATRIX
P236: FIND THE PRODUCT OF DIAGONAL MATRIX
P237: FIND THE PRODUCT OF OPPOSITE DIAGONAL MATRIX
P238: MAX ELEMENT IN MATRIX
P239: MIN ELEMENT IN MATRIX 
P240: MAX ELEMENT IN ROW OF A MATRIX
P241: MIN ELEMENT IN EACH ROW OF A MATRIX
P242: TRANSPOSE OF THE GIVEN MATRIX
P243: TRACE OF THE GIVEN MATRIX
P244: FIND THE FREQUENCY OF ODD AND EVEN
P245: IDENTITY MATRIX
P246: TWO MATRICES ARE EQUAL OR NOT
P247: ADDITION OF TWO MATRICES
P248: SUBTRACTION OF TWO MATRICES
P249: MULTIPLICATION OF TWO MATRICES
P250: SORT ALL THE ELEMENTS IN MATRIX IN ASC ORDER
P251: SORT ALL THE ELEMENTS IN MATRIX IN DESC ORDER
P252: SORT ALL THE ELEMENTS IN A ROW IN ASC ORDER
P253: SORT ALL THE ELEMENTS IN A ROW IN DESC ORDER
P254: SORT ALL THE ELEMENTS IN A COL IN ASC ORDER
P255: SORT ALL THE ELEMENTS IN A COL IN DESC ORDER
P256: SPARSE MATRIX
P257: SWAPING OF TWO ROWS
P258: SWAPING OF TWO COLUMNS
P259: INTERCHANGE THE DIAGONALS
P260: UPPER TRIANGULAR MATRIX
P261: LOWER TRIANGULAR MATRIX
P262: SCALAR MATRIX MULTIPLICATION
P263: SYMMETRIC MATRIX
P264: PRINT DIAGONAL ELEMENTS
P265: SQUARE OF EACH ELEMENT OF MATRIX
P266: SUM OF EVEN INDEXED ROWS IN MATRIX
P267: SUM OF ODD INDEXED ROWS IN MATRIX
P268: SUM OF EVEN INDEXED COLS IN MATRIX
P269: SUM OF ODD INDEXED COLS IN MATRIX
P270: SUM OF ELEMENTS WHOSE SUM OF ROW INDEX AND COL INDEX IS EVEN
P271: SUM OF ELEMENTS WHOSE SUM OF ROW INDEX AND COL INDEX IS ODD
P272: SUM OF PRIME ELEMENTS
P273: COUNT OF PRIME DIGITS IN GIVEN MATRIX
P274: REVERSE OF EACH ELEMENT IN MATRIX
P275: KEEP PALI NUMBER AND REPLACE REMAINING WITH 0'S
P276: READ AND WRITE STRING
P277: PRINT ONLY VOWELS
P278: PRINT ONLY CONSONANTS
P279: COUNT VOWELS
P280: COUNT CONSONANTS
P281: COUNT SPECIAL CHARACTERS
P282: COUNT SPACES
P283: CONVERT INTO LOWER CASE
P284: CONVERT INTO UPPER CASE
P285: SWAP CASE OR TOGGLE CASE
P286: FIRST CAPITAL LETTER
P287: NUMBER OF MISSES
P288: MAGIC DATE
P289: VIDEO LENGTH
P290: DEFANGING IP ADDRESS
P291: MAKE "ABBA"
P292: EXTRA END
P293: GET WORD COUNT
P294: CHECK IF STRING ENDING MATCHES SECOND STRING
P295: SHUFFLE NAMES
P296: REVERSE ORDER OF WORDS
P297: RE-FORM THE WORD
P298: CHECK BIRTH DAY
P299: PALIANDROME
P300: ANAGRAMS
P301: MAX OCCURRENCE OF CHARACTER
P302: COLOR OF CHESS BOARD
P303: FIND BOMB
P304: NUMBER OF X'S AND O'S
P305: SHUTTERING FUNCTION
P306: REPEATING LETTERS
P307: DOUBLE LETTERS
P308: BOARD GAME
P309: REMOVE VOWEL
P310: SPACE BETWEEN CHARACTERS
P311: VOWEL REPLACER
P312: SAY "HELLO" 
P313: ZIP CODE
P314: MIDDLE CHARACTERS
P315: INDEX OF FIRST VOWEL
P316: DECIMAL PLACES
P317: REMOVE DUPLICATES
P318: HEXA CODE
P319: EVEN LENGTH WORDS
P320: NEXT LETTER
P321: FIRST 'N' VOWELS
P322: STRING IS ORDER
P323: FANNY'S OCCURRENCE
P324: CENTER STRING
P325: PARENTHESIS BALANCING
P326: AMERICAN KEYBOARD
P327: ROTATE STRING
P328: MISSING LETTERS
P329: REPLACE LETTER WITH POSITIONS
P330: REPLACE CHARACTER WITH ITS OCCURRENCES
P331: NON-REPEATING CHARACTER
P332: PANGRAM
P333: FIRST LETTER IN EACH WORD
P334: ONLY DIGITS
P335: CAPITALIZE EACH WORD
P336: STUDENT REWARED
P337: WORD KEY
P338: SWEET SEVENTEEN
P339: BEAUTIFY ME
P340: SECREATE INFO
P341: SECOND NON-REPEATING CHARACTER
P342: PASSWORD CHANGE
P343: CODING STANDARDS
P344: SENTENCE MAKING
P345: NEXT LETTER
P346: VALID ATM PIN
P347: NEXT 5 CHARACTERS
P348: NOT HAVING ALPHABET 'A'
P349: SWAP CORNER WORDS AND REVERSE MIDDLE CHARACTERS
P350: LONGEST WORD

======================================================================================
1)Implement a program to read user name and display welcome message on the console.

Input Format

read user name

Constraints

no

Output Format

print message in the following form

welcome name

Sample Input 0

admin
Sample Output 0

welcome admin
Sample Input 1

prakash
Sample Output 1

welcome prakash
Sample Input 2

prakash babu
Sample Output 2

welcome prakash babu
========================
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String name=sc.nextLine();
        System.out.print("welcome "+name);
    }
}
=================================
2)Implement a program to read two integer values from the user and perform addition operation.

Input Format

two integer values

Constraints

no

Output Format

an integer value on the console

Sample Input 0

1
2
Sample Output 0

3
Sample Input 1

10
11
Sample Output 1

21
Sample Input 2

5
5
Sample Output 2

10

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c;
        c=a+b;
        System.out.println(c);
    }
}
========================================
3)Implement a program that takes a number as a input, increment the number by +1 and print the result.

Input Format

an integer value

Constraints

no

Output Format

incremented value

Sample Input 0

1
Sample Output 0

2
Sample Input 1

-1
Sample Output 1

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int num=sc.nextInt();
        num++;
        System.out.println(num);
    }
}
==================================
4)Implement a program to read a number and check whether it is divisible by 3 or not.

Input Format

an integer value

Constraints

no

Output Format

Yes or No

Sample Input 0

3
Sample Output 0

Yes
Sample Input 1

4
Sample Output 1

No
Sample Input 2

5
Sample Output 2

No

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println((n%3==0)?"Yes":"No");
    }
}
==============================
5)Implement a program to read seconds values in timer and convert it into hours.

Input Format

seconds value

Constraints

no

Output Format

hours value

Sample Input 0

3600
Sample Output 0

1
Sample Input 1

56000
Sample Output 1

15
Sample Input 2

3700
Sample Output 2

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int sec=sc.nextInt();
        System.out.println(sec/3600);
    }
}
============================
6)Implement a program to read number of days and convert it into number of months.
Note: consider 30 days per month

Input Format

number of days

Constraints

no

Output Format

months

Sample Input 0

50
Sample Output 0

1
Sample Input 1

60
Sample Output 1

2
Sample Input 2

90
Sample Output 2

3
Sample Input 3

10
Sample Output 3

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int day=sc.nextInt();
        System.out.println(day/30);
    }
}
==================================
7)Write a program to accept three space seperated integers and print addition of those three numbers.

Input Format

Three space seperated values

Constraints

no

Output Format

addition of those three integers

Sample Input 0

1 2 3
Sample Output 0

6
Sample Input 1

2 3 4
Sample Output 1

9
Sample Input 2

10 11 12
Sample Output 2

33

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
         int b=sc.nextInt();
         int c=sc.nextInt();
        System.out.println(a+b+c);
    }
}
==============================
8)Implement a program to clcualte the free number of cups the user gets for a specified number of cups bought by the user. The user get 1 cup free for every 6 cups bought.

Input Format

number of cups

Constraints

no

Output Format

total number of coffee cups

Sample Input 0

6
Sample Output 0

7
Sample Input 1

8
Sample Output 1

9
Sample Input 2

10
Sample Output 2

11
Sample Input 3

12
Sample Output 3

14

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(n+n/6);
    }
}
==============================
9)Implement a program to extract last three digits numbers from the given four digit number and calcualte sum of those three digits numbers.

Input Format

a four digit integer number as input

Constraints

no

Output Format

sum of last three digit numbers

Sample Input 0

1234
Sample Output 0

9
Sample Input 1

1200
Sample Output 1

2
Sample Input 2

1111
Sample Output 2

3
Sample Input 3

2222
Sample Output 3

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println((n%10)+(n/10)%10+(n/100)%10);
    }
}
===================================
10)Implement a program to perform multiplication operation on given and next to given number.

Input Format

an integer value

Constraints

no

Output Format

multiplication of current and next number

Sample Input 0

2
Sample Output 0

6
Sample Input 1

0
Sample Output 1

0
Sample Input 2

1
Sample Output 2

2
Sample Input 3

5
Sample Output 3

30
Sample Input 4

3
Sample Output 4

12

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(n*(n+1));
    }
}
===========================

11)Implement a program to find division and modulo division of given two integer values.

Input Format

two integer values (seperated by new line char)

Constraints

no

Output Format

division and modulo division

Sample Input 0

5
2
Sample Output 0

2 1
Sample Input 1

5
3
Sample Output 1

1 2
Sample Input 2

9
2
Sample Output 2

4 1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
         int b=sc.nextInt();
        System.out.print((a/b)+" "+(a%b));
    }
}
==============================================

12)Implement a program to calcualte multiplication on given two integer values.

Input Format

two integer values (seperated by new line char)

Constraints

no

Output Format

multiplication

Sample Input 0

2
3
Sample Output 0

6
Sample Input 1

4
1
Sample Output 1

4
Sample Input 2

12
2
Sample Output 2

24

output:-
===========

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println(a*b);
    }
}

=================================================
13)Write a program to check whether the given two numbers are equal or not?

Input Format

two integer values

Constraints

no

Output Format

true or false

Sample Input 0

145
144
Sample Output 0

false
Sample Input 1

234
234
Sample Output 1

true

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println((a==b)?"true":"false");
    }
}

=============================
14)Implement a program to take voltage and current and print the calculated power.

Input Format

voltage and current

Constraints

no

Output Format

power

Sample Input 0

230
10
Sample Output 0

2300
Sample Input 1

110
3
Sample Output 1

330
Sample Input 2

480
20
Sample Output 2

9600

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int voltage=sc.nextInt();
        int current=sc.nextInt();
        System.out.println(voltage*current);
    }
}

==============================
15)Implement a program to convert given age into days, by considering 365 days as the length of the year and ignore leap years.

Input Format

age as input

Constraints

no

Output Format

number of days

Sample Input 0

65
Sample Output 0

23725
Sample Input 1

20
Sample Output 1

7300

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(n*365);
    }
}

====================================

Create a program that takes the number of wins, draws and losses and calculates the number of points a football team has obtained so far.

1. wins get 3 points
2. draws get 1 points
3. losses get 0 points


Input Format

number of wins, draws and losses

Constraints

no

Output Format

number of points

Sample Input 0

3 4 2
Sample Output 0

13
Sample Input 1

5 0 2
Sample Output 1

15
Sample Input 2

0 0 1
Sample Output 2

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        System.out.println((a*3)+(b*1)+(c*0));
    }
}

==========================
17)Implement a program that takes two inputs from the user a and b, print 'true' if one of them is 10 or if their sum is 10.

Input Format

a and b value

Constraints

no

Output Format

true or false

Sample Input 0

9
10
Sample Output 0

true
Sample Input 1

9
9
Sample Output 1

false
Sample Input 2

9
1
Sample Output 2

true

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.print((a==10||b==10||(a+b)==10)?"true":"false");
    }
}
===================================================================
18)Write a program to convert given character into ascii value

Input Format

character

Constraints

no

Output Format

an integer value

Sample Input 0

A
Sample Output 0

65
Sample Input 1

a
Sample Output 1

97
Sample Input 2

0
Sample Output 2

48

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
       char ch=sc.next().charAt(0);
        System.out.println((int)ch);
    }
}

method2:-
=========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
       char ch=sc.nextLine().charAt(0);
        System.out.println((int)ch);
    }
}
================================
19)Write a program to convert given ascii value into integer

Input Format

an integer value

Constraints

no

Output Format

character

Sample Input 0

97
Sample Output 0

a
Sample Input 1

65
Sample Output 1

A
Sample Input 2

48
Sample Output 2

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        System.out.println((char)a);
    }
}
==================================
20)Implement a program to find the area of the circle

Input Format

radius value

Constraints

no

Output Format

area of the circle (round to two decimals)

Sample Input 0

1
Sample Output 0

3.14
Sample Input 1

3
Sample Output 1

28.27
Sample Input 2

10
Sample Output 2

314.16

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int r=sc.nextInt();
       double pi=3.141592653589793;
        double area=pi*r*r;
        System.out.printf("%.2f",area);
    }
}
=====================================================
21)Implement a program to convert temparature from Celsius to Fahrenheit.

Input Format

celsius value

Constraints

no

Output Format

fahrenheit value

Sample Input 0

1
Sample Output 0

33
Sample Input 1

13
Sample Output 1

55
Sample Input 2

3
Sample Output 2

37

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int c=sc.nextInt(),f;
        f=(c*9/5)+32;
        System.out.println(f);
    }
}
===================
22)Implement a program to convert temparature from Fahrenheit to Celsius.

Input Format

fahrenheit value

Constraints

no

Output Format

celsius value

Sample Input 0

44
Sample Output 0

6
Sample Input 1

99
Sample Output 1

37
Sample Input 2

222
Sample Output 2

105

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int f=sc.nextInt(),c;
        c=(f-32)*5/9;
        System.out.println(c);
    }
}
============================================
23)Implement a program that takes the memory size as an argument and returns the actual memory size.
Note: The actual storage loss on a USB device is 7% of the overall memory size!

Input Format

memory size in GB

Constraints

no

Output Format

actual memory size, round your result to two decimal places.

Sample Input 0

1
Sample Output 0

0.93
Sample Input 1

2
Sample Output 1

1.86
Sample Input 2

3
Sample Output 2

2.79

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.printf("%.2f",n-n*0.07);
    }
}
=======================================================
24)Kalyan is travelling to mumbai, but this time he is taking flight. His brother has already told him about luggage weight limits of flight but he forgot it. Now he is taking with him 3 trolly bags. As per the current airlines which amir will fly. has below weight limits.

1) There can be at max 2 check-in and 1 cabin luggage.
2) Check-in has total limit of L1.
3) Cabin has limit of L2.

Now, Kalyan has 3 luggage has weights as W1 and W2 and W3 respectively. Now he should be smart enough to make sure that he can travel with all the 3 luggage without paying extra charge.

Find out whether Kalyan can take all of his luggage without any extra charges or not. If all good and no extra changes were paid, output "Yes" otherwise "No".

Input Format

integers W1,W2,W3 and L1,L2

Constraints

no

Output Format

Yes or No

Sample Input 0

1 2 3 4 4
Sample Output 0

Yes
Sample Input 1

1 2 3 2 2
Sample Output 1

No
Sample Input 2

1 1 1 1 1
Sample Output 2

No

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int w1=sc.nextInt();
        int w2=sc.nextInt();
        int w3=sc.nextInt();
        int l1=sc.nextInt();
        int l2=sc.nextInt();
        System.out.println((w1+w2+w3)<=(l1+l2)?"Yes":"No");
    }
}
=================================================================
25)There are 3 friends named Denver, Berlin, Rio who worked at a compnay. Given thier monthly salaries and monthly expendictures, returns the highest saving amoung them.

Input Format

single line with 6 space seperated integers.

Constraints

no

Output Format

highest saving amoung the 3 of them

Sample Input 0

5 1 2 2 4 3
Sample Output 0

4
Sample Input 1

10 10 20 10 30 30
Sample Output 1

10
Sample Input 2

30 10 20 10 10 10
Sample Output 2

20

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner obj=new Scanner(System.in);
       int a1=obj.nextInt();
        int a2=obj.nextInt();
        int b1=obj.nextInt();
        int b2=obj.nextInt();
        int c1=obj.nextInt();
        int c2=obj.nextInt();
       int a,b,c;
        
        a=a1-a2;
        b=b1-b2;
        c=c1-c2;
        System.out.println(Math.max(Math.max(a,b),c));
    
    }
}

=============================================
26)Given two integer values, print or return maximum value from the given integers.

Input Format

two integer values

Constraints

no

Output Format

maximum value

Sample Input 0

4
6
Sample Output 0

6
Sample Input 1

3
9
Sample Output 1

9
Sample Input 2

56
99
Sample Output 2

99
Sample Input 3

45
15
Sample Output 3

45

method1:-
=========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println(Math.max(a,b));
        
    }
}

method2:-
===========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        if(a>b)
        {
         System.out.println(a);
        }
        else{
            System.out.println(b);
        }
    }
}
method3:-
=========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
         System.out.println((a>b)?a:b);
    }
}

================================================
27)Given two integer values, print or return minimum value from the given integers.

Input Format

two integer values

Constraints

no

Output Format

minimum value

Sample Input 0

34
11
Sample Output 0

11
Sample Input 1

4
1
Sample Output 1

1
Sample Input 2

4
5
Sample Output 2

4
Sample Input 3

45
11
Sample Output 3

11

method1:-
=========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println(Math.min(a,b));
        
    }
}

method2:-
===========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        if(a<b)
        {
         System.out.println(a);
        }
        else{
            System.out.println(b);
        }
    }
}
method3:-
=========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
         System.out.println((a<b)?a:b);
    }
}
==========================================

28)Given three integer values, print or return maximum of three integers.

Input Format

two integer values

Constraints

no

Output Format

maximum value

Sample Input 0

1
2
3
Sample Output 0

3
Sample Input 1

1
3
2
Sample Output 1

3
Sample Input 2

1
2
-3
Sample Output 2

2

method1:-
===========

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        if(a>b && a>c)
        {
            System.out.println(a);
        }
        else if(b>c)
         System.out.println(b);
        else{
            System.out.println(c);
        }
    }
}

Method 2:-
==========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        System.out.println(Math.max(Math.max(a,b),c));
    }
}
==========================


29)Given three integer values, print or return minimum of three integers.

Input Format

two integer values

Constraints

no

Output Format

minimum value

Sample Input 0

1
2
3
Sample Output 0

1
Sample Input 1

1
2
-3
Sample Output 1

-3
Sample Input 2

1
-2
45
Sample Output 2

-2

method 1:-
==========
import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        if(a<b && a<c)
        {
            System.out.println(a);
        }
        else if(b<c)
         System.out.println(b);
        else{
            System.out.println(c);
        }
    }
}

Method 2:-
===========

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        System.out.println(Math.min(Math.min(a,b),c));
    }
}

===========================================================
30)Given two non-negative integer values, return true if they have the same last digit, Ex:27 and 57.

Input Format

two integer values

Constraints

no

Output Format

true or false

Sample Input 0

10
4
Sample Output 0

false
Sample Input 1

23
33
Sample Output 1

true
Sample Input 2

22
52
Sample Output 2

true
Sample Input 3

12
11
Sample Output 3

false


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println((a%10)==(b%10)?"true":"false");
        
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner obj=new Scanner(System.in);
        int a=obj.nextInt();
        int b=obj.nextInt();
        System.out.println((a%10==b%10)?"true":"false");
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
         int n2=sc.nextInt();
        System.out.println(n1%10==n2%10);
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
         int n2=sc.nextInt();
        if(n1%10==n2%10)
        {
            System.out.println(true); 
        }
            else{
                System.out.println(false);
            }
    }
}
=====================================
31)Given two integer values, return whichever value is nearest to the value 10, or return 0 in the event of a tie.

Input Format

two integer values

Constraints

no

Output Format

nearest value to a or 0

Sample Input 0

20
9
Sample Output 0

9
Sample Input 1

10
7
Sample Output 1

10
Sample Input 2

7
7
Sample Output 2

0
Sample Input 3

5
7
Sample Output 3

7

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int aa,bb;
        aa=Math.abs(a-10);
        bb=Math.abs(b-10);
         System.out.println((aa==bb)?0:(aa<bb)?a:b);
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int aa,bb;
        aa=Math.abs(a-10);
        bb=Math.abs(b-10);
        if(aa==bb)
        {
            System.out.println(0);
        }else if(aa<bb)
        {
            System.out.println(a);
        }else{
             System.out.println(b);
        }       
    }
}
==============================
32)Given two integer values, return true if they are both in the range 30..40 inclusive, or they are both in the range 40..50 inclusive.

Input Format

two integer values

Constraints

no

Output Format

true or false

Sample Input 0

31
40
Sample Output 0

true
Sample Input 1

44
45
Sample Output 1

true
Sample Input 2

30
50
Sample Output 2

false
Sample Input 3

14
32
Sample Output 3

false

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        if((a>=30 && b<=40) && (b>=30 && b<=40) || (a>=40 && b<=50) && (b>=40 && b<=50))
        {
            System.out.println(true);
        }else{
            System.out.println(false);
        }
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
     
            System.out.println((a>=30 && b<=40) && (b>=30 && b<=40) || (a>=40 && b<=50) && (b>=40 && b<=50)?"true":"false");
    }
}


===========================================================================================
33)We'll say that a number is "teen" if it is in the range 13..19 inclusive. Given 3 int values, return true if 1 or more of them are teen.

Input Format

three integer values

Constraints

no

Output Format

true or false

Sample Input 0

13
14
15
Sample Output 0

true
Sample Input 1

20
21
22
Sample Output 1

false
Sample Input 2

13
20
21
Sample Output 2

true
Sample Input 3

20
13
21
Sample Output 3

true

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        
        if((a>=13 && a<=19) ||(b>=13 && b<=19) || (c>=13 && c<=19))
        {
            System.out.println(true);
        }else{
            System.out.println(false);
        }
        
        
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
            System.out.println((a>=13 && a<=19) ||(b>=13 && b<=19) || (c>=13 && c<=19)?"true":"false");
    }
}

=========================================================

34)Return true if the given non-negative number is a multiple of 3 or a multiple of 5.

Input Format

an integer value from the user

Constraints

no

Output Format

true or false

Sample Input 0

3
Sample Output 0

true
Sample Input 1

4
Sample Output 1

false
Sample Input 2

5
Sample Output 2

true
Sample Input 3

6
Sample Output 3

true

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        if((a%3==0)|| (a%5==0))
        {
            System.out.println(true);
        }else{
            System.out.println(false);
        }
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
            System.out.println((a%3==0)|| (a%5==0)?"true":"false");
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
            System.out.println((a%3==0)||(a%5==0));
    }
}

===============================================
35)Implement a program to read r,g and b values from the user and invert the color values.
Hint: the range of any color in hexadecimal form is from 0 to 255

Input Format

three space seperated integers

Constraints

no

Output Format

inverted color values seperated by sapaces

Sample Input 0

255 255 255
Sample Output 0

0 0 0
Sample Input 1

10 250 250
Sample Output 1

245 5 5
Sample Input 2

10 10 10
Sample Output 2

245 245 245


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int r=sc.nextInt();
        int g=sc.nextInt();
        int b=sc.nextInt();
    System.out.print((255-r)+" "+(255-g)+" "+(255-b));
    }
}
===================================

36)Implement a program to check whether the given number is even or odd number.

Input Format

an integer value from the user

Constraints

no

Output Format

even or odd

Sample Input 0

5
Sample Output 0

odd
Sample Input 1

6
Sample Output 1

even
Sample Input 2

0
Sample Output 2

even
Sample Input 3

1
Sample Output 3

odd

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        if(n%2==0)
        {
            System.out.println("even");
        }else{
             System.out.println("odd");
        }
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
   System.out.println((n%2==0)?"even":"odd");
    }
}

==================================================
37)Implement a program to find biggest of four numbers.

Input Format

four integer values from the user

Constraints

no

Output Format

biggest value

Sample Input 0

1
2
3
4
Sample Output 0

4
Sample Input 1

1
2
3
-4
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
           int a=sc.nextInt();
           int b=sc.nextInt();
           int c=sc.nextInt();
           int d=sc.nextInt();
        
        if(a>b && a>c && a>d)
        {
            System.out.println(a);
        }
        else if(b>c && b>d)
        {
            System.out.println(b);
        }
         else if(c>d)
        {
            System.out.println(c);
        }
        else{
           System.out.println(d); 
        }
    }
}

===========================================
38)Implement a program to find smallest of five numbers.

Input Format

five space seperated integers

Constraints

no

Output Format

smallest of five integers

Sample Input 0

1 2 3 4 5
Sample Output 0

1
Sample Input 1

1 2 3 4 -5
Sample Output 1

-5


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
           int a=sc.nextInt();
           int b=sc.nextInt();
           int c=sc.nextInt();
           int d=sc.nextInt();
           int e=sc.nextInt();
        
        if(a<b && a<c && a<d && a<e)
        {
            System.out.println(a);
        }
        else if(b<c && b<d && b<e)
        {
            System.out.println(b);
        }
         else if(c<d && c<e)
        {
            System.out.println(c);
        }
         else if(d<e)
        {
            System.out.println(d);
        }
        else{
           System.out.println(e); 
        }
    }
}

=======================================
39)Given an integer n, perform the following conditional actions,
If n is odd, print weird,
If n is even and in the inclusive range of 2 to 5, print not weird,
If n is even and in the inclusive range 6 to 20, print weird,
If n is even and greater than 20, print not weird.


Input Format

An integer value from the user.

Constraints

no

Output Format

weird or not weird

Sample Input 0

5
Sample Output 0

weird
Sample Input 1

4
Sample Output 1

not weird
Sample Input 2

6
Sample Output 2

weird
Sample Input 3

7
Sample Output 3

weird


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner obj = new Scanner(System.in);
        int n = obj.nextInt();
        if(n%2!=0)
            System.out.println("weird");
        else
        {
            if(n>=2 && n<=5)
                System.out.println("not weird");
            else if(n>=6 && n<=20)
                System.out.println("weird");
            else
                System.out.println("not weird");
        }
    }
}

=================================================
40)Rahul's teacher gave an assignment to all the student that when they show up tomorrow they should find a special type of number and report her. 
He thought very carefully and came up with an idea to have neon numbers.
 A neon number is a two digit number where the square of the sum of each digit of the number results in the given number.
 Given an integer N, Write a programto find whether the number N is a Neon number.

Input Format

an integer number

Constraints

no

Output Format

true or false

Sample Input 0

81
Sample Output 0

true
Sample Input 1

2
Sample Output 1

false

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
       int s=(n%10)+(n/10)%10;
        System.out.println((s*s)==n?"true":"false");
    }
}

=======================================
41)mplement a program that takes two strings as inputs and print either true or false depending on whether the total number of characters in the first string is equal to the total number of characters in the second string.

Input Format

two strings

Constraints

no

Output Format

true or false

Sample Input 0

abc
def
Sample Output 0

true
Sample Input 1

abc
xyz
Sample Output 1

true
Sample Input 2

ab
xyz
Sample Output 2

false
Sample Input 3

ab
a
Sample Output 3

false

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str1=sc.nextLine();
        String str2=sc.nextLine();
        System.out.println(str1.length()==str2.length());
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str1=sc.nextLine();
        String str2=sc.nextLine();
       
                if(str1.length()==str2.length())
                {
                    System.out.println("true");
                }else{
                    System.out.println("false");
                }
            
        
    }
}
======================================
42)Create a program that takes a number (from 1 to 12) and print its corresponding month name as a string. For example, if you're given 3 as input, your program should print "March", because March is the 3rd month.

Number Month Name
1 January
2 February
3 March
4 April
5 May
6 June
7 July
8 August
9 September
10 October
11 November
12 December

Input Format

read a number

Constraints

no

Output Format

Month name

Sample Input 0

4
Sample Output 0

April
Sample Input 1

8
Sample Output 1

August
Sample Input 2

3
Sample Output 2

March


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
       int n=sc.nextInt();
 switch(n){
         case 1:
         System.out.println("January");
         break;
         case 2:
         System.out.println("February");
         break;
         case 3:
         System.out.println("March");
         break;
         case 4:
         System.out.println("April");
         break;
         case 5:
         System.out.println("May");
         break;
         case 6:
         System.out.println("June");
         break;
         case 7:
         System.out.println("July");
         break;
         case 8:
         System.out.println("August");
         break;
         case 9:
         System.out.println("September");
         break;
         case 10:
         System.out.println("October");
         break;
         case 11:
         System.out.println("November");
         break;
         case 12:
         System.out.println("December");
         break;

 }
        
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        if(n==1)
        {
            System.out.println("January");
        }
        else if(n==2)
        {
             System.out.println("February");
        }
          else if(n==3)
        {
             System.out.println("March");
        }
           else if(n==4)
        {
             System.out.println("April");
        }
           else if(n==5)
        {
             System.out.println("May");
        }
        
           else if(n==6)
        {
             System.out.println("June");
        }
           else if(n==7)
        {
             System.out.println("July");
        }
           else if(n==8)
        {
             System.out.println("August");
        }
           else if(n==9)
        {
             System.out.println("September");
        }
           else if(n==10)
        {
             System.out.println("October");
        }
           else if(n==11)
        {
             System.out.println("November");
        }
        else{
            System.out.println("December");
        }
        
        
        
    }
}



============================
43)Write a program that takes a string name and a number num (either 0 or 1) and print "Hello" + name if num is 1, otherwise return "Bye" + name.

Input Format

string and number

Constraints

no

Output Format

"Hello name" or "Bye name"

Sample Input 0

admin
1
Sample Output 0

Hello admin
Sample Input 1

admin
0
Sample Output 1

Bye admin


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int n=sc.nextInt();
        
        if(n==1)
        {
            System.out.print("Hello "+str);
        }
        
        else if(n==0)
        {
             System.out.print("Bye "+str);
        }
        else{
            
        }
        
    }
}

===================================
44)Given 3 int values, a b c, print their sum. However, if one of the values is the same as another of the values, it does not count towards the sum.

Input Format

a,b and c values

Constraints

no

Output Format

sum or 0

Sample Input 0

1
2
3
Sample Output 0

6
Sample Input 1

1
2
1
Sample Output 1

2
Sample Input 2

1
2
2
Sample Output 2

1


Example:=
===============

1 2 3   --->6
1 2 1   --->2
1 2 2   --->1
1 1 1   --->0
1 0 2   -->3
1 1 5   --->5


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        int c=sc.nextInt();
        
        if(a!=b && b!=c && c!=a)
        {
            System.out.println(a+b+c);
        }
        else if(a==b && b==c)
        {
              System.out.println("0");
        }
        else if(a==b)
        {
             System.out.println(c);
        }
        else if(b==c)
        {
             System.out.println(a);
        }else{
             System.out.println(b);
        }
        
    }
}

==========================================
45)Given a positive integer n value, 1<=n<=9, print corresponding english word like 1->one.

Input Format

an integer value

Constraints

no

Output Format

english word

Sample Input 0

1
Sample Output 0

one
Sample Input 1

9
Sample Output 1

nine
Sample Input 2

2
Sample Output 2

two


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        if(n==1)
        {
            System.out.println("one");
        }
        else if(n==2)
        {
            System.out.println("two");
        }
        else if(n==3)
        {
            System.out.println("three");
        }
        else if(n==4)
        {
            System.out.println("four");
        }
        else if(n==5)
        {
            System.out.println("five");
        }
        else if(n==6)
        {
            System.out.println("six");
        }
        else if(n==7)
        {
            System.out.println("seven");
        }
        else if(n==8)
        {
            System.out.println("eight");
        }
        else if(n==9)
        {
            System.out.println("nine");
        }
        else{
        } 
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

      Scanner sc=new Scanner(System.in);
       int n=sc.nextInt();
 switch(n){
         case 1:
         System.out.println("one");
         break;
         case 2:
         System.out.println("two");
         break;
         case 3:
         System.out.println("three");
         break;
         case 4:
         System.out.println("four");
         break;
         case 5:
         System.out.println("five");
         break;
         case 6:
         System.out.println("six");
         break;
         case 7:
         System.out.println("seven");
         break;
         case 8:
         System.out.println("eight");
         break;
         case 9:
         System.out.println("nine");
         break;
         case 10:
         System.out.println("ten");
         break;
 }
       
    }
}

============================================

46)Given a number n, print the result value per line as follows.

=> if n is a multiple of both 3 and 5 print FizzBuzz
=> if n is a multiple of 3 (but not 5), print Fizz
=> if n is a multiple of 5 (but not 3), print Buzz
=> if n is not a multiple of 3 or 5 print the value of n.

implement a program to read number n print the output as described above.

Input Format

a number n

Constraints

no

Output Format

print n string as per the above rules.

Sample Input 0

15
Sample Output 0

FizzBuzz
Sample Input 1

45
Sample Output 1

FizzBuzz
Sample Input 2

12
Sample Output 2

Fizz


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        if(n%3==0 && n%5==0)
        {
            System.out.println("FizzBuzz");
        }else if(n%3==0 && n%5!=0)
        {
             System.out.println("Fizz");
        }else if(n%3!=0 && n%5==0)
        {
             System.out.println("Buzz");
        }else if(n%3!=0 && n%5!=0)
        {
            System.out.println(n);
        }
    }
}

=================================================
47)Currently government is taking lot of measures to control the spread of Coronavirus. As we have vaccine now, many campaigns are done to vaccination. Health dept is identifying the people in each area and recommended/vaccination of them. They are planning three stages
stage1: above 60 years
stage2: between 18 and 60
stage3: below 18 years


Implement a program to read date of birth of the person and decide he belong to which stage.

Input Format

date of birth

Constraints

no

Output Format

1 or 2 or 3

Sample Input 0

2000
Sample Output 0

2
Sample Input 1

1990
Sample Output 1

2
Sample Input 2

1950
Sample Output 2

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int year=sc.nextInt();
        int age=2024-year;
        
        if(age>60)
        {
            System.out.println("1");
        }
        else if(age>=18)
        {
            System.out.println("2");
        }
        else{
            System.out.println("3");
        }        
    }
}
==========================================
48)In a company, an employee's rating point (ERP) is calculated as the sum of the rating points given by the employee's manager and HR. The employee rating grade (ERG) is calculated according to the ERP ranges given below.

ERP ERG
30-50 D
51-60 C
61-80 B
81-100 A



Write an algorithm to find the ERG character for a given employee's ERP.

Input Format

an integer value

Constraints

no

Output Format

employee rating grade

Sample Input 0

35
Sample Output 0

D
Sample Input 1

55
Sample Output 1

C
Sample Input 2

75
Sample Output 2

B
Sample Input 3

85
Sample Output 3

A

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        if(n>=30 && n<=50)
        {
            System.out.println("D");
        }
        else if(n>=51 && n<=60)
        {
            System.out.println("C");
        }else if(n>=61 && n<=80)
        {
             System.out.println("B");
        }else if(n>=81 && n<=100)
        {
            System.out.println("A");
        }
    }
}

=============================================

49)Dennis was solving a puzle. the puzzle was to verify a number whose cube ends with the number itself. Help Dennis to find the solution of the puzzle and write the code accordingly.

Input Format

integer value to verified

Constraints

no

Output Format

boolean value True or False

Sample Input 0

2
Sample Output 0

false
Sample Input 1

5
Sample Output 1

true
Sample Input 2

4
Sample Output 2

true


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println((n*n*n)%10==n?"true":"false");
        
    }
}

==============================================================
50)In a mathematics class, the students are beign taught power function. So "a" raised to the power of "b" is shown as a^b and the calculatation goes as a*a*a...b times. Now there is slight twist to the problem, the students have to find out the last digit of the resultant a^b.

Input Format

an integer value as base and power

Constraints

no

Output Format

last digit of a^b

Sample Input 0

12
2
Sample Output 0

4
Sample Input 1

13
3
Sample Output 1

7
Sample Input 2

11
2
Sample Output 2

1
Sample Input 3

12
3
Sample Output 3

8

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println((int)Math.pow(a,b)%10);
    }
}

=====================================================

51)mplement a program to read a number and extracts digits from the given number from right to left.

Input Format

an integer value from the user

Constraints

no

Output Format

digits

Sample Input 0

123
Sample Output 0

3
2
1
Sample Input 1

4512
Sample Output 1

2
1
5
4
Sample Input 2

567
Sample Output 2

7
6
5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;

        while(n!=0)
        {
            d=n%10;
            System.out.println(d);
            n=n/10;
        }
    }
}

================================================
52)Implement a program to find sum of digits present in the given number 'n'.

Input Format

an integer value from the user

Constraints

no

Output Format

sum of digits

Sample Input 0

123
Sample Output 0

6
Sample Input 1

192
Sample Output 1

12

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,sum=0;
    while(n!=0)
    {
        d=n%10;
        sum=sum+d;
        n=n/10;
    }
        System.out.println(sum);
        
    }
}

===================================
53)Implement a program to find sum of even digits present in the given number 'n'.

Input Format

an integer value from the user

Constraints

no

Output Format

sum of even digits

Sample Input 0

1234
Sample Output 0

6
Sample Input 1

135
Sample Output 1

0
Sample Input 2

222
Sample Output 2

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,sum=0;
        while(n!=0)
        {
                d=n%10;
            if(d%2==0)
            {
                sum=sum+d;
            }
                n=n/10;
        }
        System.out.println(sum);
    }
}

======================================
54)Implement a program to find sum of odd digits present in the given number 'n'.

Input Format

an integer value from the user

Constraints

no

Output Format

sum of odd digits

Sample Input 0

123
Sample Output 0

4
Sample Input 1

111
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,sum=0;
        while(n!=0)
        {
            d=n%10;
            if(d%2!=0)
            {
                sum=sum+d;
            }
            n=n/10;
        }
        System.out.println(sum);
    }
}
==================================================
55)Implement a program to find sum of prime digits present in the given number 'n'.

Input Format

an integer value from the user

Constraints

no

Output Format

sum of prime digits

Sample Input 0

123
Sample Output 0

5
Sample Input 1

12345
Sample Output 1

10
Sample Input 2

912
Sample Output 2

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,sum=0;
        while(n!=0)
        {
            d=n%10;
            if((d==2)||(d==3)||(d==5)||(d==7))
            {
                sum=sum+d;
            }
            n=n/10;
        }
        System.out.println(sum);
    }
}

=====================================================
56)Implement a program to find sum of digits which are divisible by 3.

Input Format

an integer value from the user

Constraints

no

Output Format

sum of digits divisible by 3

Sample Input 0

123
Sample Output 0

3
Sample Input 1

3456
Sample Output 1

9

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int d;
        int sum=0;
        while(n!=0)
        {
            d=n%10;
            if(d%3==0)
            {
                sum=sum+d;
            }
            n=n/10;
        }
        System.out.println(sum);
    }
}

======================================================

57)Implement a program to count number of digits present in the given number.

Input Format

an integer value

Constraints

no

Output Format

count of digits

Sample Input 0

123
Sample Output 0

3
Sample Input 1

1234
Sample Output 1

4
Sample Input 2

11
Sample Output 2

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),count=0;
        while(n!=0)
        {
            count++;
            n=n/10;
        }
        System.out.println(count);
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        int count=0;
        while(n!=0)
        {
            d=n%10;
            count++;
            n=n/10;
        }
        System.out.println(count);
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        System.out.println(s.length());
    }
}

====================
58)Implement a program to find number of occurrences the given digit in the number.

Input Format

an integer number and digit

Constraints

no

Output Format

number of occurrences

Sample Input 0

123
3
Sample Output 0

1
Sample Input 1

12121
1
Sample Output 1

3
Sample Input 2

222
1
Sample Output 2

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        int key=sc.nextInt();
        int count=0;
        
        while(n!=0)
        {
            d=n%10;
            if(d==key)
            {
                count++;
            }
            n=n/10;
        }
        System.out.println(count);
    }
}
===========================================
59)mplement a program to find the max digit present in the given number.

Input Format

an integer value

Constraints

no

Output Format

max digit

Sample Input 0

132
Sample Output 0

3
Sample Input 1

1234
Sample Output 1

4
Sample Input 2

567
Sample Output 2

7


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        int max=-1;
        
        while(n!=0)
        {
          d=n%10;
            if(max<d)
            {
                max=d;
            }
            n=n/10;
        }
        
        System.out.println(max);
        
    }
}

===========================================
60)Implement a program to find the min digit present in the given number.

Input Format

an integer value

Constraints

no

Output Format

min digit

Sample Input 0

123
Sample Output 0

1
Sample Input 1

456
Sample Output 1

4
Sample Input 2

789
Sample Output 2

7

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
         int min=10;
        
        while(n!=0)
        {
            d=n%10;
            if(min>d)
            {
                min=d;
            }
            n=n/10;
        }
        System.out.println(min);
    }
}

==================================================
61)Implement a program that determines whether a number is oddish or evenish, a number is oddish if the sum of all digits is odd and evenish if the sum of all digits is even. print the output as oddish or evenish.

Input Format

an integer value

Constraints

no

Output Format

oddish or evenish

Sample Input 0

123
Sample Output 0

evenish
Sample Input 1

1231
Sample Output 1

oddish
Sample Input 2

111
Sample Output 2

oddish
Sample Input 3

1111
Sample Output 3

evenish


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int sum=0;
        int d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
        System.out.println((sum%2==0)?"evenish":"oddish");
    }
}

=================================================================
62)Implement a program to calcualte the reverse of the given number.

Input Format

an integer value

Constraints

no

Output Format

reverse of the given number

Sample Input 0

123
Sample Output 0

321
Sample Input 1

111
Sample Output 1

111
Sample Input 2

456
Sample Output 2

654

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int r=0;
        int d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        System.out.println(r);
    }
}
=================================================
63)Implement a program to check whether the given number is palindrome or not.

Input Format

an integer value

Constraints

no

Output Format

true or false

Sample Input 0

123
Sample Output 0

false
Sample Input 1

121
Sample Output 1

true
Sample Input 2

1234
Sample Output 2

false

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        int temp;
        int r=0;
        temp=n;
        while(n!=0)
        {
           d=n%10;
           r=r*10+d;
            n=n/10;
        }
        System.out.println((temp==r)?"true":"false");
    }
}

=========================================================
64)Implement a program to read a number and print sum of first and last digit in the given number.

Input Format

an integer value

Constraints

no

Output Format

an int value

Sample Input 0

123
Sample Output 0

4
Sample Input 1

111
Sample Output 1

2
Sample Input 2

102
Sample Output 2

3

import java.io.*;
import java.util.*;

public class Solution 
{
    public static int rev(int n)
    {
     int r=0,d;   
        while(n!=0){
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println((n%10)+rev(n)%10);  
    }
}

======================================================

65)Implement a program to count number of even and odd digits and print.

Input Format

an integer

Constraints

no

Output Format

count even and odd digits

Sample Input 0

123
Sample Output 0

1
2
Sample Input 1

1234
Sample Output 1

2
2
Sample Input 2

12345
Sample Output 2

2
3
Sample Input 3

1212
Sample Output 3

2
2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,ec=0,oc=0;
        
        while(n!=0)
        {
            d=n%10;
            
            if(d%2==0)
            {
                ec++;
            }else{
                oc++;
            }
            
            n=n/10;
        }
         System.out.println(ec);
         System.out.println(oc);
        
        
    }
}

===================================================================
66)An e-commerce company plans to give their customers a discount for the newyears holiday. The discount will be calcualted on the basis of the bill amount of the order placed. The discount amount is the sum of all the odd digits in the customers total bill amount. if no odd digits is present in the bill amount, then discount will be zero.

Input Format

bill amount

Constraints

no

Output Format

an integer value

Sample Input 0

123
Sample Output 0

4
Sample Input 1

1234
Sample Output 1

4
Sample Input 2

12345
Sample Output 2

9


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0,d;
    
    while(n!=0)
    {
        d=n%10;
        
        if(d%2!=0){
            sum=sum+d;
        }
        
        n=n/10;
    }
        System.out.println(sum);       
    }
}

====================================================
67)The IT Company "Soft ComInfo" has decided to transfer its messages through the N/W using new encryption technique. The company has decided to encrypt the data using the non-prime number concept. The message is in the form of a number and the sum of non-prime digits present in the message is used as the encryption key.

Input Format

a number from the user

Constraints

no

Output Format

encryption key

Sample Input 0

1234
Sample Output 0

4
Sample Input 1

12345
Sample Output 1

4
Sample Input 2

123456
Sample Output 2

10


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,sum=0;
        while(n!=0)
        {
            d=n%10;
            if(d==4||d==6||d==8||d==9)
            {
                sum=sum+d;
            }
            n=n/10;
        }
        System.out.println(sum);
    }
}

===============================================================
68)A company wishes to transmit data to another server. The data consists of numbers only. To secure the data during transmission, they plan to reverse the data during transmission; they plan to reverse the data first. Write an algorithm to reverse the data first.

Input Format

data to be transmitted

Constraints

no

Output Format

reverse the data

Sample Input 0

123
Sample Output 0

321
Sample Input 1

456
Sample Output 1

654
Sample Input 2

111
Sample Output 2

111


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,r=0;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        System.out.println(r);
    }
}

=================================================

69)A company wishes to devise an order confirmation procedure. They plan to require an extra confirmation instead of simply auto-confirming the order at the time it is placed. For this purpose, the system will generate one time password to be shared with the customer. The customer who is placing the order has to enter the OTP to confirm the order. The OTP generated for the requested order ID, as the product of the digits in orderID.
Write an algorithm to find the OTP for the OrderID.

Input Format

an integer value

Constraints

no

Output Format

OTP i.e. product of digits

Sample Input 0

123
Sample Output 0

6
Sample Input 1

111
Sample Output 1

1
Sample Input 2

191
Sample Output 2

9

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),p=1,d;
        while(n!=0)
        {
            d=n%10;
            p=p*d;
            n=n/10;
        }
        System.out.println(p);
    }
}
===================================================
70)Write a program to find nearest greater paliandrome

Input Format

an integer value

Constraints

no

Output Format

print nearest paliandrome value

Sample Input 0

100
Sample Output 0

101
Sample Input 1

110
Sample Output 1

111
Sample Input 2

105
Sample Output 2

111


import java.io.*;
import java.util.*;

public class Solution {
    
    
    public static int rev(int n)
    {
        int r=0,d;
        
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        while(true)
        {
            if(n==rev(n))
            {
                System.out.println(n);
                break;
            }
            n++;
        }
    }
}
=============================================================================
71)Aryan is studying in the 5th standard. He is very interested in mathematical tricks and always wanted to play with numbers. Aryan would like to replace existing numbers with some other numbers. Today he decided to replace all digits of the number (which is greater than or equal to 2 digits) by its squares and print it in reverse order. Write a program to help him to replace numbers accordingly.

Input Format

an integer value

Constraints

no

Output Format

replaced all digits by its squares and revered number

Sample Input 0

123
Sample Output 0

941
Sample Input 1

171
Sample Output 1

1491
Sample Input 2

888
Sample Output 2

646464

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        
        while(n!=0)
        {
            d=n%10;
            System.out.print(d*d);
            n=n/10;
        }
    }
}

====================================================
72)A company wishes to bucketize their item id's for better search operations. The bucket for the item ID is chosen on the basis of the maximum value of the digit in the item ID. Writean algorithm to find the bucket to which the item ID will be assigned.

Input Format

an integer value

Constraints

no

Output Format

bucket id

Sample Input 0

1234
Sample Output 0

4
Sample Input 1

191
Sample Output 1

9
Sample Input 2

123
Sample Output 2

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        
        int max=-1;
        
        while(n!=0){
            d=n%10;
            
            if(max<d)
            {
                max=d;
            }
            n=n/10;
        }
        System.out.println(max);
    }
}

====================================================
73)The country visa center generates the token number for its applicants from their application ID. the application ID is numberic value. The token number is generated in a specific form. the even digits in the applicant's ID are replaced by the digit one greater than the even digitand the odd digits in the applicant's ID are replaced by the digit one lesser than the odd digit. The numeric calue thus generated represents the token number of applicant.
Write an algorithm togenerate the token number from the applicant ID.

Input Format

application id

Constraints

no

Output Format

token id

Sample Input 0

423
Sample Output 0

532
Sample Input 1

999
Sample Output 1

888
Sample Input 2

666
Sample Output 2

777


import java.io.*;
import java.util.*;

public class Solution {

    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        n=rev(n);
        while(n!=0)
        {
            d=n%10;
            if(d%2==0)
            {
                System.out.print(d+1);
            }else{
               System.out.print(d-1);
            }
            
            n=n/10;
        }
    }
}

==============================================
74)A super market maintains a pricing format for all its products. A value N is printed on each product. When the scanner reads the value N on the item, the product of all the digits in the value N is the price of the item. the task here is to design the software such that given the code of any item N the product (multiplication) of all the digits of value should be computed (price).

Input Format

an integer value

Constraints

no

Output Format

price

Sample Input 0

215
Sample Output 0

10
Sample Input 1

213
Sample Output 1

6
Sample Input 2

717
Sample Output 2

49
Sample Input 3

123
Sample Output 3

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),p=1,d;
        
        while(n!=0)
        {
            d=n%10;
            p=p*d;
            n=n/10;
        }
        System.out.println(p);
    }
}
========================================
75)Given two positive numbers N and K. The task is to find the nunber N containers exactly K digit or not. If contains then print Truedigit_count otherwise Falsedigit_count.

Input Format

value of n and k

Constraints

no

Output Format

True or False followed by digit count

Sample Input 0

123
3
Sample Output 0

True 3
Sample Input 1

123
2
Sample Output 1

False 3
Sample Input 2

127
1
Sample Output 2

False 3
Sample Input 3

100
4
Sample Output 3

False 3


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int k=sc.nextInt();
        if(s.length()==k)
        {
            System.out.println("True "+s.length());
        }else{
            System.out.println("False "+s.length());
        }
        
        
    }
}
=======================================================
76)An e-commerce company plans to give their customers a special discount for the Christmas, they are planning to offer a flat discount. The discount value is calculated as the sum of all prime digits in the total bill amount.
Write an algorithm to find the discount value for the given total bill amount.

Input Format

an integer value

Constraints

no

Output Format

discount

Sample Input 0

120
Sample Output 0

2
Sample Input 1

123
Sample Output 1

5
Sample Input 2

124
Sample Output 2

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
     int n=sc.nextInt(),d,sum=0;
        
        while(n!=0)
        {
            d=n%10;
            if(d==2||d==3||d==5||d==7)
            {
                sum=sum+d;
            }
            n=n/10;
        }
        System.out.println(sum);
        
        
    }
}

=============================================================
77)Write a program to accept a number and check and display whether it is a Niven Number or not.
Niven Number is that a number which is divisible by its sum of digits.

Input Format

an integer value

Constraints

no

Output Format

Yes or No

Sample Input 0

12
Sample Output 0

Yes
Sample Input 1

24
Sample Output 1

Yes
Sample Input 2

17
Sample Output 2

No

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int sum(int n)
    {
          int sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
           return sum;
    }

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
 
        System.out.println((n%sum(n)==0)?"Yes":"No");
    }
}

=================================================================
78)An e-Commerce company plans to give their customers a discount for the New Year’s holiday. The discount will be calculated on the basis of the bill amount of the order place. The discount amount is the product of the sum of all odd digits and the sum of all even digits of the customers total bill amount.

Input Format

an integer representing bill amount

Constraints

no

Output Format

return discount value

Sample Input 0

123
Sample Output 0

8
Sample Input 1

1245
Sample Output 1

36


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
 Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0,ec=0,oc=0,d;
        while(n!=0)
        {
            d=n%10;
            if(d%2==0)
            {
                ec=ec+d;
            }else{
                oc=oc+d;
            }
            n=n/10;
        }
        System.out.println(ec*oc);     
}
}

=================================================
79)Write a program to find the digital root of a given number.
Digital root is the recursive sum of digits in the given number (until single digit is arrived)

Input Format

a number from the user

Constraints

no

Output Format

single digit number

Sample Input 0

12
Sample Output 0

3
Sample Input 1

123
Sample Output 1

6

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int sum(int n)
    {
        int sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
        return sum;
    }

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
       
        while(true)
        {
            if(n>=0 && n<=9)
            {
                System.out.println(n);
                break;
            }
             n=sum(n);
        }      
      
    }
}

=========================================================
80)Write a program to find the absolute difference between the original number and its reserved number.

Input Format

a number from the user

Constraints

no

Output Format

absolute difference

Sample Input 0

123
Sample Output 0

198
Sample Input 1

321
Sample Output 1

198
Sample Input 2

1234
Sample Output 2

3087


import java.io.*;
import java.util.*;

public class Solution {

    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(Math.abs(n-rev(n)));
    }
}

=======================================================
81)Program to read a number and check whether it is duck number or not. Hint: A duck number is a number which has zeros present in it, but no zero present in the begining of the number.

Input Format

a number from the user

Constraints

no

Output Format

Yes or No

Sample Input 0

12345
Sample Output 0

No
Sample Input 1

10342
Sample Output 1

Yes
Sample Input 2

12035
Sample Output 2

Yes

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,flag=0;
        
        while(n!=0)
        {
            d=n%10;
            if(d==0)
            {
                flag=1;
                break;
            }
            n=n/10;
        }
        System.out.println((flag==1)?"Yes":"No");
    }
}


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        System.out.println(s.contains("0")?"Yes":"No");
    }
}

================================================================
82)In an online exam, the test paper set is categorized by the letters A-Z. The students enrolled in the exam have been assigned a numeric value called application ID. To assign the test set to the student, firstly the sum of all digits in the application ID is calculated. If the sum is within the numeric range 1-26 the corresponding alphanetic set code is assigned to the student, else the sum of the digits are calcualted again and so on unitil the sum falls within the 1-26 range.

Input Format

application id as int

Constraints

no

Output Format

set number

Sample Input 0

123
Sample Output 0

F
Sample Input 1

786
Sample Output 1

U
Sample Input 2

781
Sample Output 2

P
Sample Input 3

127
Sample Output 3

J


import java.io.*;
import java.util.*;

public class Solution {

    public static int sum(int n)
    {
        int sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
        return sum;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        while(true)
        {
            if(n>=1 && n<=26)
            {
                System.out.println((char)(n+64));
                break;
            }
            n=sum(n);
        }
        
    }
}

==================================================================
83)Write an algorithm to generate the token number from the application ID by doing this modifications.
R1. If the digit is even add 1 to it.
R2. If the digit is odd sub 1 from it.

Input Format

a number from the user

Constraints

no

Output Format

token number

Sample Input 0

345
Sample Output 0

254
Sample Input 1

234
Sample Output 1

325
Sample Input 2

235
Sample Output 2

324

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r; 
    }
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),ec=0,oc=0,d;
        n=rev(n);
        while(n!=0)
        {
            d=n%10;
            if(d%2==0)
            {
               System.out.print(d+1);
            }else{
                  System.out.print(d-1);
            }
            n=n/10;
        }  

    }
}

=======================================================
84)In a game, organizers has given a number to the player. The player has to find out the absolute differnece between the number and the reverse of the number. The difference between two numbers is the player's score. The number given to the player and the player's score can be a negative or positive number.
Write an algorithm to find the player's score.

Input Format

an integer

Constraints

no

Output Format

player's score

Sample Input 0

123
Sample Output 0

198
Sample Input 1

321
Sample Output 1

198
Sample Input 2

1234
Sample Output 2

3087

import java.io.*;
import java.util.*;

public class Solution {

    public static int rev(int n){
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(Math.abs(n-rev(n)));
    }
}
========================================================
85)The media compnay "GlobalAdd" has received a batch of advertisements from different product brands. The batch of advertisements is a numeric value where each digit represnets the number of advertisements the media company has received from different product brands. Since the company banners permit only even numbers of advertisements to be displayed, the media company needs to know the totoal number of advertisements it will be able to display from the given batch.
Write an algorithm to calculate the total number of advertisements that will be displayed from the batch.

Input Format

an integer

Constraints

no

Output Format

count of advertisements

Sample Input 0

1234
Sample Output 0

2
Sample Input 1

12345
Sample Output 1

2
Sample Input 2

222
Sample Output 2

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,ec=0;
        
        while(n!=0)
        {
            d=n%10;
            if(d%2==0)
            {
                ec++;
            }
            n=n/10;
        }
        System.out.println(ec);
        
    }
}

======================================================
86)The happy number can be defined as a number which will yield 1 when it is replaced by the sum of the square of its digits repeatedly. If this process results in an endless cycle of numbers containing single digit, then the number is called an unhappy number.
Write a program that accepts a number and determines whether the number is a happy number or not. Return true if so, false otherwise.

Input Format

a number from the user

Constraints

no

Output Format

true or false

Sample Input 0

32
Sample Output 0

true
Sample Input 1

31
Sample Output 1

true
Sample Input 2

16
Sample Output 2

false


import java.io.*;
import java.util.*;

public class Solution {
    
    public static int sum(int n)
    {
        int sum=0,d;
        
        while(n!=0)
        {
            d=n%10;
            sum=sum+d*d;
            n=n/10;
        }
        return sum;
    }

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        while(true)
        {
            if(n==1)
            {
                System.out.println("true");
                break;
            }
            if(n>=2 && n<=9)
            {
                 System.out.println("false");
                break;
            }
            n=sum(n);
        }
        
        
    }
}

=================================================

87)The online math course provider 'MathAtTip' has designed a course for children called Learning Number Recognition and Coutning. The assessment part of the course has a question where the student is given a number and a digit. The student needs to find out the total count of the digits present in the number excluding the given digit.

Input Format

two space seperated int values

Constraints

no

Output Format

count of total digits excluding k

Sample Input 0

12321
2
Sample Output 0

3
Sample Input 1

2222
1
Sample Output 1

4
Sample Input 2

1221
1
Sample Output 2

2
Sample Input 3

1231
3
Sample Output 3

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,count=0;
        int key=sc.nextInt();
        
        while(n!=0)
        {
            d=n%10;
            if(d!=key)
            {
                count++;
            }
            n=n/10;
        }
        System.out.println(count);
    }
}

========================================
88)A company wishes to encode its data. The data is in the form of a number. They wish to encode the data with respect to a specific digit. They wish to count the number of times the specific digit reoccurs in the given data so that they can encode the data accordingly. Write an algorithm to find the count of the specific digit in the given data.

Input Format

data and digit

Constraints

no

Output Format

count of specific digit

Sample Input 0

1234
1
Sample Output 0

1
Sample Input 1

1234
5
Sample Output 1

0
Sample Input 2

1234
6
Sample Output 2

0
Sample Input 3

1234
2
Sample Output 3

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,count=0;
        int key=sc.nextInt();
        while(n!=0)
        {
            d=n%10;
            if(d==key)
            {
                count++;
            }
            n=n/10;
        }
        System.out.println(count);
    }
}

=====================================
89)Implement a program to increment all its digits by one unit

Input Format

an integer value

Constraints

no

Output Format

updated number

Sample Input 0

234
Sample Output 0

345
Sample Input 1

717
Sample Output 1

828
Sample Input 2

123
Sample Output 2

234

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        
        n=rev(n);
        while(n!=0)
        {
            d=n%10;
           System.out.print(d+1);
            n=n/10;
        }
     
    }
}

==========================================================
90)Implement a program to decrement all its digits by one unit

Input Format

an integer value

Constraints

no

Output Format

updated number

Sample Input 0

234
Sample Output 0

123
Sample Input 1

717
Sample Output 1

606
Sample Input 2

321
Sample Output 2

210

import java.io.*;
import java.util.*;

public class Solution {

    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
         
        n=rev(n);
        while(n!=0)
        {
            d=n%10;
            System.out.print(d-1);
            n=n/10;
        }
    }
}
==============================================
91)Implement a program to print the given number in words, for example if given number is 123 then excepted output is 'One Two Three'.

Input Format

an integer value

Constraints

no

Output Format

print number in words

Sample Input 0

123
Sample Output 0

One Two Three
Sample Input 1

1203
Sample Output 1

One Two Zero Three
Sample Input 2

789
Sample Output 2

Seven Eight Nine



import java.io.*;
import java.util.*;

public class Solution {

    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d;
        
        n=rev(n);
        while(n!=0)
        {
            d=n%10;
         switch(d)
            {
            case 0:System.out.print("Zero ");
            break;
            case 1:System.out.print("One ");
            break;
            case 2:System.out.print("Two ");
            break;
            case 3:System.out.print("Three ");
            break;
             case 4:System.out.print("Four ");
           break;
            case 5:System.out.print("Five ");
             break;
            case 6:System.out.print("Six ");
            break;
            case 7:System.out.print("Seven ");
            break;
             case 8:System.out.print("Eight ");
           break;
            case 9:System.out.print("Nine ");
             break;
        }
        n=n/10;
    }
    }
}

========================================================================
92)Implement a program to check whether the given number is armstrong number or not, a number is said to armstrong if it is equal to the sum of cubes of its digits.

Input Format

an integer value

Constraints

no

Output Format

true or false

Sample Input 0

1
Sample Output 0

true
Sample Input 1

153
Sample Output 1

true
Sample Input 2

154
Sample Output 2

false

import java.io.*;
import java.util.*;

public class Solution {

    public static int sum(int n)
    {
          int s=0,d;   
        
        while(n!=0){
            d=n%10;
            s=s+d*d*d;
            n=n/10;
        }
        return s;
    }
    
    
    public static void main(String[] args) {

        Scanner obj=new Scanner(System.in);
        int n=obj.nextInt();
        System.out.println(n==sum(n));
        
    }
}
==================================================
93)Implement a program to read numbers till the user wants and at the end print sum of numbers entered by the user, stop reading data if user enters '0'.

Input Format

read numbers until user enter '0'

Constraints

no

Output Format

sum of entered numbers

Sample Input 0

1
2
3
0
Sample Output 0

6
Sample Input 1

1
2
3
4
5
0
Sample Output 1

15
Sample Input 2

1
2
0
Sample Output 2

3

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n,sum=0;
        do{
            n=sc.nextInt();
            sum=sum+n;        
        }while(n!=0);
            System.out.println(sum);
    }
}

==============================================
94)Implement a program to read numbers until user enters '0', and count number of +ve and -ve numbers and display.

Input Format

read numbers until user enter '0'

Constraints

no

Output Format

count of +ve and -ve numbers

Sample Input 0

1
-2
0
Sample Output 0

1
1
Sample Input 1

1
2
3
0
Sample Output 1

3
0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n,c1=0,c2=0;
        do{
            n=sc.nextInt();
            if(n>0)
            {
                c1++;
            }
            if(n<0)
            {
                c2++;
            }
        }
        while(n!=0);
        System.out.println(c1);
        System.out.println(c2);
    }
}

=====================================================
95)Ananya purchased 'n' pencils and 'm' erasers at the cost of Rs. 7 and Rs.5 respectively. Write a program to calculate & display the total amount paid by ananya.

Input Format

'n' pencils and 'm' erasers

Constraints

no

Output Format

total amount paid

Sample Input 0

1
1
Sample Output 0

12
Sample Input 1

1
2
Sample Output 1

17
Sample Input 2

2
1
Sample Output 2

19


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        System.out.println(n*7+m*5);
    }
}

============================================
96)Implement a program to find sum of first, last digit and second,last second and so on.

Input Format

an integer value

Constraints

no

Output Format

sum of first, last digits and second, last second digits and so on

Sample Input 0

123
Sample Output 0

8
Sample Input 1

1234
Sample Output 1

10
Sample Input 2

1212
Sample Output 2

6


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner obj=new Scanner(System.in);
        String n=obj.nextLine();
        int s=0,low=0,high=n.length()-1;
        while(low<=high){
            s=s+(int)(n.charAt(low)-48)+(int)(n.charAt(high)-48);
            low++;
            high--;
        }
        System.out.println(s);
    }
}
==============================================================================
97)Implement a program to find average of digits.

Input Format

an integer value

Constraints

no

Output Format

average value round off to integer

Sample Input 0

1234
Sample Output 0

2
Sample Input 1

12
Sample Output 1

1
Sample Input 2

1
Sample Output 2

1


import java.io.*;
import java.util.*;

public class Solution {
    
    public static int sum(int n)
    {
        int sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
        return sum;
    }

    public static int count(int n)
    {
        int count=0;
        while(n!=0)
        {
            count++;
            n=n/10;
        }
        return count;
    }
    
    
    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        System.out.println(sum(n)/count(n));
        
        
    }
}

=======================================================
98)Implement a program to check whether the given number is even length number or odd length number, if it is even length number return 'true' else return 'false'.

Input Format

an integer value

Constraints

no

Output Format

true or false

Sample Input 0

12
Sample Output 0

true
Sample Input 1

123
Sample Output 1

false
Sample Input 2

1
Sample Output 2

false


import java.io.*;
import java.util.*;

public class Solution {

    public static int count(int n)
    {
        int c=0;
        while(n!=0)
        {
            c++;
            n=n/10;
        }
        return c;
    }
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println((count(n)%2==0)?"true":"false");
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        System.out.println(s.length()%2==0);
    }
}

========================================================================
99)Implement a program to find sum of middle digit number(s).

Input Format

an integer value

Constraints

no

Output Format

sum of middle digit

Sample Input 0

121
Sample Output 0

4
Sample Input 1

1234
Sample Output 1

5
Sample Input 2

1235
Sample Output 2

5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    
        Scanner obj=new Scanner(System.in);
        String s=obj.nextLine();
        if(s.length()%2==0){
             System.out.println(s.charAt(s.length()/2-1)-48+s.charAt(s.length()/2)-48);
        }else{
            System.out.println(s.charAt(s.length()/2)-48+s.charAt(s.length()/2)-48);
        }
    }
}

===================================================

100)Implement a program to find multiplication of middle digit number(s).

Input Format

an integer value

Constraints

no

Output Format

sum of middle digit

Sample Input 0

121
Sample Output 0

4
Sample Input 1

1234
Sample Output 1

6
Sample Input 2

1235
Sample Output 2

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        if(s.length()%2==0)
        {
            System.out.println((s.charAt(s.length()/2-1)-48)*(s.charAt(s.length()/2)-48));
        }else{
            System.out.println((s.charAt(s.length()/2)-48)*(s.charAt(s.length()/2)-48));
        }
    }
}
============================================================================================
101)Implement a program to print first 'n' natural numbers.

Input Format

an integer value

Constraints

no

Output Format

'n' natural numbers

Sample Input 0

5
Sample Output 0

1 2 3 4 5
Sample Input 1

10
Sample Output 1

1 2 3 4 5 6 7 8 9 10
Sample Input 2

4
Sample Output 2

1 2 3 4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=1;i<=n;i++)
        {
            System.out.print(i+" ");
        }
    }
}

=======================================================
102)Implement a program to print first 'n' even numbers.

Input Format

an integer value

Constraints

no

Output Format

'n' even numbers

Sample Input 0

5
Sample Output 0

0 2 4
Sample Input 1

10
Sample Output 1

0 2 4 6 8 10
Sample Input 2

4
Sample Output 2

0 2 4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        for(int i=0;i<=n;i++)
        {
            if(i%2==0)
            {
                System.out.print(i+" ");
            }
        }
        
    }
}

===============================================
103)Implement a program to print first 'n' odd numbers.

Input Format

an integer value

Constraints

no

Output Format

'n' odd numbers

Sample Input 0

10
Sample Output 0

1 3 5 7 9
Sample Input 1

16
Sample Output 1

1 3 5 7 9 11 13 15
Sample Input 2

5
Sample Output 2

1 3 5


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        for(int i=0;i<=n;i++)
        {
            if(i%2!=0)
            {
                System.out.print(i+" ");
            }
        }
        
    }
}

=====================================================
104)Implement a program to print first 'n' natural numbers in reverse order.

Input Format

an integer value

Constraints

no

Output Format

'n' natural numbers in reverse order

Sample Input 0

5
Sample Output 0

5 4 3 2 1 0
Sample Input 1

3
Sample Output 1

3 2 1 0
Sample Input 2

4
Sample Output 2

4 3 2 1 0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=n;i>=0;i--){
           System.out.print(i+" ");
        }
        
    }
}
=======================================================
105)Implement a program to print the sequence 'number' followed by its 'square' upto n

Input Format

an integer value

Constraints

no

Output Format

number followed by square

Sample Input 0

3
Sample Output 0

1 1 2 4 3 9
Sample Input 1

2
Sample Output 1

1 1 2 4
Sample Input 2

4
Sample Output 2

1 1 2 4 3 9 4 16


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        for(int i=1;i<=n;i++)
        {
            System.out.print(i+" "+i*i+" ");
        }
    }
}

========================================================
106)Implement a program to print the sequence 'number' followed by its 'cube' upto n

Input Format

an integer value

Constraints

no

Output Format

number followed by cube

Sample Input 0

3
Sample Output 0

1 1 2 8 3 27
Sample Input 1

2
Sample Output 1

1 1 2 8
Sample Input 2

1
Sample Output 2

1 1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        for(int i=1;i<=n;i++)
        {
            System.out.print(i+" "+(i*i*i)+" ");
        }
    }
}

=========================================================
107)Implement a program to print the sequence 1,4,7,10,13,.....

Input Format

an integer value

Constraints

no

Output Format

sequence upto n

Sample Input 0

3
Sample Output 0

1 4 7
Sample Input 1

2
Sample Output 1

1 4
Sample Input 2

4
Sample Output 2

1 4 7 10


package com.app;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=1;i<=n;i++)
        {
            System.out.print(i+" "+(i+3)+" ");
        }
        sc.close();
    }
}
===========================================================
108)Implement a program to print the sequence 1,5,9,13,17,.....

Input Format

an integer value

Constraints

no

Output Format

sequence upto n

Sample Input 0

6
Sample Output 0

1 5 9 13 17 21
Sample Input 1

7
Sample Output 1

1 5 9 13 17 21 25

package com.app;
import java.util.*;

public class Solution111 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=1;i<=n;i++)
        {
            System.out.print(i+" "+(i+4)+" ");
        }
        sc.close();
    }
}
===================================================
109)Implement a program to print the sequence 10,20,30,40,.....

Input Format

an integer value

Constraints

no

Output Format

sequence upto n

Sample Input 0

4
Sample Output 0

10 20 30 40
Sample Input 1

3
Sample Output 1

10 20 30


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i;
        for(i=1;i<=n;i++)
        {
            System.out.print(i*10+" ");
        }
    }
}
================================================
110)Implement a program to print the sequence 7, 14, 21, 28,.....

Input Format

an integer value

Constraints

no

Output Format

sequence upto n

Sample Input 0

7
Sample Output 0

7 14 21 28 35 42 49
Sample Input 1

4
Sample Output 1

7 14 21 28

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        for(int i=1;i<=n;i++)
        {
            System.out.print(i*7+" ");
        }
        
    }
    
}

==================================================
111)Implement a program to find sum of n natural numbers.

Input Format

an integer value

Constraints

no

Output Format

sum of natural numbers

Sample Input 0

5
Sample Output 0

15
Sample Input 1

4
Sample Output 1

10

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        
        for(int i=0;i<=n;i++)
        {
            sum=sum+i;
        }
    System.out.println(sum);
    }
}
=========================================
112)Implement a program to find sum of n even numbers.

Input Format

an integer value

Constraints

no

Output Format

sum of even numbers

Sample Input 0

5
Sample Output 0

6
Sample Input 1

4
Sample Output 1

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        
        for(int i=0;i<=n;i++)
        {
            if(i%2==0)
            {
                sum=sum+i;
               
            }
        }
        
         System.out.println(sum);
    }
}
===========================================
113)Implement a program to find sum of n odd numbers.

Input Format

an integer value

Constraints

no

Output Format

sum of odd numbers

Sample Input 0

5
Sample Output 0

9
Sample Input 1

4
Sample Output 1

4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        
        for(int i=0;i<=n;i++)
        {
            if(i%2!=0)
            {
                sum=sum+i;
               
            }
        }
        
         System.out.println(sum);
    }
}

==========================================
114)Implement a program to find sum of n numbers which are divisible by 3.

Input Format

an integer value

Constraints

no

Output Format

sum of numbers divisible by 3

Sample Input 0

5
Sample Output 0

3
Sample Input 1

6
Sample Output 1

9

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        for(int i=0;i<=n;i++)
        {
            if(i%3==0)
            {
                sum=sum+i;
            }
        }
        System.out.print(sum);
        
    }
}

========================================
115)Implement a program to find sum of n numbers which are divisible by 5.

Input Format

an integer value

Constraints

no

Output Format

sum of numbers divisible by 5

Sample Input 0

5
Sample Output 0

5
Sample Input 1

10
Sample Output 1

15

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        
        for(int i=0;i<=n;i++)
        {
            if(i%5==0)
            {
                sum=sum+i;
            }
        }
        
        System.out.println(sum);
    }
}
==========================================
116)Implement a program to find sum of n numbers which are divisible by 2 and 3.

Input Format

an integer value

Constraints

no

Output Format

sum of numbers divisible by 2 and 3

Sample Input 0

6
Sample Output 0

6
Sample Input 1

9
Sample Output 1

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,sum=0;
        for(i=1;i<=n;i++)
        {
            if(i%2==0 && i%3==0)
            {
                sum=sum+i;
            }
        }
       System.out.println(sum);
        
    }
}
======================================
117)Implement a program to print all even numbers between n1 and n2

Input Format

two integer values n1 and n2

Constraints

no

Output Format

list of integers

Sample Input 0

5
10
Sample Output 0

6 8 10
Sample Input 1

1
10
Sample Output 1

2 4 6 8 10

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        
        for(int i=n1;i<=n2;i++)
        {
            if(i%2==0)
            {
                System.out.print(i+" ");
            }
        }
    }
}

==================================================
118)Implement a program to print all odd numbers between n1 and n2

Input Format

two integer values n1 and n2

Constraints

no

Output Format

list of integers

Sample Input 0

5
10
Sample Output 0

5 7 9
Sample Input 1

1
10
Sample Output 1

1 3 5 7 9
Sample Input 2

1
15
Sample Output 2

1 3 5 7 9 11 13 15


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        
        for(int i=n1;i<=n2;i++)
        {
            if(i%2!=0)
            {
                System.out.print(i+" ");
            }
        }
    }
}
============================================
119)Implement a program to print all factors of the given number.

Input Format

an integer value

Constraints

no

Output Format

list of values

Sample Input 0

5
Sample Output 0

1 5
Sample Input 1

6
Sample Output 1

1 2 3 6
Sample Input 2

7
Sample Output 2

1 7

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                System.out.print(i+" ");
            }
        }
    }
}
===========================================
120)Implement a program to check whether the given number is prime or not.

Input Format

an integer value

Constraints

no

Output Format

true or false

Sample Input 0

3
Sample Output 0

true
Sample Input 1

6
Sample Output 1

false
Sample Input 2

7
Sample Output 2

true



import java.io.*;
import java.util.*;

public class Solution {

    public static boolean isPrime(int n,int i)
    {
        if(i==1)
        {
            return true;
        }
        else if(n%i==0){
            return false;
        }else{
            return isPrime(n,--i);
        }
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(Solution.isPrime(n,n/2)?"true":"false");
    }
}


import java.io.*;
import java.util.*;

public class Solution {

    public static boolean prime(int n)
    {
        int f=0;
        
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                f++;
            }
        }
        return f==2;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(prime(n)?"true":"false");
    }
}

==================================================================================

121)Implement a program to find factorial of the given number.

Input Format

an integer value from the user

Constraints

no

Output Format

factorial value

Sample Input 0

5
Sample Output 0

120
Sample Input 1

0
Sample Output 1

1
Sample Input 2

1
Sample Output 2

1


import java.io.*;
import java.util.*;

public class Solution {

    public static int fact(int n){
        
        int f=1;
        for(int i=1;i<=n;i++)
        {
            f=f*i;
        }
        return f;
    }
    
    
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(fact(n));
     }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),f=1;
        
        for(int i=1;i<=n;i++)
        {
            f=f*i;
        }
        System.out.print(f+" ");
    }
}

================================================
122)Implement a program to generate fibonacci sequence upto given number

Input Format

an integer value

Constraints

no

Output Format

fib seq upto n

Sample Input 0

9
Sample Output 0

0 1 1 2 3 5 8 13 21
Sample Input 1

3
Sample Output 1

0 1 1
Sample Input 2

2
Sample Output 2

0 1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),f1,f2,f3,i;
        f1=-1;
        f2=+1;
        for(i=1;i<=n;i++)
        {
            f3=f1+f2;
            System.out.print(f3+" ");
            f1=f2;
            f2=f3;
        }
            
    }
}

===================================================
123)Implement a program to generate tribanocci sequence upto given number

Input Format

an integer value

Constraints

no

Output Format

trib seq upto n

Sample Input 0

3
Sample Output 0

0 1 2
Sample Input 1

4
Sample Output 1

0 1 2 3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),f1,f2,f3,f4,i;
        f1=-1;
        f2=0;
        f3=+1;
        
        for(i=1;i<=n;i++)
        {
            f4=f1+f2+f3;
            System.out.print(f4+" ");
            f1=f2;
            f2=f3;
            f3=f4;
        }
        
    }
}

===============================================
124)Implement a program to check whether the given number is strong number or not.

Input Format

an integer value

Constraints

no

Output Format

true or false

Sample Input 0

1
Sample Output 0

true
Sample Input 1

2
Sample Output 1

true

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int fact(int n)
    {
        int f=1;
        for(int i=1;i<=n;i++)
        {
            f=f*i;
        }
        return f;
    }
    

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp=n,sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+fact(d);
            n=n/10;
        }
        System.out.println((temp==sum)?"true":"false");
    }
}

============================================================
125)Implement a program to generate list of prime numbers between two intervals.

Input Format

two integer values

Constraints

no

Output Format

list of prime numbers

Sample Input 0

2
10
Sample Output 0

2 3 5 7
Sample Input 1

5
10
Sample Output 1

5 7
Sample Input 2

5
15
Sample Output 2

5 7 11 13


import java.io.*;
import java.util.*;

public class Solution {
    
    public static boolean isPrime(int n)
    {
       int f=0;
        
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                f++;
            }
        }
        
        return f==2;
    }
    

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        for(int i=n1;i<=n2;i++)
        {
            if(isPrime(i))
            {
                System.out.print(i+" ");
            }
        }
        
    }
}

========================================================
126)An e-commerce website wishes to find the lucky customer who will be eligible for full value cash back.
 For this purpose,a number N is fed to the system. It will return another number that is calculated by an algorithm. 
 In the algorithm, a seuence is generated, in which each number n the sum of the preceding number. 
 initially the sequence will have two 1's in it. 
 The System will return the Nth number from the generated sequence which is treated as the order ID. 
 The lucky customer will be one who has placed that order.
 Write an alorithm to help the website find the lucky customer.

Input Format

an integer value

Constraints

no

Output Format

a new number

Sample Input 0

8
Sample Output 0

21
Sample Input 1

5
Sample Output 1

5
Sample Input 2

3
Sample Output 2

2


import java.io.*;
import java.util.*;

public class Solution {

    public static int fib(int n)
    {
        if(n==0 || n==1)
        {
            return n;
        }else{
            return fib(n-1)+fib(n-2);
        }
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(fib(n));
    }
}

=============================================
127)You are climbing a stair case. It takes n steps to reach to the top.Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
Note: Given n will be a positive integer.

Input Format

a number from the user

Constraints

no

Output Format

number of ways

Sample Input 0

1
Sample Output 0

1
Sample Input 1

2
Sample Output 1

2
Sample Input 2

3
Sample Output 2

3


import java.io.*;
import java.util.*;

public class Solution {

    public static int fib(int n)
    {
        if(n==0 || n==1)
        {
            return 1;
        }
        else{
            return fib(n-2)+fib(n-1);
        }
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(fib(n));
    }

================================================
128)"Secure Assets Private Ltd", a small company that deals with lockers has recently started manufacturing digital locks which can be locked and unlocked using PINs (passwords). 
You have been asked to work on the module that is expected to generate PINs using three input numbers. 
The three given input numbers will always consist of three digits each i.e. each of them will be in the range >=100 and <=999.
Bellow are the rules for generating the PIN.

1. The PIN should made up of 4 digits.
2. The unit (ones) position of the PIN should be the least of the units position of the three numbers.
3. The tens position of the PIN should be the least of the tens position of the three input numbers.
4. The hundreds position of the PIN should be least of the hundreds position of the three numbers.
5. The thousands position of the PIN should be the max of all digits in the three input numbers.

Input Format

three numbers

Constraints

no

Output Format

PIN value

Sample Input 0

123
582
175
Sample Output 0

8122
Sample Input 1

190
267
853
Sample Output 1

9150
Sample Input 2

123
456
789
Sample Output 2

9123


import java.io.*;
import java.util.*;

public class Solution {

    
    public static int maxD(int n)
    {
        int mx=-1,d;
        while(n!=0)
        {
            d=n%10;
            if(mx<d)
                mx=d;
            n=n/10;
        }
        return mx;
    }
    
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
         int n2=sc.nextInt();
         int n3=sc.nextInt();
        int a,b,c,d,pin;
    a=Math.min(Math.min(n1%10,n2%10),n3%10);
    b = Math.min(Math.min((n1/10)%10,(n2/10)%10),(n3/10)%10);
        c = Math.min(Math.min((n1/100)%10,(n2/100)%10),(n3/100)%10);
        d = Math.max(Math.max(maxD(n1),maxD(n2)),maxD(n3));
         pin = d*1000+c*100+b*10+a;
         System.out.println(pin);
    }
}

============================================================================
129)Jackson, a math student, is developing an application on prime numbers. 
For the given two integers on the display of the application, 
the user has to identify all the prime numbers within the given range (including the given values). 
Afterwards the application will sum all those prime numbers. 
Jackson has to write an algorithm to find the sum of all the prime numbers of the given range.
Write an algorithm to find the sum of all the prime numbers of the given range.

Input Format

two space seperated integers

Constraints

no

Output Format

sum of all prime numbers between given numbers

Sample Input 0

2
10
Sample Output 0

17
Sample Input 1

5
10
Sample Output 1

12


import java.io.*;
import java.util.*;

public class Solution {

    public static boolean prime(int n)
    {
        int f=0;
        for(int i=1;i<=n;i++){
        if(n%i==0)
        {
          f++;       
        }
        }
    return f==2;
}
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt(),i;
        int sum=0;
        for(i=n1;i<=n2;i++)
        {
         if(prime(i))
         {
             sum=sum+i;
         }
        }
        System.out.println(sum);
    }
}

====================================================

130)Create a function that tests whether or not an integer is a perfect number. A perfect number is a number that can be written as sum of its factors. (equal to sum of its proper divisors) excluding the number itself.

Input Format

a number from the user

Constraints

no

Output Format

true or false

Sample Input 0

2
Sample Output 0

false
Sample Input 1

6
Sample Output 1

true


import java.io.*;
import java.util.*;

public class Solution {
    
    public static boolean perfect(int n)
    {
        int i,sum=0;
        for(i=1;i<n;i++)
        {
            if(n%i==0)
            {
                sum=sum+i;
            }
        }
        return sum==n;
    }
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(perfect(n)?"true":"false");
    }
}

=====================================================================
131)Create a function that sums the total number of digits between two numbers inclusive. 
for example, if the numbers are 19 and 22, then (1+9)+(2+0)+(2+1)+(2+2)=19.

Input Format

a number from the user

Constraints

no

Output Format

sum of digits

Sample Input 0

19
22
Sample Output 0

19
Sample Input 1

7
8
Sample Output 1

15
Sample Input 2

17
20
Sample Output 2

29

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int sum(int n)
    {
      int sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
         return sum;
    }
    

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt(),sum=0;
         int n2=sc.nextInt();
        for(int i=n1;i<=n2;i++)
        {
           sum=sum+sum(i);
        }
        System.out.println(sum);
    }
}

=====================================================
132)Michael wants to check the parity of the given number. To find tha parity, follow below steps.
1. convert decimal number into binary number.
2. count the number of 1's and 0's in the binary representation.

if it contains odd number of 1-bits, then it is "odd parity" and if contains even number of 1-bits then "even parity"
Write a program to validate the given number belongs to odd parity or even parity.

Input Format

a number from the user

Constraints

no

Output Format

odd parity or even parity.

Sample Input 0

5
Sample Output 0

even
Sample Input 1

8
Sample Output 1

odd

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),d,count=0;
        
        while(n!=0)
        {
            d=n%2;
            if(d==1)
            {
                count++;
            }
            n=n/2;
        }
        System.out.println((count%2==0)?"even":"odd");
    }
}

===========================================================
133)The games development company "FunGames" has developed a ballon shooter games. The ballons are arranged in a linear sequence and each ballon has a number associated with it. The numbers on the ballons are fibonacci series. In the game the player shoots 'k' ballons. The player's score is the sum of numbers on k ballons.
Write an algorithm to generate the player's score.

Input Format

an integer vlaue n

Constraints

no

Output Format

sum value

Sample Input 0

1
Sample Output 0

0
Sample Input 1

2
Sample Output 1

1
Sample Input 2

3
Sample Output 2

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),f1,f2,f3,sum=0;;
        f1=-1;
        f2=+1;
       
        for(int i=1;i<=n;i++)
        {
            f3=f1+f2;
            sum=sum+f3;
            f1=f2;
            f2=f3;
        }
        System.out.println(sum);
        
    }
}
=============================================
134)Mikes likes to play with numbers. His friends are also good with numbers and often plays mathematical games. they made a small game where they will spell the last digit of a factorial of a number other than 0.
Let say the given number is 5, so 5! will be 120, Here 0 is the last digit. But we dn't want 0, we want a number other than 0. Then last digit is 2.

Input Format

an integer value

Constraints

no

Output Format

an integer vlaue

Sample Input 0

1
Sample Output 0

1
Sample Input 1

2
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int fact(int n)
    {
        int f=1;
        for(int i=1;i<=n;i++)
        {
            f=f*i;
        }
        return f;
    }

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int f=fact(n);
        if(f%10!=0)
        {
            System.out.println(f%10);
        }
        else if((f/10)%10!=0)
        {
            System.out.println((f/10)%10);
        }
        else{
            System.out.println((f/100)%10);
        }
        
        
        
    }
}

======================================================
135)While sitting in party, Tom came up with an idea of a quiz. and the quiz is, Tom will spell out a number, and a person has to tell a number which is next to it. But this number has to be perfect square.

Input Format

a number from the user

Constraints

no

Output Format

the perfect square after N

Sample Input 0

5
Sample Output 0

9
Sample Input 1

7
Sample Output 1

9


2----> 3,4------>4
5----->6,7,8,9----->9
10----->11,12,13,14,15,16-------->16

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        for(int i=1;;i++)
        {
            if(i*i>=n)
            {
                System.out.println(i*i);
                break;
            }
        }
    }
}
=================================================
136)James wants to travel by bus to reach his friend John's home. 
John gave a hint that all busses from Jame's location will reach his home if the bus number is prime number.
Write a programto help James find the bus that reaches John's home.

Input Format

a number from the user

Constraints

no

Output Format

yes or no

Sample Input 0

12
Sample Output 0

no
Sample Input 1

23
Sample Output 1

yes



import java.io.*;
import java.util.*;

public class Solution {
    
    public static boolean prime(int n,int i)
    {
        if(i==1)
        {
            return true;
        }else if(n%i==0)
        {
            return false;
        }else{
            return prime(n,--i);
        }
    }
    

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(prime(n,n/2)?"yes":"no");
    }
}


import java.io.*;
import java.util.*;

public class Solution {
     public static boolean prime(int n)
    {
        int f=0;
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
                f++;
        }
        return f==2;
    }
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n = obj.nextInt();
        System.out.println(prime(n)?"yes":"no");
    }
}

===========================================================
137)Implement a program to find sum of all integers between two integer numbers taken as input and are divisible by 7.

Input Format

an integer value

Constraints

no

Output Format

sum value

Sample Input 0

1
7
Sample Output 0

7
Sample Input 1

1
25
Sample Output 1

42





import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
         int n2=sc.nextInt();
        int i,sum=0;
        for(i=n1;i<=n2;i++)
        {
            if(i%7==0)
            {
                sum=sum+i;
            }
        }
        System.out.println(sum);
    }
}

=============================================
138)Implement a program to check whether the given number is composite number or not.

Input Format

a number from the user

Constraints

no

Output Format

true or false

Sample Input 0

4
Sample Output 0

true
Sample Input 1

5
Sample Output 1

false

import java.io.*;
import java.util.*;

public class Solution {
    
public static boolean prime(int n)
{
 int f=0;
    for(int i=1;i<=n;i++)
    {
        if(n%i==0)
        {
            f++;
        }
    }
    return f>2;
}
    

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        System.out.println(prime(n)?"true":"false");
        
    }
}

===========================================================
139)Implement a program to print the list of integers which are divisible by 5 or 7.

Input Format

a number from the user

Constraints

no

Output Format

seq of int values which are divisible by 5 or 7

Sample Input 0

10
Sample Output 0

5 7 10
Sample Input 1

15
Sample Output 1

5 7 10 14 15


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        for(int i=1;i<=n;i++)
        {
            if(i%5==0||i%7==0)
            {
               System.out.print(i+" "); 
            }
        }
    }
}
===================================================
140)Implement a program to print the list of integers which are ending with 3 in the given range.

Input Format

n1 and n2 values

Constraints

no

Output Format

list of int values

Sample Input 0

10
15
Sample Output 0

13
Sample Input 1

1
30
Sample Output 1

3 13 23

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        
        for(int i=n1;i<=n2;i++)
        {
            if(i%10==3)
            {
                System.out.print(i+" ");
            }
        }
    }
}

=====================================================
141)Implement a program to find absolute diff between sum of max digits and sum of min digits present in three integers n1,n2 and n3

Input Format

n1,n2 and n3

Constraints

no

Output Format

int value

Sample Input 0

5213
5698
2153
Sample Output 0

12
Sample Input 1

1212
1234
5678
Sample Output 1

7

n1=123 --->1 3
n2=527--->2 7
n3=901-->0 9

sum of max=19
sum of min=3

difference=max-min=19-3=16


import java.io.*;
import java.util.*;

public class Solution {
public static int maxD(int n)
    {
        int mx=-1,d;
        while(n!=0)
        {
            d=n%10;
            if(mx<d)
                mx=d;
            n=n/10;
        }
        return mx;
    }
    public static int minD(int n)
    {
        int mi=10,d;
        while(n!=0)
        {
            d=n%10;
            if(mi>d)
                mi=d;
            n=n/10;
        }
        return mi;
    }
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n1=obj.nextInt(),n2=obj.nextInt(),n3=obj.nextInt();
        System.out.println((maxD(n1)+maxD(n2)+maxD(n3))-(minD(n1)+minD(n2)+minD(n3)));
    }
}

=================================================================================================
142)Implement a program to find sum of all prime numbers from 2 to n.

Input Format

an integer value

Constraints

no

Output Format

sum of prime numbers

Sample Input 0

5
Sample Output 0

10
Sample Input 1

10
Sample Output 1

17



import java.io.*;
import java.util.*;

public class Solution {
  public static boolean prime(int n)
    {
        int f=0;
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
                f++;
        }
        return f==2;
    }
    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n = obj.nextInt(),i,s=0;
        for(i=2;i<=n;i++)
        {
            if(prime(i))
                s=s+i;
        }
        System.out.println(s);
    }
}

=================================================
143)Implement a program to find sum of all armstrong numbers from 0 to n.

Input Format

an integer value

Constraints

no

Output Format

sum of armstrong numbers

Sample Input 0

100
Sample Output 0

1
Sample Input 1

200
Sample Output 1

154

import java.io.*;
import java.util.*;

public class Solution {
    public static int armStrong(int n)
    {
        int sum=0,d;
        while(n!=0)
        {
            d=n%10;
            sum=sum+d*d*d;
            n=n/10;
        }
        return sum;
    }
    
    public static void main(String[] args) {
        Scanner obj=new Scanner(System.in);
        int n=obj.nextInt();
        int sum=0;
        
        for(int i=0;i<=n;i++)
        {
            if(i==armStrong(i))
            {
                sum=sum+i;
            }
                
        }
        System.out.println(sum);
}
}

==============================
144)Implement a program to find sum of all strong numbers from 0 to n.

Input Format

an integer value

Constraints

no

Output Format

sum of strong numbers

Sample Input 0

1
Sample Output 0

1
Sample Input 1

200
Sample Output 1

148


import java.io.*;
import java.util.*;

public class Solution {

    
static boolean checkIfStrong(int n){
 int k=n;
 int sum=0;  
    int r;  
    while(k!=0)  
    {  
        r=k%10;  
        int f=fact(r);  
        k=k/10;  
        sum=sum+f;  
    }  
    if(sum==n)  
    {  
       return true;
    }  
    else  
    {  
        return false;
    }

}

static int fact(int r)  
{  
    int mul=1;  
    for(int i=1;i<=r;i++)  
    {  
        mul=mul*i;  
    }  
    return mul;  
      
}
    public static void main(String[] args) {
    Scanner s=new Scanner(System.in);
    int n=s.nextInt();
        boolean isStrong=false;
        int sum=0;
    for(int i=1;i<=n;i++){
      isStrong = checkIfStrong(i);
      if(isStrong){
          sum=sum+i;
      }
    }
System.out.println(sum);    
}
}

==========================================
145)Implement a program to find sum of all perfect numbers from 0 to n.

Input Format

an integer value

Constraints

no

Output Format

sum of perfect numbers

Sample Input 0

5
Sample Output 0

0
Sample Input 1

10
Sample Output 1

6


import java.io.*;
import java.util.*;

public class Solution {

    public static boolean perfect(int n)
    {
       int sum=0;
        for(int i=1;i<n;i++)
        {
            if(n%i==0)
            {
                sum=sum+i;
            }
        }
        return sum==n;
    }
    
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        
        for(int i=0;i<=n;i++)
        {
            if(perfect(i))
            {
                sum=sum+i;
            }
                
        }
        System.out.println(sum);
    }
}

========================================================

146)Implement a program to print multiplication table of the given integer n

Input Format

an integer value

Constraints

no

Output Format

multiplication table

Sample Input 0

3
Sample Output 0

3 x 1 = 3
3 x 2 = 6
3 x 3 = 9
3 x 4 = 12
3 x 5 = 15
Sample Input 1

4
Sample Output 1

4 x 1 = 4 
4 x 2 = 8
4 x 3 = 12
4 x 4 = 16
4 x 5 = 20


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n=obj.nextInt();
       
        for(int i=1;i<=5;i++)
        {
         System.out.println(n+" x "+i+" = "+(n*i));   
        }
    }
}

==========================================================
147)Implement a program to find all the numbers from n1 to n2, which are ending with the digit 3.

Input Format

an integer value

Constraints

no

Output Format

list of integers

Sample Input 0

10
20
Sample Output 0

13
Sample Input 1

10
50
Sample Output 1

13 23 33 43

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        for(int i=n1;i<=n2;i++)
        {
            if(i%10==3)
            {
                System.out.print(i+" ");
            }
        }
    }
}
====================================================
148)Implement a program to find all the numbers from n1 to n2, where the numbers last before digit is 3.

Input Format

an integer value

Constraints

no

Output Format

list of integers

Sample Input 0

10
50
Sample Output 0

30 31 32 33 34 35 36 37 38 39
Sample Input 1

100
150
Sample Output 1

130 131 132 133 134 135 136 137 138 139


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        for(int i=n1;i<=n2;i++)
        {
            if((i/10)%10==3)
            {
                System.out.print(i+" ");
            }
        }
        
    }
}

================================================
149)Implement a program to find all the numbers from n1 to n2, which are begining and ending with the digit 3.

Input Format

an integer value

Constraints

no

Output Format

list of integers

Sample Input 0

100
400
Sample Output 0

303 313 323 333 343 353 363 373 383 393
Sample Input 1

1000
4000
Sample Output 1

3003 3013 3023 3033 3043 3053 3063 3073 3083 3093
 3103 3113 3123 3133 3143 3153 3163 3173 3183 3193 
 3203 3213 3223 3233 3243 3253 3263 3273 3283 3293 
 3303 3313 3323 3333 3343 3353 3363 3373 3383 3393 
 3403 3413 3423 3433 3443 3453 3463 3473 3483 3493 
 3503 3513 3523 3533 3543 3553 3563 3573 3583 3593 
 3603 3613 3623 3633 3643 3653 3663 3673 3683 3693 
 3703 3713 3723 3733 3743 3753 3763 3773 3783 3793 
 3803 3813 3823 3833 3843 3853 3863 3873 3883 3893 
 3903 3913 3923 3933 3943 3953 3963 3973 3983 3993 


import java.io.*;
import java.util.*;

public class Solution {

  static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
          int n2=sc.nextInt();
       for(int i=n1;i<=n2;i++)
{
    if(i%10==3 && rev(i)%10==3)
System.out.print(i+" ");
           }
    }
}
=======================================
150)Implement a program to find all the numbers from n1 to n2, where sum of first and last digit should be equal to 3.

Input Format

an integer value

Constraints

no

Output Format

list of integers

Sample Input 0

10
20
Sample Output 0

12
Sample Input 1

10
30
Sample Output 1

12 21 30

import java.io.*;
import java.util.*;

public class Solution {
static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
         int n2=sc.nextInt();
       for(int i=n1;i<=n2;i++)
{
    if((i%10)+(rev(i)%10)==3)
       System.out.print(i+" ");
} 
    }
}

==================================================
151)Implement a program to read and write array elements.

Input Format

array size and elements

Constraints

no

Output Format

list of array elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

1 2 3 4 5
Sample Input 1

7
9 1 8 2 7 3 7
Sample Output 1

9 1 8 2 7 3 7
Sample Input 2

3
111 222 333
Sample Output 2

111 222 333

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] a=new int[n];
        
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
        
    }
}

========================================================
152)Implement a program to read an array elements and print sum of all its elements.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all elements

Sample Input 0

3
1 2 3
Sample Output 0

6
Sample Input 1

2
11 22
Sample Output 1

33
Sample Input 2

4
1 2 3 4
Sample Output 2

10

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n],sum=0;
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            sum=sum+a[i];
        }
        System.out.print(sum);
    }
}
=============================================
153)Implement a program to read an array elements and print sum of all even elements.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all even elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

6
Sample Input 1

4
1 2 3 4
Sample Output 1

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            if(a[i]%2==0)
            {
                sum=sum+a[i];
            }
        }
        System.out.println(sum);
    }
}
======================================
154)Implement a program to read an array elements and print sum of all odd elements.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all odd elements

Sample Input 0

3
1 2 3
Sample Output 0

4
Sample Input 1

4
1 2 3 4
Sample Output 1

4
Sample Input 2

5
1 2 3 4 5
Sample Output 2

9

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            if(a[i]%2!=0)
            {
                sum=sum+a[i];
            }
        }
        System.out.println(sum);
    }
}
==========================================


155)Implement a program to read an array elements and print sum of all prime elements.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all prime elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

10
Sample Input 1

6
1 2 3 4 5 6
Sample Output 1

10
Sample Input 2

7
1 2 3 4 5 6 7
Sample Output 2

17

import java.io.*;
import java.util.*;

public class Solution {
 
    
    public static boolean prime(int n)
    {
        int f=0;
    
        for(int i=1;i<=n;i++)
        {
            if(n==2)
                return true;
            if(n%i==0)
               f++;
        }
                return f==2;
        }
   
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0,i;
        int[] a=new int[n];
        for(i=0;i<n;i++)
            a[i]=sc.nextInt();
             for(i=0;i<n;i++)
        {
            if(prime(a[i]))
                sum=sum+a[i];
            
        }
        System.out.println(sum);
       
}
}

============================================
156)Implement a program to read an array elements and print sum of all paliandrome elements in an array.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all paliandrome elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

15
Sample Input 1

5
11 12 33 43 53
Sample Output 1

44

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] a=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        int sum=0;
        
        for(int i=0;i<n;i++)
        {
            if(a[i]==rev(a[i])){
                sum=sum+a[i];
            }
        }
        System.out.println(sum);
    }
}

===================================================
157)Implement a program to read an array elements and print sum of all elements in an array which are ending with 3.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all elements ending with 3

Sample Input 0

6
1 2 3 11 12 13
Sample Output 0

16
Sample Input 1

5
11 22 33 44 55 66
Sample Output 1

33

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int[] a=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            if(a[i]%10==3)
            {
                sum=sum+a[i];
            }
        }
        System.out.println(sum);
    }
}

=================================================
158)Implement a program to read an array elements and print sum of all elements in an array which are located in even index.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all even indexed elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

9
Sample Input 1

6
1 3 5 7 9 11
Sample Output 1

15

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++)
        {
            if(i%2==0)
            {
                sum=sum+a[i];
            }
        } 
        System.out.println(sum);
    }
}
=================================================
159)Implement a program to read an array elements and print sum of all elements in an array which are located in odd index.

Input Format

size of the array and array elements

Constraints

no

Output Format

sum of all odd indexed elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

6
Sample Input 1

6
1 3 5 7 9 11
Sample Output 1

21


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++)
        {
            if(i%2!=0)
            {
                sum=sum+a[i];
            }
        } 
        System.out.println(sum);
    }
}

===========================================
160)Implement a program to read two arrays of same size and perform addition operation and return the result array

Input Format

size of the array and two array elements

Constraints

no

Output Format

new array

Sample Input 0

5
1 2 3 4 5
6 7 8 9 1
Sample Output 0

7 9 11 13 6
Sample Input 1

4
1 2 3 4
5 6 7 8
Sample Output 1

6 8 10 12

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
    int n=sc.nextInt();
        
        int a[]=new int[n];
        int b[]=new int[n];
        int c[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
           
        for(int i=0;i<n;i++)
        {
            b[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            c[i]=a[i]+b[i];
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(c[i]+" ");
        }
        
    }
}

====================================================
161)Implement a program to read two arrays of same size and perform subtraction operation and return the result array

Input Format

size of the array and two array elements

Constraints

no

Output Format

new array

Sample Input 0

5
5 4 3 2 1
1 2 1 2 1
Sample Output 0

4 2 2 0 0
Sample Input 1

3
10 11 12
13 12 10
Sample Output 1

-3 -1 2


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
    int n=sc.nextInt();
        
        int a[]=new int[n];
        int b[]=new int[n];
        int c[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
           
        for(int i=0;i<n;i++)
        {
            b[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            c[i]=a[i]-b[i];
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(c[i]+" ");
        }
        
    }
}

=========================================================================
162)Implement a program to read two arrays of same size and perform multiplication operation and return the result array

Input Format

size of the array and two array elements

Constraints

no

Output Format

new array

Sample Input 0

5
5 4 3 2 1
1 2 1 2 1
Sample Output 0

5 8 3 4 1
Sample Input 1

4
1 2 3 4 5
1 2 3 4 5
Sample Output 1

1 4 9 16 25

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
    int n=sc.nextInt();
        
        int a[]=new int[n];
        int b[]=new int[n];
        int c[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
           
        for(int i=0;i<n;i++)
        {
            b[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            c[i]=a[i]*b[i];
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(c[i]+" ");
        }
        
    }
}

====================================================================
163)Implement a program to read an array and increment its element's value by one unit.

Input Format

size and array elements

Constraints

no

Output Format

updated array

Sample Input 0

5
1 2 3 4 5
Sample Output 0

2 3 4 5 6
Sample Input 1

5
1 1 1 1 1
Sample Output 1

2 2 2 2 2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        int[] a=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
            
        }
        for(int i=0;i<n;i++)
        {
            a[i]++;
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        } 
    }
}

====================================================
164)Implement a program to read an array and reverse the entire array.

Input Format

array size and elements

Constraints

no

Output Format

reversed array

Sample Input 0

5
1 2 3 4 5
Sample Output 0

5 4 3 2 1
Sample Input 1

3 
11 22 33
Sample Output 1

33 22 11

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();    
        }
           int temp;
          int low=0;
          int high=n-1;
        while(low<=high)
        {
            temp=a[low];
            a[low]=a[high];
            a[high]=temp;
            low++;
            high--;
        }
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
        
        
    }
}

======================================================
165)Implement a program to read an array and reverse each element present in an array.

Input Format

array size and elements

Constraints

no

Output Format

updated array with reversed elements

Sample Input 0

5
123 121 111 129 138
Sample Output 0

321 121 111 921 831
Sample Input 1

6
11 12 13 14 15 16
Sample Output 1

11 21 31 41 51 61

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int rev(int n)
    {
        int r=0,d;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    
    

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        
    for(int i=0;i<n;i++)
    {
        a[i]=rev(a[i]);
    }
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
        
    }
}

=====================================================
166)Implement a program to ready array elements and sort the elements in ascending order.

Input Format

size and array elements

Constraints

no

Output Format

sorted array

Sample Input 0

5
1 5 2 3 4
Sample Output 0

1 2 3 4 5
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

1 2 3 4 5 6


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
    for(int i=0;i<n;i++)
    {
        for(int j=i+1;j<n;j++)
        {
            if(a[i]>a[j])
            {
                temp=a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
    }
        
        
for(int i=0;i<n;i++)
{
    System.out.print(a[i]+" ");
}
        
    }
}

=======================================================================
167)Implement a program to ready array elements and sort the elements in descending order.

Input Format

size and array elements

Constraints

no

Output Format

sorted array

Sample Input 0

5
1 5 2 3 4
Sample Output 0

5 4 3 2 1
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

6 5 4 3 2 1


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
    for(int i=0;i<n;i++)
    {
        for(int j=i+1;j<n;j++)
        {
            if(a[i]<a[j])
            {
                temp=a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
    }
        
        
for(int i=0;i<n;i++)
{
    System.out.print(a[i]+" ");
}
        
    }
}

=============================================
168)Implement a program to search for an element in an array using linear search.

Input Format

size, array elements and key element

Constraints

no

Output Format

index value of the key value

Sample Input 0

5
1 5 2 3 4
5
Sample Output 0

1
Sample Input 1

6
1 2 3 6 5 4
5
Sample Output 1

4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
         int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        int key=sc.nextInt();
        int index=-1;
        for(int i=0;i<n;i++)
        {
            if(key==a[i])
            {
                index=i;
                break;
            }
        }
   
         System.out.print(index+" ");
        
    }
}

=============================================
169)Implement a program to search for an element in an array using binary search.

Input Format

size, array elements and key element

Constraints

no

Output Format

index value of the key value

Sample Input 0

5
1 5 2 3 4
5
Sample Output 0

4
Sample Input 1

6
1 2 3 6 5 4
5
Sample Output 1

4


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
       Scanner obj = new Scanner(System.in);
        int n = obj.nextInt(),a[] = new int[n],i,temp,j;
        for(i=0;i<n;i++)
            a[i] = obj.nextInt();
        int key = obj.nextInt();
        //logic
        for(i=0;i<n;i++)
        {
            for(j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        int low=0,high=n-1,mid,index=-1;
        while(low<=high)
        {
            mid=(low+high)/2;
            if(key==a[mid])
            {
                index=mid;
                break;
            }
            else if(key<a[mid])
                high=mid-1;
            else
                low = mid+1;
        }
        System.out.println(index);
    }
}


================================================
170)Implement a program to find min element present in an array.

Input Format

size and array elements

Constraints

no

Output Format

min element in array

Sample Input 0

5
1 5 2 3 4
Sample Output 0

1
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] a=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        int min=a[0];
        
for(int i=0;i<n;i++)
{
    if(min>a[i])
    {
        min=a[i];
    }
}
        System.out.println(min);
    }
}


===================================================
171)Implement a program to find max element present in an array.

Input Format

size and array elements

Constraints

no

Output Format

max element in array

Sample Input 0

5
1 5 2 3 4
Sample Output 0

5
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

6


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] a=new int[n];
        
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        
        int max=a[0];
        
        for(int i=0;i<n;i++)
        {
            if(max<a[i])
            {
                max=a[i];
            }
        }
        
        System.out.println(max);
    }
}

================================================
172)Implement a program to find 2nd min element present in an array.

Input Format

size and array elements

Constraints

no

Output Format

2nd min element

Sample Input 0

5
1 5 2 3 4
Sample Output 0

2
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
            System.out.println(a[1]);
    }
}

=================================================
173)Implement a program to find 2nd max element present in an array.

Input Format

size and array elements

Constraints

no

Output Format

2nd max element

Sample Input 0

5
1 5 2 3 4
Sample Output 0

4
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

5


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
     
        System.out.print(a[n-2]);
    }
}

=====================================================
174)Implement a program to find the difference between min and max element present in an array.

Input Format

size and array elements

Constraints

no

Output Format

difference between max and min element

Sample Input 0

5
1 5 2 3 4
Sample Output 0

4
Sample Input 1

6
1 2 3 6 5 4
Sample Output 1

5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
       
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        
        System.out.print(a[n-1]-a[0]);
    }
}

===========================================
175)Implement a program to read an array and sort array elements with 0s, 1s and 2s.

Input Format

size, array elements

Constraints

no

Output Format

print sorted elements

Sample Input 0

5
0 2 1 1 2
Sample Output 0

0 1 1 2 2
Sample Input 1

6
2 1 0 0 1 2
Sample Output 1

0 0 1 1 2 2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
    
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        
       for(int i=0;i<n;i++)
       {
           System.out.print(a[i]+" ");
       }
    }
}

=================================================
176)Implement a program to find the number of occurrences of the given element.

Input Format

size,array element and key element

Constraints

no

Output Format

return number of occurrences

Sample Input 0

5
1 2 1 2 3
2
Sample Output 0

2
Sample Input 1

5
1 2 1 2 3
3
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
         int n = sc.nextInt();
        int a[] = new int[n];
        for(int i=0;i<n;i++)
            a[i] = sc.nextInt();
        int key=sc.nextInt();
        int count=0;
        for(int i=0;i<n;i++)
        {
            if(key==a[i])
            {
                count++;
            }
        }
        System.out.print(count);
    }
}

===========================================

177)Implement a program to find the number of duplicate elements present in the given array.

Input Format

size, array elements

Constraints

no

Output Format

number of duplicate elements in the array

Sample Input 0

5
1 2 1 2 3
Sample Output 0

2
Sample Input 1

5
1 1 1 2 3
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n=obj.nextInt(),i,c=0;
        int a[] = new int[n];
        for(i=0;i<n;i++)
            a[i] = obj.nextInt();
        int b[] = new int[1000];
        for(i=0;i<n;i++){
            b[a[i]]++;
        }
        c=0;
        for(i=0;i<1000;i++)
        {
            if(b[i]>1)
                c++;
        }
        System.out.println(c);
    }
}

===============================================
178)Implement a program to find the unique elements present in the given array.

Input Format

size, array elements

Constraints

no

Output Format

print unique elements present in the array

Sample Input 0

5
1 1 2 3 4
Sample Output 0

1 2 3 4
Sample Input 1

5
1 1 2 2 3
Sample Output 1

1 2 3


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]==a[j])
                {
                    a[i]=-1;
                }
            }
        }
        for(int i=0;i<n;i++)
        {
            if(a[i]!=-1)
            {
                System.out.print(a[i]+" ");
            }
        }
        
        
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
       int n=sc.nextInt(),i,j=0;
        
        int a[]=new int[100];
        for(i=0;i<n;i++)
            a[i]=sc.nextInt();
       for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]==a[j])
                break;
        }
        if(j==n)
            System.out.print(a[i]+" ");
    }
    }
}


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
       int n=sc.nextInt(),i,j=0;
        
        int a[]=new int[n];
        for(i=0;i<n;i++)
            a[i]=sc.nextInt();
       for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]==a[j])
                break;
        }
        if(j==n)
            System.out.print(a[i]+" ");
    }
        
    }
}
===============================================
179)Implement a program to read an array and replace every element with the greatest element on its right side.

Input Format

size, array elements

Constraints

no

Output Format

print updated array elements

Sample Input 0

5
1 2 1 2 4
Sample Output 0

4 4 4 4 4
Sample Input 1

5
9 2 1 2 4
Sample Output 1

9 4 4 4 4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,j,max;
        
        int[] a=new int[100];
      for(i=0;i<n;i++)
          a[i]=sc.nextInt();
        
        for(i=0;i<n;i++)
        {
              max=a[i];
            for(j=i+1;j<n;j++)
            {
                if(max<a[j])
                {
                    max=a[j];
                }
               
            }
             System.out.print(max+" ");
        }
        
    }
}

===============================================
180)Implement a program to find the sum of first and last, second and second last and so on in an array.

Input Format

size and array elements

Constraints

no

Output Format

print sum of first and last, second and second last and so on

Sample Input 0

5
1 2 3 4 5
Sample Output 0

6 6 6
Sample Input 1

6
1 2 3 4 5 6
Sample Output 1

7 7 7

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner obj = new Scanner(System.in);
        int n = obj.nextInt(),i,low,temp,high;
        int a[] = new int[n];
        for(i=0;i<n;i++)
            a[i] = obj.nextInt();
        low=0;
        high=n-1;
        while(low<=high)
        {
             System.out.print(a[low]+a[high]+" ");
            temp = a[low];
            a[low] = a[high];
            a[high] = temp;
            
            low++;
            high--;
        }
    }      
}

======================================
181)Implement a program to find number of even and odd elements in the given array

Input Format

size and array elements

Constraints

no

Output Format

print number of even and odd elements line by line

Sample Input 0

5
1 2 1 2 3
Sample Output 0

2
3
Sample Input 1

4
1 2 3 4
Sample Output 1

2
2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,ecount=0,ocount=0;
        
        int a[]=new int[n];
        
        for(i=0;i<n;i++)
            a[i]=sc.nextInt();
        
        for(i=0;i<n;i++)
        {
            if(a[i]%2==0)
            {
             ecount++;   
            }
            else{
                ocount++;
            }
        }
              System.out.println(ecount);
  System.out.println(ocount);
        
   
    }
}

==============================================
182)Implement a program to sort only first half of the array and keep remaining elements as original.

Input Format

size and array elements

Constraints

no

Output Format

reverse only first half of the array

Sample Input 0

5
4 1 3 5 2
Sample Output 0

1 4 3 5 2
Sample Input 1

6
3 1 2 6 5 4
Sample Output 1

1 2 3 6 5 4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n/2;i++)
        {
            for(int j=i+1;j<n/2;j++)
            {
                if(a[i]>a[j]){
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
        
        
    }
}

=====================================================
183)Implement a program to rearrange an array in such an order that- smallest,largest,2nd smallest, 2nd largest and so on.

Input Format

size and array elements

Constraints

no

Output Format

print the elements smallest,largest,2nd smallest,2nd largest and so on.

Sample Input 0

5
3 1 2 5 4
Sample Output 0

1 5 2 4 3 3
Sample Input 1

6
3 1 2 6 4 5
Sample Output 1

1 6 2 5 3 4


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }        
        int low=0;
        int high=n-1;
        
        while(low<=high)
        {
            System.out.print(a[low]+" ");
             System.out.print(a[high]+" ");
            low++;
            high--;
        }
    }
}

============================================================
184)Implement a program to create an array with n elements by taking multiples of m.

Input Format

m and n

Constraints

no

Output Format

return an array with n elements which contains multiples of m.

Sample Input 0

4
5
Sample Output 0

5 10 15 20
Sample Input 1

3
2
Sample Output 1

2 4 6


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        int a[]=new int[n];
       
            for(int i=1;i<=n;i++)
            {
                a[i-1]=m*i;
            }
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
    }
}

===========================================
185)Write a function that, given the start startNum and end endNum values, return an array containing all the numbers inclusive to that range.
Note:
The numbers in the array are sorted in ascending order.
If startNum is greater than endNum, return an array with the higher value.

Input Format

n and m values

Constraints

no

Output Format

return an array with elements from n to m.

Sample Input 0

5
10
Sample Output 0

5 6 7 8 9 10
Sample Input 1

5
4
Sample Output 1

5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n1=sc.nextInt();
        int n2=sc.nextInt();
        
        if(n1<n2)
        {
            for(int i=n1;i<=n2;i++)
            {
                System.out.print(i+" ");
            }
        }else{
            System.out.print(n1+" ");
        }
        
    }
}

==================================================
186)Create a function that takes an array of numbers and returns only the even values.

Note:

Return all even numbers in the order they were given.
All test cases contain valid numbers ranging from 1 to 3000.

Input Format

size and an array

Constraints

no

Output Format

remove all odd numbers and print

Sample Input 0

5
1 2 3 4 5
Sample Output 0

2 4
Sample Input 1

6
1 2 3 4 5 6
Sample Output 1

2 4 6


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++)
        {
            if(a[i]%2==0)
            {
                System.out.print(a[i]+" ");
            }
        }
    }
}

=============================================
187)Create a function that takes an array of positive and negative numbers. Return an array where the first element is the count of positive numbers and the second element is the sum of negative numbers.

Input Format

size and an array

Constraints

If given an empty array, return an empty array and 0 is not positive.

Output Format

two space seperated int values.

Sample Input 0

5
-1 2 3 -4 5
Sample Output 0

3 -5
Sample Input 1

5
1 -2 3 -4 0
Sample Output 1

2 -6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),cp=0,sn=0;
        int[] a=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            if(a[i]>0)
            {
                cp++;
            }else{
                sn=sn+a[i];
            }
        }
        
        System.out.print(cp+" "+sn);
        
    }
}

=========================================
188)Create a function that takes an array of numbers and returns the sum of the two lowest positive numbers.

Input Format

size and an array

Constraints

Dn't count negative numbers

Output Format

sum of two smallest positive numbers

Sample Input 0

5
4 2 -1 5 3
Sample Output 0

5
Sample Input 1

4
-1 -2 3 4
Sample Output 1

7

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        Arrays.sort(a);
         for(int i=0;i<n;i++)
        {
            if(a[i]>0)
            {
                System.out.println(a[i]+a[i+1]);
                break;
            }
        }
        
    }
}

======================================================

189)Write a function that retrieves the last n elements from an array.

Input Format

size, an array and N value

Constraints

return 0 if n exceeds size of the array

Output Format

last N elements

Sample Input 0

5
1 2 3 4 5
3
Sample Output 0

3 4 5
Sample Input 1

5
1 2 3 4 5
6
Sample Output 1

0


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        } 
        int m=sc.nextInt();
        if(m<n)
        {
            for(int i=n-m;i<n;i++)
             {
            System.out.print(a[i]+" ");
            }
        }
        else{
            System.out.print("0");
        }   
    }
}

==============================================
190)Write a function that returns all the elements in an array that are strictly greater than their adjacent left and right neighbors.

Input Format

size, an array

Constraints

Do not count boundary numbers, since they only have one left/right neighbor.

Output Format

an array

Sample Input 0

5
1 3 2 6 4
Sample Output 0

3 6
Sample Input 1

6
1 2 3 2 1 3
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=1;i<n-1;i++)
        {
            if(a[i]>a[i-1] && a[i]>a[i+1])
            {
                System.out.print(a[i]+" ");
            }
        }
    }
}

====================================================
191)Create a function thats takes an array of integers and returns true if every number is prime. Otherwise, return false.

Input Format

size and an array

Constraints

1 is not a prime number.

Output Format

true or false

Sample Input 0

5
2 3 4 5 6
Sample Output 0

false
Sample Input 1

5
2 3 5 7 11
Sample Output 1

true


import java.io.*;
import java.util.*;

public class Solution {

    public static boolean isPrime(int n)
    {
        int f=0;
        
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                f++;
            }
        }
        return f==2;
    }
    
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),count=0;
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
           if(isPrime(a[i]))
            {
                count++;
            }
        }
       
       System.out.print((count==n)?"true":"false");

    }
}


===========================================================
192)Write a program to calculate the sum of distances between the adjacent numbers in an array of positive integers.

Input Format

array size and array elements

Constraints

no

Output Format

print sum of distances between adjacent numbers.

Sample Input 0

5
10 11 7 12 14
Sample Output 0

12
Sample Input 1

5
10 11 12 13 14
Sample Output 1

4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n-1;i++)
        {
            sum=sum+Math.abs(a[i]-a[i+1]);
        }
        System.out.println(sum);
        
    }
}

=====================================
193)Implement a program to insert an element into an array at the first position

Input Format

size,array elements and element to be inserted

Constraints

no

Output Format

return array after insertion

Sample Input 0

5
1 2 3 4 5
8
Sample Output 0

8 1 2 3 4 5
Sample Input 1

4
1 2 3 4
55
Sample Output 1

55 1 2 3 4


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
    LinkedList<Integer> l=new  LinkedList<Integer>();
        for(int i=0;i<n;i++)
        {
            l.add(sc.nextInt());
        }
        l.addFirst(sc.nextInt());
        
        for(Integer i:l)
        {
            System.out.print(i+" ");
        }
        
    }
}

============================================
194)Implement a program to insert an element into an array at the last position

Input Format

size,array elements and element to be inserted

Constraints

no

Output Format

return array after insertion

Sample Input 0

5
1 2 3 4 5
8
Sample Output 0

1 2 3 4 5 8
Sample Input 1

4
1 2 3 4
55
Sample Output 1

1 2 3 4 55


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        LinkedList<Integer> l=new LinkedList<Integer>();
        for(int i=0;i<n;i++)
        {
            l.add(sc.nextInt());
        }
        l.addLast(sc.nextInt());
        for(int i:l)
        {
            System.out.print(i+" ");
        }        
        
    }
}

=-----------------------------------------------
195)Implement a program to delete an element from an array at the first position

Input Format

size,array elements

Constraints

no

Output Format

return array after deleting from first location

Sample Input 0

5
1 2 3 4 5
Sample Output 0

2 3 4 5
Sample Input 1

4
1 2 3 4
Sample Output 1

2 3 4


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        LinkedList<Integer> l=new LinkedList<Integer>();
        for(int i=0;i<n;i++)
        {
            l.add(sc.nextInt());
        }
          l.removeFirst();
        
        for(Integer i:l)
        {
            System.out.print(i+" ");
        }
        
        
    }
}

==============================================
196)Implement a program to delete an element from an array at the last position

Input Format

size,array elements

Constraints

no

Output Format

return array after deleting from last location

Sample Input 0

5
1 2 3 4 5
Sample Output 0

1 2 3 4
Sample Input 1

4
1 2 3 4
Sample Output 1

1 2 3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        LinkedList<Integer> l=new LinkedList<Integer>();
        
        for(int i=0;i<n;i++)
        {
            l.add(sc.nextInt());
        }
        l.removeLast();
        
        for(int i:l)
        {
            System.out.print(i+" ");
        } 
    }
}

==================================================
197)Implement a program to delete an element from an array at the position

Input Format

size,array elements and position

Constraints

no

Output Format

return array after deleting from the location

Sample Input 0

5
1 2 3 4 5
2
Sample Output 0

1 2 4 5
Sample Input 1

5
11 22 33 44 55
1
Sample Output 1

11 33 44 55

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        LinkedList<Integer> l=new LinkedList<Integer>();
        for(int i=0;i<n;i++)
        {
            l.add(sc.nextInt());
        }
        l.remove(sc.nextInt());
            
            for(int i:l)
            {
                System.out.print(i+" ");
            }
    }
}

===============================================
198)Implement a program to delete the given element from an array

Input Format

size,array elements and element

Constraints

no

Output Format

return array after deleting

Sample Input 0

5
11 22 33 44 55
44
Sample Output 0

11 22 33 55
Sample Input 1

5
11 22 33 44 55
66
Sample Output 1

-1

import java.io.*;
import java.util.*;

public class Solution {

   public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n = obj.nextInt();
        LinkedList<Integer> L = new LinkedList<Integer>();
        for(int i=0;i<n;i++)
            L.add(obj.nextInt());
        if(L.remove(new Integer(obj.nextInt())))
        {
            for(Integer I:L)
                System.out.print(I+" ");
        }
        else
            System.out.println(-1);
    }
}
===============================================
199)Implement a program to update an element in the given array

Input Format

size,array elements and element to be updated (old element & new element)

Constraints

no

Output Format

return array after updating

Sample Input 0

5
1 2 3 4 5
2
999
Sample Output 0

1 999 3 4 5
Sample Input 1

5
1 2 3 4 5
1
888
Sample Output 1

888 2 3 4 5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        int oe=sc.nextInt();
        int ne=sc.nextInt();
        
        for(int i=0;i<n;i++)
        {
            if(a[i]==oe)
            {
                a[i]=ne;
            }
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
    }
}

===========================================
200)Implement a program to update an element in the given array based on position

Input Format

size,array elements and element to be updated and location

Constraints

no

Output Format

return array after updating

Sample Input 0

5
1 2 3 4 5
2
999
Sample Output 0

1 2 999 4 5
Sample Input 1

5
1 2 3 4 5
0
888
Sample Output 1

888 2 3 4 5


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
         int index=sc.nextInt(); 
         int ne=sc.nextInt();
         
        a[index]=ne;        
        
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }  
    }
}

===========================================
201)You are playing an online game. In the game, a list of N numbers is given. The player has to arrange the numbers so that all the odd numbers of the list come after even numbers. Write an algorithm to arrange the given list such that all the odd numbers of the list come after the even numbers.

Input Format

array size and array elements.

Constraints

no

Output Format

modified array.

Sample Input 0

8
10 98 3 33 12 22 21 11
Sample Output 0

10 98 12 22 3 33 21 11
Sample Input 1

5
1 2 3 4 5
Sample Output 1

2 4 1 3 5


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] a=new int[n];
        
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            if(a[i]%2==0)
                System.out.print(a[i]+" ");
        }
        for(int i=0;i<n;i++)
        {
            if(a[i]%2!=0)
              System.out.print(a[i]+" ");  
        }
        
    }
}

==================================================
202)The garments company apparel wishes to open outlets at various locations. 
The company shortlisted several plots in these locations and wishes to select only plots that are square shaped. 
Write an algorithm to help Apparel find the number of plots that it can select for its outlets.

Input Format

an integer N, and A1,A2,...AN representing areas of outlets.

Constraints

no

Output Format

print an integer representing the number of plots selected for outlets.

Sample Input 0

5
12 13 14 15 16
Sample Output 0

1
Sample Input 1

4
1 2 3 4
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),count=0;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
    
        for(int i=0;i<n;i++)
        {
            
            for(int j=1;j<=a[i];j++)
            {
                if(j*j==a[i]){
                    count++;
                    break;
                }
            }
        }
        
        System.out.print(count);
        
        
    }
}

======================================

203)A compnay wishes to provide cab service for their N employees. The employees have distance ranging from 0 to N-1. The company has calculated the total distance from an employee's residence to the company, considering the path to be followed by the cab is a straight path. The distance of the company from it self is 0. The distance for the employees who live to the left side of the company is represented with a negative sign. The distance for the employees who live to the right side of the company is represented with a positive sign. the cab will be allotted a range of distance. The company wishes to find the distance for the employees who live within the particular distance range. write a alogrithm to find the distance for the employees who live within the distance range.

Input Format

size of the list N ,SD,ED and an array of distance

Constraints

no

Output Format

distance within the range else -1

Sample Input 0

6
30 50
29 38 12 48 39 55
Sample Output 0

38 48 39
Sample Input 1

5
20 30
10 20 30 40 25
Sample Output 1

20 30 25

import java.io.*;
import java.util.*;


public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
             int n=sc.nextInt();
             int a[]=new int[n];
             int s=sc.nextInt();
             int e=sc.nextInt();
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
       
        for(int i=0;i<n;i++)
        { 
            if(Math.abs(a[i])>=s && Math.abs(a[i])<=e)
            {
                System.out.print(a[i]+" ");
            } 
           
        }
        }
}

==========================================
204)A company wishes to modify the technique by which tasks in the processing queue are executed. There are N processes with unique ID's from 0 to N-1. Each of these tasks has its own execution time. The company wishes to implement a new algorithm for processing tasks. for this purpose they have identified a value K by the new algorithm, the processor will first process the task that has the Kth shortest execution time.
Write an algorithm to find the Kth shortest execution time.

Input Format

array size, k value and array

Constraints

no

Output Format

kth shortest execution time.

Sample Input 0

7
5
9 -3 8 -6 -7 18 10
Sample Output 0

9
Sample Input 1

6
3
1 2 3 4 5 6
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int k=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j]){
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
            System.out.print(a[k-1]+" ");
    }
}

==================================================
205)Create a function that takes an array of numbers and return "Boom!" if the digit 7 appears in the array. Otherwise, return "there is no 7 in the array".

Input Format

an array from the user

Constraints

no

Output Format

Boom! or "there is no 7 in the array".

Sample Input 0

7
1 2 3 4 5 6 7
Sample Output 0

Boom!
Sample Input 1

4
8 6 33 100
Sample Output 1

there is no 7 in the array

import java.io.*;
import java.util.*;

public class Solution {
    public static boolean contains(int n)
    {
        int d;
        while(n!=0)
        {
            d=n%10;
            if(d==7)
            {
                return true;
            }
            n=n/10;
            }
        return false;
    }
    public static void main(String[] args) {
    
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i;
        int a[]=new int[n];
       
        for(i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
         int flag=0;
        for(i=0;i<n;i++)
        {
         if(contains(a[i]))
         {
             flag=1;
             break;
         }
        }
        
        System.out.print((flag==1)?"Boom!":"there is no 7 in the array"); 
    }
}

==============================================================================
206)Create a function which validates whether a given array alternates between positive and negative numbers.

Input Format

an array size and array

Constraints

no

Output Format

true or false

Sample Input 0

3
3 -2 5
Sample Output 0

true
Sample Input 1

6
3 -2 5 -5 2 -8
Sample Output 1

true

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),flag;
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        flag=1;
        
        for(int i=0;i<n-1;i++)
        {
            if(a[i]>0 && a[i+1]>0){
                flag=0;
                break;
            }
            
            if(a[i]<0 && a[i+1]<0){
                flag=0;
                break;
            }
        }
       System.out.print((flag==1)?"true":"false"); 
    }
}

========================================================
207)Write a function that returns true if all parameters are truthy, and false otherwise

Input Format

an array size and array

Constraints

no

Output Format

true or false

Sample Input 0

3
1 2 3
Sample Output 0

true
Sample Input 1

6
3 2 5 0 2 8
Sample Output 1

false


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),flag;
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        flag=1;
        
        for(int i=0;i<n-1;i++)
        {
            if(a[i]==0){
                flag=0;
                break;
            }
        }
       System.out.print((flag==1)?"true":"false"); 
    }
}

===================================================
208)Given an integer array and an integer N denoting the array length as input. your task is to return the sum of third largest and second minimum elements of the array.

Input Format

array size and array elements

Constraints

no

Output Format

an int value

Sample Input 0

5
5 3 2 1 4
Sample Output 0

5
Sample Input 1

6
12 45 100 250 300 450
Sample Output 1

295

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,j,temp;
        int a[]=new int[n];
        for(i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]>a[j])
            {
                temp=a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
    } 
    System.out.print(a[n-3]+a[1]+" ");
        
        }
}

=============================================
209)Implement a program to check whether an array is paliandrome or not.

Input Format

Array size N and Array Elements

Constraints

no

Output Format

true or false

Sample Input 0

5
1 2 3 2 1
Sample Output 0

true
Sample Input 1

4
10 11 12 13
Sample Output 1

false

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),low,high,flag;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        flag=1;
        low=0;
        high=n-1;
        while(low<=high)
        {
            if(a[low]!=a[high])
            {
                flag=0;
                break;
            }
            low++;
            high--;
        }
        System.out.print((flag==1)?"true":"false");
    }
}

=================================================
210)Given an integer N and integer array A as the input, where N denotes the length of A write a program to find an integer the sum of all these product with successors.

Input Format

array size and elements

Constraints

no

Output Format

an int value

Sample Input 0

5
1 2 3 4 5
Sample Output 0

70
Sample Input 1

4
1 2 3 5
Sample Output 1

50


Example:-
======

5 
1 5 2 0 4---> 1*2+5*6+2*3+0*1+4*5=2+30+6+0+20=58

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            sum=sum+(a[i]*(a[i]+1));
        }
        System.out.println(sum);
        
    }
}

==============================================
211)You are given an array of integers, a of size n, Your task is to find the number of elements whose positions will remain unchanged after sorted in ascending order.

Input Format

array size and elements

Constraints

no

Output Format

unchanged count

Sample Input 0

5
1 2 5 3 4
Sample Output 0

2
Sample Input 1

10
1 10 2 9 3 8 4 7 5 6
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp,count=0;
        
        int[] a=new int[n];
        int[] b=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            b[i]=a[i];
        }
        
        
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        }
        
        count=0;
        for(int i=0;i<n;i++)
        {
            if(a[i]==b[i])
            {
                count++;
            }
        }
        System.out.println(count);
    }
}

================================================
212)You are given a list of integers of size N,
write an algorithm to sort the first K elements of the list in ascending order and the remaining elements in descending order.

Input Format

an arry size and elements

Constraints

no

Output Format

updated array

Sample Input 0

5
1 2 3 4 5
Sample Output 0

1 2 5 4 3
Sample Input 1

8
1 8 2 7 3 6 4 5
Sample Output 1

1 2 3 4 8 7 6 5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    
        Scanner sc=new Scanner(System.in);
    int n=sc.nextInt(),i,j,temp;
    
    int[] a=new int[n];
    
    for(i=0;i<n;i++)
    {
        a[i]=sc.nextInt();
    }
    
    for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]>a[j])
            {
                temp=a[i];
                a[i]=a[j];
                a[j]=temp;
            }
        }
    }
    for(i=0;i<n/2;i++)
    {
        System.out.print(a[i]+" ");
    }
    for(i=n-1;i>=n/2;i--)
    {
        System.out.print(a[i]+" ");
    }
    }
}

============================================
213)A person went to an exhibition. A lucky draw is going on, where one should buy a ticket. And if they ticket number appear on the screen, that ticket will be considered as jackpot winner.

he knows the secret of picking up the ticket number, which will be considered for the jackpot.

sort the ticket number in the increasing order.
Now, the difference between the adjacent digits should not be more than 2.
If his ticket follows the above condition, then there are more chances to win the jackpot.

Given a ticket number, find whether the ticket is eligible to be part of jackpot or not. Print "Yes/No" based on the result.

Input Format

ticket number

Constraints

no

Output Format

Yes or No

Sample Input 0

171
Sample Output 0

No
Sample Input 1

123
Sample Output 1

Yes

example:-
=============
15243->12345 ->1-2 2-3 3-4 4-5 > yes
15943->13459->1-5 5-9 9-4 4-3->No


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,j,temp,flag,d,len;
        
        int a[]=new int[n];
        
        i=0;
        while(n!=0)
        {
            d=n%10;
            a[i++]=d;
            n=n/10;
        }
        len=i;
        for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
        {
            if(a[i]>a[j])
            {
                temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
        flag=1;
    for(i=0;i<len-1;i++)
    {
        if(a[i+1]-a[i]>2)
        {
            flag=0;
            break;
        }
    }
        System.out.print((flag==1)?"Yes":"No");
}    
    }


===========================================
214)a game developing company has developed a math game for kids called "Brain Fun". The game is for smartphone users and the player is given list of N positive numbers and a random number K. the player need to divide all the numbers in the list with random number k and then need to add all the quotients received in each division. the sum of all the quotients is the score of the player.

Write an algorithm to generate the score of the player.

Input Format

array size, elements and random number k

Constraints

no

Output Format

an int value

Sample Input 0

5
1 2 3 4 5
3
Sample Output 0

3
Sample Input 1

5
7 3 2 10 4
5
Sample Output 1

3


5
1 2 3 4 5
k=3

1/3+2/3+3/3+4/3+5/3
0+0+1+1+1=---->3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0,key;
        int a[]=new int[n];
        for(int i=0;i<n;i++){
            a[i]=sc.nextInt();
        }
        key=sc.nextInt();
        sum=0;
        
        for(int i=0;i<n;i++)
        {
            sum=sum+a[i]/key;
        }
        
        System.out.println(sum);
    }
}

=======================================
215)Perfect math is an online math program. In once of the assignments the system displays a list of N number and a value K, and students need to calculate the sum of remainders after dividing all the numbers from the list of N numbers by K. The system need to develop a program to calcualte the correct answer for the assignment.

Write an algorithm to calcualte the correct answer for the assignment.

Input Format

size N and N elements and K value

Constraints

no

Output Format

an int value.

Sample Input 0

5
1 2 3 4 5
3
Sample Output 0

6
Sample Input 1

5
7 3 2 10 4
5
Sample Output 1

11


5
1 2 3 4 5
k=3

1%3+2%3+3%3+4%3+5%3
 1+2+0+1+2=6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,s,k;
        int a[]=new int[n];
        for(i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
            
        }
        k=sc.nextInt();
        s=0;
        for(i=0;i<n;i++)
        {
            s=s+a[i]%k;
        }
        System.out.print(s);
    }
}
==================================
216)Given an array A of N integer numbers. The task is to rewrite the array by putting all multiples of 10 at the end of the given array.

Note: The order of the numbers which arenot multiples of 10 should remain unaltered, and similarly. the order of all multiples of 10 should be unaltered.

Input Format

N and Array Elements

Constraints

no

Output Format

updated array

Sample Input 0

9
10 12 5 40 30 7 50 9 10
Sample Output 0

12 5 7 9 10 40 30 50 10
Sample Input 1

5
10 2 3 4 5
Sample Output 1

2 3 4 5 10

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
 Scanner obj = new Scanner(System.in);
        int n=obj.nextInt(),i,a[] = new int[n];
        for(i=0;i<n;i++)
            a[i] = obj.nextInt();
        for(i=0;i<n;i++)
        {
            if(a[i]%10!=0)
                System.out.print(a[i]+" ");
        }
        for(i=0;i<n;i++)
        {
            if(a[i]%10==0)
                System.out.print(a[i]+" ");
        }
    }
}

==================================================
217)A company trasfers an encrypted code to one of its clients. The code needed to be decrypted so that it can be used for accessing all the required information. The code can be decrypted by interchanging each consecutive digit and once if the digit got interchanged then it cannot be used again. If at a certain point there is no digits to be interchanged with, then that single digit must be left as it is.
Write an algorithm to decrypt the code so that it can be used to access the required information.

Input Format

a number from the user

Constraints

no

Output Format

an integer value

Sample Input 0

5
1 2 3 4 5
Sample Output 0

2 1 4 3 5
Sample Input 1

6
1 2 3 4 5 6
Sample Output 1

2 1 4 3 6 5


5
1 2 3 4 5--->2 1 4 3 5


6
1 2 3 4 5 6-->2 1 4 3 6 5

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i;
        int a[]=new int[n];
       
        for(i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        if(n%2==0)
        {
            for(i=0;i<n;i=i+2)
            {
                System.out.print(a[i+1]+" "+a[i]+" ");
            }
        }
        else{
             for(i=0;i<n-1;i=i+2)
            {
                 System.out.print(a[i+1]+" "+a[i]+" ");
                
            }
            System.out.print(a[i]+" ");
        }
        
    }
}

==================================================
218)The e-commerce company 'Easy Shopping' displays some specific products for its premium customers on its user interface. The company shortlisted a list of N products. The list contains the price of each product. The company will select random products for display. The selection criteria states that only those products whose price is a perfect cube number will be selected for display. The selection process is automated and is done by the company's system. The company wishes to know the total count of the products selected for display.
Write an algorithm to find the count of products that will be displayed.

Input Format

an array size and elements

Constraints

no

Output Format

perfect cube elements

Sample Input 0

5
1 2 3 4 5
Sample Output 0

1
Sample Input 1

10
1 2 3 4 5 6 7 8 9 10
Sample Output 1

1 8

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,j;
        int a[]=new int[n];
        for(i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(i=0;i<n;i++)
        {
            for(j=1;j<=a[i];j++)
            {
                if(j*j*j==a[i])
                {
                    System.out.print(a[i]+" ");
                }
            }
        }   
    }
}

==============================================
219)Implement a function that takes an array of numbers and returns the mean (average) of all those numbers.

Input Format

an array size and elements

Constraints

no

Output Format

print mean value and round to two decimal places.

Sample Input 0

6
5 3 6 7 5 3
Sample Output 0

4.83
Sample Input 1

5
1 2 3 4 6
Sample Output 1

3.20
    


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        int sum=0;
        for(int i=0;i<n;i++)
        {
            sum=sum+a[i];
        }
         System.out.printf("%.2f",sum/(float)n);
    }
}

=================================================
220)The e-commerce company "TodaysApparel" has a list of sales values of N days. Some days the company made a profit, represented as a positive value. Other days the company incurred a loss, represented as a negative sales value. The company wishes to know the number of profitable days in the list.
Write an algorithm to help the company know the number of profitable days in the list.

Input Format

array size and elements

Constraints

no

Output Format

count number of +ve values

Sample Input 0

7
23 -7 13 -34 56 43 -12
Sample Output 0

4
Sample Input 1

5
1 2 -3 4 5
Sample Output 1

4

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),count=0;
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            if(a[i]>0)
            {
              count++;  
            }
        }
        System.out.println(count);  
    }
}

======================================
221)A data company wishes to store its data files on the server. They N files. Each file has a particular size. the server stires the files in bucket list. The bucket ID is calculated as the sum of the digits of its file size. The server.. the bucket ID for every file request where the file is stored.

Write an algorithm to find the bucketIDs where the files are stored.

Input Format

an array size and elements

Constraints

no

Output Format

bucketIds

Sample Input 0

4
43 345 20 987
Sample Output 0

7 12 2 24
Sample Input 1

5
12 13 14 15 16
Sample Output 1

3 4 5 6 7

123----->1+2+3=6

5
123 451 909 45 444---> 6 10 18 9 12


import java.io.*;
import java.util.*;

public class Solution {
    
    public static int fun(int n)
    {
        int sum=0,d;
        
        while(n!=0)
        {
            d=n%10;
            sum=sum+d;
            n=n/10;
        }
        return sum;
        
        
    }
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++)
        {
            System.out.print(fun(a[i])+" ");
        }  
    }
}

===============================================
222)Implement a program to convert the given decimal value into octal

Input Format

decimal value

Constraints

no

Output Format

octal number

Sample Input 0

16
Sample Output 0

20
Sample Input 1

10
Sample Output 1

12

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),i,len;
        int[] a=new int[n];
        i=0;
        while(n!=0)
        {
            a[i++]=n%8;
            n=n/8;
        }
        len=i;
        for(i=len-1;i>=0;i--)
        {
            System.out.print(a[i]);
        }
        
        
        
    }
}

import java.io.*;
import java.util.*;

public class Solution {
public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int n = obj.nextInt();
        System.out.println(Integer.toOctalString(n));
    }
}
======================================================
223)Implement a program to find sum of adjacent elements in the array

Input Format

an array size and elements

Constraints

no

Output Format

array with sum of adjacent elements

Sample Input 0

3
1 2 3
Sample Output 0

1 3 6
Sample Input 1

4
1 0 1 4
Sample Output 1

1 1 2 6

5
1 2 3 4 5 -->1 3 6 10 15

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),sum=0;
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        sum=0;
        
        for(int i=0;i<n;i++)
        {
            sum=sum+a[i];
            System.out.print(sum+" ");
        } 
    }
}

============================================
224)Airport Security officials have confiscated several items of the passenger at the security checkpoint. All the items have been dumped into a huge box (array). Each item possessed a certain amount of risk (0, 1, 2). Here is the risk severity of the item representing an array [] of N number of integer values. The risk here is to sort the item based on their level of risk values range from 0 to 2.

Input Format

array size and elements

Constraints

no

Output Format

sorted items based on risk

Sample Input 0

7
1 0 2 0 1 0 2
Sample Output 0

0 0 0 1 1 2 2
Sample Input 1

10
2 1 0 2 1 0 0 1 2 0
Sample Output 1

0 0 0 0 1 1 1 2 2 2


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),temp;
        int[] a=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        for(int i=0;i<n;i++)
        {
            for(int j=i+1;j<n;j++)
            {
                if(a[i]>a[j])
                {
                    temp=a[i];
                    a[i]=a[j];
                    a[j]=temp;
                }
            }
        } 
        for(int i=0;i<n;i++)
        {
            System.out.print(a[i]+" ");
        }
    }
}

=========================================
225)A chocolate factory is packing chocolates into the packets. The chocolate packets here represent an array of n number of integer values. The task is to find the empty packets (0) of chocolate and push it to the end of the conveyor belt (array).

Input Format

array size and elements

Constraints

no

Output Format

updated array

Sample Input 0

7
4 5 0 1 9 5 0
Sample Output 0

4 5 1 9 5 0 0
Sample Input 1

6
6 0 1 8 0 2
Sample Output 1

6 1 8 2 0 0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int a[]=new int[n];
        for(int i=0;i<n;i++)
        {
            a[i]=sc.nextInt();
        }
        
        for(int i=0;i<n;i++){
            if(a[i]!=0)
            {
                System.out.print(a[i]+" ");
            }
        }
        for(int i=0;i<n;i++)
        {
            if(a[i]==0)
            {
                System.out.print(a[i]+" ");
            }
        }
    }
}

======================================
226)Program to read matrix elements of order n x m and display on the console.

Input Format

A matrix of order n x m.

Constraints

no

Output Format

Print the same matrix

Sample Input 0

2
2
1 2
3 4
Sample Output 0

1 2
3 4
Sample Input 1

3
3
1 2 3
4 5 6
7 8 9
Sample Output 1

1 2 3
4 5 6
7 8 9


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        int[][] a=new int[n][m];
        
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }
        
    }
}

============================================
227)Program to read matrix elements and find sum of all elements in the matrix.

Input Format

A matrix of order n x m.

Constraints

no

Output Format

Print sum of matrix elements

Sample Input 0

2
2
1 2
3 4
Sample Output 0

10
Sample Input 1

3
3
1 2 3
4 5 6
7 8 9
Sample Output 1

45


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        
       int[][] a=new int[n][m];
        
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
        int sum=0;
        
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++){
                sum=sum+a[i][j];
            }
        }
        System.out.print(sum);  
    }
}

=====================================
228)Write a program to find sum of all even elements in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of all even elements

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

20
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args){
   Scanner obj = new Scanner(System.in);
        int a[][] = new int[3][3];
    int i,j,sum;
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j] = obj.nextInt();
            }
        }
        sum=0;
         for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(a[i][j]%2==0)
                sum=sum+a[i][j];
            }
        }
        System.out.print(sum);
    }
}

======================================
229)Write a program to find sum of all odd elements in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of all odd elements

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

25
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

4


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args){
   Scanner obj = new Scanner(System.in);
        int a[][] = new int[3][3];
    int i,j,sum;
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j] = obj.nextInt();
            }
        }
        sum=0;
         for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(a[i][j]%2!=0)
                sum=sum+a[i][j];
            }
        }
        System.out.print(sum);
    }
}

=====================================
230)Write a program to find sum of all prime elements in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of all prime elements

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

17
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

5

import java.io.*;
import java.util.*;

public class Solution {
    
    public static boolean prime(int n)
    {
        int f=0;
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                f++;
            }
        }
        return f==2;
    }
    

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);        
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(prime(a[i][j]))
                {
                    sum=sum+a[i][j];
                }
            }
        }
            System.out.print(sum);
    }
}

====================================
231)Write a program to find row wise sum in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of each row

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

6
15
24
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

1
2
3

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum;
        for(int i=0;i<3;i++)
        {
            sum=0;
            for(int j=0;j<3;j++)
            {
                sum=sum+a[i][j];
            }
             System.out.println(sum);
        }
    }
}
==================================
232)Write a program to find column wise sum in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of each column

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

12
15
18
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

1
2
3

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum;
        
        for(int i=0;i<3;i++)
        {
            sum=0;
            for(int j=0;j<3;j++)
            {
                sum=sum+a[j][i];
            }
            System.out.println(sum);
        }
    }
}

=====================================
233)Write a program to find sum of diagonal elements in matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of diagonal elements

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

15
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
       int sum=0;
        
        for(int i=0;i<3;i++)
        {
                sum=sum+a[i][i];
        }
            System.out.println(sum);
    }
}

=================================
234)Write a program to find sum of opposite diagonal elements in matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of opposite diagonal elements

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

15
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }    
        int sum=0;
        for(int i=0;i<3;i++)
        {
                 sum=sum+a[i][3-i-1];
       } 
        System.out.println(sum);
    }
}
======================================
235)Write a program to find sum of first and last element in a matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum of first and last element in matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

10
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                 sum=a[0][0]+a[3-1][3-1]; 
            }
        }
        System.out.println(sum);
    }
}

==========================================
236)Write a program to find the product of diagonal matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

find the product of diagonal matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

45
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

6

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
      int a[][]=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int p=1;
        for(int i=0;i<3;i++)
        {
            p=p*a[i][i];
        }
        System.out.println(p);
    }
}

======================================
237)Write a program to find the product of opposite diagonal matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

find the product of opposite diagonal matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

105
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int p=1;
        for(int i=0;i<3;i++)
        {
           p=p*a[i][3-i-1]; 
        }
    System.out.println(p);
    }
}

================================
238)Implement a program to print max element in an matrix

Input Format

a 3x3 matrix

Constraints

no

Output Format

max element in matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

9
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int max=a[0][0];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(max<a[i][j])
                {
                    max=a[i][j];
                }
            }
        }
        System.out.println(max);
    }
}


=========================================
239)Implement a program to print min element in an matrix

Input Format

a 3x3 matrix

Constraints

no

Output Format

min element in matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int min=a[0][0];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(min>a[i][j])
                {
                    min=a[i][j];
                }
            }
        }
        System.out.println(min);
    }
}

========================================
240)Implement a program to print max element in each row of a matrix

Input Format

a 3x3 matrix

Constraints

no

Output Format

print max element in each row of a matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

3
6
9
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

1
2
3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            int max=a[i][0];
            for(int j=0;j<3;j++)
            {
                if(max<a[i][j])
                {
                    max=a[i][j];
                }
            }
              System.out.println(max);
        }
    }
}

===================================
241)Implement a program to print min element in each row of a matrix

Input Format

a 3x3 matrix

Constraints

no

Output Format

print min element in each row of a matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1
4
7
Sample Input 1

1 0 0 
0 2 0
0 0 3
Sample Output 1

0
0
0
Sample Input 2

-1 0 0 
0 -2 0
0 0 -3
Sample Output 2

-1
-2
-3


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
        for(int i=0;i<3;i++)
        {
            int min=a[i][0];
            for(int j=0;j<3;j++)
            {
                if(min>a[i][j])
                {
                    min=a[i][j];
                }
            }
            System.out.println(min);
        }  
    }
}

=======================================
242)Implement a program to print transpose of a matrix

Input Format

a 3x3 matrix

Constraints

no

Output Format

print transpose of the matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1 4 7
2 5 8
3 6 9
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

1 0 0
0 2 0
0 0 3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                System.out.print(a[j][i]+" ");
            }
            System.out.println();
        }
    }
}

=============================================
243)Implement a program to find trace(sum of diagonal elements) of the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

print trace of the matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

15
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
        for(int i=0;i<3;i++)
        {
            sum=sum+a[i][i];
        }
        System.out.println(sum); 
    }
}

=========================================
244)Write a program to find frequency of odd and even elements in the matrix excluding 0.

Input Format

a 3x3 matrix

Constraints

no

Output Format

odd elements and even elements count

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

5
4
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

3
0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int ce=0,co=0;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(a[i][j]%2==0)
                {
                    ce++;
                }else{
                    co++;
                }
            }
        }
        System.out.println(co);
        System.out.println(ce); 
    }
}

====================================================
245)Implement a program to check whether the given matrix is identity matrix or not

Input Format

a 3x3 matrix

Constraints

no

Output Format

Yes or No

Sample Input 0

1 0 0
0 1 0
0 0 1
Sample Output 0

Yes
Sample Input 1

1 2 3
4 5 6
7 8 9
Sample Output 1

No

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        
       for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
               a[i][j]=sc.nextInt(); 
            }
        }
        
        boolean flag=true;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
              if(i==j && a[i][j]!=1)
              {
                  flag=false;
                  break;
              }
                 if(i!=j && a[i][j]!=0)
              {
                  flag=false;
                  break;
              }
        
                
            }
        }
        System.out.println((flag)?"Yes":"No");
    }
}

=================================================
246)Implement a program to check whether the given matrices are equal or not

Input Format

a 3x3 matrix

Constraints

no

Output Format

Yes or No

Sample Input 0

1 2 3
4 5 6
7 8 9
1 2 3
4 5 6
7 8 9
Sample Output 0

Yes
Sample Input 1

1 2 3
4 5 6
7 8 9
1 0 0
0 1 0
0 0 1
Sample Output 1

No

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
         for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }
        boolean flag=true;
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++)
            {
             if(a[i][j]!=b[i][j])
             {
              flag=false;
                 break;
             }
            }
        }
        System.out.println((flag)?"Yes":"No");
    }
}

===================================================
247)Write a program to perform addition operation on two matrices

Input Format

two 3x3 matrices

Constraints

no

Output Format

resultent matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
1 0 0
0 1 0
0 0 1
Sample Output 0

2 2 3
4 6 6
7 8 10
Sample Input 1

1 0 0
0 1 0
0 0 1
1 0 0
0 1 0
0 0 1
Sample Output 1

2 0 0
0 2 0
0 0 2


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        int[][] c=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
          for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }
        
          for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                c[i][j]=a[i][j]+b[i][j];
            }
        }
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
            System.out.print(c[i][j]+" ");
            }
            System.out.println();
        } 
    }
}

============================================
248)Write a program to perform subtraction operation on two matrices

Input Format

two 3x3 matrices

Constraints

no

Output Format

resultent matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
1 0 0
0 1 0
0 0 1
Sample Output 0

0 2 3
4 4 6
7 8 8
Sample Input 1

1 2 3
4 5 6
7 8 9
1 1 1
1 1 1
1 1 1
Sample Output 1

0 1 2
3 4 5
6 7 8

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        int[][] c=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
          for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }
        
          for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                c[i][j]=a[i][j]-b[i][j];
            }
        }
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
            System.out.print(c[i][j]+" ");
            }
            System.out.println();
        } 
    }
}

====================================
249)Write a program to perform multiplication operation on two matrices

Input Format

two 3x3 matrices

Constraints

no

Output Format

resultent matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
1 0 0
0 1 0
0 0 1
Sample Output 0

1 2 3
4 5 6
7 8 9
Sample Input 1

1 0 0 
0 1 0
0 0 1
1 0 0
0 1 0
0 0 1
Sample Output 1

1 0 0
0 1 0
0 0 1

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int[][] b=new int[3][3];
        int[][] c=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                b[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                c[i][j]=0;
                for(int k=0;k<3;k++)
                {
                     c[i][j] = c[i][j] + (a[i][k]*b[k][j]);
                }
            }
        }
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
               System.out.print(c[i][j]+" ");
            }
                 System.out.println();
        }
         
    }
}

=================================================
250)Implement a program to sort all the elements in asc order in the matrix

Input Format

a matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 3 2
6 7 9
5 4 8
Sample Output 0

1 2 3
4 5 6
7 8 9
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

0 0 0
0 0 0
1 2 3


***Sorting order is applicable for only single dimentional array not two dimentional array

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int i,j,temp;
     int[][] a=new int[3][3];
        int[] b=new int[9];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int k=0;
        for(i=0;i<3;i++)
        {
        for(j=0;j<3;j++)
        {
                b[k++]=a[i][j]; 
                 
        }
        }

        for(i=0;i<9;i++)
        {
            for(j=i+1;j<9;j++)
            {
                if(b[i]>b[j])
                {
                    temp=b[i];
                    b[i]=b[j];
                    b[j]=temp;
                }
            }
        }
        k=0;
       for(i=0;i<3;i++)
       {
           for(j=0;j<3;j++)
           {
                   a[i][j]=b[k++];
           }
       }
         for(i=0;i<3;i++)
       {
           for(j=0;j<3;j++)
           {
               System.out.print(a[i][j]+" ");
           }
             System.out.println();
       }  
    }
}

================================================
251)Implement a program to sort all the elements in desc in the matrix

Input Format

a matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

9 8 7
6 5 4
3 2 1
Sample Input 1

1 0 0
0 2 0
0 0 3
Sample Output 1

3 2 1
0 0 0
0 0 0

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int i,j,temp;
     int[][] a=new int[3][3];
        int[] b=new int[9];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int k=0;
        for(i=0;i<3;i++)
        {
        for(j=0;j<3;j++)
        {
                b[k++]=a[i][j]; 
                 
        }
        }

        for(i=0;i<9;i++)
        {
            for(j=i+1;j<9;j++)
            {
                if(b[i]<b[j])
                {
                    temp=b[i];
                    b[i]=b[j];
                    b[j]=temp;
                }
            }
        }
        k=0;
       for(i=0;i<3;i++)
       {
           for(j=0;j<3;j++)
           {
                   a[i][j]=b[k++];
           }
       }
         for(i=0;i<3;i++)
       {
           for(j=0;j<3;j++)
           {
               System.out.print(a[i][j]+" ");
           }
             System.out.println();
       }  
    }
}

====================================
252)Implement a program to sort all the row wise elements in asc order

Input Format

a matrix

Constraints

no

Output Format

result matrix

Sample Input 0

3 2 1
4 6 5
9 7 8
Sample Output 0

1 2 3
4 5 6
7 8 9
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

0 0 1
0 0 1
0 0 1

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int i,j,k,temp;
        int[][] a=new int[3][3];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
     
          for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
               for(k=j+1;k<3;k++)
               {
                   if(a[i][j]>a[i][k])
                   {
                       temp=a[i][j];
                       a[i][j]=a[i][k];
                       a[i][k]=temp;
                   }
               }
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
                System.out.println();
            }   
    }
}

==========================================
253)Implement a program to sort all the row wise elements in desc order

Input Format

a matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 3 2
4 5 6
7 9 8
Sample Output 0

3 2 1
6 5 4
9 8 7
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

1 0 0
1 0 0
1 0 0


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int i,j,k,temp;
        int[][] a=new int[3][3];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
     
          for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
               for(k=j+1;k<3;k++)
               {
                   if(a[i][j]<a[i][k])
                   {
                       temp=a[i][j];
                       a[i][j]=a[i][k];
                       a[i][k]=temp;
                   }
               }
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
                System.out.println();
            }   
    }
}

======================================
254)Implement a program to sort all the column values in asc order

Input Format

a matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1 2 3
4 5 6
7 8 9
Sample Input 1

9 8 7 
6 5 4
3 2 1
Sample Output 1

3 2 1
6 5 4
9 8 7


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int i,j,k;
        int[][] a=new int[3][3];
        int temp;
        int[][] b=new int[3][3];
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                b[i][j]=a[j][i];
            }
        }
          for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
               for(k=j+1;k<3;k++)
               {
                   if(b[i][j]>b[i][k])
                   {
                       temp=b[i][j];
                       b[i][j]=b[i][k];
                       b[i][k]=temp;
                   }
               }
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=b[j][i];
            }
        }
    
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
                System.out.println();
            }   
    }
}

=============================================
255)Implement a program to sort the all the column values in desc order

Input Format

a matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

7 8 9
4 5 6
1 2 3
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

1 1 1
0 0 0
0 0 0

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int i,j,k;
        int[][] a=new int[3][3];
        int temp;
        int[][] b=new int[3][3];
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                b[i][j]=a[j][i];
            }
        }
          for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
               for(k=j+1;k<3;k++)
               {
                   if(b[i][j]<b[i][k])
                   {
                       temp=b[i][j];
                       b[i][j]=b[i][k];
                       b[i][k]=temp;
                   }
               }
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=b[j][i];
            }
        }
    
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
                System.out.println();
            }   
    }
}

==============================================
256)Implement a program to check whether the given matrix is sparse matrix or not.
Note: a sparse matrix is a matrix with the majority of its elements equal to zero.

Input Format

a matrix

Constraints

no

Output Format

Yes or No

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

No
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

Yes

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int i,j,count=0;
     int[][] a=new int[3][3];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
           
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                if(a[i][j]==0)
                {
                    count++;
                }
            }
        }
        
       System.out.println((count>=5)?"Yes":"No");
    }
}

======================================
257)Implement a program to swap two given rows.

Input Format

matrix and m and n values

Constraints

no

Output Format

modified matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
1
2
Sample Output 0

4 5 6
1 2 3
7 8 9
Sample Input 1

1 0 0
0 1 0
0 0 1
1
2
Sample Output 1

0 1 0
1 0 0
0 0 1


Example:-
========
1 2 3 4
5 5 5 5
6 7 8 9
9 9 9 9

Row 1=0
Row 2=2

6 7 8 9
5 5 5 5
1 2 3 4
9 9 9 9

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int i,j,m,n,temp;
        int[][] a=new int[3][3];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        m=sc.nextInt();
        n=sc.nextInt();
        
        for(i=0;i<3;i++)
        {
            temp=a[m-1][i];
            a[m-1][i]=a[n-1][i];
            a[n-1][i]=temp;
        }
        
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }  
    }
}

=============================================
258)Implement a program to swap two given columns

Input Format

matrix and m and n values

Constraints

no

Output Format

modified matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
1
2
Sample Output 0

2 1 3
5 4 6
8 7 9
Sample Input 1

1 2 3
4 5 6
7 8 9
1 
3
Sample Output 1

3 2 1
6 5 4
9 8 7


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int i,j,m,n,temp;
        int[][] a=new int[3][3];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        m=sc.nextInt();
        n=sc.nextInt();
        
        for(i=0;i<3;i++)
        {
            temp=a[i][m-1];
            a[i][m-1]=a[i][n-1];
            a[i][n-1]=temp;
        }
        
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }  
    }
}

============================================
259)Program to accept a matrix of order 3x3 & interchange the diagonals.

Input Format

a 3x3 matrix

Constraints

no

Output Format

modified matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

3 2 1
4 5 6
9 8 7
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

0 0 1
0 1 0
1 0 0


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int i,j,temp;
        int[][] a=new int[3][3];
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }        
        for(i=0;i<3;i++)
        {
            temp=a[i][i];
            a[i][i]=a[i][3-i-1];
            a[i][3-i-1]=temp;
        }
        
        
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                System.out.print(a[i][j]+" ");
            }
            System.out.println();
        }  
    }
}
==============================================
260)Program to accept a matrix and check whether it is upper triangular matrix or not

Input Format

a 3x3 matrix

Constraints

no

Output Format

Yes or No

Sample Input 0

1 2 3
0 5 6
0 0 9
Sample Output 0

Yes
Sample Input 1

1 2 3
4 5 6
7 8 9
Sample Output 1

No

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        boolean flag=true;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
               if(i>j && a[i][j]!=0)
               {
                   flag=false;
               }
            }
        }
        System.out.println((flag==true)?"Yes":"No"); 
    }
}

============================================
261)Program to accept a matrix and check whether it is lower triangular matrix or not

Input Format

a 3x3 matrix

Constraints

no

Output Format

Yes or No

Sample Input 0

1 0 0
4 5 0
7 8 9
Sample Output 0

Yes
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

Yes


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        boolean flag=true;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
               if(i<j && a[i][j]!=0)
               {
                   flag=false;
               }
            }
        }
        System.out.println((flag==true)?"Yes":"No"); 
    }
}

=====================================================
262)Implement a program to read a matrix and multiplier and return scalar matrix multiplication.

Input Format

a 3x3 matrix and multiplier

Constraints

no

Output Format

resultent matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
2
Sample Output 0

2 4 6
8 10 12
14 16 18
Sample Input 1

1 1 1
1 1 1
1 1 1
5
Sample Output 1

5 5 5
5 5 5
5 5 5

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int m=sc.nextInt();
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                System.out.print(m*(a[i][j])+" ");
            }
            System.out.println();
        }  
    }
}

=========================================
263)Implement a program to read a matrix and check whether the given matrix is symmertric matrix or not

Input Format

a 3x3 matrix

Constraints

no

Output Format

Yes or No

Sample Input 0

1 2 3
2 4 5
3 5 8
Sample Output 0

Yes
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

Yes

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
         for(int i=0;i<3;i++)
         {
             for(int j=0;j<3;j++)
             {
                 a[i][j]=sc.nextInt();
             }
         }
        boolean flag=true;
        for(int i=0;i<3;i++)
         {
             for(int j=0;j<3;j++)
             {
                
                 if(a[i][j]!=a[j][i]){
                     flag=false;
                     break;
                 }
             }
         }
        System.out.println((flag==true)?"Yes":"No");
    }
}

===========================================
264)Implement a program to read a matrix and display only diagonal elements.

Input Format

a 3x3 matrix

Constraints

no

Output Format

print only diagonal elements

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1   
  5  
    9
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

1   
  1  
    1


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(i==j)
                {
                    System.out.print(a[i][j]+" ");
                }else{
                     System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}

======================================================
265)Implement a program to find square of each element present in a matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

resultent matrix

Sample Input 0

1 0 0
0 1 0
0 0 1
Sample Output 0

1 0 0
0 1 0
0 0 1
Sample Input 1

1 1 1
2 2 2
3 3 3
Sample Output 1

1 1 1
4 4 4
9 9 9

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                System.out.print((a[i][j])*(a[i][j])+" ");
            }
            System.out.println();
        }
    }
}

================================================
266)Implement a program to find sum of even indexed rows in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

30
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(i%2==0)
                {
                    sum=sum+a[i][j];
                }
            }
        }
               System.out.println(sum);
    }
}

=========================================
267)Implement a program to find sum of odd indexed rows in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

15
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
        
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(i%2!=0)
                {
                    sum=sum+a[i][j];
                }
            }
        }
               System.out.println(sum);
    }
}
=========================================
268)Implement a program to find sum of even indexed cols in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

30
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
        int sum=0;
         for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
              if(j%2==0)
              {
                  sum=sum+a[i][j];
              }
            }
        }
        
        System.out.println(sum);
    }
}

=====================================
269)Implement a program to find sum of odd indexed cols in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

15
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
        int sum=0;
         for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
              if(j%2!=0)
              {
                  sum=sum+a[i][j];
              }
            }
        }
        
        System.out.println(sum);
    }
}


===============================================
270)Implement a program to find sum of row index and col index is even in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

25
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if((i+j)%2==0)
                {
                    sum=sum+a[i][j];
                }
            }
        }
        
        System.out.println(sum);
        
        
    }
}

============================================
271)Implement a program to find sum of row index and col index is odd in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

20
Sample Input 1

1 0 0
0 1 0
0 0 1
Sample Output 1

0

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
            int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        int sum=0;
           for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if((i+j)%2!=0)
                {
                    sum=sum+a[i][j];
                }
            }
        }
        System.out.println(sum);
    }
}

==================================
272)Implement a program to find sum of prime elements in the given matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

sum as an int

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

17
Sample Input 1

10 11 12
13 14 15
16 17 18
Sample Output 1

41

import java.io.*;
import java.util.*;

public class Solution {
    
    public static boolean prime(int n)
    {
        int f=0;
        for(int i=1;i<=n;i++)
        {
            if(n%i==0)
            {
                f++;
            }
        }
        return f==2;
    }
    
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
      int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
        int sum=0;
         for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                   if(prime(a[i][j]))
                   {
                       sum=sum+a[i][j];
                   }
            }
         } 
        System.out.println(sum);
    }
}

=========================================
273)Implement a program to count number of prime digits present in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

prime digits count

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

4
Sample Input 1

10 11 12
13 14 15
16 17 18
Sample Output 1

4

import java.io.*;
import java.util.*;

public class Solution {
    
static int countdigits(int n)
{
    int c=0,d;
    while(n!=0)
    {
        d=n%10;
        if(d==2||d==3||d==5||d==7)
            c=c+1;
        n=n/10;
    }
    return c;
}
     public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        int a[][] = new int[3][3],i,j,c=0;
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                a[i][j] = obj.nextInt();
            }
        }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                c=c+countdigits(a[i][j]);
               
            }
        }
  System.out.print(c);
    }
}

=================================================
274)Implement a program to reverse each element in the matrix.

Input Format

a 3x3 matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1 2 3
4 5 6
7 8 9
Sample Input 1

11 12 13
14 15 16
17 18 19
Sample Output 1

11 21 31
41 51 61
71 81 91

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int rev(int n)
    {
        int d,r=0;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
               System.out.print(rev(a[i][j])+" ");
            }
            System.out.println();
        }
    }
}

================================================
275)Implement a program to keep all pali numbers as it is and replace remaining with 0.

Input Format

a 3x3 matrix

Constraints

no

Output Format

result matrix

Sample Input 0

1 2 3
4 5 6
7 8 9
Sample Output 0

1 2 3
4 5 6
7 8 9
Sample Input 1

111 122 133
141 151 168
888 567 999
Sample Output 1

111 0 0
141 151 0
888 0 999

import java.io.*;
import java.util.*;

public class Solution {
    
    public static int rev(int n)
    {
        int d,r=0;
        while(n!=0)
        {
            d=n%10;
            r=r*10+d;
            n=n/10;
        }
        return r;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                a[i][j]=sc.nextInt();
            }
        }
        
         for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                if(a[i][j]==rev(a[i][j]))
                {
                    System.out.print(a[i][j]+" ");
                }else{
                    System.out.print("0"+" ");
                }
            }
             System.out.println();
        }
    }
}

==================================================
276)Implement program to read a string from the user and print on the screen.

Input Format

a string

Constraints

no

Output Format

same string

Sample Input 0

hello
Sample Output 0

hello
Sample Input 1

prakash
Sample Output 1

prakash


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        System.out.println(s);
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        String name=sc.nextLine();
        System.out.print(name+" ");
    }
}
========================================
277)Implement a program to read a string and print only vowels present in string.

Input Format

string

Constraints

no

Output Format

only vowels

Sample Input 0

hello
Sample Output 0

e
o
Sample Input 1

welcome
Sample Output 1

e
o
e

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String vowels=sc.nextLine();
        
        for(int i=0;i<vowels.length();i++)
        {
        
if(vowels.charAt(i)=='a' ||vowels.charAt(i)=='e'|| vowels.charAt(i)=='i'|| vowels.charAt(i)=='o'|| vowels.charAt(i)=='u')
            System.out.println(vowels.charAt(i));
        }
    }
}


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String vowels=sc.nextLine();
        
        for(int i=0;i<vowels.length();i++)
        {
            char ch=vowels.charAt(i);
            if(ch=='a' || ch=='e' || ch=='i'|| ch=='o' || ch=='u'){
                System.out.println(ch);
            }
        }
        
        
        
    }
}
========================================

278)Implement a program to read a string and print only consonants present in string.

Input Format

string

Constraints

no

Output Format

only consonants

Sample Input 0

hello
Sample Output 0

h
l
l
Sample Input 1

welcome
Sample Output 1

w
l
c
m

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String consonants=sc.nextLine();
        
        for(int i=0;i<consonants.length();i++)
        {
            if(!(consonants.charAt(i)=='a'|| consonants.charAt(i)=='e'|| consonants.charAt(i)=='i'|| consonants.charAt(i)=='o'|| consonants.charAt(i)=='u')){
                System.out.println(consonants.charAt(i));
            }
        }
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String consonants=sc.nextLine();
        
        for(int i=0;i<consonants.length();i++)
        {
            char ch=consonants.charAt(i);
            if(!(ch=='a'|| ch=='e'|| ch=='i'|| ch=='o'|| ch=='u')){
                System.out.println(ch);
            }
        }
    }
}

========================================================
279)Implement a program to read a string and print number of vowels present in string.

Input Format

string

Constraints

no

Output Format

number of vowels

Sample Input 0

hello
Sample Output 0

2
Sample Input 1

welcome
Sample Output 1

3

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String vowels=sc.nextLine();
        int count=0;
        
        for(int i=0;i<vowels.length();i++)
        {
            char ch=vowels.charAt(i);
            
            if(ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u')
            {
                 count++;
            }
        }
       System.out.println(count);
    }
}

====================================
280)Implement a program to count number of consonants present in string

Input Format

string

Constraints

no

Output Format

number of consonants

Sample Input 0

hello
Sample Output 0

3
Sample Input 1

welcome
Sample Output 1

4

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String consonants=sc.nextLine();
        int count=0;
        for(int i=0;i<consonants.length();i++)
        {
            char ch=consonants.charAt(i);
            if(!(ch=='a'|| ch=='e'|| ch=='i'|| ch=='o'|| ch=='u'))
            {
                count++;
            }
        }
    System.out.println(count); 
    }
}

===========================================
281)Implement a program to count number of special characters in string

Input Format

string

Constraints

no

Output Format

special characters

Sample Input 0

he$$o
Sample Output 0

2
Sample Input 1

welcome
Sample Output 1

0

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int count=0;
        for(int i=0;i<str.length();i++)
        {
           char ch=str.charAt(i);
         if(!(ch>='a'&& ch<='z' ||ch>='A' && ch<='Z' || ch>='0'&& ch<='9'))
         {
              count++;
         }
        } 
        System.out.println(count);
    } 
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        
        String name=sc.nextLine();
        
        String ss="";
        
        int count=0;
        for(int i=0;i<name.length();i++)
        {
            char ch=name.charAt(i);
            
            if(!(ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || (ch>='0' && ch<='9') ){
                ss=ss+ch;
               count++;
            }
        }
        System.out.println(count);
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int count=0;
        for(int i=0;i<str.length();i++)
        {
           char ch=str.charAt(i);
         if(!(ch==' '||ch>='a'&& ch<='z' ||ch>='A' && ch<='Z' || ch>='0'&& ch<='9'))
         {
              count++;
         }
        } 
        System.out.println(count);
    } 
}
===================================================
282)Implement a program to count number of spaces in string

Input Format

string

Constraints

no

Output Format

count of spaces

Sample Input 0

welcome abc
Sample Output 0

1
Sample Input 1

abc def ghi
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int count=0;
        
        for(int i=0;i<str.length();i++)
        {
            char ch=str.charAt(i);
            if(ch==' ')
            {
                count++;
            }
        }
        System.out.println(count);
    }
}

================================

283)Implement a program to convert the given string into lower case

Input Format

string

Constraints

no

Output Format

string in lower case

Sample Input 0

Prakash
Sample Output 0

prakash
Sample Input 1

abc
Sample Output 1

abc


import java.io.*;
import java.util.*;

public class Solution {

       public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        for(int i=0;i<str.length();i++)
        {
            char ch=str.charAt(i);
            if(ch>='A' && ch<='Z')
            {
                System.out.print((char)(ch+32));
            }else{
                System.out.print(ch);
            }
        }
    }
}


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        System.out.println(str.toLowerCase());
    }
}

===========================================
284)Implement a program to convert the given string into upper case

Input Format

string

Constraints

no

Output Format

string in upper case

Sample Input 0

abc
Sample Output 0

ABC
Sample Input 1

wElCoMe
Sample Output 1

WELCOME


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        
        for(int i=0;i<str.length();i++)
        {
            char ch=str.charAt(i);
            if(ch>='a' && ch<='z')
            {
                System.out.print((char)(ch-32));
            }else{
                 System.out.print(ch);
            }
        }
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        System.out.println(str.toUpperCase());
    }
}

=========================================
285)Implement a program to convert lower case into upper case and upper case into lower case (swap case)

Input Format

string

Constraints

no

Output Format

toggle case

Sample Input 0

abc
Sample Output 0

ABC
Sample Input 1

Prakash
Sample Output 1

pRAKASH

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        
        for(int i=0;i<str.length();i++)
        {
            char ch=str.charAt(i);
            if(ch>='a' && ch<='z')
            {
                System.out.print((char)(ch-32));
            }
            else if(ch>='A' && ch<='Z')
            {
                System.out.print((char)(ch+32));
            }else{
                System.out.print(ch);
            }
        }
    }
}


import java.io.*;
import java.util.*;

public class Solution {
public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        String s = obj.nextLine();
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)>='a' && s.charAt(i)<='z')
                System.out.print((char)(s.charAt(i)-32));
            else if(s.charAt(i)>='A' && s.charAt(i)<='Z')
                System.out.print((char)(s.charAt(i)+32));
            else
                System.out.print(s.charAt(i));
        }
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String swap=sc.nextLine();
        
        String sum="";
        for(int i=0;i<swap.length();i++)
        {
            char ch=swap.charAt(i);
            if(ch>='a' && ch<='z')
            {
                sum=sum+(char)(ch-32);
            }
            
            if(ch>='A' && ch<='Z')
            {
                sum=sum+(char)(ch+32);
            } 
        }
    System.out.print(sum);
    }
}

================
286)Implement a program to return first capital letter in the given string

Input Format

string

Constraints

no

Output Format

first capital letter

Sample Input 0

welCOME
Sample Output 0

C
Sample Input 1

aBCd
Sample Output 1

B


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        for(int i=0;i<str.length();i++)
        {
            char ch=str.charAt(i);
            if(ch>='A' && ch<='Z')
            {
                System.out.print(ch);
                break;
            }
        }       
    }
}

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)>='A' && s.charAt(i)<='Z')
            {
                System.out.println(s.charAt(i));
                break;
            }
        }
    }
}

=======================

287)Implement a program to count number of misses in the given string

Input Format

string

Constraints

no

Output Format

count of misses

Sample Input 0

he$$o
Sample Output 0

2
Sample Input 1

welcome
Sample Output 1

0

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int count=0;
        for(int i=0;i<str.length();i++)
        {
            char ch=str.charAt(i);
            if(!(ch>='a' && ch<='z' || ch>='A' && ch<='Z'|| ch>='0' && ch<='9'))
            {
               count++;
            }
        }
        System.out.println(count);
    }
}

======================================
288)Program to read date, month and year from the user and check whether it is magic date or not.
Here are the rules for magic date.

1) mm*dd is a 1-digit number that matches the last digit in YYYY
2) mm*dd is a 2-digit number that matches the last two digits in YYYY
3) mm*dd is a 3-digit number that matches the last three digits in YYYY

Input Format

three int values

Constraints

no

Output Format

true or false

Sample Input 0

1-1-2001
Sample Output 0

True
Sample Input 1

5-2-2010
Sample Output 1

True


Example:-
=========

5-2-2010

"5-2-2010"

2010>> endsWith(10);
5*2=10->"10"(toString)


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String[] str=sc.nextLine().split("-");
        int d=Integer.parseInt(str[0]);
        int m=Integer.parseInt(str[1]);
        String ss=Integer.toString(d*m);
        System.out.println(str[2].endsWith(ss)?"True":"False");
    }
}

===========================================
289)Accept video length in minutes the format is mm:ss in String format, create a function that takes video length and return it in seconds.

Input Format

video length in mm:ss

Constraints

no

Output Format

length in seconds

Sample Input 0

01:00
Sample Output 0

60
Sample Input 1

13:56
Sample Output 1

836

Example:-
===========
2:10---> 2*60+10---->120+10----->130 Seconds


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String[] str=sc.nextLine().split(":");
        int mm=Integer.parseInt(str[0]);
         int ss=Integer.parseInt(str[1]);
        System.out.println(mm*60+ss);
    }
}

=============================================
290)Given a valid IP address, return a defanged version of that IP address. A defanged IP address replaces every period '.' with "[.]".

Input Format

A string

Constraints

no

Output Format

replacement String

Sample Input 0

1.1.1.1
Sample Output 0

1[.]1[.]1[.]1
Sample Input 1

255.100.50.0
Sample Output 1

255[.]100[.]50[.]0


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
       String ss=str.replace("[.]","");
        
        for(int i=0;i<ss.length();i++)
        {
            char ch=str.charAt(i);
            
            if(ch=='.')
            {
                System.out.print("[.]");
            }else{
                System.out.print(ch);
            }   
        }
    }
}
==============================================
291)Given two strings, a and b, return the result of putting them together in the order abba,
e.g. "Hi" and "Bye" returns "HiByeByeHi".

Input Format

two strings s1 and s2

Constraints

no

Output Format

return expected string

Sample Input 0

Hi
Bye
Sample Output 0

HiByeByeHi
Sample Input 1

What
Up
Sample Output 1

WhatUpUpWhat

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String str1=sc.nextLine();
          String str2=sc.nextLine();
        System.out.print(str1+""+str2+""+str2+""+str1);   
    }
}

import java.io.*;
import java.util.*;

public class Solution {
 public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
      String s2=sc.nextLine();
        System.out.print(s1+""+s2+""+s2+""+s1);
    
    
    }
}

import java.io.*;
import java.util.*;

public class Solution {
 public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
      String s2=sc.nextLine();
        System.out.print(s1+s2+s2+s1);
    }
}
====================================

292)Given a string, return a new string made of 3 copies of the last 2 chars of the original string. The string length will be at least 2.

Input Format

a string from the user

Constraints

no

Output Format

new string

Sample Input 0

Hello
Sample Output 0

lololo
Sample Input 1

Prakash
Sample Output 1

shshsh


Example:-
=========
Last two characters 3 times
python --->ononon
Hello---->lololo


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int n=s.length();
System.out.print(s.charAt(n-2)+""+s.charAt(n-1)+""+s.charAt(n-2)+""+s.charAt(n-1)+""+s.charAt(n-2)+""+s.charAt(n-1));
    }
    
}

=======================================
293)Create a function/method that takes a string and return the word count. The string will be a sentence.

Input Format

a string from the user

Constraints

no

Output Format

word count

Sample Input 0

This is test
Sample Output 0

3
Sample Input 1

Test Demo
Sample Output 1

2

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int count=1;
        
        for(int i=0;i<s.length();i++)
        {
            if((s.charAt(i)==' ') && (s.charAt(i+1)!=' '))
            {
                count++;
            }
        }
        System.out.println(count);
    }
}

======================================
294)Create a function/method that takes two Strings and returns true of the first string ends with second string, otherwise return false.

Input Format

a string from the user

Constraints

no

Output Format

true or false

Sample Input 0

abc
bc
Sample Output 0

true
Sample Input 1

abc
d
Sample Output 1

false

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
        String s2=sc.nextLine();
        
        int count=0;
        for(int i=0;i<s1.length();i++)
        {
            for(int j=0;j<s2.length()-1;j++)
            {
                if(s1.charAt(i)==s2.charAt(j))
                {
                    count++;
                    break;
                }
            }
        }
        System.out.println((count>0)?"true":"false");
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
        String s2=sc.nextLine();
        
        int count=0;
        for(int i=0;i<s1.length();i++)
        {
            for(int j=0;j<s2.length();j++)
            {
                if(s1.charAt(i)==s2.charAt(j))
                {
                    count++;
                    break;
                }
            }
        }
        System.out.println((count>0)?"true":"false");
    }
}


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
        String s2=sc.nextLine();
        System.out.println(s1.endsWith(s2));
    
    }
}
========================================

295)Problem Statement: Create a function/method that accepts a string (of person’s first and last name) and returns a string with in first and last name swapped).

Input Format

two space seperated strings

Constraints

no

Output Format

last name followed by first name

Sample Input 0

Donald
Trump
Sample Output 0

Trump Donald
Sample Input 1

Pawan
Kalyan
Sample Output 1

Kalyan Pawan



import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
        String s2=sc.nextLine();
       System.out.print(s2+" "+s1);
    }
}

====================================
296)create a method/function that takes a string as its argument and returns the string in reversed order.

Input Format

a string

Constraints

no

Output Format

reversed string

Sample Input 0

Hello Word
Sample Output 0

droW olleH
Sample Input 1

The quick brown fox
Sample Output 1

xof nworb kciuq ehT

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        for(int i=s.length()-1;i>=0;i--)
        {
            System.out.print(s.charAt(i));
        }
    }
}

========================================
297)A word has been split into a left part and right part.
Re-form the word by adding both halves together changing the first to an uppercase letter.

Input Format

two strings from the user

Constraints

no

Output Format

concatenated string with caps in first character

Sample Input 0

prakash
babu 
Sample Output 0

Prakashbabu
Sample Input 1

hacker
rank
Sample Output 1

Hackerrank


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
        String s2=sc.nextLine();
     System.out.print((s1.charAt(0)+"").toUpperCase()+s1.substring(1)+s2);
    }
}

==============================================================================
298)Lisa always forgets her birthday which is on th 5th July. So develop a function/method which will be helpful to remember her birthday.
The function/method checkBirthday return an integer 1, if it is her birthday else return 0.
 the function/method checkBirthday accepts two arguments.Month, a string representing the month of her birth and day, 
 an integer representing the data of her birthday.

Input Format

month & day

Constraints

no

Output Format

1 or 0

Sample Input 0

july
5
Sample Output 0

1
Sample Input 1

june
5
Sample Output 1

0


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int n=sc.nextInt();
        
        if(n==5 && s.equals("july"))
        {
            System.out.print("1");
        }else{
             System.out.print("0");
        }
        
    }
}

=====================================
299)Implement a program to check whether the given string is paliandrome or not, if yes return true else return false

Input Format

string from the user

Constraints

no

Output Format

true or false

Sample Input 0

abab
Sample Output 0

false
Sample Input 1

abba
Sample Output 1

true

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
       Scanner obj = new Scanner(System.in);
        String s = obj.nextLine();
        StringBuffer sb = new StringBuffer(s);
        sb.reverse();
        System.out.println(s.equals(sb.toString()));
    }
}

=====================================
300)Two strings a and b are called anagrams, if they contain all the same characters in the same frequencies.

Input Format

two strings a and b

Constraints

no

Output Format

true or false

Sample Input 0

anagram
margana
Sample Output 0

true
Sample Input 1

python
java
Sample Output 1

false

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String str1=sc.nextLine();
        String str2=sc.nextLine();
            char[] ch1=str1.toLowerCase().toCharArray();
            char[] ch2=str2.toLowerCase().toCharArray();
            Arrays.sort(ch1);
            Arrays.sort(ch2);
       System.out.println(Arrays.equals(ch1,ch2));
    }
}


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner obj = new Scanner(System.in);
        String s1 = obj.nextLine();
        String s2 = obj.nextLine();
        char ch1[] = s1.toCharArray();
        char ch2[] = s2.toCharArray();
        Arrays.sort(ch1);
        Arrays.sort(ch2);
        System.out.println(Arrays.equals(ch1,ch2));
    }
}
=======================================================
301)Given a string, implement a program to find max occurring character in the given string

Input Format

A string from the user.

Constraints

no

Output Format

max occurring character

Sample Input 0

welcome
Sample Output 0

e
Sample Input 1

java
Sample Output 1

a


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int max;
        int a[]=new int[256];
        String s=sc.nextLine();
        for(int i=0;i<s.length();i++)
        {
            a[s.charAt(i)]++;
        }
        max=0;
        for(int i=0;i<256;i++)
        {
            if(a[max]<a[i])
            {
                max=i;
            }
        }
    System.out.println((char)(max));
    }
}

==================================
ChessBoard:-
============
302)You are given coordinates, a string that represents the coordinates of a square of the chess board. 
bellow is the chess board for your reference.

Return True if the saquare is in white, and false if the square is in Black.
The coordinates will always represent a valid chess board square. The coordinates will always have the letter first, and the number second.

Input Format

a string

Constraints

no

Output Format

true or false

Sample Input 0

a1
Sample Output 0

false
Sample Input 1

h3
Sample Output 1

true

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int x,y;
        x=s.charAt(0)-96;
        y=s.charAt(1);
        if((x+y)%2==0)
       {
        System.out.println("false");
       }else {
           System.out.println("true");
       }
    }
}

========================================
303)Write a function that finds the word "bomb" in the given string (not case sensitive) return "DUCK!" if found, else return "Relax there's no bomb."

Input Format

a string

Constraints

no

Output Format

"DUCK!" or "Relax, There's no bomb."

Sample Input 0

There is a bomb.
Sample Output 0

DUCK!
Sample Input 1

Hey, did you think there is a BOMB?
Sample Output 1

DUCK!


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine().toLowerCase();
        System.out.println(s.contains("bomb")?"DUCK!":"Relax, There's no bomb.");
    }
}

================
304)Create a function that takes a string, check if it has the same number of x's and o's and returns either true or false.
Rules:
1. return boolean value true or false.
2. returns true if the amount x's and o's are the same.
3. returns false if they are not the same amount.
4. the string can contains any character.
5. when 'x' and 'o' are not in the string, return true.

Input Format

a strinG

Constraints

no

Output Format

true or false

Sample Input 0

ooxx
Sample Output 0

true
Sample Input 1

xooxx
Sample Output 1

false



import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int countX=0;
        int countO=0;
        
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='x')
            {
                countX++;
            }
            if(s.charAt(i)=='o')
            {
                countO++;
            }
        }
        System.out.print((countX==countO)?"true":"false");
    }
}

=========================================
305)write a function that shutters a word as if someone is struggling to read it. The first two letters are repeated twice with an ellipsis ... , and then the word is pronounced with a question mark?

Input Format

a string

Constraints

no

Output Format

xx...xx...~~~~~~~?

Sample Input 0

incredible
Sample Output 0

in...in...incredible?
Sample Input 1

abc
Sample Output 1

ab...ab...abc?

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        System.out.printf(s.charAt(0)+""+s.charAt(1)+"..."+s.charAt(0)+""+s.charAt(1)+"..."+s+"?");
    }
}

======================================================================================================
306)Create a method that takes a string and returns a string in which each character is repeated once.

Input Format

String from the user

Constraints

no

Output Format

String

Sample Input 0

string
Sample Output 0

ssttrriinngg
Sample Input 1

hello
Sample Output 1

hheelllloo

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        
        for(int i=0;i<s.length();i++)
        {
             System.out.print(s.charAt(i)+""+s.charAt(i));
        }
    }
}

=============================================================
307)Create a function that takes a word and returns true if the word has two consecutive identical letters.

Input Format

A string

Constraints

no

Output Format

true or false

Sample Input 0

loop
Sample Output 0

true
Sample Input 1

yummy
Sample Output 1

true

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
    int count=0;
        for(int i=0;i<s.length();i++)
        {
            for(int j=i+1;j<s.length();j++)
            {
                if(s.charAt(i)==s.charAt(j))
                {
                    count++;
                    break;
                }
            }
        }
    System.out.println((count>0)?"true":"false");
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int flag=0;
        for(int i=0;i<s.length()-1;i++)
        {
            if(s.charAt(i)==s.charAt(i+1))
            {
                flag=1;
                break;
            }
        }
        System.out.println((flag==1)?"true":"false");
    }
}

=========================================
308)Andy, Ben and Charlotte are playing a board game. The three of them decided to come up with a new scoring system. A player's first initial ("A","B" & "C") denotes that players scoring a single point. Given a string of capital letters. returns an array of the player's scores.

Input Format

A String

Constraints

no

Output Format

score

Sample Input 0

A
Sample Output 0

1 0 0
Sample Input 1

ABC
Sample Output 1

1 1 1

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int countA=0;
        int countB=0;
        int countC=0;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='A')
            {
                countA++;
            }
             if(s.charAt(i)=='B')
            {
                countB++;
            }
             if(s.charAt(i)=='C')
            {
                countC++;
            }
        }
        System.out.println(countA+" "+countB+" "+countC);
    }
}

==========================================================
309)Create a function that takes a string and returns a new string with all vowels removed.

Input Format

a string

Constraints

no

Output Format

a string

Sample Input 0

welcome 
Sample Output 0

wlcm
Sample Input 1

demo
Sample Output 1

dm

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='a'||s.charAt(i)=='e'||s.charAt(i)=='i'||s.charAt(i)=='o'||s.charAt(i)=='u')
            {
               continue;
            }else{
                System.out.print(s.charAt(i));
            }
        }
        
    }
}

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        for(int i=0;i<s.length();i++)
        {
            if(!(s.charAt(i)=='a'|| s.charAt(i)=='e'||s.charAt(i)=='i'||s.charAt(i)=='o'||s.charAt(i)=='u'))
            {
                 System.out.print(s.charAt(i));
            }
        }
    }
}
===============================================================================================================
310)Create a function that takes a string and returns a string with spaces in between all of the characters.

Input Format

a string

Constraints

no

Output Format

string

Sample Input 0

abc
Sample Output 0

a b c
Sample Input 1

space
Sample Output 1

s p a c e

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        for(int i=0;i<s.length();i++)
        {
            System.out.print(s.charAt(i)+" ");
        }
    }
}

311)Create a function that replaces all the vowels in a string with a specified character,

Input Format

A string from the user and a character

Constraints

no

Output Format

A string

Sample Input 0

welcome
#
Sample Output 0

w#lc#m#
Sample Input 1

prakash
.
Sample Output 1

pr.k.sh


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
          String s=sc.nextLine();
        char ch=sc.next().charAt(0);
        
         for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='a'|| s.charAt(i)=='e'||s.charAt(i)=='i'||s.charAt(i)=='o'||s.charAt(i)=='u')
            {
                 System.out.print(ch);
            }else{
                 System.out.print(s.charAt(i));
            }
         }        
    }
}

====================================================
312)Write a function that takes a string name and number num (either 1 or 0) and return "Hello"+name if number is 1, otherwise "Bye"+name.

Input Format

a string from the user

Constraints

no

Output Format

a string

Sample Input 0

prakash
1
Sample Output 0

Hello prakash
Sample Input 1

java
0
Sample Output 1

Bye java


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
   Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int i=sc.nextInt();
        
        if(i==1)
        {
            System.out.print("Hello "+s);
        }
        else{
            System.out.print("Bye "+s);
        }
    }
}

=======================================
313)zipcodes consists of 5 consecutive digits. Given a string, write a function to determine whether the input is a valid zip code. a valid zipcode is as follows
1. must contain only numbers.
2. it should not contain any spaces.
3. length should be only 5.

Input Format

A string

Constraints

no

Output Format

true or false

Sample Input 0

59001
Sample Output 0

true
Sample Input 1

853a7
Sample Output 1

false


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    String s=sc.nextLine();
    int count=0;
    for(int i=0;i<s.length();i++)
    {
        if(s.charAt(i)>='0' && s.charAt(i)<='9')
        {
            count++;
        }
    }
        System.out.print((count==5)?"true":"false");
    }
}

=======================================================
314)create a function that takes a string and returns, the middle character(s). if the word's length is odd return the midlle character. if the word's length is even, return the middle two characters.

Input Format

a string from the user

Constraints

no

Output Format

middle character(s)

Sample Input 0

test
Sample Output 0

es
Sample Input 1

testing
Sample Output 1

t

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int n=s.length();
         if(n%2==0)
        {
            System.out.print(s.charAt(n/2-1)+""+s.charAt(n/2));
        }else{
            System.out.print(s.charAt(n/2));
        }
    }
}
================================================================
315)create a function that returns the index of first vowel in a string

Input Format

a string

Constraints

no

Output Format

an int value

Sample Input 0

apple
Sample Output 0

0
Sample Input 1

hello
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        
        int count=0;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='a' ||s.charAt(i)=='e' ||s.charAt(i)=='i'||s.charAt(i)=='o'||s.charAt(i)=='u')
            {
               System.out.print(i);
                break;
                
            }
        }
    }
}

==========================================================================================
316)Implement a program that returns the number of decimal places a number (given as a string) has. 
Any zeros after the decimal point count towards the number of decimal places.

Input Format

string from the user

Constraints

non empty string

Output Format

count of decimal places

Sample Input 0

43.20
Sample Output 0

2
Sample Input 1

400
Sample Output 1

0
Sample Input 2

3.1
Sample Output 2

1
Sample Input 3

7.333
Sample Output 3

3


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int count=0,i,flag=0;
        
        for(i=s.length()-1;i>=0;i--)
        {
            if(s.charAt(i)=='.')
            {
                flag=1;
                break;
            }else{
                count++;
            }
        }
        
        if(flag==1)
        {
            System.out.print(count);
        }else{
            System.out.print("0");
        }
    }
}

======================================
317)Given a string S, the task is to remove all the duplicates in the given string.

Input Format

a string from the user

Constraints

remove all duplicates

Output Format

a string without duplicates

Sample Input 0

hello
Sample Output 0

helo
Sample Input 1

welcome
Sample Output 1

welcom


import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
       Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();   
        String res="";
        for(int i=0;i<s.length();i++)
        {
            if(res.indexOf(s.charAt(i))==-1)
            {
                res=res+s.charAt(i);
            }
        }
      System.out.println(res);
    }
}

==============================================
318)Write a Java program to take an input string and exchange the first and last word and reverse the middle word.

Input Format

a string

Constraints

no

Output Format

a string

Sample Input 0

Hello welcome to java
Sample Output 0

java ot emoclew Hello
Sample Input 1

java is very easy
Sample Output 1

easy yrev si java

import java.io.*;
import java.util.*;

public class Solution {
// Method 1
    // To swap corners words
    static void swap(String m, int length)
    {
 
        // Declaring string variables to
        // store the first and last characters
        String first = "";
        String last = "";
 
        // Creating first address variable
        // Initially initializing with zero
        int index = 0;
 
        for (index = 0; index < length; ++index) {
 
            // Checking the space
            if (m.charAt(index) == 32) {
                break;
            }
 
            // Storing the last word in the last variable
            last += m.charAt(index);
        }
 
        // Now creating second address variable
        // Initially initializing with zero
        int index1 = 0;
 
        for (index1 = length - 1; index1 >= 0; --index1) {
            if (m.charAt(index1) == 32) {
                break;
            }
 
            // Storing the First word of the given string
            first = m.charAt(index1) + first;
        }
 
        String middle = "";
        for (int i = index1 - 1; i > index; --i) {
            if (m.charAt(i) == 32) {
                middle += " ";
            }
            else {
 
                // Storing all the middle words
                middle += m.charAt(i);
            }
        }
 
        // Print and display all the string variables
        System.out.print(first + " " + middle + " " + last);
    }
 
   public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String m = sc.nextLine();
       // Calculating string length using length() method
        // and storing it in a variable
        int length = m.length();
 
        // Calling the method 1 to swap the words
        // for our custom input string
        swap(m, length);
    }
}


========================================================
319)Create a function that determines whether a string is a valid hex code. 
A hex code must begin with a pound key # and is exactly 6 characters in length. 
Each character must be a digit from 0-9 or an alphabetic character from A-F. 
All alphabetic characters may be uppercase or lowercase.

Input Format

a string from the user

Constraints

no

Output Format

true or false

Sample Input 0

#CD5C5C
Sample Output 0

true
Sample Input 1

#CD5C5G
Sample Output 1

false


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int count=0,flag=1;
        if(s.charAt(0)!='#')
        {
            flag=0;
        }
        for(int i=1;i<s.length();i++)
        {
            if(s.charAt(i)>='0' && s.charAt(i)<='9'||s.charAt(i)>='a' && s.charAt(i)<='f' ||s.charAt(i)>='A' && s.charAt(i)<='F' )
            {
                count++;
            }
        }
        
        if(flag==1 && count==6)
        {
            System.out.print("true");
        }else{
             System.out.print("false");
        }
    }
}

=============================================================
320)Write a program to print even length words in a string?

Input Format

a string from the user

Constraints

no

Output Format

list of strings with even length

Sample Input 0

hello world java
Sample Output 0

java
Sample Input 1

python is very easy
Sample Output 1

python is very easy

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
        String[] s=sc.nextLine().split(" ");
        for(String ss:s)
        {
            if(ss.length()%2==0)
            {
                System.out.print(ss+" ");
            }
        }
    }
}
===========================================

321)Write a function that changes every letter to the next letter:
"a" becomes "b"
"b" becomes "c"
"d" becomes "e"
and so on ...

note: there is no z's in test cases, be happy.

Input Format

a string from the user

Constraints

no

Output Format

modified string

Sample Input 0

hello
Sample Output 0

ifmmp
Sample Input 1

bye
Sample Output 1

czf
Sample Input 2

welcome
Sample Output 2

xfmdpnf

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        char[] ch=s.toCharArray();
        int val=0;
        String s1="";
        for(int i=0;i<ch.length;i++)
        {
            val=ch[i]+1;
            char c=(char)val;
            s1=s1+c;
        }
        System.out.print(s1);
    }
}

====================================
322)Write a function that returns the first n vowels of a string.

Input Format

a string from the user and an integer value

Constraints

Return "invalid" if the n exceeds the number of vowels in a string.

Output Format

return first n vowels in the string

Sample Input 0

sharpening skills
3
Sample Output 0

aei
Sample Input 1

major league
5
Sample Output 1

aoeau
Sample Input 2

hostess
5
Sample Output 2

invalid


Example:-
==========
welcome ----> eoe ---> 1----> e, 2=> eo 3=> eoe



import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int n=sc.nextInt();
        String ss="";
        int j=0;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='a' ||s.charAt(i)=='e' || s.charAt(i)=='i' ||s.charAt(i)=='o' || s.charAt(i)=='u')
            {
                ss=ss+s.charAt(i);
            }
        }
        if(n<ss.length())
        {
            for(int i=0;i<n;i++)
            {
                System.out.print(ss.charAt(i));
            }
        }
        else{
                System.out.print("invalid");
            }
    }
}

==============================================
323)Implement a program that takes a string and returns true or false, depending on whether the characters are in order or not.

Input Format

String from the user

Constraints

non-empty string

Output Format

true or false

Sample Input 0

abc
Sample Output 0

true
Sample Input 1

prakash
Sample Output 1

false
Sample Input 2

123
Sample Output 2

true


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s = sc.nextLine();
        char ch1[] = s.toCharArray();
        char ch2[] = s.toCharArray();
        Arrays.sort(ch1);
        System.out.println(Arrays.equals(ch1,ch2));
    }
}
========================================================
324)Fanny is given a string along with the string which contains single character x. She has to remove the character x from the given string. Help her write a program to remove all occurrences of x character from the given string.

Input Format

String and character from the user

Constraints

non-empty string

Output Format

Updated string

Sample Input 0

welcome
e
Sample Output 0

wlcom
Sample Input 1

prakash
a
Sample Output 1

prksh
Sample Input 2

java
a
Sample Output 2

jv
Sample Input 3

python
y
Sample Output 3

pthon

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
         String s=sc.nextLine();
        char ch=sc.next().charAt(0);
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)!=ch)
            {
                System.out.print(s.charAt(i));
            }
        }
    }
}

===============================================
325)Someone has attempted to censor my strings by replacing every vowel with a *, l*k* th*s. Luckily, I've been able to find the vowels that were removed.
Given a censored string and a string of the censored vowels, return the original uncensored string.

Input Format

censored string and removed vowels as string

Constraints

non-empty string

Output Format

updated string

Sample Input 0

w*lc*m*
eoe
Sample Output 0

welcome
Sample Input 1

Wh*r* d*d my v*w*ls g*?
eeioeo
Sample Output 1

Where did my vowels go?
Sample Input 2

*bcd*
ae
Sample Output 2

abcde
Sample Input 3

*PP*RC*S*
UEAE
Sample Output 3

UPPERCASE



import java.io.*;
import java.util.*;

public class Solution {
     public static void main(String[] args) {
            Scanner sc=new Scanner(System.in);
            String s=sc.nextLine();
            String ss=sc.nextLine();
            int j=0;
            for(int i=0;i<s.length();i++)
            {
                  if(s.charAt(i)=='*')
                    {
                        System.out.print(ss.charAt(j));
                        j++;
                    }
                    else{
                        System.out.print(s.charAt(i));
                    }
                }
            }
        }

===================================================
326)Given a string S of '(' and ')' parentheses, we add the minimum number of parentheses ( '(' or ')', and in any positions ) so that the resulting parentheses string is valid. Formally, a parentheses string is valid if and only if: It is the empty string, or It can be written as AB (A concatenated with B), where A and B are valid strings, or It can be written as (A), where A is a valid string. Given a parentheses string, return the minimum number of parentheses we must add to make the resulting string valid.

Input Format

string from the user

Constraints

non-empty string

Output Format

minimum number of ( or ) required.

Sample Input 0

()
Sample Output 0

0
Sample Input 1

()(
Sample Output 1

1

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int count1=0;
        int count2=0;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='(')
            {
                count1++;
            }
            else if(s.charAt(i)==')')
            {
                count2++;
            }
            else{
                
            }
        }
        System.out.print(Math.abs(count1-count2));
    }
}

========================================================
327)Given a string, 
return the true if that can be typed 
using letters of alphabet on only one row's of American keyboard like the image below.

In the American keyboard:
=> the first row consists of the characters "qwertyuiop",
=> the second row consists of the characters "asdfghjkl", and
=> the third row consists of the characters "zxcvbnm".

Note:
1. You may use one character in the keyboard more than once.
2. You may assume the input string will only contain letters of alphabet.

Input Format

A string from the user

Constraints

length of string >1

Output Format

true or false

Sample Input 0

mom
Sample Output 0

false
Sample Input 1

dad
Sample Output 1

true
Sample Input 2

love
Sample Output 2

false
Sample Input 3

false
Sample Output 3

false


import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

    public class Solution {
    public static void main(String args[] ) throws Exception {
    Scanner obj = new Scanner(System.in);
        String s = obj.nextLine();
        String r1 = "qwertyuiop";
        String r2 = "asdfghjkl";
        String r3 = "zxcvbnm";
        int c1=0,c2=0,c3=0;

        for(int i=0;i<s.length();i++){
            if(r1.contains(""+s.charAt(i)))
                c1++;
            if(r2.contains(""+s.charAt(i)))
                c2++;
            if(r3.contains(""+s.charAt(i)))
                c3++;
        }
        System.out.println(c1==s.length() || c2==s.length() || c3==s.length()?"true":"false");
    }
    }

=================================================================
328)Given two strings s and goal, return true if and only if s can become goal after some number of shifts on s. 
A shift on s consists of moving the leftmost character of s to the rightmost position. 
For example, if s = "abcde", then it will be "bcdea" after one shift.

Input Format

string s1 and s2 from the user

Constraints

non empty string

Output Format

true or false

Sample Input 0

abcde
bcdea
Sample Output 0

true
Sample Input 1

kpb
psk
Sample Output 1

false
Sample Input 2

xyz
yzx
Sample Output 2

true
Sample Input 3

nayan
abcde
Sample Output 3

false

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s1=sc.nextLine();
        String s2=sc.nextLine();
         System.out.println((s1+s1).contains(s2));
    }
}

=====================================
329)Given a string containing unique letters, return a sorted string with the letters that don't appear in the string.

Input Format

A string from the user

Constraints

non empty string

Output Format

return missing characters in the given string

Sample Input 0

abcdefgpqrstuvwxyz
Sample Output 0

hijklmno
Sample Input 1

zyxwvutsrq
Sample Output 1

abcdefghijklmnop
Sample Input 2

abc
Sample Output 2

defghijklmnopqrstuvwxyz
Sample Input 3

abcdefhijklmnopqrstuvwxyz
Sample Output 3

g

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int a[]=new int[26];
        for(int i=0;i<s.length();i++)
        {
            a[s.charAt(i)-97]++;
        }
        
        for(int i=0;i<26;i++)
        {
            if(a[i]==0)
            {
                System.out.print((char)(i+97));
            }
        }
    }
}

===================================================
330)Create a function that takes a string and replaces each letter with its appropriate position in the alphabet. 
"a" is 1, "b" is 2, "c" is 3, etc, etc.
Note: If any character in the string is n't a letter, ignore it.

Input Format

a string from the user

Constraints

non-empty string

Output Format

position of characters seperated by space

Sample Input 0

abc
Sample Output 0

1 2 3
Sample Input 1

xyz
Sample Output 1

24 25 26


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        String ss=" ";
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)>='A' && s.charAt(i)<='Z')
            {
                ss=ss+s.charAt(i)+32;
            }
        }
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)>='a' && s.charAt(i)<='z')
            {
               System.out.print((s.charAt(i)-96)+" ");
            }
        }
    }
}


=============================
331)Implement a Program to replace a character with it's occurrence in given string.

Input Format

a string and a character from the user.

Constraints

non-empty string

Output Format

replaced string

Sample Input 0

test
t
Sample Output 0

1es2
Sample Input 1

prakash
a
Sample Output 1

pr1k2sh


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
 Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
       char ch=sc.next().charAt(0);
        int count=1;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)==ch)
            {
                System.out.print(count++);
            }else{
                 System.out.print(s.charAt(i));
            }
        }
    }
}

======================================================
332)Program to find first non-repeated character

Input Format

a non-empty string from the user

Constraints

no

Output Format

non-repeated character

Sample Input 0

aabcdbe
Sample Output 0

c
Sample Input 1

prakash
Sample Output 1

p
Sample Input 2

indian
Sample Output 2

d
Sample Input 3

india
Sample Output 3

n


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int i,j,u;
        for(i=0;i<s.length();i++)
        {
            u=1;
            for(j=0;j<s.length();j++)
            {
            if(i!=j && s.charAt(i)==s.charAt(j))
            {
                u=0;
                break;
            }
        }
        if(u==1)
        {
            System.out.print(s.charAt(i));
            break;
        }
    }
}
}

=================================================================

333)Implement a program to check whether the given string pangram or not. A pangram is a string that contains all the letters of the English alphabet. An example of a pangram is "The quick brown fox jumps over the lazy dog"

Input Format

a string from the user

Constraints

non-empty string

Output Format

Yes or No

Sample Input 0

abc
Sample Output 0

No
Sample Input 1

abcdefghijklmnopqrstuvwxyz
Sample Output 1

Yes
Sample Input 2

the quick brown fox jumps over the lazy dog
Sample Output 2

Yes
Sample Input 3

abcdefghijklnopqrstuvwxyz
Sample Output 3

No


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
          int a[]=new int[26];
        int flag=1;
        for(int i=0;i<s.length();i++)
        {
            a[s.charAt(i)-97]++;
        }
        
        for(int i=0;i<26;i++)
        {
            if(a[i]==0)
            {
                flag=0;
                break;
            }
        }
    System.out.print((flag==1)?"Yes":"No");
    }
}

===========================================================
334)Implement a function/Method to return first character in each word from the given input string.

Input Format

a string

Constraints

no

Output Format

first character in each string

Sample Input 0

welcome to java programming
Sample Output 0

wtjp
Sample Input 1

Hi hello every one
Sample Output 1

Hheo


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String[] s=sc.nextLine().split(" ");
        int i=0;
        for(String ss:s)
        {
            System.out.print(ss.charAt(i));
        }
    }
}

==================================================
335)Implement a program to check if a string contains only digits.

Input Format

a string from the user

Constraints

no

Output Format

Yes or No

Sample Input 0

abc123
Sample Output 0

No
Sample Input 1

123456
Sample Output 1

Yes
Sample Input 2

prakash22
Sample Output 2

No

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int count=0;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)>='0' && s.charAt(i)<='9')
            {
                count++;
            }
        }
        
        System.out.print((s.length()==count)?"Yes":"No");
    }
}
=============================================================
336)Implement a program to capitalize first letter of each word in a string.

Input Format

a string from the user

Constraints

non-empty string

Output Format

a string with capitalization

Sample Input 0

welcome to java
Sample Output 0

Welcome To Java
Sample Input 1

welcome to logic based programming
Sample Output 1

Welcome To Logic Based Programming
Sample Input 2

python programming by prakash sir
Sample Output 2

Python Programming By Prakash Sir



import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        StringBuffer sb = new StringBuffer();
        StringTokenizer st = new StringTokenizer(s);
        while(st.hasMoreTokens())
        {
            String ss=st.nextToken();
            sb.append(ss.substring(0,1).toUpperCase()+ss.substring(1));
            sb.append(" ");
        }
      System.out.println(sb.toString());
    }
}
===========================================================================
337)You are given a string representing an attendance record for a student. The record only contains the following three characters: 'A' : Absent. 'L' : Late. 'P' : Present.
A student could be rewarded if his attendance record doesn't contain more than one 'A' (absent) or more than two continuous 'L' (late).

You need to return whether the student could be rewarded according to his attendance record.

Input Format

a string from the user

Constraints

non empty string

Output Format

Yes or No

Sample Input 0

PPALLP
Sample Output 0

Yes
Sample Input 1

PPALLL
Sample Output 1

No
Sample Input 2

PPP
Sample Output 2

Yes


import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.nextLine();
        int absentCount=0,leaveCount=0;
        for(int i=0;i<s.length();i++)
        {
            if(s.charAt(i)=='A')
            {
                absentCount++;
            }
            if(s.charAt(i)=='L' && s.charAt(i)+1=='L' && s.charAt(i)+2=='L')
            {
                leaveCount++;
            }
        }
        System.out.print((absentCount>1||leaveCount==1)?"No":"Yes");
    }
}

===============================================================
































